<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_battelike.php');

securePost();
secureGet();

//update vues
$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `vues` = vues + 1 WHERE token = :token");
$updateReq->bindParam(':token', $_POST['token'], PDO::PARAM_STR);
$updateReq->execute();

$select_post = $dbh->prepare(
"   SELECT id, battle, `type`, title, post, lien_post, user, likes AS nb_likes, vues, statut,
        (SELECT COUNT(distinct(user)) FROM bl_likes_post WHERE bl_likes_post.posts = bl_battle_posts.id) AS nb_users
    FROM `bl_battle_posts`
    WHERE `statut` = '1' AND token = :token
    LIMIT 0,1
");
$select_post->bindParam(':token', $_POST['token'], PDO::PARAM_STR);
$select_post->execute();

$row_post = $select_post->fetch(PDO::FETCH_OBJ);
$title_battle = name_battle($row_post->battle, $dbh);
$statut_battle = statut_battle($row_post->battle, $dbh);
$id_battle = $row_post->battle;

list($post, $type) = photo_post($row_post->id, $dbh);

if(in_array($type, [1, 4])) { $imagePartage = $post; }
if($type == 2){ $imagePartage = 'images/partage_video_republike.jpg'; } 
if($type == 3){ $imagePartage = 'images/partage_texte_republike.jpg'; }

list($width, $height, $type, $attr) = getimagesize($imagePartage);

$rank = $_POST['ordre'];

$select_posts = $dbh->prepare(
"   SELECT id, token,
        (SELECT count(id) FROM bl_likes_post WHERE posts = bl_battle_posts.id) as nb_likes
    FROM `bl_battle_posts`
    WHERE `statut` = '1' AND battle = :id 
    ORDER BY nb_likes DESC
");
$select_posts->bindParam(':id', $id_battle, PDO::PARAM_STR);
$select_posts->execute();

if ($select_posts->rowCount() > 0) { 
	$posts = $select_posts->fetchAll(PDO::FETCH_OBJ);
}

?>

<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-7 col-12">
        <!--section-->
        <div class="sh-section mb-1" style="height:100%;">
        	
            <a class="btn-popup-post_suivant precedent link" href="javascript:void(0)"  
            data-id="<?php if($rank==1){ echo $posts[$select_posts->rowCount()-1]->id; }else{echo $posts[$rank-2]->id;} ?>" 
            data-token="<?php if($rank==1){ echo $posts[$select_posts->rowCount()-1]->token; }else{echo $posts[$rank-2]->token;} ?>" 
            data-ordre="<?php if($rank==1){ echo $select_posts->rowCount(); }else{echo $rank-1;} ?>">
             	<i class="fa fa-chevron-left"></i>
             </a>
            
             <a class="btn-popup-post_suivant suivant link" href="javascript:void(0)"  
             data-id="<?php if($rank==$select_posts->rowCount()){ echo $posts[0]->id; }else{echo $posts[$rank]->id;} ?>" 
             data-token="<?php if($rank==$select_posts->rowCount()){ echo $posts[0]->token; }else{echo $posts[$rank]->token;} ?>" 
             data-ordre="<?php if($rank==$select_posts->rowCount()){ echo 1; }else{echo $rank+1;} ?>">
             	<i class="fa fa-chevron-right"></i>
             </a>
             
            <div class="sh-section__content">
                <?php if($row_post->type == 1){ ?>
                    <div class="sh-section__image postimg">
                        <img src="/<?= $row_post->post ?>" alt="<?= $row_post->title ?>">
                    </div>
                <?php } ?>
                                
                <?php if($row_post->type == 2){ list($video, $image) = video($row_post->post); ?>
                    <div class="responsive-video">
                        <iframe type="text/html" width="100%" src="<?= $video ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                <?php } ?>
                
                <?php if($row_post->type == 3){ ?>
                      <p class="text-muted mb-2" style="padding: 20px 55px 0;"><?= nl2br($row_post->post) ?> </p>
                      <p class="text-muted mb-2" style="padding: 20px 55px 0;"><a href="<?= $row_post->lien_post ?>" target="_blank"><?= $row_post->lien_post ?></a> </p>
                <?php } ?>
            </div>
         </div>
     </div>
     
     <div class="col-xl-6 col-lg-6 col-md-5 col-12">
     	
        <div class="sh-section mt-1 mb-1">
            <h6 class="text-center mt-3"><?= $row_post->title ?></h6>
            <div class="row no-gutters mt-2">
                <div class="col-lg-3 col-md-4 col-12">
                <?php
                        $title = speudo_user($row_post->user, $dbh) .' '. grade_user_between(points_user($row_post->user,$dbh), $code,$dbh);
                        if($_SESSION['id_user'] == $row_post->user) {
                            $href = "/" .$code. "/" .$_['url_compte'];
                        } else {
                            $href= "/" .$code. "/". $_['url_user']. "/" .url_user($row_post->user, $dbh);
                        }
                ?>
                    <a data-toggle="tooltip" data-placement="left" title="<?= prenom_user($row_post->user, $dbh) ?>" 
                        href="<?php if($_SESSION['id_user'] == $row_post->user){ ?>
                            /<?= $code ?>/<?= $_['url_compte'] ?>
                        <?php }else{ ?>
                            /<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_post->user, $dbh) ?>
                        <?php } ?>" 
                        class="sh-section__btn-link sh-btn-icon">
                        <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_post->user,$dbh),$dbh) ?>; background-image:url(<?= '/'.photo_user($row_post->user, $dbh) ?>)"></span>
                    </a>
                </div>
                <div class="col-lg-6 col-md-4" style="align-self:center">
                
                	<!-- <?php $progess = ($row_post->nb_likes / NB_MAX_LIKES)*100; ?>
					<?php if(round($progess) < 100 ) { ?>
						<div class="col-sm-8 col-11 w3-container">
                        <div class="col-12 w3-light-grey w3-round-xlarge" style="padding: 0">
                            <div class="w3-container w3-round-xlarge w3-black" style="width:<?= $progess + 15 ?>%"></b></div>
                        </div>
                    </div>
					 
					 <?php } else { ?>
					 
						<div class="folder lines mt-4">
							<div class="projectLine slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all ui-slider-disabled ui-state-disabled" aria-disabled="true">
								<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 100%;"></div>
							</div>
						</div>
					 
					 <?php } ?>
                      -->
					 <span class="popup-likes"><b><span id="popup-nblikes"><?= $row_post->nb_likes ?></span>&nbsp;/&nbsp;<?= NB_MAX_LIKES ?> <i class="fa fa-heart"></i></b></span>
				                                     
                </div>
                <div class="text-center col-lg-3 col-md-4">
                <?php 
                    $laurels = [
                        1 => ['title' => 'or', 'img' => 'gold-number.svg'],
                        2 => ['title' => 'argent', 'img' => 'silver-number.svg'],
                        3 => ['title' => 'bronze', 'img' => 'bronze-number.svg']
                    ];
                ?>
                    <div data-toggle="tooltip" data-placement="top" data-original-title="Classement <?= ucfirst($laurels[$rank]['title']) ?>">
                <?php
                    if ($rank < 4) {
                ?>
                           <img src="/images/icons/<?= $laurels[$rank]['img'] ?>" class="icons-50">
                <?php
                    } else {
                ?>
                            <div class="chiffre" style="background-color:#FFFFFF; border-radius: 50%">
                                <?= $rank ?><span>/<?= $select_posts->rowCount() ?></span>
                            </div>
                <?php
                    }                
                ?>
                    </div>
                    <!-- <div class="chiffreOrdre text-center" data-toggle="tooltip" data-placement="top" 
                    data-original-title="Classement <?php if($rank==1){ echo "Or";} ?> <?php if($rank==2){ echo "Argent";} ?> <?php if($rank==3){ echo "Bronze";} ?>">
						<?php if($rank<4){?>
                            <div class="chiffre <?php if($rank==1){ echo "or";} ?> <?php if($rank==2){ echo "argent";} ?> <?php if($rank==3){ echo "bronze";} ?>"> <?= $rank ?></div>
                            <i class="repu-laurels <?php if($rank==1){ echo "or";} ?> <?php if($rank==2){ echo "argent";} ?> <?php if($rank==3){ echo "bronze";} ?>"></i>
                        <?php }else{?>
                            <div class="chiffre"> <?= $rank ?><span>/<?= $select_posts->rowCount() ?></span></div>
                        <?php } ?>
                    
                    </div> -->
                </div>
               
            </div>
             <hr>  
        </div>
     
         <div class="sh-section mt-1 mb-1">
            <div class="sh-section__content">                            
                <?php
                    $select_comments = $dbh->prepare(
                    "   SELECT id, user, message, date
                        FROM `bl_comments_post` 
                        WHERE `post` = :post AND `statut` = 1 
                        ORDER BY date DESC 
                        LIMIT 0, 10
                    ");
                    $select_comments->bindParam(':post', $row_post->id, PDO::PARAM_STR);
                    $select_comments->execute();
                    
                if ($select_comments->rowCount() > 0) { ?>
                <div class="sh-comments">
                <?php while ( $row_comments = $select_comments->fetch(PDO::FETCH_OBJ) ){ 
                    $datetime = new DateTime($row_comments->date); ?>
                    <!--comment-->
                    <div class="sh-comments__section">
                        <div class="sh-comments__content">
                            <a href="/user.php?id=<?= $row_comments->user ?>&code=<?= $code ?>" class="sh-comments__name"><?= prenom_user($row_comments->user, $dbh) ?></a>
                            <span class="sh-comments__passed"><?= ago( $datetime ) ?></span>
                            <span style="cursor:pointer" >
                                <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="left" data-comment="<?= $row_comments->id ?>" title="<?= $_['signaler_comment'] ?>" class="sh-comments__signaler icons-20">
                            </span>
                            <p><?= $row_comments->message ?></p>
                        </div>
                    </div>
                    <!--comment-->
                  <?php } ?>
                </div>
                <?php }?>  
            </div>
            <?php if(!empty($_SESSION['securite'])) { ?>
                <div class="sh-add-comment">
                <form method="post" action="" name="form_save_commentaire" id="form_save_commentaire" enctype="multipart/form-data">
                    <textarea name="commentaire" id="commentaire" cols="3" maxlength="140" class="form-control ttg-border-none" placeholder="<?= $_['msg_comment'] ?>"></textarea>
                    <input name="post" value="<?= $row_post->id ?>" required type="hidden">
                    <div id="errorLogCommentaire" class="text-center mb-3"></div>
                    <div class="sh-login__send mb-1">
                        <button type="submit" class="sh-btn" id="btn-commentaire"><?= $_['bt_comment_envoyer'] ?></button>
                    </div>
                </form>
            </div>
            <?php }?>
        </div>
        
         <div class="sh-section mt-1">
            <div class="sh-section__footer">
            <div>
                <span>
                    <img src="/images/icons/friends.svg" class="icons">
                    <span><?= $row_post->nb_users; ?></span>
                </span>
                    
                <span>
                    <img src="/images/icons/eye.svg" class="icons">
                    <span><?= $row_post->vues; ?></span>
                </span>
            </div>
            <div style="padding-bottom: 10px;"> 
            <?php 
                if(!empty($_SESSION['securite'])){
            ?>
                <span style="cursor:pointer">
                    <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" 
                        id="like_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" 
                        class="<?= ($row_post->statut == 1) ? "likeSave" : "inactif"; ?> sh-section__btn-like sh-btn-icon icons-30">
            <?php 
                } else { 
            ?>
                <span style="cursor:pointer">
                    <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>"
                        data-id="<?= $row_post->id ?>" 
                        class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon icons-30">
            <?php 
                } 
            ?>
                    <span><?= $row_post->nb_likes; ?></span>
                </span>      
            <?php 
                if(!empty($_SESSION['securite'])) {
            ?>
                <span style="cursor:pointer">
                    <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" id="btn-partage-contenu" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon icons-30" 
                    data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" data-image="<?= $imagePartage ?>">
                    <span><?= $row_share->nb_shares; ?></span>
                </span>
                                    
            <?php 
                } else { 
            ?>
                <span style="cursor:pointer">
                    <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon icons-30" 
                        data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" data-image="<?= $imagePartage ?>">
                        <span><?= $row_share->nb_shares; ?></span>
                    </span>  
                </span>
            <?php 
                } 
                if(!empty($_SESSION['securite'])){ 
					//FAVORIS
                    $isFavorite = fav_user($_SESSION['id_user'], $row_post->id, $dbh);
            ?>
                    <span style="cursor:pointer;">
            <?php
                if (!$isFavorite) {
            ?>
                    <img src="/images/icons/favorite-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['fav_contenu'] ?>" href="javascript:void(0)"  
                    id="fav_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?php if($statut_battle ==1){ echo "favSave";} else { echo "inactif";} ?> sh-section__btn-like sh-btn-icon icons-30">
            <?php
                } else {
            ?>
                    <img src="/images/icons/favorite-pink.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['fav_contenu'] ?>" href="javascript:void(0)"  
                    id="fav_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?php if($statut_battle ==1){ echo "favSave";} else { echo "inactif";} ?> sh-section__btn-like sh-btn-icon icons-30">
            <?php
                }
            ?>
                    </span>
            <?php } ?>
            <?php 
                if(isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user){ 
            ?>
                    
                    <span style="cursor:pointer" >
                        <img src="/images/icons/edit-grey.svg" data-toggle="tooltip" data-placement="left" title="<?= $_['modif_contenu'] ?>" id="edit_<?= $row_post->id ?>" 
                         data-id="<?= $row_post->id ?>" data-name="<?= $row_post->title ?>"  class="editPost sh-section__btn-link sh-btn-icon icons-30">
                    </span>                      
            <?php 
                }
                if(!empty($_SESSION['securite'])) {
            ?>
                    <span style="cursor:pointer" >
                        <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" title="<?= $_['signaler_contenu'] ?>" class="sh-post__signaler sh-btn-icon icons-20">
                    </span>
            <?php
                }
            ?>
        </div>
            <!-- <div class="sh-section__footer">
                <?php if(!empty($_SESSION['securite'])){?>
                        
                    <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)"  
                    id="like_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?php if($statut_battle ==1){ echo "likeSave";}else{ echo "inactif";} ?> sh-section__btn-like sh-btn-icon">
                        <i class="fa fa-heart"></i><span class="like"><?= $row_post->likes ?></span>
                    </a>
                <?php }else{ ?>
                
                    <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)" data-id="<?= $row_post->id ?>" 
                    class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon">
                        <i class="fa fa-heart"></i><span class="like"><?= $row_post->likes ?></span>
                    </a>
                
                <?php } ?>
                                               
                <div class="sh-section__btn-comment sh-btn-icon"><i class="fa fa-eye"></i><span  class="users"><?= $row_post->vues ?></span></div>
                
                <?php if(!empty($_SESSION['securite'])) { ?>
                        
                   <div data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon" 
                      data-post="<?= $row_post->token ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" 
                      data-image="/<?= $imagePartage ?>">
                        <i class="fa fa-share-alt"></i></span>
                    </div>
                        
                <?php }else{ ?>
                
                    <div data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon" data-post="<?= $row_post->token ?>">
                        <i class="fa fa-share-alt"></i></span>
                    </div>
                
                <?php } ?>
                
                <?php if(!empty($_SESSION['securite'])){ 
					//FAVORIS
					$fav = fav_user($_SESSION['id_user'], $row_post->id, $dbh);
					?>
                    <a data-toggle="tooltip" data-placement="top" title="<?= $_['fav_contenu'] ?>" href="javascript:void(0)"  
                    id="fav_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?php if($statut_battle ==1){ echo "favSave";}else{ echo "inactif";} ?> sh-section__btn-like sh-btn-icon">
                        <i class="fa fa-star<?php if($fav==0){ echo '-o';}?>"></i>
                    </a>
                <?php } ?>
                
            </div> -->
            
        </div>
    </div>
    
</div>