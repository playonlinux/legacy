"use strict";

//scripts availabel
require.config({
  baseUrl: "",
  waitSeconds: 300,
  paths: {
    //PLUGINS
    jquery: "/vendor/jquery/jquery.min",
    "plugins/modernizr": "/vendor/modernizr/modernizr",
    "plugins/bootstrap_v3": "/vendor/bootstrap/v3/js/bootstrap.min",
    "plugins/slick": "/vendor/slick/slick.min",
    "plugins/progress": "/vendor/progress/dist/circle-progress.min",
    "plugins/magnificPopup":
      "/vendor/magnificPopup/dist/jquery.magnific-popup.min",
    "plugins/bridget": "/vendor/jquery/jquery-bridget",
    "plugins/isotope": "/vendor/isotope/isotope.pkgd.min",
    "plugins/datePicker": "/vendor/datePicker/js/datepicker",
    "plugins/wow": "/vendor/wow.min",

    //MODULES
    "modules/index": "/js/modules/index",
    "modules/main": "/js/modules/main",
    "modules/player": "/js/modules/player",
    "modules/progress": "/js/modules/progress",
    "modules/upload": "/js/modules/upload",
    "modules/signup": "/js/modules/signup",
    "modules/userToggle": "/js/modules/user-toggle",
    "modules/menu": "/js/modules/menu",
    "modules/statistic": "/js/modules/statistic",
    "modules/posts": "/js/modules/posts",
    "modules/googleCharts": "/js/modules/google-charts",
    "modules/datePicker": "/js/modules/datepicker",
    "modules/profil": "/js/modules/profil",
    "modules/home": "/js/modules/home",
    "modules/home_message": "/js/modules/home_message",
    "modules/battle": "/js/modules/battle",
    "modules/battle_popup": "/js/modules/battle_popup",
    "modules/search": "/js/modules/search",
    "modules/cat": "/js/modules/cat",
    "modules/hall": "/js/modules/hall",
    "modules/search_users": "/js/modules/search_users",
    "modules/felicitations": "/js/modules/felicitations",
    "modules/footer": "/js/modules/footer"
  },
  shim: {
    //PLUGINS
    "plugins/modernizr": {
      exports: "Modernizr"
    },
    "plugins/bootstrap_v3": {
      deps: ["jquery"],
      exports: "Bootstrap"
    },
    "plugins/datePicker": {
      deps: ["jquery"],
      exports: "datePicker"
    },
    "plugins/wow": {
      exports: "WOW"
    }
  }
});
