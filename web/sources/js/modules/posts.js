define( [
    'require',
    'jquery',
    'plugins/isotope'
], function( require, $, Isotope ) {

    require( [
        'plugins/bridget'
    ], function( jQueryBridget ) {

        jQueryBridget( 'isotope', Isotope, $ );

        var $sect_wrap = $('.sh-section__wrap'),
		 $more_btn = $('.sh-footer__more-btn');
		 
		 // quick search regex
		var qsRegex;
			
		var $grid = $sect_wrap.isotope({
			itemSelector: '.sh-section__item',
			percentPosition: true,
			masonry: {
				columnWidth: '.sh-section__item',
				horizontalOrder: true
		   },
			transitionDuration: '0.4s',
			getSortData: {
				like: '.like parseInt',
				users: '.users parseInt',
				number: '.number parseInt',
				category: '[data-category]',
				weight: function( itemElem ) {
				  var weight = $( itemElem ).find('.weight').text();
				  return parseFloat( weight.replace( /[\(\)]/g, '') );
				},
				date: '.date parseInt'
			},
			filter: function(itemElem) {
				//console.log($(itemElem).text());
				if($(this).text()){
					return qsRegex ? $(this).text().match( qsRegex ) : true;
				}else{
					return qsRegex ? $(itemElem).text().match( qsRegex ) : true;
				}
			  }
		}).addClass('sh-section__isotope-init');
		
		// filter functions
		var filterFns = {
		  // show if number is greater than 50
		  numberGreaterThan50: function() {
			var number = $(this).find('.number').html();
			return parseInt( number, 10 ) > 1;
		  },
		  // show if name ends with -ium
		  ium: function() {
			var name = $(this).find('.name').text();
			return name.match( /ium$/ );
		  }
		};
		
		// bind sort button click
		$('#sorts a').on( 'click', function() {
		  var sortByValue = $(this).attr('data-sort-by');
		   $('#sorts a').removeClass('active');
		   $(this).addClass('active');
		   $grid.isotope({ filter: '*', sortBy: sortByValue , sortAscending : false });
			$('[data-toggle="tooltip"]').tooltip("hide");
		});	
		
		
		$('#sorts a.filters-button-group').on( 'click', function() {
		  var filterValue = $(this).attr('data-filter');
		  $('#sorts a').removeClass('active');
		  $(this).addClass('active');
		  filterValue = filterFns[ filterValue ] || filterValue;
		  $grid.isotope({ filter: filterValue });
			$('[data-toggle="tooltip"]').tooltip("hide");
		});
		
		
		// use value of search field to filter
		var $quicksearch = $('.quicksearch').keyup( debounce( function() {
			console.log($quicksearch.val());
		  	qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		  	$grid.isotope();
		}, 200 ) );
		
		// debounce so filtering doesn't happen every millisecond
		function debounce( fn, threshold ) {
		  var timeout;
		  threshold = threshold || 100;
		  return function debounced() {
			clearTimeout( timeout );
			var args = arguments;
			var _this = this;
			function delayed() {
			  fn.apply( _this, args );
			}
			timeout = setTimeout( delayed, threshold );
			
		  };
		}
				
		if(page =='battle'){
			$grid.isotope({ sortBy: 'like' , sortAscending : false });
			
		}else if(page =='post'){
			$grid.isotope({ sortBy: 'like' , sortAscending : false });
			
		}else if(page =='battlePost'){
			$grid.isotope({ sortBy: 'date' , sortAscending : false });	
			setTimeout(function(){
				$grid.isotope({ sortBy: 'like' , sortAscending : false });
			},3000);
			
		}else{
			$grid.isotope({ 
				percentPosition: true,
				masonry: {
					columnWidth: '.sh-section__item',
					horizontalOrder: true
			   },
			   sortBy: 'users' , 
			   sortAscending : false 
			});	
		}
		
		
		
    });
	
	
});