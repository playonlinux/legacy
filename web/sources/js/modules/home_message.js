define(["require", "jquery", "plugins/bootstrap_v3"], function(require) {
  var $ = require("jquery");

  $.magnificPopup.open({
    mainClass: "mfp-with-zoom",
    removalDelay: 300,
    items: [
      {
        src: ".sh-message",
        type: "inline"
      }
    ]
  });
});
