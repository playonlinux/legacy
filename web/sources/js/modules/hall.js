define(["require", "jquery", "plugins/isotope"], function (require, $, Isotope) {
  //lang
  if (lang == "en") {
    var nobattle = "No campaign";
    var nopost = "No content";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var plusdelike =
      "Your stock of LIKES is sold out. Rebuild it by converting your LIKECOINS, posting your best contents and campaigns, or inviting your friends.";
    var maxvotecontenu =
      "You have reached the maximum number of votes for this content";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";

    var lienbattle = "battle";
    var lienpost = "post";
  } else {
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var plusdelike =
      "Votre stock de LIKES est épuisé. Reconstituez-le en convertissant vos LIKECOINS, en postant vos meilleurs contenus et batailles ou en invitant vos amis.";
    var maxvotecontenu =
      "Vous avez atteint le nombre maximal de votes pour ce contenu";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";

    var lienbattle = "bataille";
    var lienpost = "contenu";
  }

  require(["plugins/bridget"], function (jQueryBridget) {
    jQueryBridget("isotope", Isotope, $);
    var $sect_wrap = $(".sh-section__wrap");

    var $grid = $sect_wrap
      .isotope({
        itemSelector: ".sh-section__item",
        percentPosition: true,
        masonry: {
          columnWidth: ".sh-section__item",
          horizontalOrder: true
        },
        transitionDuration: "0.4s",
        getSortData: {
          like: ".like parseInt",
          users: ".users parseInt",
          points: ".points parseInt"
        }
      })
      .addClass("sh-section__isotope-init");

    $grid.isotope({
      percentPosition: true,
      masonry: {
        columnWidth: ".sh-section__item",
        horizontalOrder: true
      },
      sortBy: "points",
      sortAscending: false
    });

    var post_act = false;
    var defis_act = false;
    var users_act = true;

    $(".sh-content-head__btn-submit").on("click", function () {
      var tab = $(this).data("id");

      $(".tab-submit").css("display", "none");
      $(".sh-content-head__btn-submit").removeClass("active");

      $("#" + tab).css("display", "block");
      $(this).addClass("active");

      var $grid = $(".sh-section__wrap")
        .isotope({
          itemSelector: ".sh-section__item",
          percentPosition: true,
          masonry: {
            columnWidth: ".sh-section__item",
            horizontalOrder: true
          },
          transitionDuration: "0.4s",
          getSortData: {
            like: ".like parseInt",
            users: ".users parseInt",
            points: ".points parseInt"
          }
        })
        .addClass("sh-section__isotope-init");

      if (tab == "defis") {
        if (defis_act == false) {
          $("#" + tab).css("height", "100px");
          send_ajax_search("battles", tab, 10);
          defis_act = true;
        }
        $(".filtre").css("display", "block");

        $(".defis").css("display", "flex");
        $(".posts").css("display", "none");
        $(".users").css("display", "none");

        $grid.isotope({
          percentPosition: true,
          masonry: {
            columnWidth: ".sh-section__item",
            horizontalOrder: true
          },
          filter: ".closed",
          sortBy: "date",
          sortAscending: false
        });
      } else if (tab == "posts") {
        if (post_act == false) {
          $("#" + tab).css("height", "100px");
          send_ajax_search("posts", tab, 10);
          post_act = true;
        }
        $(".defis").css("display", "none");
        $(".posts").css("display", "flex");
        $(".users").css("display", "none");

        $grid.isotope({
          percentPosition: true,
          masonry: {
            columnWidth: ".sh-section__item",
            horizontalOrder: true
          },
          filter: "*",
          sortBy: "like",
          sortAscending: false
        });
      } else if (tab == "users") {
        if (users_act == false) {
          $("#" + tab).css("height", "100px");
          users_act = true;
        }

        $(".defis").css("display", "none");
        $(".users").css("display", "flex");
        $(".posts").css("display", "none");

        $grid.isotope({
          percentPosition: true,
          masonry: {
            columnWidth: ".sh-section__item",
            horizontalOrder: true
          },
          filter: "*",
          sortBy: "points",
          sortAscending: false
        });
      }

      return false;
    });

    function send_ajax_search(type, tab, number) {
      $.ajax({
        data: "number=" + number + "&code=" + lang,
        type: "POST",
        url: "/php/get_" + type + ".php",
        cache: false,
        async: true,
        beforeSend: function () {
          document.getElementById(tab).innerHTML =
            '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
            '<span class="center-block text-center">' +
            chargement +
            "</span></div>";
        },
        success: function (data) {
          var $elem = $(data);
          if (data != "null") {
            setTimeout(function () {
              $(".reload").css("display", "none");
              $("#" + tab)
                .isotope("insert", $elem)
                //.isotope('layout')
                .isotope({
                  percentPosition: true,
                  masonry: {
                    columnWidth: ".sh-section__item",
                    horizontalOrder: true
                  }
                });
              actions();
            }, 500);
          } else {
            if (tab == "defis") {
              document.getElementById(tab).innerHTML =
                '<p class="text-center center-block">' + nobattle + "</p>";
            } else {
              document.getElementById(tab).innerHTML =
                '<p class="text-center center-block">' + nopost + "</p>";
            }
          }
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    }

    //debut action();
    function actions() {
      $('[data-toggle="tooltip"]').tooltip();

      //FONCTION FAVORIS
      $(".likeSave").on("click", function () {
        var id_post = $(this).data("id");
        $.ajax({
          type: "POST",
          url: "/php/favoris.php",
          async: false,
          data: "type=like&id_post=" + id_post + "&code=" + lang,
          success: function (result) {
            console.log(result);
            if (result[0] != "non") {
              var f = document.getElementById("like_" + id_post);
              f.innerHTML = "";
              f.innerHTML =
                '<img src="/images/icons/liked-pink.svg" class="icons"><span>' +
                result[0] +
                "</span>";

              var f = document.getElementById("user_likes");
              f.innerHTML = "";
              f.innerHTML = '<i class="fa fa-heart"></i> ' + result[1];

              var f = document.getElementById("user_likeCoins");
              f.innerHTML = "";
              f.innerHTML = '<i class="repu-coins"></i> ' + result[5];
            }

            if (result[1] == "0") {
              console.log("oui");
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", plusdelike)
                .tooltip("show");
              $.magnificPopup.open({
                mainClass: "mfp-with-zoom",
                removalDelay: 300,
                closeMarkup:
                  '<i title="%title%" class="mfp-close fa fa-close"></i>',
                items: [
                  {
                    src: ".sh-likemessage",
                    type: "inline"
                  }
                ]
              });
            } else if (result[2] == "non") {
              console.log("oui");
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", maxvotecontenu)
                .tooltip("show");
            }
          }
        });

        return false;
      });

      //partage BATAILLE
      $(".btn-partage-battle").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis").show();
          $(".div_mail").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-battle",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-battle .partage").show();
        $("#errorLogPartageBattle").hide();
        const id_battle = $(this).data("battle");
        const url_battle = $(this).data("url");
        const title_battle = $(this).data("title");
        const image_battle = $(this).data("image");
        $("#form_save_partage_battle #id_battle").val(id_battle);

        $(".sh-partage-battle .sh-login__social").html(
          '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=/' +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" href="http://twitter.com/home?status=' +
          title_battle +
          " - /" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" href="http://pinterest.com/pin/create/button/?url=/' +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          "&description=" +
          title_battle +
          "&media=" +
          image_battle +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" id="btn-partage-battle-email" class="sh-btn-social__send sh-btn-social" data-battle="' +
          id_battle +
          '" data-url="' +
          url_battle +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-battle-email").on("click", function () {
          $(".div_amis").hide();
          $(".div_mail").show();
          var id_battle = $(this).data("battle");
          $("#form_save_partage_battle_send #id_battle").val(id_battle);

          return false;
        });

        e.preventDefault();
        return false;
      });

      //partage CONTENU
      $(".btn-partage-contenu").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis_contenu").show();
          $(".div_mail_contenu").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-contenu",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-contenu .partage").show();
        $("#errorLogPartagePost").hide();
        var id_post = $(this).data("post");
        var title_post = $(this).data("title");
        var token_post = $(this).data("token");
        var image_post = $(this).data("image");
        $("#form_save_partage_contenu #id_post").val(id_post);

        $(".sh-partage-contenu .sh-login__social").html(
          '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=/' +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" href="http://twitter.com/home?status=' +
          title_post +
          " - /" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" href="http://pinterest.com/pin/create/button/?url=/' +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          "&description=" +
          title_post +
          "&media=" +
          image_post +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" id="btn-partage-contenu-email" class="sh-btn-social__send sh-btn-social" data-token="' +
          token_post +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-contenu-email").on("click", function () {
          $(".div_amis_contenu").hide();
          $(".div_mail_contenu").show();
          var token_battle = $(this).data("token");
          $("#form_save_partage_contenu_send #token_post").val(token_post);
          return false;
        });

        e.preventDefault();
        return false;
      });

      $(".sh-login__btn-signup, .btn-upload_btn-signup").on("click", function (
        e
      ) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-login",
              type: "inline"
            }
          ]
        });

        e.preventDefault();
        return false;
      });

      //signaler post
      $(".sh-post__signaler").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-signalerPost",
              type: "inline"
            }
          ]
        });
        var id_post = $(this).data("post");
        document.form_signaler_post.post.value = id_post;

        var title_post = $(this).data("title");
        $(".sh-signalerPost .namePost").html(title_post);

        e.preventDefault();
        return false;
      });
    }
    //fin action();

    //partage BATAILLE
    $("#btn-partage-battle").on("click", function () {
      $("#errorLogPartageBattle").hide();

      var form = $("#form_save_partage_battle").get(0);
      var formData = new FormData(form); // get the form data

      var ch_data = [];

      $('#form_save_partage_battle input[type="checkbox"]:checked').each(
        function () {
          ch_data.push($(this).attr("id"));
        }
      );

      formData.append("amis", ch_data);

      if (ch_data.length != "0") {
        $.ajax({
          type: "POST",
          url: "/php/partageAmis.php?type=battle",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPartageBattle").show();
              var f = document.getElementById("errorLogPartageBattle");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-partage-battle .partage").hide();
                $(".sh-partage-battle .confirm").show();
              }, 500);

              setTimeout(function () {
                $(".sh-partage-battle .confirm").hide();
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPartageBattle").show();
        var f = document.getElementById("errorLogPartageBattle");
        f.innerHTML = "";
        f.innerHTML = "<strong class='text-danger'>" + selectami + "</strong>";
      }
      return false;
    });
    $("#btn-partage-battle-send").on("click", function () {
      $("#errorLogPartageBattleSend").hide();
      const form = $("#form_save_partage_battle_send").get(0);
      var formData = new FormData(form); // get the form data

      if (
        !$.trim(
          $(form)
            .find("#email1")
            .val().length
        )
      ) {
        $("#errorLogPartageBattleSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattleSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();

            return;
          }

          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $(".div_amis").show();
            $(".div_mail").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });
    $("#cancelSend").on("click", function () {
      $(".div_amis").show();
      $(".div_mail").hide();
      return false;
    });

    //partage CONTENU
    $("#btn-partage-contenu").on("click", function () {
      $("#errorLogPartagePost").hide();

      var form = $("#form_save_partage_contenu").get(0);
      var formData = new FormData(form); // get the form data

      var ch_data = [];

      $('#form_save_partage_contenu input[type="checkbox"]:checked').each(
        function () {
          ch_data.push($(this).attr("id"));
        }
      );

      formData.append("amis", ch_data);

      if (ch_data.length != "0") {
        $.ajax({
          type: "POST",
          url: "/php/partageAmis.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPartageBattle").show();
              var f = document.getElementById("errorLogPartagePost");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-partage-contenu .partage").hide();
                $(".sh-partage-contenu .confirm").show();
              }, 500);

              setTimeout(function () {
                $(".sh-partage-contenu .confirm").hide();
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPartagePost").show();
        var f = document.getElementById("errorLogPartagePost");
        f.innerHTML = "";
        f.innerHTML = "<strong class='text-danger'>" + selectami + "</strong>";
      }
      return false;
    });
    $("#btn-partage-contenu-send").on("click", function () {
      $("#errorLogPartageContenuSend").hide();
      const form = $("#form_save_partage_contenu_send").get(0);
      var formData = new FormData(form); // get the form data

      if (!document.form_save_partage_contenu_send.email1.value) {
        $("#errorLogPartageContenuSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=post",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLogPartageContenuSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          setTimeout(function () {
            // $(".sh-partage-contenu .partage").show();
            $(".sh-partage-contenu .confirm").hide();
            $(".div_amis_contenu").show();
            $(".div_mail_contenu").hide();
            $.magnificPopup.close();
          }, 2000);
        }
      });

      return false;
    });

    $("#cancelSendContenu").on("click", function () {
      $(".div_amis_contenu").show();
      $(".div_mail_contenu").hide();
      return false;
    });

    //signaler CONTENU
    $("#btn-signaler-post").on("click", function () {
      $("#errorLogPostSignaler").hide();

      var form = $("#form_signaler_post").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_post.post.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            if (data == "non") {
              $("#errorLogPostSignaler").show();
              var f = document.getElementById("errorLogPostSignaler");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerPost .signaler").hide();
                $(".sh-signalerPost .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerPost .signaler").show();
                $(".sh-signalerPost .confirm").hide();
                document.form_signaler_post.mes.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPostSignaler").show();
        var f = document.getElementById("errorLogPostSignaler");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>" + errormessage + "</strong>";
      }
      return false;
    });
  });
});
