define(["require", "jquery", "plugins/isotope"], function (require, $, Isotope) {
  //lang
  if (lang == "en") {
    var nobattle = "No battle";
    var nopost = "No content";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var plusdelike =
      "Your stock of Likes is sold out. Rebuild it by converting your LikeCoins, posting your best contents and campaigns, or inviting your friends.";
    var maxvotecontenu =
      "You have reached the maximum number of votes for this content";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";

    var lienbattle = "battle";
    var lienpost = "post";
  } else {
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var plusdelike =
      "Votre stock de Likes est épuisé. Reconstituez-le en convertissant vos LikeCoins, en postant vos meilleurs contenus et batailles ou en invitant vos amis.";
    var maxvotecontenu =
      "Vous avez atteint le nombre maximal de votes pour ce contenu";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";

    var lienbattle = "bataille";
    var lienpost = "contenu";
  }

  var currentParams;

  require(["plugins/bridget"], function (jQueryBridget) {
    jQueryBridget("isotope", Isotope, $);

    var $sect_wrap = $(".sh-section__wrap");

    var $grid = $sect_wrap
      .isotope({
        itemSelector: ".sh-section__item",
        percentPosition: true,
        masonry: {
          columnWidth: ".sh-section__item",
          horizontalOrder: true
        },
        layoutMode: "masonry",
        transitionDuration: "0.4s",
        getSortData: {
          like: ".like parseInt",
          category: "[data-category]",
          weight: function (itemElem) {
            var weight = $(itemElem)
              .find(".weight")
              .text();
            return parseFloat(weight.replace(/[\(\)]/g, ""));
          },
          date: ".date parseInt"
        }
      })
      .addClass("sh-section__isotope-init");

    // filter functions
    var filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function () {
        var number = $(this)
          .find(".number")
          .html();
        return parseInt(number, 10) > 1;
      },
      // show if name ends with -ium
      ium: function () {
        var name = $(this)
          .find(".name")
          .text();
        return name.match(/ium$/);
      }
    };

    // bind sort button click
    $("#sorts a").on("click", function () {
      var sortByValue = $(this).attr("data-sort-by");
      $("#sorts a").removeClass("active");
      $(this).addClass("active");
      $grid.isotope({ filter: "*", sortBy: sortByValue, sortAscending: false });
      $('[data-toggle="tooltip"]').tooltip("hide");
    });

    $("#sorts a.filters-button-group").on("click", function () {
      var filterValue = $(this).attr("data-filter");
      $("#sorts a").removeClass("active");
      $(this).addClass("active");
      filterValue = filterFns[filterValue] || filterValue;
      $grid.isotope({ filter: filterValue });
      $('[data-toggle="tooltip"]').tooltip("hide");
    });

    $grid.isotope({
      percentPosition: true,
      masonry: {
        columnWidth: ".sh-section__item",
        horizontalOrder: true
      } /*,
		   	sortBy: 'like' , sortAscending : false */
    });

    //partage BATAILLE
    $(".btn-partage-battle").on("click", function (e) {
      $.magnificPopup.instance.close = function () {
        $(".div_amis").show();
        $(".div_mail").hide();
        $.magnificPopup.proto.close.call(this);
      };
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-partage-battle",
            type: "inline"
          }
        ]
      });

      $(".sh-partage-battle .partage").show();

      $("#errorLogPartageBattle").hide();
      const id_battle = $(this).data("battle");
      const url_battle = $(this).data("url");
      const title_battle = $(this).data("title");
      const image_battle = $(this).data("image");
      $("#form_save_partage_battle #id_battle").val(id_battle);

      $(".sh-partage-battle .sh-login__social").html(
        '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
        window.location.origin +
        "/" +
        lang +
        "/" +
        lienbattle +
        "/" +
        url_battle +
        '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
        '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
        title_battle +
        " - " +
        window.location.origin +
        "/" +
        lang +
        "/" +
        lienbattle +
        "/" +
        url_battle +
        '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
        '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
        window.location.origin +
        "/" +
        lang +
        "/" +
        lienbattle +
        "/" +
        url_battle +
        "&description=" +
        title_battle +
        "&media=" +
        window.location.origin +
        image_battle +
        '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
        '<a href="#" data-id="email" id="btn-partage-battle-email" class="sh-btn-social__send sh-btn-social" data-battle="' +
        id_battle +
        '" data-url="' +
        url_battle +
        '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
      );

      $("#btn-partage-battle-email").on("click", function () {
        $(".div_amis").hide();
        $(".div_mail").show();
        var id_battle = $(this).data("battle");
        $("#form_save_partage_battle_send #id_battle").val(id_battle);

        return false;
      });

      $(".sh-btn-social").on("click", function () {
        var value = $(this).data("id");
        $.ajax({
          type: "POST",
          url: "/php/saveLog.php",
          data: "value=partage&val=" + encodeURI(value + " / " + title_battle),
          cache: true,
          success: function (data) { }
        });
        // return false;
      });

      e.preventDefault();
      return false;
    });
    $("#btn-partage-battle").on("click", function () {
      $("#errorLogPartageBattle").hide();

      const form = $("#form_save_partage_battle").get(0);
      const params = {
        type: "battle",
        id_battle: $(form)
          .find("#id_battle")
          .val(),
        amis: []
      };
      $('#form_save_partage_battle input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartageBattle")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();

        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattle")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          const idBattle = $(form)
            .find("#id_battle")
            .val();

          const nbShares =
            parseInt($("#share_battle_" + idBattle).html()) +
            params.amis.length;
          $("#share_battle_" + idBattle).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });

      return false;
    });
    $("#btn-partage-battle-send").on("click", function () {
      $("#errorLogPartageBattleSend").hide();
      const form = $("#form_save_partage_battle_send").get(0);
      var formData = new FormData(form); // get the form data

      if (
        !$.trim(
          $(form)
            .find("#email1")
            .val().length
        )
      ) {
        $("#errorLogPartageBattleSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattleSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();

            return;
          }

          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $(".div_amis").show();
            $(".div_mail").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });
    $("#cancelSend").on("click", function () {
      $(".div_amis").show();
      $(".div_mail").hide();
      return false;
    });

    //signaler commentaire
    $(".sh-comments__signaler").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-signalerComment",
            type: "inline"
          }
        ]
      });
      var id_comment = $(this).data("comment");
      document.form_signaler_commentaire.comment.value = id_comment;

      e.preventDefault();
      return false;
    });
    $("#btn-signaler-comment").on("click", function () {
      $("#errorLogCommentaireSignaler").hide();

      var form = $("#form_signaler_commentaire").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_commentaire.comment.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=commentaire",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogCommentaireSignaler").show();
              var f = document.getElementById("errorLogPartagePost");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerComment .signaler").hide();
                $(".sh-signalerComment .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerComment .signaler").show();
                $(".sh-signalerComment .confirm").hide();
                document.form_signaler_commentaire.comment.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogCommentaireSignaler").show();
        var f = document.getElementById("errorLogCommentaireSignaler");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>" + errormessage + "</strong>";
      }
      return false;
    });

    //signaler post
    $(".sh-post__signaler").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-signalerPost",
            type: "inline"
          }
        ]
      });
      var id_post = $(this).data("post");
      document.form_signaler_post.post.value = id_post;

      var title_post = $(this).data("title");
      $(".sh-signalerPost .namePost").html(title_post);

      e.preventDefault();
      return false;
    });
    $("#btn-signaler-post").on("click", function () {
      $("#errorLogPostSignaler").hide();

      var form = $("#form_signaler_post").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_post.post.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPostSignaler").show();
              var f = document.getElementById("errorLogPostSignaler");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerPost .signaler").hide();
                $(".sh-signalerPost .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerPost .signaler").show();
                $(".sh-signalerPost .confirm").hide();
                document.form_signaler_post.mes.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPostSignaler").show();
        var f = document.getElementById("errorLogPostSignaler");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>" + errormessage + "</strong>";
      }
      return false;
    });

    //signaler post
    /* SUPPRIMER POST*/
    $(".sh-post__trash").on("click", function () {
      const idPost = $(this).data("post");
      const namePost = $(this).data("title");

      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-supp",
            type: "inline"
          }
        ]
      });

      $("#id_post").val(idPost);
      $("#postName").html(namePost);

      return false;
    });
    $("#suppPost").on("click", function () {
      const id_post = $("#id_post").val();
      console.log(id_post);

      $.ajax({
        type: "POST",
        url: "/php/suppPost.php",
        async: false,
        data: "id_post=" + id_post,
        success: function (result) {
          if (result == 'oui') {
            console.log(result);
            $.magnificPopup.close();
            $("#post_" + id_post).remove();
          }
        }
      });
      return false;
    });

    // Modifier BATAILLE
    $(".editBattle").on("click", function (e) {
      const id_battle = $(this).data("id");
      const name_battle = $(this).data("name");
      const category_battle = $(this).data("category");
      const info_battle = $(this).data("info");

      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-editBattle",
            type: "inline"
          }
        ]
      });
      const form = $("#form_edit_battle").get(0);
      $(form)
        .find("#id_battle")
        .val(id_battle);
      $(form)
        .find("#name_battle")
        .val(name_battle);
      $(form)
        .find("#info_battle")
        .val(info_battle);
      $(form)
        .find("#cat_battle")
        .val(category_battle);

      e.preventDefault();
      return false;
    });
    $("#editBattle").on("click", function () {
      const form = $("#form_edit_battle").get(0);

      const params = {
        type: "battle",
        id_battle: $(form)
          .find("#id_battle")
          .val(),
        name_battle: $(form)
          .find("#name_battle")
          .val(),
        cat_battle: $(form)
          .find("#cat_battle")
          .val(),
        info_battle: $(form)
          .find("#info_battle")
          .val()
      };

      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php",
        data: $.param(params),
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            $("#edit-b_" + params.id_battle).data("name", params.name_battle);
            $("#edit-b_" + params.id_battle).data(
              "category",
              params.cat_battle
            );
            $("#edit-b_" + params.id_battle).data("info", params.info_battle);
            $(".battle-title").html(params.name_battle);
            $(".battle-info").html(params.info_battle);

            $.magnificPopup.close();
          }
        }
      });
      return false;
    });

    /* MODIFIER POST*/
    $(".editPost").on("click", function (e) {
      const id_post = $(this).data("id");
      const name_post = $(this).data("name");

      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-editPost",
            type: "inline"
          }
        ]
      });
      const form = $("#form_edit_post").get(0);

      $(form)
        .find("#id_post")
        .val(id_post);
      $(form)
        .find("#name_post")
        .val(name_post);

      e.preventDefault();
      return false;
    });

    $("#editPost").on("click", function () {
      const form = $("#form_edit_post").get(0);

      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        name_post: $(form)
          .find("#name_post")
          .val()
      };
      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            const idPost = $(form)
              .find("#id_post")
              .val();
            const namePost = $(form)
              .find("#name_post")
              .val();

            $("#post_" + idPost + " h3.title")
              .attr("data-original-title", namePost)
              .html(namePost);

            $("#edit_" + idPost).data("name", namePost);

            $.magnificPopup.close();
          }
        }
      });
      return false;
    });

    // Open location
    $(".redirect-avatar").on("click", function (e) {
      const location = $(this).data("location");
      window.location.href = location;

      e.preventDefault();
      return false;
    });

    $(".editPost_one").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-editPost",
            type: "inline"
          }
        ]
      });

      e.preventDefault();
      return false;
    });
    $("#editPost_one").on("click", function () {
      const form = $("#form_edit_post").get(0);
      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        name_post: $(form)
          .find("#name_post")
          .val()
      };
      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php",
        data: $.param(params),
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            setTimeout(function () {
              location.reload();
            }, 500);
          }
        }
      });
      return false;
    });

    actions();

    //POPUP POST
    $(".btn-popup-post").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-popup-post",
            type: "inline"
          }
        ]
      });

      const idPost = $(this).data("id");
      const token = $(this).data("token");
      const ordre = $(this).data("ordre");

      $.ajax({
        type: "POST",
        url: "/popup_post.php",
        data: "token=" + token + "&code=" + lang + "&ordre=" + ordre,
        async: false,
        success: function (result) {
          $("#contenu_post").html(result);

          const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
          $("#view_post_" + idPost).html(nbViews);
        }
      });

      $('[data-toggle="tooltip"]').tooltip();
      actions();

      return false;
    });

    //debut action();
    function actions() {
      //$('[data-toggle="tooltip"]').tooltip();
      //FONCTION LIKE
      $(".likeSave").on("click", function () {
        const idPost = $(this).data("id");
        const params = {
          type: "like",
          id_post: idPost,
          code: lang
        };

        $.ajax({
          type: "POST",
          url: "/php/favoris.php",
          async: false,
          data: $.param(params),
          success: function (result) {
            if (result[0] != "non") {
              $("#like_" + idPost)
                .attr("src", "/images/icons/liked-pink.svg")
                .parent()
                .find("span")
                .html(result[0]);

              $("#battle-header-likes")
                .find("img")
                .attr("src", "/images/icons/liked-pink.svg")
                .parent()
                .find("span")
                .html(result[3]);

              $("#user_likes").html('<i class="fa fa-heart"></i> ' + result[1]);

              $("#user_likeCoins").html(
                '<i class="repu-coins"></i> ' + result[5]
              );

              $("#popup-nblikes").html(result[0]);
            }

            if (result[1] == "0") {
              console.log("oui");
              $("#like_" + idPost + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + idPost + '[data-toggle="tooltip"]')
                .attr("data-original-title", plusdelike)
                .tooltip("show");
              $.magnificPopup.open({
                mainClass: "mfp-with-zoom",
                removalDelay: 300,
                closeMarkup:
                  '<i title="%title%" class="mfp-close fa fa-close"></i>',
                items: [
                  {
                    src: ".sh-likemessage",
                    type: "inline"
                  }
                ]
              });
            } else if (result[2] == "non") {
              console.log("oui");
              $("#like_" + idPost + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + idPost + '[data-toggle="tooltip"]')
                .attr("data-original-title", maxvotecontenu)
                .tooltip("show");
            }

            // if (result[3] != "0" && document.getElementById("likeBataille")) {
            //   var f = document.getElementById("likeBataille");
            //   f.innerHTML = "";
            //   f.innerHTML = result[3];
            // }

            // if (result[4] != "0" && document.getElementById("top3")) {
            //   var f = document.getElementById("top3");
            //   f.innerHTML = "";
            //   f.innerHTML = result[4];
            // }
          }
        });

        return false;
      });

      //FONCTION FAVORIS
      $(".favSave").on("click", function () {
        var id_post = $(this).data("id");
        $.ajax({
          type: "POST",
          url: "/php/saveFavoris.php",
          async: false,
          data: "id_post=" + id_post + "&code=" + lang,
          success: function (result) {
            console.log(result);

            if (result == "save") {
              $("#fav_" + id_post).attr(
                "src",
                "/images/icons/favorite-pink.svg"
              );
            }

            if (result == "remove") {
              $("#fav_" + id_post).attr(
                "src",
                "/images/icons/favorite-grey.svg"
              );
            }
          }
        });

        return false;
      });

      //partage CONTENU
      $(".btn-partage-contenu").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis_contenu").show();
          $(".div_mail_contenu").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-contenu",
              type: "inline"
            }
          ]
        });

        $(".sh-partage-contenu .partage").show();

        $("#errorLogPartagePost").hide();
        const id_post = $(this).data("post");
        const title_post = $(this).data("title");
        const token_post = $(this).data("token");
        const image_post = $(this).data("image");

        $("#form_save_partage_contenu #id_post").val(id_post);

        $(".sh-partage-contenu .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_post +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          "&description=" +
          title_post +
          "&media=" +
          window.location.origin +
          image_post +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-contenu-email" class="sh-btn-social__send sh-btn-social" data-token="' +
          token_post +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-contenu-email").on("click", function () {
          $(".div_amis_contenu").hide();
          $(".div_mail_contenu").show();
          var token_post = $(this).data("token");
          $("#form_save_partage_contenu_send #token_post").val(token_post);
          return false;
        });

        $(".sh-btn-social").on("click", function () {
          var value = $(this).data("id");
          $.ajax({
            type: "POST",
            url: "/php/saveLog.php",
            data:
              "value=partContenu&val=" + encodeURI(value + " / " + title_post),
            cache: true,
            success: function (data) { }
          });
        });

        e.preventDefault();
        return false;
      });

      $("#btn-partage-contenu").on("click", function () {
        $("#errorLogPartagePost").hide();

        const form = $("#form_save_partage_contenu").get(0);

        const params = {
          type: "post",
          id_post: $(form)
            .find("#id_post")
            .val(),
          amis: []
        };

        $('#form_save_partage_contenu input[type="checkbox"]:checked').each(
          function () {
            params.amis.push($(this).attr("id"));
          }
        );

        if (!params.amis.length) {
          $("#errorLogPartagePost")
            .html("<strong class='text-danger'>" + selectami + "</strong>")
            .show();
          return false;
        }
        $.ajax({
          type: "POST",
          url: "/php/partageAmis.php",
          data: $.param(params),
          async: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPartagePost")
                .html("<strong class='text-danger'>" + erreur + "</strong>")
                .show();
              return;
            }
            setTimeout(function () {
              $(".sh-partage-contenu .partage").hide();
              $(".sh-partage-contenu .confirm").show();
            }, 500);

            const idPost = $(form)
              .find("#id_post")
              .val();

            const nbShares =
              parseInt($("#share_post_" + idPost).html()) + params.amis.length;
            $("#share_post_" + idPost).html(nbShares);

            setTimeout(function () {
              $(".sh-partage-contenu .confirm").hide();
              $.magnificPopup.close();
              form.reset();
            }, 2000);
          },
          error: function (jqXHR, exception) {
            if (jqXHR.status === 0) {
              console.log("Not connect.\n Verify Network.");
            } else if (jqXHR.status == 404) {
              console.log("Requested page not found. [404]");
            } else if (jqXHR.status == 500) {
              console.log("Internal Server Error [500].");
            } else if (exception === "parsererror") {
              console.log(jqXHR.responseText);
            } else if (exception === "timeout") {
              console.log("Time out error.");
            } else if (exception === "abort") {
              console.log("Ajax request aborted.");
            } else {
              console.log("Uncaught Error.\n" + jqXHR.responseText);
            }
          }
        });

        return false;
      });

      $("#btn-partage-contenu-send").on("click", function () {
        $("#errorLogPartageContenuSend").hide();
        const form = $("#form_save_partage_contenu_send").get(0);
        var formData = new FormData(form); // get the form data

        if (!document.form_save_partage_contenu_send.email1.value) {
          $("#errorLogPartageContenuSend")
            .html("<strong class='text-danger'>" + errormail + "</strong>")
            .show();
          return false;
        }
        $.ajax({
          type: "POST",
          url: "/php/saveInviter.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            if (data == "non") {
              $("#errorLogPartageContenuSend")
                .html("<strong class='text-danger'>" + erreur + "</strong>")
                .show();
              return;
            }
            $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

            setTimeout(function () {
              $(".sh-partage-contenu .partage").hide();
              $(".sh-partage-contenu .confirm").show();
            }, 500);

            setTimeout(function () {
              // $(".sh-partage-contenu .partage").show();
              $(".sh-partage-contenu .confirm").hide();
              $(".div_amis_contenu").show();
              $(".div_mail_contenu").hide();
              $.magnificPopup.close();
            }, 2000);
          }
        });

        return false;
      });

      $("#cancelSendContenu").on("click", function () {
        $(".div_amis_contenu").show();
        $(".div_mail_contenu").hide();
        return false;
      });

      //commentaire
      $("#btn-commentaire").on("click", function () {
        $("#errorLogCommentaire").hide();
        var form = $("#form_save_commentaire").get(0);
        var formData = new FormData(form); // get the form data

        if (document.form_save_commentaire.commentaire.value != "") {
          $.ajax({
            type: "POST",
            url: "/php/saveCommentaire.php",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
              if (data == "non") {
                $("#errorLogPartageContenuSend").show();
                var f = document.getElementById("errorLogPartageContenuSend");
                f.innerHTML = "";
                f.innerHTML =
                  "<strong class='text-danger'>" + erreur + "</strong>";
              } else if (data == "oui") {
                $("#errorLogPartageContenuSend").show();
                var f = document.getElementById("errorLogPartageContenuSend");
                f.innerHTML = "";
                f.innerHTML =
                  "<strong class='text-success'>Votre commentaire a bien été enregistré !</strong>";

                setTimeout(function () {
                  location.reload();
                }, 500);
              }
            }
          });
        } else {
          $("#errorLogCommentaire").show();
          var f = document.getElementById("errorLogCommentaire");
          f.innerHTML = "";
          f.innerHTML =
            "<strong class='text-danger'>" + errormessage + "</strong>";
        }
        return false;
      });

      //POPUP SUIVANT
      $(".btn-popup-post_suivant").on("click", function (e) {
        const idPost = $(this).data("id");
        const params = {
          code: lang,
          token: $(this).data("token"),
          ordre: $(this).data("ordre")
        };

        $.ajax({
          type: "POST",
          url: "/popup_post.php",
          data: $.param(params),
          async: false,
          success: function (result) {
            $("#contenu_post").html(result);
            const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
            $("#view_post_" + idPost).html(nbViews);
          }
        });

        $('[data-toggle="tooltip"]').tooltip();
        actions();

        return false;
      });
      /* SUPPRIMER BATTLE*/
      $(".sh-battle__trash").on("click", function () {
        const id_battle = $(this).data("battle");
        const name_battle = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-suppBattle",
              type: "inline"
            }
          ]
        });

        $("#id_battle").val(id_battle);
        $("#battleName").html(name_battle);
        return false;
      });
      $("#suppBattle").on("click", function () {
        const id_battle = $("#id_battle").val();
        console.log(id_battle);

        $.ajax({
          type: "POST",
          url: "/php/suppBattle.php",
          async: false,
          data: "id_battle=" + id_battle,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              window.location.href = "/" + lang + "/" + url_bienvenue;
            }
          }
        });
        return false;
      });
    }
  });
});
