define([
  "require",
  "jquery",
  "plugins/isotope",
  "plugins/bootstrap_v3",
  "plugins/wow"
], function(require, $, Isotope) {
  //lang
  if (lang == "en") {
    var nobattle = "No campaign";
    var nopost = "No content";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var plusdelike =
      "Your stock of LIKES is sold out. Rebuild it by converting your LIKECOINS, posting your best contents and campaigns, or inviting your friends.";
    var maxvotecontenu =
      "You have reached the maximum number of votes for this content";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";

    var lienbattle = "battle";
  } else {
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var plusdelike =
      "Votre stock de LIKES est épuisé. Reconstituez-le en convertissant vos LIKECOINS, en postant vos meilleurs contenus et batailles ou en invitant vos amis.";
    var maxvotecontenu =
      "Vous avez atteint le nombre maximal de votes pour ce contenu";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";

    var lienbattle = "bataille";
  }

  require(["plugins/bridget", "plugins/wow"], function(jQueryBridget, WOW) {
    new WOW().init();
    jQueryBridget("isotope", Isotope, $);
    var $ = require("jquery");

    var $sect_wrap = $(".sh-section__wrap"),
      $more_btn = $(".sh-footer__more-btn");

    var $grid = $sect_wrap
      .isotope({
        itemSelector: ".sh-section__item",
        percentPosition: true,
        layoutMode: "fitRows",
        transitionDuration: "0.4s",
        getSortData: {
          like: ".like parseInt",
          users: ".users parseInt",
          number: ".number parseInt",
          category: "[data-category]",
          weight: function(itemElem) {
            var weight = $(itemElem)
              .find(".weight")
              .text();
            return parseFloat(weight.replace(/[\(\)]/g, ""));
          },
          date: ".date parseInt"
        }
      })
      .addClass("sh-section__isotope-init");

    $grid.isotope({
      percentPosition: true,
      layoutMode: "fitRows",
      filter: ".open.cat",
      sortAscending: false
    });

    type = "battles";
    tab = "defis";

    //send_ajax_accueil(type, tab);
    var defis_act = true;
  });
});
