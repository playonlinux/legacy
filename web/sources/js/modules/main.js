define(["jquery", "plugins/bootstrap_v3"], function() {
  $('[data-toggle="tooltip"]').tooltip({ trigger: "click hover" });

  function copy(str) {
    var tmp = document.createElement("INPUT"),
      focus = document.activeElement;

    tmp.value = str;

    document.body.appendChild(tmp);
    tmp.select();
    document.execCommand("copy");
    document.body.removeChild(tmp);
    focus.focus();
  }

  $(".sh-input-copy > a").on("click", function(e) {
    var $input = $(this)
      .parents(".sh-input-copy")
      .find("input");

    copy($input.val());

    e.preventDefault();
    return false;
  });

  $("#mainKeywords").focus(function(e) {
    $(this)
      .parents(".dropdown")
      .addClass("dropped");
    e.stopPropagation();
  });

  $("#mainKeywords").keypress(function(e) {
    // logs
    if (e.which == 13 || event.keyCode == 13) {
      const keywords = encodeURI(btoa($(this).val()));
      setTimeout(function() {
        // logs
        //console.log($('#mainKeywords').val());
        $.ajax({
          type: "POST",
          url: "/php/saveLog.php",
          data: "value=recherche&val=" + keywords,
          cache: true,
          success: function(data) {}
        });
      }, 10);

      if ($(this).val()) {
        location.href = "/" + lang + "/" + url_search + "/" + keywords;
      }
    }
  });

  $("#icon-keyword-search-glass").click(function(e) {
    setTimeout(function() {
      // logs
      //console.log($('#mainKeywords').val());
      $.ajax({
        type: "POST",
        url: "/php/saveLog.php",
        data: "value=recherche&val=" + $("#mainKeywords").val(),
        cache: true,
        success: function(data) {}
      });
    }, 10);

    // document.searchForm.submit();
    location.href =
      "/" +
      lang +
      "/" +
      url_search +
      "/" +
      encodeURI(btoa($("#mainKeywords").val()));
    e.stopPropagation();
  });

  $(".dropdown").click(function(e) {
    e.stopPropagation();
  });

  $(document).click(function() {
    $(".dropdown").removeClass("dropped");
  });

  // if (screen.width <= 768) {
  // is mobile..
  var lastScrollTop = 0;

  $(window).on("scroll", function() {
    var st = $(this).scrollTop();

    if (st > 70 && st > lastScrollTop) {
      $("header").css("display", "none");
      // console.log("scroll vers le bas", st);
    } else {
      $("header").css("display", "block");
      // console.log("scroll vers le haut", st);
    }

    lastScrollTop = st;
  });
  // }
});
