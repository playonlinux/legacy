define(["jquery"], function($) {
  //lang
  if (lang == "en") {
    var aucunUser = "No user";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var sendinvitation = "Invitation sent";
    var devenirami = "Become friends";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";
  } else {
    var aucunUser = "Aucun participant";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var sendinvitation = "Invitation envoyée";
    var devenirami = "Devenir ami";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";
  }

  var cgv_act = false;
  var notif_act = false;

  $(".sh-header__cgv").on("click", function() {
    if (cgv_act || $(this).hasClass("active")) return;

    cgv_act = true;
    $(".mfp-bg").trigger("click");
    var $bg = $("<div>").addClass("mfp-bg mfp-with-zoom sh-cgv__bg");
    $(this).addClass("active");
    $(".sh-cgv")
      .stop()
      .fadeIn();
    $("body").append($bg);

    setTimeout(function() {
      $bg.addClass("mfp-ready");
      cgv_act = false;
    }, 0);
  });

  $(document).on(
    "click",
    ".sh-cgv__close, .sh-cgv__bg, .sh-header__cgv, .sh-header__avatar, .sh-header__btn-notification",
    function() {
      if (cgv_act) return;

      cgv_act = true;
      $(".sh-cgv")
        .stop()
        .hide();
      $(".sh-cgv__bg").remove();
      $(".sh-header__cgv").removeClass("active");
      cgv_act = false;
    }
  );

  $(".btn-inviter-menu").on("click", function(e) {
    $.magnificPopup.open({
      mainClass: "mfp-with-zoom",
      removalDelay: 300,

      items: [
        {
          src: ".sh-inviter-menu",
          type: "inline"
        }
      ]
    });

    $(".sh-inviter-menu .invitation").show();
    e.preventDefault();
    return false;
  });

  $("#btn-inviter-menu").on("click", function() {
    $("#errorLogInviterMenu").hide();

    const form = $("#form_save_inviter_menu").get(0);
    var formData = new FormData(form); // get the form data

    if (
      !$.trim(
        $(form)
          .find("#email1")
          .val().length
      )
    ) {
      $("#errorLogInviterMenu")
        .html("<strong class='text-danger'>" + errormail + "</strong>")
        .show();
      return false;
    }
    $.ajax({
      type: "POST",
      url: "/php/saveInviter.php",
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        console.log(data);

        if (data == "non") {
          $("#errorLogInviterMenu")
            .html("<strong class='text-danger'>" + erreur + "</strong>")
            .show();
          return;
        }
        setTimeout(function() {
          $(".sh-inviter-menu .invitation").hide();
          $(".sh-inviter-menu .confirm").show();
        }, 500);
        setTimeout(function() {
          $(".sh-inviter-menu .confirm").hide();
          $.magnificPopup.close();
          form.reset();
        }, 2000);
      }
    });

    return false;
  });

  $(document).ready(function() {
    // updating the view with notifications using ajax
    function load_unseen_notification(view = "") {
      $.ajax({
        url: "/php/get_notifs.php",
        method: "POST",
        data: { view: view },
        dataType: "json",
        success: function(data) {
          $(".notifContent").html(data.notification);

          if (data.count_notif > 0) {
            $(".notif").html(data.count_notif);
          }
        }
      });
    }

    load_unseen_notification();

    $(".sh-header__btn-notification").on("click", function() {
      if (notif_act || $(this).hasClass("active")) return;

      notif_act = true;
      $(".mfp-bg").trigger("click");
      var $bg = $("<div>").addClass("mfp-bg mfp-with-zoom sh-notif__bg");
      $(this).addClass("active");
      $(".sh-notif")
        .stop()
        .fadeIn();
      $("body").append($bg);

      setTimeout(function() {
        $bg.addClass("mfp-ready");
        notif_act = false;
      }, 0);

      $(".notif").html("");
      load_unseen_notification("yes");
    });

    $(document).on(
      "click",
      ".sh-notif__close, .sh-notif__bg, .sh-header__avatar, .sh-header__cgv",
      function() {
        if (notif_act) return;

        notif_act = true;
        $(".sh-notif")
          .stop()
          .hide();
        $(".sh-notif__bg").remove();
        $(".sh-header__btn-notification").removeClass("active");
        notif_act = false;
      }
    );

    setInterval(function() {
      load_unseen_notification();
    }, 30000);
  });
});
