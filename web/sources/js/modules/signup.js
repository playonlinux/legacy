define(["require", "jquery", "plugins/magnificPopup"], function (require) {
  var $ = require("jquery"),
    magnificPopup = require("plugins/magnificPopup");
  $.urlParam = function (name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    }

    return results[1] || 0;
  };
  //lang
  if (lang == "en") {
    var battle = "battle";
    var post = "post";
    var erreur = "An error occurred";
    var oublichamps = "Thank you fill in all fields";
    var compteexistant = "An account already exists, please login";
    var errorcompte =
      "This account doesn't exist or your login credentials are incorrect!";
    var errormdp = "Non-identical passwords!";
    var errormail = "Email unknown!";
    var validmail = "An email has been sent to you";
    var videmail = "Thank you to fill an email";
    var savecompte = "Congratulations! You will be redirected in a few seconds";
    var savetheme = "Choose a campaign and post your first content! ";
    var errortheme = "Thank you to fill at least 5 categories";

    var btfinaliser = "POST YOUR FIRST CONTENT <br />AND WIN 1 000 LIKECOINS!";
  } else {
    var battle = "bataille";
    var post = "contenu";
    var erreur = "Une erreur est survenue";
    var oublichamps = "Merci de renseigner tous les champs";
    var compteexistant = "Un compte existe deja, merci de vous connecter";
    var errorcompte =
      "Ce compte n'existe pas ou vos identifiants de connexion sont incorrects !";
    var errormdp = "Mots de passe non identiques !";
    var errormail = "E-mail inconnu !";
    var validmail = "Un mail vient de vous être envoyé";
    var videmail = "Merci de renseigner un email";
    var savecompte =
      "Félicitations ! Vous allez être redirigé dans quelques secondes";
    var savetheme = "Choisissez une bataille et postez votre premier contenu !";
    var errortheme = "Merci de renseigner au moins 5 thèmes";

    var btfinaliser =
      "POSTEZ VOTRE PREMIER CONTENU <br />ET GAGNEZ 1 000 LIKECOINS";
  }

  if (demande.length != 0 && demande == 1) {
    $.magnificPopup.open({
      mainClass: "mfp-with-zoom",
      removalDelay: 300,

      items: [
        {
          src: ".sh-login",
          type: "inline"
        }
      ]
    });
  }

  $(".sh-login__btn-signup, .btn-upload_btn-signup").on("click", function (e) {
    $.magnificPopup.open({
      mainClass: "mfp-with-zoom",
      removalDelay: 300,

      items: [
        {
          src: ".sh-login",
          type: "inline"
        }
      ]
    });

    e.preventDefault();
    return false;
  });

  $("#bt_inscription").on("click", function () {
    $("#errorLog").hide();

    var form = $("#form_save").get(0);
    var formData = new FormData(form); // get the form data

    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailA = document.form_save.email.value;

    console.log(document.form_save.password.value.length);

    if (
      document.form_save.email.value != "" &&
      regex.test(emailA) &&
      document.form_save.password.value != ""
    ) {
      $.ajax({
        type: "POST",
        url: "/php/saveUser.php",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLog").show();
            var f = document.getElementById("errorLog");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else if (data == "identique") {
            $("#errorSave").show();
            var f = document.getElementById("errorSave");
            f.innerHTML = "";
            f.innerHTML =
              "<strong class='text-danger'>" + compteexistant + "</strong>";
          } else {
            $(".save").hide();

            var user = data[0];
            var token = data[1];

            window.location.href =
              "/post_first.php?id=" +
              user +
              "&token=" +
              token +
              "&redirect_uri=" +
              window.location.pathname;
          }
        }
      });
    } else {
      if (document.form_save.email.value == "") {
        $("#form_save #email").css("border", "1px solid #f00");
      }
      if (document.form_save.password.value == "") {
        $("#form_save #password").css("border", "1px solid #f00");
      }

      $("#errorLog").show();
      var f = document.getElementById("errorLog");
      f.innerHTML = "";
      f.innerHTML = "<strong class='text-danger'>" + oublichamps + "</strong>";
    }
    return false;
  });

  $("#logConnexion").on("click", function () {
    $(".log").show();
    $(".save").hide();
    $(".mdp").hide();

    return false;
  });

  $("#cancel, #cancel2").on("click", function () {
    $(".log").hide();
    $(".save").show();
    $(".mdp").hide();

    return false;
  });

  $("#cancel3").on("click", function () {
    $(".log").show();
    $(".save").hide();
    $(".mdp").hide();

    return false;
  });

  $("#mdp").on("click", function () {
    $(".log").hide();
    $(".save").hide();
    $(".mdp").show();
    $("#errorMdp").hide();
    return false;
  });

  $("#bt_signup").on("click", function () {
    $("#errorLog").hide();

    const form = $("#form_log").get(0);

    const params = {
      email: $(form)
        .find("#email")
        .val(),
      password: $(form)
        .find("#password")
        .val(),
      remember: $(form)
        .find("#remember")
        .is(":checked")
    };

    if (!params.email || !params.password) {
      if (!params.email) {
        $(form)
          .find("#email")
          .css("border", "1px solid #f00");
      }
      if (!params.password) {
        $(form)
          .find("#password")
          .css("border", "1px solid #f00");
      }
      $("#errorLog")
        .html("<strong class='text-danger'>" + oublichamps + "</strong>")
        .show();
      return false;
    }

    $.ajax({
      type: "POST",
      url: "/php/logUser.php",
      data: $.param(params),
      async: false,
      cache: false,
      success: function (data) {
        console.log(data);
        if (data == "non") {
          $("#errorLog")
            .html("<strong class='text-danger'>" + errorcompte + "</strong>")
            .show();
          return false;
        }
        window.location.reload();
      },
      error: function (jqXHR, exception) {
        if (jqXHR.status === 0) {
          console.log("Not connect.\n Verify Network.");
        } else if (jqXHR.status == 404) {
          console.log("Requested page not found. [404]");
        } else if (jqXHR.status == 500) {
          console.log("Internal Server Error [500].");
        } else if (exception === "parsererror") {
          console.log(jqXHR.responseText);
        } else if (exception === "timeout") {
          console.log("Time out error.");
        } else if (exception === "abort") {
          console.log("Ajax request aborted.");
        } else {
          console.log("Uncaught Error.\n" + jqXHR.responseText);
        }
      }
    });

    return false;
  });

  //mot de passe
  $(".show-password").click(function () {
    if (
      $(this)
        .prev("input")
        .prop("type") == "password"
    ) {
      //Si c'est un input type password
      $(this)
        .prev("input")
        .prop("type", "text");
      $(this).html('<i class="fa fa-eye-slash"></i>');
    } else {
      //Sinon
      $(this)
        .prev("input")
        .prop("type", "password");
      $(this).html('<i class="fa fa-eye"></i>');
    }
  });

  $("#bt_save").on("click", function () {
    $("#errorLog").hide();

    const form = $("#form_save_mdp").get(0);
    const params = {
      token: $(form).find("#token").val(),
      id: $(form).find("#id").val(),
      pass: $.trim($(form).find('#pass').val()),
      pass_confirm: $.trim($(form).find('#pass_confirm').val())
    };

    console.log(params);
    if (!params.pass.length ||
      !params.pass_confirm.length ||
      params.pass !== params.pass_confirm
    ) {

      if (!params.pass.length) {
        $(form).find("#pass").css("border", "1px solid #f00");
      }
      if (!params.pass_confirm.length) {
        $(form).find("#pass_confirm").css("border", "1px solid #f00");
      }
      $("#errorLog")
        .html("<strong class='text-danger'>" + errormdp + "</strong>")
        .show();
      return false;
    }
    $.ajax({
      type: "POST",
      url: "/php/saveNewMdp.php",
      data: $.param(params),
      async: false,
      cache: false,
      success: function (data) {
        if (data != "ok") {
          $("#errorLog")
            .html("<strong class='text-danger'>" + data + "</strong>")
            .show();
          return false;
        }
        window.location.reload();
      }
    });

    return false;
  });

  $("#bt_mdp").on("click", function () {

    const form = $("#form_mdp").get(0);
    var formData = new FormData(form); // get the form data

    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailA = document.form_mdp.email.value;

    if (document.form_mdp.email.value != "" && regex.test(emailA)) {
      $.ajax({
        type: "POST",
        url: "/php/forgotMdp.php",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorMdp").css("display", "block");
            var f = document.getElementById("errorMdp");
            f.innerHTML = "";
            f.innerHTML = "<b class='text-danger'>" + errormail + "</b>";
          } else {
            $("#errorMdp").html("<b class='text-success'>" + validmail + "</b>").show();

            setTimeout(function () {
              $(".log").show();
              $(".save").hide();
              $(".mdp").hide();
              form.reset();
            }, 1500);
          }
        }
      });
    } else {
      if (document.form_mdp.email.value == "") {
        $("#form_mdp #email_mdp").css("border", "1px solid #f00");
      }

      $("#errorMdp").css("display", "block");
      var f = document.getElementById("errorMdp");
      f.innerHTML = "";
      f.innerHTML = "<b class='text-danger'>" + videmail + "</b>";
    }
    return false;
  });

  $("#bt_confirm").on("click", function () {
    $("#errorSaveConfirm").hide();

    var form = $("#form_save_confirm").get(0);
    var formData = new FormData(form); // get the form data

    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailA = document.form_save_confirm.email_u.value;

    var ch_data = [];

    $('input[type="checkbox"]:checked').each(function () {
      ch_data.push($(this).attr("id"));
    });

    formData.append("catok", ch_data);

    if (
      document.form_save_confirm.email_u.value != "" &&
      regex.test(emailA) &&
      ch_data.length >= 5
    ) {
      $.ajax({
        type: "POST",
        url: "/php/updateUser.php",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorSaveConfirm").show();
            var f = document.getElementById("errorSaveConfirm");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else if (data == "oui") {
            $("#errorSaveConfirm").show();
            var f = document.getElementById("errorSaveConfirm");
            f.innerHTML = "";
            f.innerHTML =
              "<strong class='text-danger'>" + savecompte + "</strong>";
            setTimeout(function () {
              $("#form_save_confirm").attr("action", "felicitations.php");
              $("#form_save_confirm").submit();
            }, 500);
          }
        }
      });
    } else {
      var messageError = oublichamps;
      $("#errorSaveConfirm").show();

      if (document.form_save_confirm.nom.value == "") {
        $("#form_save_confirm #nom_u").css("border", "1px solid #f00");
      }
      if (document.form_save_confirm.prenom.value == "") {
        $("#form_save_confirm #prenom_u").css("border", "1px solid #f00");
      }
      if (document.form_save_confirm.email_u.value == "") {
        $("#form_save_confirm #email_u").css("border", "1px solid #f00");
      }

      if (document.form_save_confirm.naissance.value == "") {
        $("#form_save_confirm #naissance").css("border", "1px solid #f00");
      }
      if (document.form_save_confirm.ville.value == "") {
        $("#form_save_confirm #ville").css("border", "1px solid #f00");
      }

      if (ch_data.length < 5) {
        messageError = errortheme + "<br>" + oublichamps;
      }

      var f = document.getElementById("errorSaveConfirm");
      f.innerHTML = "";
      f.innerHTML = "<strong class='text-danger'>" + messageError + "</strong>";
    }
    return false;
  });

  //Premier post
  $("#bt_confirm_theme").on("click", function () {
    $("#errorSaveConfirmTheme").hide();

    const form = $("#form_save_theme").get(0);

    const params = {
      user: $.urlParam("id"),
      token: $.urlParam("token"),
      gender: $(form).find('input[type="radio"]:checked').val(),
      catok: []
    };

    console.log($(form)
      .find('input.chk-categories:checked'));
    $(form)
      .find('.chk-categories:checked')
      .each(function () {
        params.catok.push($(this).attr("id"));
      });

    if (params.catok.length < 5) {
      $("#errorSaveConfirmTheme")
        .html('<strong style="color:#FFFFFF">' + errortheme + "</strong>")
        .show();
      return false;
    }

    $(form).find('.chk_alert').each(function () {
      params[$(this).attr('name')] = ($(this).is(':checked')) ? 1 : 5;
    });

    $.ajax({
      type: "POST",
      url: "/php/updateThemes.php",
      data: $.param(params),
      cache: false,
      success: function (data) {
        if (data == "non") {
          $("#errorSaveConfirmTheme")
            .html('<strong style="color:#FFFFFF">' + erreur + "</strong>")
            .show();
        } else if (data == "oui") {
          $("#errorSaveConfirmTheme")
            .html('<strong style="color:#FFFFFF">' + savetheme + "</strong>")
            .show();
          setTimeout(function () {
            const href =
              $.urlParam("redirect_uri") || "/" + lang + "/" + url_bienvenue;
            window.location.href = href;
          }, 500);
        }
      }
    });
    return false;
  });
});
