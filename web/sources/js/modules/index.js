define([
    'jquery',
    'plugins/bootstrap_v3'
], function () {
	
	require( [
		'plugins/wow'
    ], function( WOW ) {
			new WOW().init();
	});
});