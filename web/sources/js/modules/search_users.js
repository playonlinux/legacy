define(["require", "jquery", "plugins/bootstrap_v3"], function(require) {
  //lang
  if (lang == "en") {
    var aucunUser = "No user";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var sendinvitation = "Invitation sent";
    var devenirami = "Become friends";
    var annulerami = "Cancel the request";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";
  } else {
    var aucunUser = "Aucun participant";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var sendinvitation = "Invitation envoyée";
    var devenirami = "Devenir ami";
    var annulerami = "Annuler la demande";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";
  }

  var $ = require("jquery");

  send_ajax_search("users", "users");
  var users_act = true;

  function send_ajax_search(type, tab) {
    $.ajax({
      data: "code=" + lang,
      type: "POST",
      url: "/php/get_" + type + ".php",
      cache: false,
      async: true,
      beforeSend: function() {
        document.getElementById(tab).innerHTML =
          '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
          '<span class="center-block text-center">' +
          chargement +
          "</span></div>";
      },
      success: function(data) {
        var $elem = $(data);
        if (data != "null") {
          setTimeout(function() {
            $(".reload").css("display", "none");
            $("#" + tab)
              .isotope("insert", $elem)
              .isotope("layout");
            actions();
          }, 1000);
        } else {
          document.getElementById(tab).innerHTML =
            '<p class="text-center center-block">' + aucunUser + "</p>";
        }
      },
      error: function(jqXHR, exception) {
        if (jqXHR.status === 0) {
          console.log("Not connect.\n Verify Network.");
        } else if (jqXHR.status == 404) {
          console.log("Requested page not found. [404]");
        } else if (jqXHR.status == 500) {
          console.log("Internal Server Error [500].");
        } else if (exception === "parsererror") {
          console.log(jqXHR.responseText);
        } else if (exception === "timeout") {
          console.log("Time out error.");
        } else if (exception === "abort") {
          console.log("Ajax request aborted.");
        } else {
          console.log("Uncaught Error.\n" + jqXHR.responseText);
        }
      }
    });
  }

  function actions() {
    $('[data-toggle="tooltip"]').tooltip();

    //demande d'amis
    $(".btn-friend_search").on("click", onFriendRequest());

    $(".btn-friend_annule").on("click", onCancelFriendRequest());
  }

  function onFriendRequest() {
    return function() {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      if (!user || !friend) {
        return false;
      }

      const params = {
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveDemandeAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function(data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          console.log(
            "generateBtnCancelFriendRequest = ",
            generateBtnCancelFriendRequest(user, friend)
          );
          $("#user_" + friend + " .sh-section__footer").html(
            generateBtnCancelFriendRequest(user, friend)
          );

          $.magnificPopup.open({
            mainClass: "mfp-with-zoom",
            removalDelay: 300,
            closeMarkup:
              '<i title="%title%" class="mfp-close fa fa-times"></i>',
            items: [
              {
                src: ".sh-reponse",
                type: "inline"
              }
            ]
          });

          setTimeout(function() {
            $.magnificPopup.close();
          }, 2000);
        }
      });
      return false;
    };
  }
  function generateBtnFriendRequest(user, friend) {
    return $("<a></a>")
      .attr("id", "btn-friend-" + friend)
      .attr("data-user", user)
      .attr("data-friend", friend)
      .attr("href", "javascript:void(0)")
      .addClass("sh-btn center-block btn-friend_search")
      .html(devenirami)
      .on("click", onFriendRequest());
  }

  function onCancelFriendRequest() {
    return function() {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      console.log(user, friend);

      if (!user || !friend) {
        return false;
      }

      const params = {
        type: "annuler",
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function(data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }

          $("#user_" + friend + " .sh-section__footer").html(
            generateBtnFriendRequest(user, friend)
          );

          $.magnificPopup.open({
            mainClass: "mfp-with-zoom",
            removalDelay: 300,
            closeMarkup:
              '<i title="%title%" class="mfp-close fa fa-close"></i>',
            items: [
              {
                src: ".sh-annuler",
                type: "inline"
              }
            ]
          });

          setTimeout(function() {
            $.magnificPopup.close();
          }, 2000);
        }
      });
      return false;
    };
  }
  function generateBtnCancelFriendRequest(user, friend) {
    return $("<a></a>")
      .attr("id", "btn-annule-friend-" + friend)
      .attr("data-user", user)
      .attr("data-friend", friend)
      .attr("href", "#")
      .addClass("sh-btn center-block btn-friend_annule")
      .html(annulerami)
      .on("click", onCancelFriendRequest());
  }
});
