define(["require", "jquery", "plugins/isotope"], function (require, $, Isotope) {
  //lang
  if (lang == "en") {
    var nobattle = "No campaign";
    var nopost = "No content";
    var notrophy = "No trophy";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var plusdelike =
      "Your stock of LIKES is sold out. Rebuild it by converting your LIKECOINS, posting your best contents and campaigns, or inviting your friends.";
    var maxvotecontenu =
      "You have reached the maximum number of votes for this content";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var successdelete =
      "Your account has been deleted :-( In a few seconds you will be redirected to homepage and be disconnected.";
    var chargement = "Loading";

    var lienbattle = "battle";
    var lienpost = "post";

    var urlparametre = "/en/my-account";
    if (parametre) {
      urlparametre += "/settings";
    }
  } else {
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
    var notrophy = "Aucun trophée";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var plusdelike =
      "Votre stock de LIKES est épuisé. Reconstituez-le en convertissant vos LIKECOINS, en postant vos meilleurs contenus et batailles ou en invitant vos amis.";
    var maxvotecontenu =
      "Vous avez atteint le nombre maximal de votes pour ce contenu";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var successdelete =
      "Votre compte a été supprimé :-( Dans quelques secondes, vous allez être redirigé vers la page d'accueil et être déconnecté.";
    var chargement = "Chargement en cours";

    var lienbattle = "bataille";
    var lienpost = "contenu";

    var urlparametre = "/fr/mon-compte";
    if (parametre) {
      urlparametre += "/parametres";
    }
  }

  var currentParams;
  var canLoad;
  var currentTab;

  require([], function (jQueryBridget) {
    // jQueryBridget("isotope", Isotope, $);

    var $ = require("jquery");

    var defis_act = false;
    var post_act = false;
    var trophee_act = false;
    var fav_act = false;
    var parametre_act = false;

    $(".menu").on("click change", function () {
      let context;
      if ($(this).hasClass("visible-xs") && $(this).css("display") != "none") {
        context = $(this).find("option:selected");
      } else {
        context = this;
      }

      currentTab = $(context).data("id");
      const user = $(context).data("user");
      $(".tab-content").hide();
      $(".tab-menu").hide();
      $(".menu").removeClass("active");

      $("#" + currentTab).show();
      $("#" + currentTab + "_menu").show();

      $(context).addClass("active");

      if (currentTab == "defis" && defis_act == false) {
        get_battles({
          get: user
        });
        defis_act = true;
      } else if (currentTab == "posts" && post_act == false) {
        get_posts({
          get: user
        });
        post_act = true;
      } else if (currentTab == "trophees" && trophee_act == false) {
        get_trophees({
          get: user
        });
        trophee_act = true;
      } else if (currentTab == "favoris" && fav_act == false) {
        get_favoris({
          get: user
        });
        fav_act = true;
      }

      return false;
    });

    if (parametre && !parametre_act) {
      parametre_act = true;

      $(".sh-header__parametres").trigger("click");
      $(this).addClass("active");
      $(".sh-parametres")
        .stop()
        .fadeIn();
      $(".header-battle").hide();

      $("#parametres, #themes, #defis").hide();
      $("#profil").show();

      parametre_act = false;
    } else if (trophees && !trophee_act) {
      if ($(".menu.visible-xs").css("display") == "none") {
        $('a.menu[data-id="trophees"]').trigger("click");
      } else {
        $(".menu.visible-xs")
          .find('[data-id="trophees"]')
          .prop("selected", true)
          .trigger("change");
      }
    } else {
      if (defis_act == false) {
        const user = $("#defis").data("user");
        currentTab = "defis";
        get_battles({
          get: user
        });
        defis_act = true;
      }
    }
    $(".sh-header__parametres").on("click", function () {
      if (parametre_act) return;

      parametre_act = true;
      $(".mfp-bg").trigger("click");
      // var $bg = $("<div>").addClass("mfp-bg mfp-with-zoom sh-parametres__bg");
      $(this).addClass("active");
      $(".sh-parametres")
        .stop()
        .fadeIn();
      // $("body").append($bg);
      $(".header-battle").hide();

      setTimeout(function () {
        //$bg.addClass('mfp-ready');
        parametre_act = false;
      }, 0);
    });

    $(document).on(
      "click",
      ".sh-parametres__close, .sh-header__parametres",
      function () {
        if (parametre_act) return;

        parametre_act = true;
        $(".sh-parametres")
          .stop()
          .hide();
        $(".sh-parametres__bg").remove();
        $(".sh-header__parametres").removeClass("active");
        parametre_act = false;
        $(".header-battle").show();
        $("#profil, #themes, #parametres").hide();
        $("#defis").show();
        const user = $("#defis").data("user");
        currentTab = "defis";
        get_battles({
          get: user
        });
        defis_act = true;
      }
    );

    $(".sous-menu").on("click", function () {
      currentTab = $(this).data("id");
      const menu = $(this).data("menu");
      const user = $(this).data("user");

      $(".tab-content").hide();
      $("#" + menu + " .sous-menu").removeClass("active");

      $("#" + currentTab).show();

      $(this).addClass("active");

      if (currentTab == "defis" && defis_act == false) {
        get_battles({
          get: user
        });
        defis_act = true;
      } else if (currentTab == "posts" && post_act == false) {
        get_posts({
          get: user
        });
        post_act = true;
      } else if (currentTab == "trophees" && trophee_act == false) {
        trophee_act = true;
        get_trophees({
          get: user
        });
      }

      return false;
    });

    $(window).scroll(function () {
      if (!canLoad) {
        return false;
      }
      if (currentTab !== "defis") {
        return false;
      }
      const scrollHeight = Math.floor($(document).height() / 10);
      const scrollPosition = Math.floor(
        ($(window).height() + $(window).scrollTop()) / 10
      );

      if (scrollHeight - scrollPosition === 0) {
        if (currentParams && currentParams.page) {
          currentParams.page = currentParams.page + 1;
          // TODO: battles or posts
          get_battles(currentParams);
        }
      }
    });

    function request(type, container, params = {}) {
      params.page = params.page || 1;
      $.post({
        data: $.param(params),
        url: "/php/get_" + type + ".php",
        cache: false,
        async: true,
        beforeSend: function () {
          if (!canLoad) {
            return;
          }
          if (params.page == 1) {
            $("#" + container).html(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          } else {
            $("#" + container).append(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          }
        },
        success: function (data) {
          canLoad = data != "null";
          $(".reload").remove();

          if (data != "null") {
            if (params.page == 1) {
              $("#" + container).html(data);
            } else {
              $("#" + container).append(data);
            }
            actions();
            currentParams = params;
          } else if (container == "defis") {
            if (params.page == 1) {
              $("#" + container).html(
                '<p class="text-center center-block">' + nobattle + "</p>"
              );
            }
          } else if (params.page == 1) {
            $("#" + container).html(
              '<p class="text-center center-block">' + nopost + "</p>"
            );
          }
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    }

    function get_battles(params = {}) {
      request("battles", "defis", params);
    }
    function get_posts(params = {}) {
      params.trophee = 0;
      request("posts", "posts", params);
    }
    function get_trophees(params = {}) {
      params.trophee = 1;
      request("posts", "trophees", params);
    }
    function get_favoris(params = {}) {
      params.trophee = 2;
      request("posts", "favoris", params);
    }

    //debut action();
    function actions() {
      // $('[data-toggle="tooltip"]').tooltip();

      //FONCTION FAVORIS
      $(".likeSave").on("click", function () {
        var id_post = $(this).data("id");
        $.ajax({
          type: "POST",
          url: "/php/favoris.php",
          async: false,
          data: "type=like&id_post=" + id_post + "&code=" + lang,
          success: function (result) {
            console.log(result);
            if (result[0] != "non") {
              $("#like_" + id_post)
                .attr("src", "/images/icons/liked-pink.svg")
                .parent()
                .find("span")
                .html(result[0]);
              $("#popup-nblikes").html(result[0]);
              $("#user_likes").html('<i class="fa fa-heart"></i> ' + result[1]);

              $("#user_likeCoins").html(
                '<i class="repu-coins"></i> ' + result[5]
              );
            }
            if (result[1] == "0") {
              console.log("oui");
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", plusdelike)
                .tooltip("show");
              $.magnificPopup.open({
                mainClass: "mfp-with-zoom",
                removalDelay: 300,
                closeMarkup:
                  '<i title="%title%" class="mfp-close fa fa-close"></i>',
                items: [
                  {
                    src: ".sh-likemessage",
                    type: "inline"
                  }
                ]
              });
            } else if (result[2] == "non") {
              console.log("oui");
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", maxvotecontenu)
                .tooltip("show");
            }
          }
        });

        return false;
      });

      /* SUPPRIMER POST*/
      $(".sh-post__trash").on("click", function () {
        const idPost = $(this).data("post");
        const namePost = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-supp",
              type: "inline"
            }
          ]
        });

        $("#id_post").val(idPost);
        $("#postName").html(namePost);

        return false;
      });
      $("#suppPost").on("click", function () {
        const id_post = $("#id_post").val();
        console.log(id_post);

        $.ajax({
          type: "POST",
          url: "/php/suppPost.php",
          async: false,
          data: "id_post=" + id_post,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              $("#post_" + id_post).remove();
            }
          }
        });
        return false;
      });

      $(".btn-popup-post").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-popup-post",
              type: "inline"
            }
          ]
        });

        const idPost = $(this).data("id");
        var token = $(this).data("token");
        var ordre = $(this).data("ordre");

        $.ajax({
          type: "POST",
          url: "/popup_post.php",
          data: "token=" + token + "&code=" + lang + "&ordre=" + ordre,
          async: false,
          success: function (result) {
            $("#contenu_post").html(result);

            const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
            $("#view_post_" + idPost).html(nbViews);
          }
        });

        $('[data-toggle="tooltip"]').tooltip();
        actions();

        return false;
      });
      //POPUP SUIVANT
      $(".btn-popup-post_suivant").on("click", function (e) {
        const idPost = $(this).data("id");
        const params = {
          code: lang,
          token: $(this).data("token"),
          ordre: $(this).data("ordre")
        };

        $.ajax({
          type: "POST",
          url: "/popup_post.php",
          data: $.param(params),
          async: false,
          success: function (result) {
            $("#contenu_post").html(result);
            const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
            $("#view_post_" + idPost).html(nbViews);
          }
        });

        $('[data-toggle="tooltip"]').tooltip();
        actions();

        return false;
      });

      /* MODIFIER POST*/
      $(".editPost").on("click", function (e) {
        const id_post = $(this).data("id");
        const name_post = $(this).data("name");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-editPost",
              type: "inline"
            }
          ]
        });
        const form = $("#form_edit_post").get(0);

        $(form)
          .find("#id_post")
          .val(id_post);
        $(form)
          .find("#name_post")
          .val(name_post);

        e.preventDefault();
        return false;
      });
      $(".editBattle").on("click", function (e) {
        const id_battle = $(this).data("id");
        const name_battle = $(this).data("name");
        const cat_battle = $(this).data("cat");
        const info_battle = $(this).data("info");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-editBattle",
              type: "inline"
            }
          ]
        });
        const form = $("#form_edit_battle").get(0);
        $(form)
          .find("#id_battle")
          .val(id_battle);
        $(form)
          .find("#name_battle")
          .val(name_battle);
        $(form)
          .find("#info_battle")
          .val(info_battle);
        $(form)
          .find("#cat_battle")
          .val(cat_battle);

        e.preventDefault();
        return false;
      });

      //partage BATAILLE
      $(".btn-partage-battle").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis").show();
          $(".div_mail").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-battle",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-battle .partage").show();
        $("#errorLogPartageBattle").hide();
        const id_battle = $(this).data("battle");
        const url_battle = $(this).data("url");
        const title_battle = $(this).data("title");
        const image_battle = $(this).data("image");
        $("#form_save_partage_battle #id_battle").val(id_battle);

        $(".sh-partage-battle .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_battle +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          "&description=" +
          title_battle +
          "&media=" +
          window.location.origin +
          image_battle +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-battle-email" class="sh-btn-social__send sh-btn-social" data-battle="' +
          id_battle +
          '" data-url="' +
          url_battle +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-battle-email").on("click", function () {
          $(".div_amis").hide();
          $(".div_mail").show();
          const id_battle = $(this).data("battle");
          $("#form_save_partage_battle_send #id_battle").val(id_battle);

          return false;
        });

        e.preventDefault();
        return false;
      });

      //partage CONTENU
      $(".btn-partage-contenu").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis_contenu").show();
          $(".div_mail_contenu").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-contenu",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-contenu .partage").show();
        $("#errorLogPartagePost").hide();
        const id_post = $(this).data("post");
        const title_post = $(this).data("title");
        const token_post = $(this).data("token");
        const image_post = $(this).data("image");
        $("#form_save_partage_contenu #id_post").val(id_post);

        $(".sh-partage-contenu .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_post +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          "&description=" +
          title_post +
          "&media=" +
          window.location.origin +
          image_post +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-contenu-email" class="sh-btn-social__send sh-btn-social" data-token="' +
          token_post +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-contenu-email").on("click", function () {
          $(".div_amis_contenu").hide();
          $(".div_mail_contenu").show();
          const token_post = $(this).data("token");
          $("#form_save_partage_contenu_send #token_post").val(token_post);
          return false;
        });

        e.preventDefault();
        return false;
      });

      //signaler post
      $(".sh-post__signaler").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-signalerPost",
              type: "inline"
            }
          ]
        });
        const id_post = $(this).data("post");
        document.form_signaler_post.post.value = id_post;

        const title_post = $(this).data("title");
        $(".sh-signalerPost .namePost").html(title_post);

        e.preventDefault();
        return false;
      });

      /* SUPPRIMER BATTLE*/
      $(".sh-battle__trash").on("click", function () {
        const id_battle = $(this).data("battle");
        const name_battle = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-suppBattle",
              type: "inline"
            }
          ]
        });

        $("#id_battle").val(id_battle);
        $("#battleName").html(name_battle);
        return false;
      });
      $("#suppBattle").on("click", function () {
        const id_battle = $("#id_battle").val();
        console.log(id_battle);

        $.ajax({
          type: "POST",
          url: "/php/suppBattle.php",
          async: false,
          data: "id_battle=" + id_battle,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              $("#battle_" + id_battle).remove();
            }
          }
        });
        return false;
      });
    }
    //fin action();

    //partage BATAILLE
    $("#btn-partage-battle").on("click", function () {
      $("#errorLogPartageBattle").hide();

      const form = $("#form_save_partage_battle").get(0);

      const params = {
        type: "battle",
        id_battle: $(form)
          .find("#id_battle")
          .val(),
        amis: []
      };
      $('#form_save_partage_battle input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartageBattle")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();

        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattle")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          const idBattle = $(form)
            .find("#id_battle")
            .val();

          const nbShares =
            parseInt($("#share_battle_" + idBattle).html()) +
            params.amis.length;
          $("#share_battle_" + idBattle).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });

      return false;
    });
    $("#btn-partage-battle-send").on("click", function () {
      $("#errorLogPartageBattleSend").hide();
      const form = $("#form_save_partage_battle_send").get(0);
      var formData = new FormData(form); // get the form data

      if (
        !$.trim(
          $(form)
            .find("#email1")
            .val().length
        )
      ) {
        $("#errorLogPartageBattleSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattleSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();

            return;
          }

          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $(".div_amis").show();
            $(".div_mail").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });

    $("#cancelSend").on("click", function () {
      $(".div_amis").show();
      $(".div_mail").hide();
      return false;
    });

    //partage CONTENU
    $("#btn-partage-contenu").on("click", function () {
      $("#errorLogPartagePost").hide();

      const form = $("#form_save_partage_contenu").get(0);

      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        amis: []
      };

      $('#form_save_partage_contenu input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartagePost")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartagePost")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          const idPost = $(form)
            .find("#id_post")
            .val();
          const nbShares =
            parseInt($("#share_post_" + idPost).html()) + params.amis.length;
          $("#share_post_" + idPost).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-contenu .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });

    $("#btn-partage-contenu-send").on("click", function () {
      $("#errorLogPartageContenuSend").hide();
      const form = $("#form_save_partage_contenu_send").get(0);
      var formData = new FormData(form); // get the form data

      if (!document.form_save_partage_contenu_send.email1.value) {
        $("#errorLogPartageContenuSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=post",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLogPartageContenuSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          setTimeout(function () {
            // $(".sh-partage-contenu .partage").show();
            $(".sh-partage-contenu .confirm").hide();
            $(".div_amis_contenu").show();
            $(".div_mail_contenu").hide();
            $.magnificPopup.close();
          }, 2000);
        }
      });

      return false;
    });

    $("#cancelSendContenu").on("click", function () {
      $(".div_amis_contenu").show();
      $(".div_mail_contenu").hide();
      return false;
    });

    // Modifier POST
    $("#editPost").on("click", function () {
      const form = $("#form_edit_post").get(0);

      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        name_post: $(form)
          .find("#name_post")
          .val()
      };
      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php",
        data: $.param(params),
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            const idPost = $(form)
              .find("#id_post")
              .val();
            const namePost = $(form)
              .find("#name_post")
              .val();

            $("#post_" + idPost + " h3.title")
              .attr("data-original-title", namePost)
              .html(namePost);

            $("#edit_" + idPost).data("name", namePost);

            $.magnificPopup.close();
          }
        }
      });
      return false;
    });

    // Modifier BATTLE
    $("#editBattle").on("click", function () {
      var form = $("#form_edit_battle").get(0);
      var formData = new FormData(form); // get the form data

      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            $(
              "#battle_" +
              document.form_edit_battle.id_battle.value +
              " a.title"
            ).html(document.form_edit_battle.name_battle.value);
            $("#edit-b_" + document.form_edit_battle.id_battle.value).data(
              "name",
              document.form_edit_battle.name_battle.value
            );
            $("#edit-b_" + document.form_edit_battle.id_battle.value).data(
              "cat",
              document.form_edit_battle.cat_battle.value
            );
            $("#edit-b_" + document.form_edit_battle.id_battle.value).data(
              "info",
              document.form_edit_battle.info_battle.value
            );
            $.magnificPopup.close();
          }
        }
      });
      return false;
    });

    // Signaler POST
    $("#btn-signaler-post").on("click", function () {
      $("#errorLogPostSignaler").hide();

      var form = $("#form_signaler_post").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_post.post.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPostSignaler").show();
              var f = document.getElementById("errorLogPostSignaler");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerPost .signaler").hide();
                $(".sh-signalerPost .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerPost .signaler").show();
                $(".sh-signalerPost .confirm").hide();
                document.form_signaler_post.mes.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPostSignaler")
          .html(
            "<strong class='text-danger'>Merci de laisser un message !</strong>"
          )
          .show();
      }
      return false;
    });

    //formulaire

    //mot de passe
    $(".show-password").click(function () {
      if (
        $(this)
          .prev("input")
          .prop("type") == "text"
      ) {
        //Si c'est un input type password
        $(this)
          .prev("input")
          .prop("type", "text");
        $(this).html('<i class="fa fa-eye-slash"></i>');
      } else {
        //Sinon
        $(this)
          .prev("input")
          .prop("type", "password");
        $(this).html('<i class="fa fa-eye"></i>');
      }
    });
    $("#bt_save_mdp").on("click", function () {
      $("#errorSaveMdp").hide();

      var form = $("#form_save_mdp").get(0);
      var formData = new FormData(form); // get the form data

      if (
        document.form_save_mdp.pass.value != "" &&
        document.form_save_mdp.pass_confirm.value != "" &&
        document.form_save_mdp.pass.value ==
        document.form_save_mdp.pass_confirm.value
      ) {
        $.ajax({
          type: "POST",
          url: "/php/saveProfil.php?type=mdp",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorSaveMdp").show();
              var f = document.getElementById("errorSaveMdp");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                window.location.reload(1);
              }, 500);
            }
          }
        });
      } else {
        if (document.form_save_mdp.pass.value == "") {
          $("#form_save_mdp #pass").css("border", "1px solid #f00");
        }
        if (document.form_save_mdp.pass_confirm.value == "") {
          $("#form_save_mdp  #pass_confirm").css("border", "1px solid #f00");
        }

        $("#errorSaveMdp").show();
        var f = document.getElementById("errorSaveMdp");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>Mots de passe non identiques !</strong>";
      }
      return false;
    });

    //profil
    $("#bt_save_profil").on("click", function () {
      $("#errorSave").hide();

      var form = $("#form_save_profil");

      for (i = 0; i < document.form_save_profil.elements.length; i++) {
        if (document.form_save_profil.elements[i].type == "file") {
          if (document.form_save_profil.elements[i].value == "") {
            document.form_save_profil.elements[i].parentNode.removeChild(
              document.form_save_profil.elements[i]
            );
          }
        }
      }

      var formdata = false;
      if (window.FormData) {
        formdata = new FormData(form[0]);
      }

      if (
        formdata instanceof FormData &&
        navigator.userAgent.match(/version\/11((\.[0-9]*)*)? .*safari/i)
      ) {
        try {
          eval(
            "for (var pair of formdata.entries()) {\
						if (pair[1] instanceof File && pair[1].name === '' && pair[1].size === 0) {\
							formdata.delete(pair[0]);\
						}\
					}"
          );
        } catch (e) { }
      }

      var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      var emailA = document.form_save_profil.email.value;

      if (
        document.form_save_profil.email.value != "" &&
        regex.test(emailA) &&
        document.form_save_profil.naissance.value != "" &&
        document.form_save_profil.ville.value != ""
      ) {
        $.ajax({
          type: "POST",
          url: "/php/saveProfil.php?type=profil",
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            if (data == "non") {
              $("#errorSave").show();
              var f = document.getElementById("errorSave");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else if (data == "ok") {
              setTimeout(function () {
                //window.location.reload(1);
                location.href = urlparametre;
              }, 500);
            }
          }
        });
      } else {
        if (document.form_save_profil.nom.value == "") {
          $("#form_save_profil #nom").css("border", "1px solid #f00");
        }
        if (document.form_save_profil.prenom.value == "") {
          $("#form_save_profil #prenom").css("border", "1px solid #f00");
        }
        if (document.form_save_profil.email.value == "") {
          $("#form_save_profil #email").css("border", "1px solid #f00");
        }

        if (document.form_save_profil.naissance.value == "") {
          $("#form_save_profil #naissance").css("border", "1px solid #f00");
        }
        if (document.form_save_profil.ville.value == "") {
          $("#form_save_profil #ville").css("border", "1px solid #f00");
        }

        $("#errorSave").show();
        var f = document.getElementById("errorSave");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>Merci de renseigner tous les champs.</strong>";
      }
      return false;
    });

    //theme
    $("#bt_save_theme").on("click", function () {
      $("#errorSaveCat").hide();

      var form = $("#form_save_theme").get(0);
      var formData = new FormData(form); // get the form data

      var ch_data = [];

      $('input[type="checkbox"]:checked').each(function () {
        ch_data.push($(this).attr("id"));
      });

      formData.append("catok", ch_data);

      if (ch_data) {
        $.ajax({
          type: "POST",
          url: "/php/saveProfil.php?type=cat",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorSaveCat").show();
              var f = document.getElementById("errorSaveCat");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                //window.location.reload(1);
                location.href = urlparametre;
              }, 500);
            }
          }
        });
      } else {
        $("#errorSaveCat").show();
        var f = document.getElementById("errorSaveCat");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>Merci de sélectionner au moins un thème !</strong>";
      }
      return false;
    });

    //notif
    $("#bt_save_parametre").on("click", function () {
      $("#errorSaveParametre").hide();

      const form = $("#form_save_parametre").get(0);
      const params = {
        type: 'notif',
      };

      $('.chk_alert').each(function () {
        params[$(this).attr('name')] = ($(this).is(':checked')) ? 1 : 5;
      });

      console.log(params);

      $.ajax({
        type: "POST",
        url: "/php/saveProfil.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveParametre").html("<strong class='text-danger'>" + erreur + "</strong>").show();
            return false;
          }
          // setTimeout(function () {
          //   //window.location.reload(1);
          //   location.href = urlparametre;
          // }, 500);

        }
      });

      return false;
    });

    //supp compte
    $(".bt_supp_compte").on("click", function () {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-supp-compte",
            type: "inline"
          }
        ]
      });

      return false;
    });

    //supp compte
    $("#bt_supp_compte").on("click", function () {
      $("#errorSuppCompte").hide();

      var form = $("#form_save_parametre").get(0);
      var formData = new FormData(form); // get the form data

      $.ajax({
        type: "POST",
        url: "/php/saveProfil.php?type=supp",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);
          if (data == "non") {
            $("#errorSuppCompte")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return false;
          }
          $(".sh-supp-compte .form").hide();
          $(".sh-supp-compte .confirm").show();

          setTimeout(function () {
            location.href = "./";
          }, 1500);
        }
      });

      return false;
    });

    //demande d'amis
    $("#btn-friend").on("click", function () {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      if (!user || !friend) {
        return false;
      }

      const params = {
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveDemandeAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          $.magnificPopup.open({
            mainClass: "mfp-with-zoom",
            removalDelay: 300,
            closeMarkup:
              '<i title="%title%" class="mfp-close fa fa-close"></i>',
            items: [
              {
                src: ".sh-reponse",
                type: "inline"
              }
            ]
          });
          setTimeout(function () {
            $.magnificPopup.close();
            window.location.reload(1);
          }, 1500);
        }
      });

      return false;
    });
    $("#btn-yes").on("click", function () {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      console.log(user, friend);

      if (!user || !friend) {
        return false;
      }

      const params = {
        type: "yes",
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat").show();
            var f = document.getElementById("errorSaveCat");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else {
            $.magnificPopup.open({
              mainClass: "mfp-with-zoom",
              removalDelay: 300,
              closeMarkup:
                '<i title="%title%" class="mfp-close fa fa-close"></i>',
              items: [
                {
                  src: ".sh-yes",
                  type: "inline"
                }
              ]
            });

            setTimeout(function () {
              $.magnificPopup.close();
              window.location.reload(1);
            }, 1500);
          }
        }
      });

      return false;
    });
    $("#btn-no").on("click", function () {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      console.log(user, friend);

      if (!user || !friend) {
        return false;
      }

      const params = {
        type: "no",
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat").show();
            var f = document.getElementById("errorSaveCat");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else {
            $.magnificPopup.open({
              mainClass: "mfp-with-zoom",
              removalDelay: 300,
              closeMarkup:
                '<i title="%title%" class="mfp-close fa fa-close"></i>',
              items: [
                {
                  src: ".sh-no",
                  type: "inline"
                }
              ]
            });

            setTimeout(function () {
              $.magnificPopup.close();
              window.location.reload(1);
            }, 1500);
          }
        }
      });

      return false;
    });

    $(".btn-inviter").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-inviter",
            type: "inline"
          }
        ]
      });

      e.preventDefault();
      return false;
    });
    $("#bt-inviter").on("click", function () {
      $("#errorLogInviter").hide();

      var form = $("#form_save_inviter").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_save_inviter.email1.value != "") {
        $.ajax({
          type: "POST",
          url: "/php/saveInviter.php",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogInviter").show();
              var f = document.getElementById("errorLogInviter");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".invitation").hide();
                $(".confirm").show();
              }, 500);
            }
          }
        });
      }
      return false;
    });

    //convertir
    $("#form_convertir #like_coins").on("keyup change click", function (e) {
      var max_coins = document.getElementById("like_coins").max;

      if ($(this).val() > Math.floor(max_coins)) {
        $(this).val(max_coins);
      } else {
        document.form_convertir.likes.value = Math.floor($(this).val() / 10);
      }

      e.preventDefault();
      return false;
    });
    $("#form_convertir #likes").on("keyup change click", function (e) {
      var max_likes = document.getElementById("likes").max;

      if ($(this).val() > Math.floor(max_likes)) {
        $(this).val(max_likes);
      } else {
        document.form_convertir.like_coins.value = Math.floor(
          $(this).val() * 10
        );
      }

      e.preventDefault();
      return false;
    });

    $(".btn-convertir_action").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-convertir",
            type: "inline"
          }
        ]
      });

      e.preventDefault();
      return false;
    });
    $("#bt_convertir").on("click", function () {
      $("#errorConvertir").hide();

      var form = $("#form_convertir").get(0);
      var formData = new FormData(form); // get the form data

      if (
        document.form_convertir.like_coins.value != "0" &&
        document.form_convertir.likes.value != "0"
      ) {
        $.ajax({
          type: "POST",
          url: "/php/saveConvertir.php",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorConvertir")
                .html("<strong class='text-danger>" + erreur + "</strong>")
                .show();
            } else {
              $(".sh-convertir .convertisseur").hide();
              $(".sh-convertir .confirm").show();
              setTimeout(function () {
                window.location.reload();
              }, 2000);
            }
          }
        });
      }
      return false;
    });

    $(".btn-friend_annule").on("click", function () {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      console.log(user, friend);

      if (!user || !friend) {
        return false;
      }

      const params = {
        type: "annuler",
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat").show();
            var f = document.getElementById("errorSaveCat");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else {
            $.magnificPopup.open({
              mainClass: "mfp-with-zoom",
              removalDelay: 300,
              closeMarkup:
                '<i title="%title%" class="mfp-close fa fa-close"></i>',
              items: [
                {
                  src: ".sh-annuler",
                  type: "inline"
                }
              ]
            });

            setTimeout(function () {
              $.magnificPopup.close();
              window.location.reload(1);
            }, 1500);
          }
        }
      });

      return false;
    });

    $(".btn-supp-friend").on("click", function () {
      const user = $(this).data("user");
      const friend = $(this).data("friend");

      console.log(user, friend);

      if (!user || !friend) {
        return false;
      }

      const params = {
        type: "supp",
        user,
        friend
      };
      $.ajax({
        type: "POST",
        url: "/php/saveAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorSaveCat").show();
            var f = document.getElementById("errorSaveCat");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else {
            setTimeout(function () {
              $.magnificPopup.close();
              window.location.reload(1);
            }, 1500);
          }
        }
      });

      return false;
    });
  });
});
