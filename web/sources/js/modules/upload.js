define(["require", "jquery", "plugins/magnificPopup"], function (require) {
  var $ = require("jquery"),
    magnificPopup = require("plugins/magnificPopup");

  if (lang == "en") {
    var battlenom = "battle";
    var post = "post";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var errormail = "Please to fill at least one email!";
  } else {
    var battlenom = "bataille";
    var post = "contenu";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";
    var errormail = "Merci de renseigner au moins un email !";
  }

  $(".btn-post").on("click", function (e) {
    $.magnificPopup.open({
      mainClass: "mfp-with-zoom",
      removalDelay: 300,

      items: [
        {
          src: ".sh-upload",
          type: "inline"
        }
      ]
    });

    e.preventDefault();
    return false;
  });

  $("#bt_publier").on("click", function () {
    $("#errorLogPost").hide();

    var form = $("#form_save_post");

    var battle = document.form_save_post.battle.value;
    var url_battle = document.form_save_post.url_battle.value;

    if (
      document.form_save_post.type.value != "" &&
      ((document.form_save_post.image &&
        document.form_save_post.image.value != "") ||
        (document.form_save_post.lien &&
          document.form_save_post.lien.value != "") ||
        (document.form_save_post.son &&
          document.form_save_post.son.value != "") ||
        (document.form_save_post.text &&
          document.form_save_post.text.value != ""))
    ) {
      for (i = 0; i < document.form_save_post.elements.length; i++) {
        if (document.form_save_post.elements[i].type == "file") {
          if (document.form_save_post.elements[i].value == "") {
            document.form_save_post.elements[i].parentNode.removeChild(
              document.form_save_post.elements[i]
            );
          }
        }
      }

      var formdata = false;
      if (window.FormData) {
        formdata = new FormData(form[0]);
      }

      if (
        formdata instanceof FormData &&
        navigator.userAgent.match(/version\/11((\.[0-9]*)*)? .*safari/i)
      ) {
        try {
          eval(
            "for (var pair of formdata.entries()) {\
						if (pair[1] instanceof File && pair[1].name === '' && pair[1].size === 0) {\
							formdata.delete(pair[0]);\
						}\
					}"
          );
        } catch (e) { }
      }

      $.ajax({
        type: "POST",
        url: "/php/savePost.php",
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLogPost").show();
            var f = document.getElementById("errorLogPost");
            f.innerHTML = "";
            f.innerHTML = "<strong class='text-danger'>" + erreur + "</strong>";
          } else {
            setTimeout(function () {
              location.reload();
            }, 500);
          }
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
            console.log(jqXHR);
            console.log(exception);
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    } else {
      if (document.form_save_post.type.value == "") {
        $("#type").css("border", "1px solid #f00");
      }

      $("#errorLogPost").show();
      var f = document.getElementById("errorLogPost");
      f.innerHTML = "";
      f.innerHTML = "<strong class='text-danger'>" + oublichamps + "</strong>";
    }
    return false;
  });

  $(".btn-battle").on("click", function (e) {
    $.magnificPopup.open({
      mainClass: "mfp-with-zoom",
      removalDelay: 300,

      items: [
        {
          src: ".sh-battle",
          type: "inline"
        }
      ]
    });

    e.preventDefault();
    return false;
  });

  $(".btn-type").on("click", function () {
    var id = $(this).data("id");

    $(".btn-type").removeClass("active");
    $(this).addClass("active");

    if (id == "1") {
      $("#formLien").css("display", "none");
      $("#formText").css("display", "none");
      $("#formImage").css("display", "block");
      $("#formSon").css("display", "none");
    } else if (id == "2") {
      $("#formLien").css("display", "block");
      $("#formText").css("display", "none");
      $("#formImage").css("display", "none");
      $("#formSon").css("display", "none");
    } else if (id == "3") {
      $("#formLien").css("display", "none");
      $("#formText").css("display", "block");
      $("#formImage").css("display", "none");
      $("#formSon").css("display", "none");
    } else if (id == "4") {
      $("#formLien").css("display", "none");
      $("#formText").css("display", "none");
      $("#formImage").css("display", "none");
      $("#formSon").css("display", "block");
    }
    if ($("#form_save_battle").length) {
      document.form_save_battle.type.value = id;
    } else {
      document.form_save_post.type.value = id;
    }
    return false;
  });

  $("#bt_publier_battle").on("click", function () {
    $("#errorLogBattle").hide();
    const form = $("#form_save_battle").get(0);

    if (
      document.form_save_battle.titleB.value != "" &&
      document.form_save_battle.type.value != "" &&
      (document.form_save_battle.cat.value != "0" ||
        document.form_save_battle.cat.value != "0") &&
      ((document.form_save_battle.image &&
        document.form_save_battle.image.value != "") ||
        (document.form_save_battle.lien &&
          document.form_save_battle.lien.value != "") ||
        (document.form_save_battle.son &&
          document.form_save_battle.son.value != "") ||
        (document.form_save_battle.text &&
          document.form_save_battle.text.value != ""))
    ) {
      for (i = 0; i < document.form_save_battle.elements.length; i++) {
        if (document.form_save_battle.elements[i].type == "file") {
          if (document.form_save_battle.elements[i].value == "") {
            document.form_save_battle.elements[i].parentNode.removeChild(
              document.form_save_battle.elements[i]
            );
          }
        }
      }

      var formdata = false;
      if (window.FormData) {
        formdata = new FormData(form);
      }

      if (
        formdata instanceof FormData &&
        navigator.userAgent.match(/version\/11((\.[0-9]*)*)? .*safari/i)
      ) {
        try {
          for (var pair of formdata.entries()) {
            if (pair[1] instanceof File && pair[1].name === '' && pair[1].size === 0) {
              formdata.delete(pair[0]);
            }
          }
        } catch (e) { }
      }

      $.ajax({
        type: "POST",
        url: "/php/saveBattle.php",
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "non") {
            $("#errorLogBattle").html("<strong class='text-danger'>" + erreur + "</strong>").show();
            return false;
          }
          $(".sh-battle .save").hide();
          $(".sh-battle .invite").show();

          document.form_save_battle_invite.id_battle.value = result[0];
          document.form_save_battle_invite.url_battle.value = result[1];
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
            console.log(jqXHR);
            console.log(exception);
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    } else {
      if (document.form_save_battle.cat.value == "0") {
        $("#cat").css("border-color", "red");
      }
      if (document.form_save_battle.titleB.value == "") {
        $("#titleB").css("border-color", "red");
      }

      $("#errorLogBattle").html("<strong class='text-danger'>" + oublichamps + "</strong>").show();
    }
    return false;
  });

  $("#btn-partage-battle-invite").on("click", function () {
    $("#errorLogPartageBattleInvite").hide();
    const form = $("#form_save_battle_invite").get(0);
    const formData = new FormData(form); // get the form data
    const url_battle = document.form_save_battle_invite.url_battle.value;

    if (
      !$.trim(
        $(form)
          .find("#email1")
          .val().length
      )
    ) {
      $("#errorLogPartageBattleInvite")
        .html("<strong class='text-danger'>" + errormail + "</strong>")
        .show();
      return false;
    }
    $.ajax({
      type: "POST",
      url: "/php/saveInviter.php?type=battle",
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      success: function (data) {
        console.log(data);

        if (data == "non") {
          $("#errorLogPartageBattleInvite")
            .html("<strong class='text-danger'>" + erreur + "</strong>")
            .show();

          return;
        }

        $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

        setTimeout(function () {
          document.location.href =
            "/" + lang + "/" + battlenom + "/" + url_battle;
        }, 500);
      }
    });

    return false;
  });

  $("#cancelSendInviteBattle").on("click", function () {
    setTimeout(function () {
      const url_battle = document.form_save_battle_invite.url_battle.value;
      document.location.href = "/" + lang + "/" + battlenom + "/" + url_battle;
    }, 10);
  });
});
