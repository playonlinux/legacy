define(["require", "jquery", "plugins/bootstrap_v3"], function (require, $) {
  if (lang == "en") {
    var chargement = "Loading";
    var lienbattle = "battle";
    var lienpost = "post";
    var nobattle = "No campaign";
    var nopost = "No content";
  } else {
    var chargement = "Chargement en cours";
    var lienbattle = "bataille";
    var lienpost = "contenu";
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
  }

  var currentParams;
  var canLoad;
  var currentTab;

  require([], function () {
    var $ = require("jquery");

    $(".theme-switcher.switcher").on("click", function () {
      $(".theme-switcher.switcher.active").removeClass("active");
      $(this).addClass("active");
      currentTab = $(this).data("id");
      if (currentTab === "theme-defis") {
        $("#theme-defis").show();
        $("#theme-sorts.defis").show();
        $("#theme-posts").hide();
        $("#theme-sorts.posts").hide();

        $("#theme-sorts.defis a")
          .first()
          .click()
          .tooltip("hide");
      } else if (currentTab === "theme-posts") {
        $("#theme-defis").hide();
        $("#theme-sorts.defis").hide();
        $("#theme-posts").show();
        $("#theme-sorts.posts").show();
        $("#theme-sorts.posts a")
          .first()
          .click()
          .tooltip("hide");
      }
    });
    $("#theme-sorts.defis a").on("click", function () {
      $("#theme-sorts.defis a.active")
        .removeClass("active")
        .find("img")
        .removeClass("active");
      $(this)
        .addClass("active")
        .find("img")
        .addClass("active");

      const value = $(this).attr("data-sort-by");
      const orderBy = $(this).attr("data-sort-by");
      console.log($(this));
      // logs
      $.post({
        url: "/php/saveLog.php",
        data: $.param({ value: value }),
        cache: true,
        success: false
      });

      canLoad = true;
      get_battles({ orderBy });
    });
    $("#theme-sorts.posts a").on("click", function () {
      $("#theme-sorts.posts a.active")
        .removeClass("active")
        .find("img")
        .removeClass("active");
      $(this)
        .addClass("active")
        .find("img")
        .addClass("active");
      console.log($(this));
      const value = $(this).attr("data-sort-by");
      const orderBy = $(this).attr("data-sort-by");

      // logs
      $.post({
        url: "/php/saveLog.php",
        data: $.param({ value: value }),
        cache: true,
        success: false
      });

      get_posts({ orderBy });
    });

    // get_battles(undefined);
    $(".theme-switcher.switcher.active").click();

    function request(type, container, params = {}) {
      params.cat = cat;
      params.page = params.page || 1;

      $.post({
        data: $.param(params),
        url: "/php/get_" + type + ".php",
        cache: false,
        async: true,
        beforeSend: function () {
          if (!canLoad) {
            return;
          }
          if (params.page == 1) {
            $("#" + container).html(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          } else {
            $("#" + container).append(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          }
        },
        success: function (data) {
          canLoad = data != "null";
          $(".reload").remove();

          if (data != "null") {
            if (params.page == 1) {
              $("#" + container).html(data);
            } else {
              $("#" + container).append(data);
            }
            actions();
            currentParams = params;
          } else {
            if (params.page == 1) {
              if (container == "theme-defis") {
                $("#" + container).html(
                  '<p class="text-center center-block">' + nobattle + "</p>"
                );
              } else {
                $("#" + container).html(
                  '<p class="text-center center-block">' + nopost + "</p>"
                );
              }
            }
          }
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    }

    function get_battles(params = {}) {
      request("battles", "theme-defis", params);
    }
    function get_posts(params = {}) {
      request("posts", "theme-posts", params);
    }

    function actions() {
      console.log("oui");
      $('[data-toggle="tooltip"]').tooltip();

      $(".likeSave").on("click", function () {
        const idPost = $(this).data("id");
        const params = {
          type: "like",
          id_post: idPost,
          code: lang
        };
        $.ajax({
          type: "POST",
          url: "/php/favoris.php",
          async: false,
          data: $.param(params),
          success: function (result) {
            console.log(result);
            if (result[0] != "non") {
              $("#like_" + idPost)
                .attr("src", "/images/icons/liked-pink.svg")
                .parent()
                .find("span")
                .html(result[0]);

              $("#popup-nblikes").html(result[0]);
              $("#user_likes").html('<i class="fa fa-heart"></i> ' + result[1]);

              $("#user_likeCoins").html(
                '<i class="repu-coins"></i> ' + result[5]
              );
            }

            if (result[1] == "0") {
              console.log("oui");
              $("#like_" + idPost + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + idPost + '[data-toggle="tooltip"]')
                .attr(
                  "data-original-title",
                  "Votre stock de Likes est épuisé. Reconstituez-le en converstissant vos LikeCoins, en postant vos meilleurs contenus et batailles ou en invitant vos amis."
                )
                .tooltip("show");
              $.magnificPopup.open({
                mainClass: "mfp-with-zoom",
                removalDelay: 300,
                closeMarkup:
                  '<i title="%title%" class="mfp-close fa fa-close"></i>',
                items: [
                  {
                    src: ".sh-likemessage",
                    type: "inline"
                  }
                ]
              });
            } else if (result[2] == "non") {
              console.log("oui");
              $("#like_" + idPost + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + idPost + '[data-toggle="tooltip"]')
                .attr(
                  "data-original-title",
                  "Vous avez atteint le nombre maximal de votes pour ce contenu"
                )
                .tooltip("show");
            }
          }
        });

        return false;
      });
      //partage BATAILLE
      $(".btn-partage-battle").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis").show();
          $(".div_mail").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-battle",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-battle .partage").show();
        $("#errorLogPartageBattle").hide();
        const id_battle = $(this).data("battle");
        const url_battle = $(this).data("url");
        const title_battle = $(this).data("title");
        const image_battle = $(this).data("image");
        $("#form_save_partage_battle #id_battle").val(id_battle);

        $(".sh-partage-battle .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_battle +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          "&description=" +
          title_battle +
          "&media=" +
          window.location.origin +
          image_battle +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-battle-email" class="sh-btn-social__send sh-btn-social" data-battle="' +
          id_battle +
          '" data-url="' +
          url_battle +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-battle-email").on("click", function () {
          $(".div_amis").hide();
          $(".div_mail").show();
          var id_battle = $(this).data("battle");
          $("#form_save_partage_battle_send #id_battle").val(id_battle);

          return false;
        });

        e.preventDefault();
        return false;
      });

      //partage CONTENU
      $(".btn-partage-contenu").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis_contenu").show();
          $(".div_mail_contenu").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-contenu",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-contenu .partage").show();

        $("#errorLogPartagePost").hide();
        const id_post = $(this).data("post");
        const title_post = $(this).data("title");
        const token_post = $(this).data("token");
        const image_post = $(this).data("image");
        $("#form_save_partage_contenu #id_post").val(id_post);

        $(".sh-partage-contenu .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_post +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          "&description=" +
          title_post +
          "&media=" +
          window.location.origin +
          image_post +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-contenu-email" class="sh-btn-social__send sh-btn-social" data-token="' +
          token_post +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-contenu-email").on("click", function () {
          $(".div_amis_contenu").hide();
          $(".div_mail_contenu").show();
          var token_battle = $(this).data("token");
          $("#form_save_partage_contenu_send #token_post").val(token_post);
          return false;
        });

        e.preventDefault();
        return false;
      });

      $(".sh-login__btn-signup, .btn-upload_btn-signup").on("click", function (
        e
      ) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-login",
              type: "inline"
            }
          ]
        });

        e.preventDefault();
        return false;
      });

      //signaler post
      $(".sh-post__signaler").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-signalerPost",
              type: "inline"
            }
          ]
        });
        var id_post = $(this).data("post");
        document.form_signaler_post.post.value = id_post;

        var title_post = $(this).data("title");
        $(".sh-signalerPost .namePost").html(title_post);

        e.preventDefault();
        return false;
      });

      /* SUPPRIMER POST*/
      $(".sh-post__trash").on("click", function () {
        const idPost = $(this).data("post");
        const namePost = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-supp",
              type: "inline"
            }
          ]
        });

        $("#id_post").val(idPost);
        $("#postName").html(namePost);

        return false;
      });
      $("#suppPost").on("click", function () {
        const id_post = $("#id_post").val();
        console.log(id_post);

        $.ajax({
          type: "POST",
          url: "/php/suppPost.php",
          async: false,
          data: "id_post=" + id_post,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              $("#post_" + id_post).remove();
            }
          }
        });
        return false;
      });

      //POPUP POST
      $(".btn-popup-post").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-popup-post",
              type: "inline"
            }
          ]
        });

        const idPost = $(this).data("id");
        const token = $(this).data("token");
        const ordre = $(this).data("ordre");

        $.ajax({
          type: "POST",
          url: "/popup_post.php",
          data: "token=" + token + "&code=" + lang + "&ordre=" + ordre,
          async: false,
          success: function (result) {
            $("#contenu_post").html(result);

            const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
            $("#view_post_" + idPost).html(nbViews);
          }
        });

        $('[data-toggle="tooltip"]').tooltip();
        actions();

        return false;
      });

      //POPUP SUIVANT
      $(".btn-popup-post_suivant").on("click", function (e) {
        const idPost = $(this).data("id");
        const params = {
          code: lang,
          token: $(this).data("token"),
          ordre: $(this).data("ordre")
        };

        $.ajax({
          type: "POST",
          url: "/popup_post.php",
          data: $.param(params),
          async: false,
          success: function (result) {
            $("#contenu_post").html(result);
            const nbViews = parseInt($("#view_post_" + idPost).html()) + 1;
            $("#view_post_" + idPost).html(nbViews);
          }
        });

        $('[data-toggle="tooltip"]').tooltip();
        actions();

        return false;
      });

      /* MODIFIER POST*/
      $(".editPost").on("click", function (e) {
        const id_post = $(this).data("id");
        const name_post = $(this).data("name");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-editPost",
              type: "inline"
            }
          ]
        });
        const form = $("#form_edit_post").get(0);

        $(form)
          .find("#id_post")
          .val(id_post);
        $(form)
          .find("#name_post")
          .val(name_post);

        e.preventDefault();
        return false;
      });

      // Modifier POST
      $("#editPost").on("click", function () {
        const form = $("#form_edit_post").get(0);
        const formData = new FormData(form); // get the form data

        const params = {
          type: "post",
          id_post: $(form)
            .find("#id_post")
            .val(),
          name_post: $(form)
            .find("#name_post")
            .val()
        };
        $.ajax({
          type: "POST",
          url: "/php/edit_postBattle.php",
          data: $.param(params),
          async: false,
          cache: false,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              const idPost = $(form)
                .find("#id_post")
                .val();
              const namePost = $(form)
                .find("#name_post")
                .val();

              $("#post_" + idPost + " h3.title")
                .attr("data-original-title", namePost)
                .html(namePost);

              $("#edit_" + idPost).data("name", namePost);

              $.magnificPopup.close();
            }
          }
        });
        return false;
      });
      /* SUPPRIMER BATTLE*/
      $(".sh-battle__trash").on("click", function () {
        const id_battle = $(this).data("battle");
        const name_battle = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-suppBattle",
              type: "inline"
            }
          ]
        });

        $("#id_battle").val(id_battle);
        $("#battleName").html(name_battle);
        return false;
      });
      $("#suppBattle").on("click", function () {
        const id_battle = $("#id_battle").val();
        console.log(id_battle);

        $.ajax({
          type: "POST",
          url: "/php/suppBattle.php",
          async: false,
          data: "id_battle=" + id_battle,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              $("#battle_" + id_battle).remove();
            }
          }
        });
        return false;
      });
    }

    $(window).scroll(function () {
      if (!canLoad) {
        return false;
      }
      if (currentTab !== "theme-defis") {
        return false;
      }
      const scrollHeight = Math.floor($(document).height() / 10);
      const scrollPosition = Math.floor(
        ($(window).height() + $(window).scrollTop()) / 10
      );

      if (scrollHeight - scrollPosition === 0) {
        if (currentParams && currentParams.page) {
          currentParams.page = currentParams.page + 1;
          // TODO: battles or posts
          get_battles(currentParams);
        }
      }
    });
    //fin action();

    //partage BATAILLE
    $("#btn-partage-battle").on("click", function () {
      $("#errorLogPartageBattle").hide();

      const form = $("#form_save_partage_battle").get(0);

      const params = {
        type: "battle",
        id_battle: $(form)
          .find("#id_battle")
          .val(),
        amis: []
      };
      $('#form_save_partage_battle input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartageBattle")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();

        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattle")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          const idBattle = $(form)
            .find("#id_battle")
            .val();

          const nbShares =
            parseInt($("#share_battle_" + idBattle).html()) +
            params.amis.length;
          $("#share_battle_" + idBattle).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });

      return false;
    });

    $("#btn-partage-battle-send").on("click", function () {
      $("#errorLogPartageBattleSend").hide();
      const form = $("#form_save_partage_battle_send").get(0);
      var formData = new FormData(form); // get the form data

      if (
        !$.trim(
          $(form)
            .find("#email1")
            .val().length
        )
      ) {
        $("#errorLogPartageBattleSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattleSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();

            return;
          }

          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $(".div_amis").show();
            $(".div_mail").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });
    $("#cancelSend").on("click", function () {
      $(".div_amis").show();
      $(".div_mail").hide();
      return false;
    });

    //partage CONTENU
    $("#btn-partage-contenu").on("click", function () {
      $("#errorLogPartagePost").hide();

      const form = $("#form_save_partage_contenu").get(0);

      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        amis: []
      };

      $('#form_save_partage_contenu input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartagePost")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartagePost")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          const idPost = $(form)
            .find("#id_post")
            .val();

          const nbShares =
            parseInt($("#share_post_" + idPost).html()) + params.amis.length;
          $("#share_post_" + idPost).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-contenu .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });
    $("#btn-partage-contenu-send").on("click", function () {
      $("#errorLogPartageContenuSend").hide();
      const form = $("#form_save_partage_contenu_send").get(0);
      var formData = new FormData(form); // get the form data

      if (!document.form_save_partage_contenu_send.email1.value) {
        $("#errorLogPartageContenuSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=post",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLogPartageContenuSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          setTimeout(function () {
            // $(".sh-partage-contenu .partage").show();
            $(".sh-partage-contenu .confirm").hide();
            $(".div_amis_contenu").show();
            $(".div_mail_contenu").hide();
            $.magnificPopup.close();
          }, 2000);
        }
      });

      return false;
    });

    $("#cancelSendContenu").on("click", function () {
      $(".div_amis_contenu").show();
      $(".div_mail_contenu").hide();
      return false;
    });

    $("#btn-signaler-post").on("click", function () {
      $("#errorLogPostSignaler").hide();

      var form = $("#form_signaler_post").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_post.post.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPostSignaler").show();
              var f = document.getElementById("errorLogPostSignaler");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>Une erreur est survenue.</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerPost .signaler").hide();
                $(".sh-signalerPost .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerPost .signaler").show();
                $(".sh-signalerPost .confirm").hide();
                document.form_signaler_post.mes.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPostSignaler").show();
        var f = document.getElementById("errorLogPostSignaler");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>Merci de laisser un message !</strong>";
      }
      return false;
    });
  });
});
