define([
  "require",
  "jquery",
  // "plugins/isotope",
  "plugins/bootstrap_v3",
  "plugins/wow"
], function (require, $ /*, Isotope*/) {
  //lang
  if (lang == "en") {
    var nobattle = "No campaign";
    var nopost = "No content";
    var erreur = "An error occurred.";
    var oublichamps = "Thank you fill in all fields.";
    var compteexistant = "An account already exists, please login";

    var plusdelike =
      "Your stock of LIKES is sold out. Rebuild it by converting your LIKECOINS, posting your best contents and campaigns, or inviting your friends.";
    var maxvotecontenu =
      "You have reached the maximum number of votes for this content";
    var selectami = "Please select at least one friend!";
    var dejainscrit = "Already registered:";
    var errormail = "Please to fill at least one email!";
    var errormessage = "Please leave a message !";
    var chargement = "Loading";

    var lienbattle = "battle";
    var lienpost = "post";
  } else {
    var nobattle = "Aucune bataille";
    var nopost = "Aucun contenu";
    var erreur = "Une erreur est survenue.";
    var oublichamps = "Merci de renseigner tous les champs.";

    var plusdelike =
      "Votre stock de LIKES est épuisé. Reconstituez-le en convertissant vos LIKECOINS, en postant vos meilleurs contenus et batailles ou en invitant vos amis.";
    var maxvotecontenu =
      "Vous avez atteint le nombre maximal de votes pour ce contenu";
    var selectami = "Merci de sélectionner au moins un amis !";
    var dejainscrit = "Déjà inscrits :";
    var errormail = "Merci de renseigner au moins un email !";
    var errormessage = "Merci de laisser un message !";
    var chargement = "Chargement en cours";

    var lienbattle = "bataille";
    var lienpost = "contenu";
  }

  var currentParams;
  var canLoad;
  require(["plugins/wow"], function (WOW) {
    new WOW().init();
    var $ = require("jquery");

    $(".switcher").on("click change", function () {
      let context;
      if ($(this).hasClass("visible-xs")) {
        context = $(this).find("option:selected");
        $(context).removeClass("active");
      } else {
        context = this;
        $("#sorts a.active").removeClass("active");
      }

      $(context).addClass("active");

      let value;
      const params = {};
      if ($(context).hasClass("filter-button")) {
        value = $(context).attr("data-filter");
        params.filterBy = $(context).attr("data-filter");
      } else if ($(context).hasClass("sort-button")) {
        value = $(context).attr("data-sort-by");
        params.orderBy = $(context).attr("data-sort-by");
      }

      // logs
      $.post({
        url: "/php/saveLog.php",
        data: $.param({ value }),
        cache: true,
        success: false
      });

      canLoad = true;
      get_battles(params);
    });

    $("#sorts a.active").click();

    function get_battles(params = {}) {
      params.code = lang;
      params.page = params.page || 1;

      $.post({
        data: $.param(params),
        url: "/php/get_battles.php",
        cache: false,
        async: true,
        beforeSend: function () {
          if (!canLoad) {
            return;
          }
          if (params.page == 1) {
            $("#defis").html(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          } else {
            $("#defis").append(
              '<div class="reload center-block mb-5"><i class="fa fa-spinner fa-spin fa-3x fa-fw center-block"></i>' +
              '<span class="center-block text-center">' +
              chargement +
              "</span></div>"
            );
          }
        },
        success: function (data) {
          canLoad = data != "null";
          $(".reload").remove();
          if (data != "null") {
            if (params.page == 1) {
              $("#defis").html(data);
            } else {
              $("#defis").append(data);
            }
            actions();
            currentParams = params;
          } else {
            if (params.page == 1) {
              $("#defis").html(
                '<p class="text-center center-block">' + nobattle + "</p>"
              );
            }
          }
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });
    }

    //debut action();
    function actions() {
      $('[data-toggle="tooltip"]').tooltip();

      //FONCTION FAVORIS
      $(".likeSave").on("click", function () {
        var id_post = $(this).data("id");
        $.ajax({
          type: "POST",
          url: "/php/favoris.php",
          async: false,
          data: "type=like&id_post=" + id_post + "&code=" + lang,
          success: function (result) {
            if (result[0] != "non") {
              var f = document.getElementById("like_" + id_post);
              f.innerHTML = "";
              f.innerHTML =
                '<img src="/images/icons/liked-pink.svg" class="icons"><span>' +
                result[0] +
                "</span>";

              var f = document.getElementById("user_likes");
              f.innerHTML = "";
              f.innerHTML = '<i class="fa fa-heart"></i> ' + result[1];

              var f = document.getElementById("user_likeCoins");
              f.innerHTML = "";
              f.innerHTML = '<i class="repu-coins"></i> ' + result[5];
            }

            if (result[1] == "0") {
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", plusdelike)
                .tooltip("show");
              $.magnificPopup.open({
                mainClass: "mfp-with-zoom",
                removalDelay: 300,
                closeMarkup:
                  '<i title="%title%" class="mfp-close fa fa-close"></i>',
                items: [
                  {
                    src: ".sh-likemessage",
                    type: "inline"
                  }
                ]
              });
            } else if (result[2] == "non") {
              $("#like_" + id_post + '[data-toggle="tooltip"]').tooltip("hide");
              $("#like_" + id_post + '[data-toggle="tooltip"]')
                .attr("data-original-title", maxvotecontenu)
                .tooltip("show");
            }
          }
        });

        return false;
      });

      //partage BATAILLE
      $(".btn-partage-battle").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis").show();
          $(".div_mail").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-battle",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-battle .partage").show();
        $("#errorLogPartageBattle").hide();
        const id_battle = $(this).data("battle");
        const url_battle = $(this).data("url");
        const title_battle = $(this).data("title");
        const image_battle = $(this).data("image");
        $("#form_save_partage_battle #id_battle").val(id_battle);

        $(".sh-partage-battle .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_battle +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienbattle +
          "/" +
          url_battle +
          "&description=" +
          title_battle +
          "&media=" +
          window.location.origin +
          image_battle +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-battle-email" class="sh-btn-social__send sh-btn-social" data-battle="' +
          id_battle +
          '" data-url="' +
          url_battle +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-battle-email").on("click", function () {
          $(".div_amis").hide();
          $(".div_mail").show();
          var id_battle = $(this).data("battle");
          $("#form_save_partage_battle_send #id_battle").val(id_battle);
          return false;
        });

        $(".sh-btn-social").on("click", function () {
          var value = $(this).data("id");
          $.ajax({
            type: "POST",
            url: "/php/saveLog.php",
            data:
              "value=partage&val=" + encodeURI(value + " / " + title_battle),
            cache: true,
            success: function (data) { }
          });
        });

        e.preventDefault();
        return false;
      });

      //partage CONTENU
      $(".btn-partage-contenu").on("click", function (e) {
        $.magnificPopup.instance.close = function () {
          $(".div_amis_contenu").show();
          $(".div_mail_contenu").hide();
          $.magnificPopup.proto.close.call(this);
        };
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-partage-contenu",
              type: "inline"
            }
          ]
        });
        $(".sh-partage-contenu .partage").show();
        $("#errorLogPartagePost").hide();
        const id_post = $(this).data("post");
        const title_post = $(this).data("title");
        const token_post = $(this).data("token");
        const image_post = $(this).data("image");
        $("#form_save_partage_contenu #id_post").val(id_post);

        $(".sh-partage-contenu .sh-login__social").html(
          '<a target="_blank" data-id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__facebook sh-btn-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="twitter" href="http://twitter.com/home?status=' +
          title_post +
          " - " +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          '" class="sh-btn-social__twitter sh-btn-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>' +
          '<a target="_blank" data-id="pinterest" href="http://pinterest.com/pin/create/button/?url=' +
          window.location.origin +
          "/" +
          lang +
          "/" +
          lienpost +
          "/" +
          token_post +
          "&description=" +
          title_post +
          "&media=" +
          window.location.origin +
          image_post +
          '" class="sh-btn-social__pinterest sh-btn-social"><i class="fa fa-pinterest" aria-hidden="true"></i></a>' +
          '<a href="#" data-id="email" id="btn-partage-contenu-email" class="sh-btn-social__send sh-btn-social" data-token="' +
          token_post +
          '" ><i class="fa fa-send" aria-hidden="true"></i></a>'
        );

        $("#btn-partage-contenu-email").on("click", function () {
          $(".div_amis_contenu").hide();
          $(".div_mail_contenu").show();
          var token_battle = $(this).data("token");
          $("#form_save_partage_contenu_send #token_post").val(token_post);
          return false;
        });

        $(".sh-btn-social").on("click", function () {
          var value = $(this).data("id");
          $.ajax({
            type: "POST",
            url: "/php/saveLog.php",
            data:
              "value=partContenu&val=" + encodeURI(value + " / " + title_post),
            cache: true,
            success: function (data) { }
          });
        });

        e.preventDefault();
        return false;
      });

      //inscription
      $(".sh-login__btn-signup, .btn-upload_btn-signup").on("click", function (
        e
      ) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-login",
              type: "inline"
            }
          ]
        });

        e.preventDefault();
        return false;
      });

      //signaler post
      $(".sh-post__signaler").on("click", function (e) {
        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-signalerPost",
              type: "inline"
            }
          ]
        });
        var id_post = $(this).data("post");
        document.form_signaler_post.post.value = id_post;

        var title_post = $(this).data("title");
        $(".sh-signalerPost .namePost").html(title_post);

        e.preventDefault();
        return false;
      });

      /* SUPPRIMER BATTLE*/
      $(".sh-battle__trash").on("click", function () {
        const id_battle = $(this).data("battle");
        const name_battle = $(this).data("title");

        $.magnificPopup.open({
          mainClass: "mfp-with-zoom",
          removalDelay: 300,

          items: [
            {
              src: ".sh-suppBattle",
              type: "inline"
            }
          ]
        });

        $("#id_battle").val(id_battle);
        $("#battleName").html(name_battle);
        return false;
      });
      $("#suppBattle").on("click", function () {
        const id_battle = $("#id_battle").val();
        console.log(id_battle);

        $.ajax({
          type: "POST",
          url: "/php/suppBattle.php",
          async: false,
          data: "id_battle=" + id_battle,
          success: function (result) {
            console.log(result);
            if (result == "oui") {
              $.magnificPopup.close();
              $("#battle_" + id_battle).remove();
            }
          }
        });
        return false;
      });
    }

    $(window).scroll(function () {
      if (!canLoad) {
        return false;
      }
      const scrollHeight = Math.floor($(document).height() / 10);
      const scrollPosition = Math.floor(
        ($(window).height() + $(window).scrollTop()) / 10
      );

      if (scrollHeight - scrollPosition === 0) {
        if (currentParams && currentParams.page) {
          currentParams.page = currentParams.page + 1;
          // TODO: battles or posts
          get_battles(currentParams);
        }
      }
    });
    //fin action();

    //partage BATAILLE
    $("#btn-partage-battle").on("click", function () {
      $("#errorLogPartageBattle").hide();

      const form = $("#form_save_partage_battle").get(0);
      const params = {
        type: "battle",
        id_battle: $(form)
          .find("#id_battle")
          .val(),
        amis: []
      };
      $('#form_save_partage_battle input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartageBattle")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();

        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattle")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          const idBattle = $(form)
            .find("#id_battle")
            .val();

          const nbShares =
            parseInt($("#share_battle_" + idBattle).html()) +
            params.amis.length;
          $("#share_battle_" + idBattle).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });

      return false;
    });

    $("#btn-partage-battle-send").on("click", function () {
      $("#errorLogPartageBattleSend").hide();
      const form = $("#form_save_partage_battle_send").get(0);
      var formData = new FormData(form); // get the form data

      if (
        !$.trim(
          $(form)
            .find("#email1")
            .val().length
        )
      ) {
        $("#errorLogPartageBattleSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }

      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartageBattleSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();

            return;
          }

          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-battle .partage").hide();
            $(".sh-partage-battle .confirm").show();
          }, 500);

          setTimeout(function () {
            $(".sh-partage-battle .confirm").hide();
            $(".div_amis").show();
            $(".div_mail").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        }
      });

      return false;
    });
    $("#cancelSend").on("click", function () {
      $(".div_amis").show();
      $(".div_mail").hide();
      return false;
    });

    //partage CONTENU
    $("#btn-partage-contenu").on("click", function () {
      $("#errorLogPartagePost").hide();

      const form = $("#form_save_partage_contenu").get(0);

      const params = {
        type: "post",
        id_post: $(form)
          .find("#id_post")
          .val(),
        amis: []
      };

      $('#form_save_partage_contenu input[type="checkbox"]:checked').each(
        function () {
          params.amis.push($(this).attr("id"));
        }
      );

      if (!params.amis.length) {
        $("#errorLogPartagePost")
          .html("<strong class='text-danger'>" + selectami + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/partageAmis.php",
        data: $.param(params),
        async: false,
        cache: false,
        success: function (data) {
          console.log(data);

          if (data == "non") {
            $("#errorLogPartagePost")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          const idPost = $(form)
            .find("#id_post")
            .val();

          const nbShares =
            parseInt($("#share_post_" + idPost).html()) + params.amis.length;
          $("#share_post_" + idPost).html(nbShares);

          setTimeout(function () {
            $(".sh-partage-contenu .confirm").hide();
            $.magnificPopup.close();
            form.reset();
          }, 2000);
        },
        error: function (jqXHR, exception) {
          if (jqXHR.status === 0) {
            console.log("Not connect.\n Verify Network.");
          } else if (jqXHR.status == 404) {
            console.log("Requested page not found. [404]");
          } else if (jqXHR.status == 500) {
            console.log("Internal Server Error [500].");
          } else if (exception === "parsererror") {
            console.log(jqXHR.responseText);
          } else if (exception === "timeout") {
            console.log("Time out error.");
          } else if (exception === "abort") {
            console.log("Ajax request aborted.");
          } else {
            console.log("Uncaught Error.\n" + jqXHR.responseText);
          }
        }
      });

      return false;
    });
    $("#btn-partage-contenu-send").on("click", function () {
      $("#errorLogPartageContenuSend").hide();
      const form = $("#form_save_partage_contenu_send").get(0);
      var formData = new FormData(form); // get the form data

      if (!document.form_save_partage_contenu_send.email1.value) {
        $("#errorLogPartageContenuSend")
          .html("<strong class='text-danger'>" + errormail + "</strong>")
          .show();
        return false;
      }
      $.ajax({
        type: "POST",
        url: "/php/saveInviter.php?type=post",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
          if (data == "non") {
            $("#errorLogPartageContenuSend")
              .html("<strong class='text-danger'>" + erreur + "</strong>")
              .show();
            return;
          }
          $("#user_likeCoins").html('<i class="repu-coins"></i> ' + data[2]);

          setTimeout(function () {
            $(".sh-partage-contenu .partage").hide();
            $(".sh-partage-contenu .confirm").show();
          }, 500);

          setTimeout(function () {
            // $(".sh-partage-contenu .partage").show();
            $(".sh-partage-contenu .confirm").hide();
            $(".div_amis_contenu").show();
            $(".div_mail_contenu").hide();
            $.magnificPopup.close();
          }, 2000);
        }
      });

      return false;
    });

    $("#cancelSendContenu").on("click", function () {
      $(".div_amis_contenu").show();
      $(".div_mail_contenu").hide();
      return false;
    });

    //signaler CONTENU
    $("#btn-signaler-post").on("click", function () {
      $("#errorLogPostSignaler").hide();

      var form = $("#form_signaler_post").get(0);
      var formData = new FormData(form); // get the form data

      if (document.form_signaler_post.post.value != "0") {
        $.ajax({
          type: "POST",
          url: "/php/signaler.php?type=post",
          data: formData,
          processData: false,
          contentType: false,
          cache: false,
          success: function (data) {
            console.log(data);

            if (data == "non") {
              $("#errorLogPostSignaler").show();
              var f = document.getElementById("errorLogPostSignaler");
              f.innerHTML = "";
              f.innerHTML =
                "<strong class='text-danger'>" + erreur + "</strong>";
            } else {
              setTimeout(function () {
                $(".sh-signalerPost .signaler").hide();
                $(".sh-signalerPost .confirm").show();
              }, 100);

              setTimeout(function () {
                $(".sh-signalerPost .signaler").show();
                $(".sh-signalerPost .confirm").hide();
                document.form_signaler_post.mes.value = "";
                $.magnificPopup.close();
              }, 2000);
            }
          }
        });
      } else {
        $("#errorLogPostSignaler").show();
        var f = document.getElementById("errorLogPostSignaler");
        f.innerHTML = "";
        f.innerHTML =
          "<strong class='text-danger'>" + errormessage + "</strong>";
      }
      return false;
    });
    // Modifier BATAILLE
    $(".editBattle").on("click", function (e) {
      $.magnificPopup.open({
        mainClass: "mfp-with-zoom",
        removalDelay: 300,

        items: [
          {
            src: ".sh-editBattle",
            type: "inline"
          }
        ]
      });

      e.preventDefault();
      return false;
    });
    $("#editBattle").on("click", function () {
      var form = $("#form_edit_battle").get(0);
      var formData = new FormData(form); // get the form data

      $.ajax({
        type: "POST",
        url: "/php/edit_postBattle.php?type=battle",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
          console.log(result);
          if (result == "oui") {
            setTimeout(function () {
              location.reload();
            }, 500);
          }
        }
      });
      return false;
    });
  });
});
