<?php
session_start();

require_once 'src/Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '2865617423500592', // Replace {app-id} with your app id
  'app_secret' => 'b57a3a867a7c21f8d809a91f4db930a7',
  'default_graph_version' => 'v4.0',
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['public_profile', 'email', 'user_birthday']; // Optional permissions
$loginUrl = $helper->getLoginUrl('callback.php', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';



?>
V2