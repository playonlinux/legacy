<?php
session_start();

require_once 'Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '2865617423500592', // Replace {app-id} with your app id
  'app_secret' => 'b57a3a867a7c21f8d809a91f4db930a7',
  'default_graph_version' => 'v4.0',
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email', 'user_birthday', 'user_gender', 'user_age_range']; // Optional permissions
// ?
$loginUrl = $helper->getLoginUrl((!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/js/Facebook/callback.php', $permissions);

echo '<br>  <a class="btn btn-block facebookbtn"  href="' . htmlspecialchars($loginUrl) . '"><span class="fa fa-facebook" style="padding-right: 30px;
"></span>' . $_['login_facebook'] . ' </a>';


?>
<style>
  .facebookbtn {
    background-color: #4267B2;
    color: white !important;
  }
</style>