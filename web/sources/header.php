<?php if (isset($_GET['demande']) && $_GET['demande'] != '' && !isset($_SESSION['securite'])) { ?>
    <script>
        demande = 1;
    </script>
<?php } else { ?>
    <script>
        demande = 0;
    </script>
<?php } ?>

<script>
    var lang = '<?= $code ?>';
    var url_bienvenue = '<?= $_["url_bienvenue"] ?>';
    var url_search = '<?= $_["url_search"] ?>';
</script>


<script>
    require(['app'], function() {
        require(['modules/main']);
        require(['modules/signup']);
    });

    function onFacebookLogin() {
        FB.getLoginStatus(function(loginStatus) {
            console.log(loginStatus);
            console.log(FB);
            if (!loginStatus.authResponse || loginStatus.status != 'connected') {
                console.log('kfghdf;kghfg');
                return false;
            }
            FB.api('/me', {
                fields: 'last_name,email,first_name,gender,birthday,age_range,picture',
                access_token: loginStatus.authResponse.accessToken
            }, function(response) {
                console.log(response);

                // if (response.age_range.min <= 13) {
                //     alert('Too young');
                //     return false;
                // }

                const params = {
                    access_token: loginStatus.authResponse.accessToken,
                    ...response,
                    picture: response.picture.data.url
                }
                $.ajax({
                    type: "POST",
                    url: "/php/facebookConnect.php",
                    data: $.param(params),
                    async: false,
                    cache: false,
                    success: function(data) {
                        if (data) {
                            data = JSON.parse(data);
                            console.log(data);
                            if (data.isFirstTime) {
                                window.location.href = '/post_first.php?code=' + data.code + '&id=' + data.idUser +
                                    '&token=' + data.token +
                                    '&redirect_uri=' + window.location.pathname;
                                return false;
                            }
                        }
                        window.location.reload();
                        return false;
                    },
                    error: function(jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            console.log("Not connect.\n Verify Network.");
                        } else if (jqXHR.status == 404) {
                            console.log("Requested page not found. [404]");
                        } else if (jqXHR.status == 500) {
                            console.log("Internal Server Error [500].");
                        } else if (exception === "parsererror") {
                            console.log(jqXHR.responseText);
                        } else if (exception === "timeout") {
                            console.log("Time out error.");
                        } else if (exception === "abort") {
                            console.log("Ajax request aborted.");
                        } else {
                            console.log("Uncaught Error.\n" + jqXHR.responseText);
                        }
                    }
                });
            });
        });
    }
</script>
<?php
$select_themes = $dbh->prepare("SELECT `id`, `name_" . $code . "` as name, `visuel`, `statut`, `url_" . $code . "` as url 
FROM `bl_categories` 
WHERE `statut` ='1' ORDER BY name_" . $code . " ASC");
$select_themes->execute();
$catBattle = array();

if ($select_themes->rowCount() > 0) {
    $nbCatProjet = $select_themes->rowCount();
    $i = 0;
    while ($row_theme = $select_themes->fetch(PDO::FETCH_OBJ)) {
        $catBattle[$i]['id'] = $row_theme->id;
        $catBattle[$i]['url'] = $row_theme->url;
        if ($row_theme->visuel != '') {
            $catBattle[$i]['visuel'] = $row_theme->visuel;
        } else {
            $catBattle[$i]['visuel'] = 'images/logo-republilke.png';
        }
        $catBattle[$i]['title'] = $row_theme->name;
        $i++;
    }
}

?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=UA-125774665-1" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- HEADER -->
<header>
    <div class="sh-header">
        <?php if (!empty($_GET['token'])) { ?>
            <div class="container-fluid" style="justify-content:flex-end">
                <span id="errorSaveConfirmTheme" class="mb-3"></span>
                &nbsp;
                <a href="javascript:void(0)" class="sh-btn big" id="bt_confirm_theme" style="color:#000000;">
                    <?= $_['play']; ?>
                </a>
            <?php } else { ?>
                <div class="container-fluid row">
                    <div class="col-lg-2 col-sm-3 col-2">
                        <a href="/<?= $code ?>/<?= $_['url_bienvenue'] ?>" class="sh-logo">
                            <img src="/images/logo-republilke.png" style="width:42px; height:42px" alt="REPUBLIKE">
                            <img src="/images/icons/beta.svg" style="display:inline-flex; width:60px; height:60px" alt="REPUBLIKE">
                        </a>

                    </div>
                    <script>
                        require(['app'], function() {
                            require(['modules/menu']);
                        });
                    </script>

                    <div class="col-lg-4 col-sm-8 sh-header__search">

                        <div class="dropdown callout">
                            <label>
                                <div>
                                    <input id="mainKeywords" type="text" value="<?= $keywords ?>" class="form-control" spellcheck="true" autocorrect="on" autocomplete="off" placeholder="<?= $_['search'] ?>">

                                </div>
                                <input type="hidden" name="code" class="form-control" value="<?= $code ?>">
                                <ul id="simple-criteria" class="dropdown-menu">
                                    <?php foreach ($catBattle as $category) { ?>
                                        <li class="mb-1">
                                            <a data-name="<?= $category['title']; ?>" data-key="<?= $category['id']; ?>" href="/<?= $code ?>/<?= $_['url_theme'] ?>/<?= $category['url'] ?>" class="simple-category-option">
                                                <img src="/<?= $category['visuel']; ?>" alt="<?= $category['title'] ?>"> <?= $category['title'] ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </label>
                        </div>

                    </div>
                    <div class="col-xl-5 sh-header__control">
                        <a href="/<?= $code ?>/<?= $_['url_condition_1'] ?>" data-toggle="tooltip" data-placement="bottom" style="color: #FFFFFF;" class="hidden-xs">
                            <?= $_['menu_condition_1'] ?>
                        </a>
                        <?php if (!empty($_SESSION['securite'])) { ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['menu_inviter'] ?>" class="btn-inviter-menu sh-btn-icon">
                                <img class="icons" src="/images/icons/friends.svg">
                            </a>
                        <?php } ?>
                        <a href="/<?= $code ?>/hall-of-fame" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['menu_hall'] ?>" class="sh-header__btn-trophy sh-btn-icon <?php if ($page == "hall") {
                                                                                                                                                                                                            echo "active";
                                                                                                                                                                                                        } ?>">
                            <img class="icons" src="/images/icons/laurel.svg">
                        </a>
                        <?php if (!empty($_SESSION['securite'])) { ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['menu_notif'] ?>" class="sh-header__btn-notification sh-btn-icon">
                                <img class="icons" src="/images/icons/thunder.svg">
                                <span class="notif label-danger count"></span>
                            </a>
                            <a href="/<?= $code ?>/<?= $_['url_compte'] ?>" class="sh-user__head" style="border:0;">
                                <img src="/<?= $_SESSION['photo']; ?>" style="width: 50px;height: 50px;border-radius: 50%;margin-right: 10px;">
                                <div style="float:right;color:#FFFFFF">
                                    <div><?php echo $_SESSION['prenom']; ?></div>
                                    <div>
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="LIKES" id="user_likes" class="likes text-left"><i class="fa fa-heart"></i> <?= $_SESSION['likes']; ?></span>
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="LIKESCOINS" id="user_likeCoins" class="likes text-left"><i class="repu-coins"></i> <?= $_SESSION['likeCoins']; ?></span>
                                    </div>
                                </div>
                            </a>
                            <a href="javascript:void(0)" class="sh-header__cgv sh-btn-icon"><i class="fa fa-2x fa-bars"></i></a>

                        <?php } else { ?>
                            <a href="javascript:void(0)" class="sh-login__btn-signup sh-btn big"><?= $_['bt_connecter'] ?></a>
                            <a href="javascript:void(0)" class="sh-header__cgv sh-btn-icon"><i class="fa fa-2x fa-bars"></i></a>
                            <div class="sh-login">
                                <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                                <div class="sh-login__content save" style="display:none;">
                                    <p class="text-center"><b><?= $_['bt_creer_compte'] ?></b></p>
                                    <div class="sh-login__form">
                                        <form method="post" action="" name="form_save" id="form_save" enctype="multipart/form-data" autocomplete="off">
                                            <input type="text" class="form-control2" placeholder='<?= $_['compte_prenom'] ?>' name="prenom" required>
                                            <input type="text" class="form-control2" placeholder='<?= $_['compte_nom'] ?>' name="nom" required>
                                            <input type="email" autocomplete="username" class="form-control" placeholder='<?= $_['compte_email'] ?>' name="email" required>
                                            <input type="password" class="form-control" autocomplete="new-password" placeholder='<?= $_['compte_mdp'] ?>' name="password" required>

                                            <div class="sh-login__send">
                                                <button type="submit" class="sh-btn" id="bt_inscription"><?= $_['bt_inscription'] ?></button>
                                            </div>
                                            <div id="errorSave"></div>
                                            <hr>
                                            <p class="text-center"><?= $_['un_compte'] ?> <b><a href="javascript:void(0)" id="logConnexion"><?= $_['bt_connecter'] ?></a></b></p>
                                        </form>


                                    </div>
                                </div>

                                <div class="sh-login__content point" style="display:none;">
                                    <p class="text-center"><?= $_['compte_felicitation'] ?></p>
                                    <div id="btFinaliser" class="text-center mt-3"></div>
                                    <div class="sh-login__form text-center">
                                        <?= $_['compte_inviter'] ?>
                                    </div>
                                </div>

                                <div class="sh-login__content log" style="padding-top:10px;">
                                    <div class="sh-login__form">
                                        <form method="post" action="" name="form_log" id="form_log">
                                            <input type="text" autocomplete="username" tabindex="1" class="form-control" value="<?= !empty($_COOKIE["member_login"]) ? $_COOKIE["member_login"] : '' ?>" placeholder='<?= $_['connection_email'] ?>' name="email" id="email" required>
                                            <input type="password" autocomplete="current-password" tabindex="2" class="form-control" value="<?php if (isset($_COOKIE["member_pwd"]) && $_COOKIE["member_pwd"] != '') {
                                                                                                                                                        echo $_COOKIE["member_pwd"];
                                                                                                                                                    } ?>" placeholder='<?= $_['connection_mdp'] ?>' name="password" id="password" required>

                                            <p class="news mb-4">
                                                <label for="remember"><input <?php if (isset($_COOKIE["member_login"]) && $_COOKIE["member_login"] != '') {
                                                                                            echo 'checked="checked"';
                                                                                        } ?> tabindex="3" name="remember" id="remember" type="checkbox"> <?= $_['bt_souvenir'] ?></label>
                                            </p>

                                            <div class="sh-login__send">
                                                <button type="submit" class="sh-btn" id="bt_signup"><?= $_['bt_connecter'] ?></button>
                                            </div>
                                            <!-- Facebook login -->
                                            <div id="fb-root"></div>
                                            <div class="fb-login-button" data-width="1px" data-size="large" data-button-type="continue_with" data-use-continue-as="true" data-auto-logout-link="true" data-scope="public_profile,email" data-onlogin="onFacebookLogin()"></div>

                                            <p class="text-center"><a href="javascript:void(0)" id="mdp" class="text-center"><?= $_['mdp_oublie'] ?></a></p>
                                            <div id="errorLog"></div>
                                            <hr>
                                            <p class="text-center"><?= $_['pas_compte'] ?> <b><a href="javascript:void(0)" id="cancel"><?= $_['bt_creer_compte'] ?></a></b></p>
                                        </form>

                                    </div>
                                </div>
                                <div class="sh-login__content mdp" style="display:none;">
                                    <p class="text-center"><b><?= $_['mdp_oublier'] ?></b></p>
                                    <small class="text-center"><?= $_['mes_mdp'] ?></small>
                                    <div class="sh-login__form">
                                        <form method="post" action="" name="form_mdp" id="form_mdp" enctype="multipart/form-data">

                                            <input type="text" class="form-control" placeholder='<?= $_['mdp_email'] ?>' name="email" id="email" required>

                                            <div class="sh-login__send">
                                                <button type="submit" class="sh-btn" id="bt_mdp"><?= $_['bt_envoyer'] ?></button>
                                            </div>
                                            <div id="errorMdp" class="text-center"></div>
                                            <p class="text-center"><a href="javascript:void(0)" id="cancel3" class="text-center"><?= $_['bt_annuler'] ?></a></p>
                                            <hr>
                                            <p class="text-center"><?= $_['pas_compte'] ?> <b><a href="javascript:void(0)" id="cancel2"><?= $_['bt_creer_compte'] ?></a></b></p>
                                        </form>

                                    </div>
                                </div>

                            </div>

                    <?php }
                    } ?>
                    </div>

                    <div class="sh-notif">
                        <div class="sh-notif__head">
                            <p><?= $_['menu_notif'] ?></p>
                            <a href="javascript:void(0)" class="sh-notif__close sh-btn-icon"><i class="fa fa-close"></i></a>
                        </div>
                        <div class="notifContent"></div>
                    </div>


                    <div class="sh-cgv">
                        <div class="sh-cgv__content">
                            <ul class="sh-cgv__dropdown" style="display: block;">
                                <li>
                                    <a href="<?= $footerFR ?>"><img class="flag <?php if ($code == 'fr') {
                                                                                    echo "courant";
                                                                                } ?>" src="/images/flag-fr.png" alt="<?= $_['lang1'] ?>" width="30" /></a>
                                    <a href="<?= $footerEN ?>"><img class="flag <?php if ($code == 'en') {
                                                                                    echo "courant";
                                                                                } ?>" src="/images/flag-en2.png" alt="<?= $_['lang2'] ?>" width="30" /></a></li>
                                <?php if (!empty($_SESSION['securite'])) { ?>
                                    <hr />
                                    <li><a href="/<?= $code ?>/<?= $_['url_compte'] ?>/<?= $_['views']['settings'] ?>"><?= $_['menu_compte3'] ?></a></li>
                                    <hr class="visible-xs" />
                                    <li><a href="/<?= $code ?>/<?= $_['url_condition_1'] ?>" data-toggle="tooltip" data-placement="bottom" class="visible-xs">
                                            <?= $_['menu_condition_1'] ?>
                                        </a></li>
                                    <hr />
                                    <li><a href="/php/deconnexion.php"><?= $_['menu_condition_6'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                    <div class="sh-partage-battle">
                        <div class="sh-upload__logo"><a href="/index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                        <div class="sh-upload__content partage">
                            <div class="sh-upload__form">

                                <p class="text-center m-3"><b><?= $_['partage_battle'] ?></b></p>

                                <div class="row">
                                    <div class="col-lg-8 offset-2 mt-1 mb-1">
                                        <div class="sh-login__social text-center"></div>
                                    </div>
                                </div>

                                <form class="div_amis" method="post" action="" name="form_save_partage_battle" id="form_save_partage_battle" enctype="multipart/form-data">

                                    <p class="text-center m-3"><b><?= $_['ou_envoyer'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-8 offset-2 mt-3 mb-3" style="height: 200px;overflow-y: auto;">
                                            <?php
                                            $select_amis = $dbh->prepare(
                                                "   SELECT friend AS user_id FROM bl_user_friend WHERE user = :id AND statut = 1
                                            UNION 
                                                SELECT user FROM bl_user_friend WHERE friend = :id AND statut = 1
                                            "
                                            );
                                            $select_amis->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
                                            $select_amis->execute();

                                            if ($select_amis->rowCount() > 0) {
                                                while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) {
                                                    $row_amis->user_id  = $row_amis->user_id;
                                                    ?>
                                                    <!--section-->
                                                    <label class="sh-checkbox">
                                                        <input type="checkbox" name="amis[]" id="<?= $row_amis->user_id  ?>" value="<?= $row_amis->user_id  ?>">
                                                        <span></span>
                                                        <p>
                                                            <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_amis->user_id, $dbh), $dbh) ?>; background-image:url(/<?= photo_user($row_amis->user_id, $dbh) ?>)"></span>
                                                            <span class="pseudo"><?= prenom_user($row_amis->user_id, $dbh) ?> - <?= grade_user_between(points_user($row_amis->user_id, $dbh), $code, $dbh) ?></span>
                                                        </p>
                                                    </label>

                                                    <hr class="mt-1 mb-2" />
                                                <?php
                                                    }
                                                } else {
                                                    ?>
                                                <p class="text-center"> <?= $_['pas_amis'] ?></p>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">
                                    <input class="form-control" name="id_battle" id="id_battle" value="" required type="hidden">

                                    <?php if ($select_amis->rowCount() > 0) { ?>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn" id="btn-partage-battle"><?= $_['bt_partager'] ?></button>
                                        </div>
                                    <?php } ?>
                                    <div class="text-center" id="errorLogPartageBattle"></div>
                                </form>

                                <form style="display:none;" class="div_mail" method="post" action="" name="form_save_partage_battle_send" id="form_save_partage_battle_send" enctype="multipart/form-data">
                                    <p class="text-center m-3"><b><?= $_['envoyer_a'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 1*" name="email1" id="email1" required type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 3" name="email3" id="email3" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 2" name="email2" id="email2" type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 4" name="email4" id="email4" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">
                                    <input class="form-control" name="id_battle" id="id_battle" value="" required type="hidden">

                                    <div class="sh-login__send mb-1">
                                        <button type="submit" class="sh-btn" id="btn-partage-battle-send"><?= $_['bt_envoyer'] ?></button>
                                    </div>
                                    <p class="text-center mt-0 mb-3"><a href="javascript:void(0)" id="cancelSend" class="text-center"><?= $_['bt_annuler'] ?></a></p>
                                    <div class="text-center" id="errorLogPartageBattleSend"></div>
                                </form>
                            </div>
                        </div>

                        <div class="sh-login__content confirm" style="display:none;">
                            <p class="text-center"><?= $_['bravo_partage'] ?></p>
                        </div>

                    </div>

                    <div class="sh-partage-contenu">
                        <div class="sh-upload__logo"><a href="/<?= $code ?>" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                        <div class="sh-upload__content partage">
                            <div class="sh-upload__form">
                                <p class="text-center m-3"><b><?= $_['partage_contenu'] ?></b></p>

                                <div class="row">
                                    <div class="col-lg-8 offset-2 mt-1 mb-1">
                                        <div class="sh-login__social text-center"></div>
                                    </div>
                                </div>
                                <form method="post" class="div_amis_contenu" action="" name="form_save_partage_contenu" id="form_save_partage_contenu" enctype="multipart/form-data">

                                    <p class="text-center m-3"><b><?= $_['ou_envoyer'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-8 offset-2 mt-3 mb-3" style="height: 200px;overflow-y: auto;">
                                            <?php $select_amis = $dbh->prepare(
                                                "   SELECT friend AS user_id FROM bl_user_friend WHERE user = :id AND statut = 1
                                        UNION 
                                            SELECT user FROM bl_user_friend WHERE friend = :id AND statut = 1
                                        "
                                            );

                                            $select_amis->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
                                            $select_amis->execute();

                                            if ($select_amis->rowCount() > 0) {
                                                while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) { ?>

                                                    <!--section-->
                                                    <label class="sh-checkbox">
                                                        <input type="checkbox" name="amis[]" id="<?= $row_amis->user_id  ?>" value="<?= $row_amis->user_id  ?>">
                                                        <span></span>
                                                        <p>
                                                            <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_amis->user_id, $dbh), $dbh) ?>; background-image:url(/<?= photo_user($row_amis->user_id, $dbh) ?>)"></span>
                                                            <span class="pseudo"><?= prenom_user($row_amis->user_id, $dbh) ?> - <?= grade_user_between(points_user($row_amis->user_id, $dbh), $code, $dbh) ?></span>
                                                        </p>
                                                    </label>

                                                    <hr class="mt-1 mb-2" />

                                                <?php
                                                    }
                                                } else {
                                                    ?>
                                                <p class="text-center"> <?= $_['pas_amis'] ?></p>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">
                                    <input class="form-control" name="id_post" id="id_post" value="" required type="hidden">

                                    <?php if ($select_amis->rowCount() > 0) { ?>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn" id="btn-partage-contenu"><?= $_['bt_partager'] ?></button>
                                        </div>
                                    <?php } ?>
                                    <div class="text-center" id="errorLogPartagePost"></div>
                                </form>

                                <form style="display:none;" class="div_mail_contenu" method="post" action="" name="form_save_partage_contenu_send" id="form_save_partage_contenu_send" enctype="multipart/form-data">
                                    <p class="text-center m-3"><b><?= $_['envoyer_a'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 1*" name="email1" required type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 3" name="email3" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 2" name="email2" type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 4" name="email4" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">
                                    <input class="form-control" name="token_post" id="token_post" value="" required type="hidden">

                                    <div class="sh-login__send mb-1">
                                        <button type="submit" class="sh-btn" id="btn-partage-contenu-send"><?= $_['bt_envoyer'] ?></button>
                                    </div>
                                    <p class="text-center mt-0 mb-3"><a href="javascript:void(0)" id="cancelSendContenu" class="text-center"><?= $_['bt_annuler'] ?></a></p>
                                    <div class="text-center" id="errorLogPartageContenuSend"></div>
                                </form>

                            </div>
                        </div>

                        <div class="sh-login__content confirm" style="display:none;">
                            <p class="text-center"><?= $_['bravo_partage'] ?></p>
                        </div>

                    </div>

                    <div class="sh-inviter-menu">
                        <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                        <div class="sh-upload__content invitation">
                            <div class="sh-upload__form">
                                <form method="post" action="" name="form_save_inviter_menu" id="form_save_inviter_menu" enctype="multipart/form-data">

                                    <p class="text-center m-3"><b><?= $_['inviter_amis'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 1*" name="email1" id="email1" required type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 3" name="email3" id="email3" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3 mb-1">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 2" name="email2" id="email2" type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 4" name="email4" id="email4" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">

                                    <div id="errorLogInviterMenu" class="text-center mb-3"></div>

                                    <div class="sh-login__send mb-1">
                                        <button type="submit" class="sh-btn sh-btn-rose" id="btn-inviter-menu"><?= $_['bt_inviter_amis'] ?></button>
                                    </div>
                                    <p class="text-center m-3"><b><?= $_['ou'] ?></b></p>

                                    <div class="sh-login__send">
                                        <a href="/<?= $code ?>/<?= $_['url_search_amis'] ?>" class="sh-btn sh-btn-rose"><?= $_['bt_trouver_amis'] ?></a>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="sh-login__content confirm" style="display:none;">
                            <p class="text-center"><?= $_['bravo_invitation'] ?></p>
                        </div>

                    </div>

                    <div class="sh-likemessage">
                        <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                        <div class="sh-upload__content">
                            <p class="text-center mb-3">
                                <b><?= $_['convertir_likecoins'] ?></b><br>
                                <a target="_blank" class="sh-btn center-block m-3" href="/<?= $code ?>/<?= $_['url_compte'] ?>">
                                    <?= $_['bt_convertir_maintenant'] ?>
                                </a>
                            </p>
                        </div>
                    </div>

                    <!-- Signaler -->
                    <div class="sh-signalerPost">
                        <div class="sh-upload__logo"><a href="/index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                        <div class="sh-upload__content signaler">
                            <p class="text-center mb-3">
                                <b><?= $_['titre_signaler'] ?></b> : <span class="namePost"></span><br>
                                <form method="post" action="" name="form_signaler_post" id="form_signaler_post" enctype="multipart/form-data">
                                    <select class="form-control mb-4" name="cat">
                                        <option value="1"><?= $_['type_signaler1'] ?></option>
                                        <option value="2"><?= $_['type_signaler2'] ?></option>
                                        <option value="3"><?= $_['type_signaler3'] ?></option>
                                    </select>
                                    <textarea name="mes" id="mes" cols="3" maxlength="140" class="form-control" placeholder="<?= $_['msg_signaler'] ?>"></textarea>
                                    <input name="post" value="" required type="hidden">
                                    <div id="errorLogPostSignaler" class="text-center mb-3"></div>
                                    <div class="sh-login__send mb-4">
                                        <button type="submit" class="sh-btn sh-btn-rose" id="btn-signaler-post"><?= $_['bt_signaler'] ?></button>
                                    </div>
                                </form>
                            </p>
                        </div>
                        <div class="sh-login__content confirm" style="display:none;">
                            <p class="text-center"><?= $_['bravo_signalement'] ?></p>
                        </div>
                    </div>

                    <script>
                        require(['app'], function() {
                            require(['modules/userToggle']);
                        });
                    </script>

                </div>
            </div>
</header>

<div class="sh-supp">
    <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
    <div class="sh-login__content">
        <div class="sh-login__form text-center mt-1 mb-5">
            <?= $_['supp_contenu1'] ?> <br>"<b><span id="postName"></span></b>" ?
            <br><br><?= $_['supp_contenu2'] ?>
        </div>
        <div class="sh-login__send">
            <input type="hidden" name="id_post" id="id_post" required>
            <button type="submit" class="sh-btn" id="suppPost"><?= $_['bt_confirm'] ?></button>
        </div>
    </div>
</div>
<div class="sh-suppBattle">
    <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
    <div class="sh-login__content">
        <div class="sh-login__form text-center mt-1 mb-5">
            <?= $_['supp_contenu1'] ?> <br>"<b><span id="battleName"></span></b>" ?
            <br><br><?= $_['supp_contenu2'] ?>
        </div>
        <div class="sh-login__send">
            <input type="hidden" name="id_battle" id="id_battle" required>
            <button type="submit" class="sh-btn" id="suppBattle"><?= $_['bt_confirm'] ?></button>
        </div>
    </div>
</div>

<!-- edit POST -->
<div class="sh-editPost">
    <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
    <div class="sh-upload__content">
        <div class="sh-upload__form">
            <form method="post" name="form_edit_post" id="form_edit_post" enctype="multipart/form-data">
                <p><?= $_['edit_contenu'] ?></p>
                <input class="form-control mt-3" placeholder="<?= $_['edit_titre'] ?>" name="name_post" id="name_post" type="text">
                <input type="hidden" name="id_post" id="id_post" required>
                <div class="sh-login__send">
                    <button type="submit" class="sh-btn mx-auto" id="editPost"><?= $_['bt_edit'] ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- edit Battle -->
<div class="sh-editBattle">
    <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
    <div class="sh-upload__content">
        <div class="sh-upload__form">
            <form method="post" name="form_edit_battle" id="form_edit_battle" enctype="multipart/form-data">
                <p><?= $_['edit_catbattle'] ?></p>
                <?php $select_cat = $dbh->prepare("SELECT id, name_" . $code . " as name  FROM `bl_categories` WHERE `statut` = '1' order by name_" . $code . " ");
                $select_cat->execute(); ?>
                <select class="form-control mt-3 mb-4" name="cat_battle" id="cat_battle">
                    <?php if ($select_cat->rowCount() > 0) {
                        while ($row_cat = $select_cat->fetch(PDO::FETCH_OBJ)) { ?>
                            <option value="<?= $row_cat->id ?>"><?= $row_cat->name ?></option>
                    <?php }
                    } ?>
                </select>
                <p><?= $_['edit_titrebattle'] ?></p>
                <input class="form-control mt-3" placeholder="<?= $_['champ_battle_3'] ?>" name="name_battle" id="name_battle" type="text">
                <p><?= $_['edit_descbattle'] ?></p>
                <textarea name="info_battle" id="info_battle" class="form-control mb-4" placeholder="<?= $_['champ_battle_5'] ?>"></textarea>
                <input type="hidden" name="id_battle" id="id_battle" required>
                <div class="sh-login__send">
                    <button type="submit" class="sh-btn mx-auto" id="editBattle"><?= $_['bt_edit'] ?></button>
                </div>
            </form>
        </div>
    </div>
</div>