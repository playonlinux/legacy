<!-- <div class="sh-head-user__info"> -->
<?php if (!empty($_SESSION['securite'])) { ?>
    <!-- <div class="sh-head-user__info-head text-center">
            <button type="submit" class="sh-btn center-block btn-post"> <?= $_['bt_contenu'] ?></button>
        </div> -->

    <div class="sh-upload">
        <div class="sh-upload__logo">
            <a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a>
        </div>
        <div class="sh-upload__content save">
            <p class="text-center"><b><?= $_['post_contenu'] ?></b></p>
            <div class="sh-upload__form">
                <form method="post" action="" name="form_save_post" id="form_save_post" enctype="multipart/form-data">

                    <div class="sh-head-user__upload mx-auto" style="width:255px;">
                        <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_1'] ?>" class="sh-btn-icon btn-type active" data-id="1"><i class="fa fa-file-image-o"></i></div>
                        <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_2'] ?>" class="sh-btn-icon btn-type" data-id="2"><i class="fa fa-file-video-o"></i></div>
                        <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_3'] ?>" class="sh-btn-icon btn-type" data-id="4"><i class="fa fa-file-audio-o"></i></div>
                        <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_4'] ?>" class="sh-btn-icon btn-type" data-id="3"><i class="fa fa-file-text-o"></i></div>
                    </div>


                    <div id="formImage">
                        <p class="small"><?= $_['champ_type1'] ?> : (JPG, GIF, PNG)</p>
                        <input type="file" name="image" id="image" class="form-control inputfile" />
                        <label for="image">
                            <i class="fa fa-upload">&nbsp;<?= $_['upload']['image'] ?></i>
                        </label>
                    </div>

                    <div id="formSon" style="display:none;">
                        <p class="small"><?= $_['champ_type2'] ?> : (MP3)</p>
                        <input type="file" name="son" id="son" class="form-control inputfile" />
                        <label for="son">
                            <i class="fa fa-upload">&nbsp;<?= $_['upload']['sound'] ?></i>
                        </label>
                    </div>

                    <div id="formLien" style="display:none;">
                        <p class="small"><?= $_['champ_type3'] ?> : (Youtube, Vimeo, Dailymotion)</p>
                        <input class="form-control" placeholder="<?= $_['champs_post_1'] ?>" name="lien" id="lien" type="text">
                    </div>

                    <div id="formText" style="display:none;">
                        <p class="small"><?= $_['champ_type4'] ?></p>
                        <textarea name="text" id="text" class="form-control" placeholder="<?= $_['champs_post_2'] ?>"></textarea>
                        <input class="form-control mt-1" placeholder="<?= $_['placehoder2_type4'] ?>" name="lien_text" id="lien_text" type="text">
                    </div>

                    <input type="text" class="form-control mt-4" placeholder='<?= $_['champs_post_3'] ?>' name="title">


                    <input type="hidden" class="form-control" name="type" value="1" required>
                    <input type="hidden" class="form-control" name="battle" value="<?= $id_battle ?>" required>
                    <input type="hidden" class="form-control" name="url_battle" value="<?= $_GET['id'] ?>" required>

                    <input type="hidden" class="form-control" name="code" value="<?= $code ?>" required>

                    <hr>
                    <div class="sh-login__send">
                        <button type="submit" class="sh-btn sh-btn-rose mx-auto" id="bt_publier"><?= $_['bt_publier'] ?></button>
                    </div>
                    <div id="errorLogPost" class="text-center mb-3"></div>
                </form>
            </div>
        </div>

    </div>

    <script>
        require(['app'], function() {
            require(['modules/main']);
        });
    </script>

<?php } else { ?>

    <div class="sh-head-user__info-head text-center">
        <button type="submit" class="sh-btn center-block btn-upload_btn-signup"><?= $_['bt_contenu'] ?></button>
    </div>

    <script>
        require(['app'], function() {
            require(['modules/main']);
            require(['modules/signup']);
        });
    </script>

<?php } ?>

</div>