<div class="col-lg-6 col-md-6 col-sm-12">
    <?php
    $progess = (!empty($_SESSION['securite'])) ? get_user_progress($_SESSION['id_user']) : '';
    $color = (!empty($_SESSION['securite'])) ? grade_user_color((points_user($_SESSION['id_user'], $dbh)), $dbh) : '#FFFFFF';
    $colorClass = (!empty($_SESSION['securite'])) ? get_grade_color_class(points_user($_SESSION['id_user'], $dbh)) : '';
    if (!empty($_SESSION['securite'])) {
        ?>
        <div class="row">
            <div class="col-2">
                <img src="<?= get_user_avatar($_SESSION['id_user']); ?>" class="avatar">
            </div>
            <div class="col-10">
                <div class="row col-12">
                    <div class="col-12"><?= grade_user_between(points_user($_SESSION['id_user'], $dbh), $code, $dbh); ?></div>
                    <div class="col-sm-8 col-11 w3-container">
                        <div class="col-12 w3-light-grey w3-round-xlarge" style="justify-content:start;padding: 0">
                            <div class="w3-container w3-round-xlarge <?= $colorClass ?>" style="width:<?= $progess + 15 ?>%;padding-left:<?= ($progess) ? $progess - 10 : 1 ?>%"><b><?= $progess ?>%</b></div>
                        </div>
                    </div>
                    <div class="col-1" style="font-size: smaller; padding-left:15px;"><?= get_user_next_grade($_SESSION['id_user']); ?></div>
                    <div class="row col-12 mt-5 pull-1 mr-5">
                        <button type="submit" class="sh-btn btn-battle" style="font-size:20px;color: <?= $color ?>; border-color: <?= $color ?>;width:100%;height:60px;" onmouseover="this.style.backgroundColor='<?= $color ?>';this.style.color='#ffffff'" onmouseout="this.style.backgroundColor='#ffffff';this.style.color='<?= $color ?>'">
                            <?= $_['bt_battle'] ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <button type="submit" class="sh-btn big sh-login__btn-signup" style="color: <?= $color ?> !important;background-color: #fe585c"><?= $_['bt_battle'] ?></button>
    <?php } ?>

    <?php if (!empty($_SESSION['securite'])) { ?>
        <div class="sh-battle">
            <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
            <div class="sh-upload__content save">
                <div class="sh-upload__form">
                    <form method="post" action="" name="form_save_battle" id="form_save_battle" enctype="multipart/form-data">

                        <p class="mb-3">1- <?= $_['champ_battle_1'] ?></p>

                        <div class="sh-head-user__upload mx-auto" style="width:255px;">
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['champ_battle_type1'] ?>" class="sh-btn-icon btn-type active" data-id="1"><i class="fa fa-file-image-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['champ_battle_type2'] ?>" class="sh-btn-icon btn-type" data-id="2"><i class="fa fa-file-video-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['champ_battle_type3'] ?>" class="sh-btn-icon btn-type" data-id="4"><i class="fa fa-file-audio-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['champ_battle_type4'] ?>" class="sh-btn-icon btn-type" data-id="3"><i class="fa fa-file-text-o"></i></div>
                        </div>

                        <div id="formImage">
                            <p class="small"><?= $_['champ_type1'] ?> : (JPG, GIF, PNG)</p>
                            <input type="file" name="image" id="image" class="form-control inputfile" />
                            <label for="image">
                                <i class="fa fa-upload">&nbsp;<?= $_['upload']['image'] ?></i>
                            </label>
                        </div>

                        <div id="formSon" style="display:none;">
                            <p class="small"><?= $_['champ_type2'] ?> : (MP3)</p>
                            <input type="file" name="son" id="son" class="form-control inputfile" />
                            <label for="son">
                                <i class="fa fa-upload">&nbsp;<?= $_['upload']['sound'] ?></i>
                            </label>
                        </div>

                        <div id="formLien" style="display:none;">
                            <p class="small"><?= $_['champ_type3'] ?> : (Youtube, Vimeo, Dailymotion)</p>
                            <input class="form-control" placeholder="<?= $_['placehoder_type3'] ?>*" name="lien" id="lien" type="text">
                        </div>

                        <div id="formText" style="display:none;">
                            <p class="small"><?= $_['champ_type4'] ?></p>
                            <textarea name="text" id="text" class="form-control" placeholder="<?= $_['placehoder_type4'] ?>*"></textarea>
                            <input class="form-control mt-1" placeholder="<?= $_['placehoder2_type4'] ?>" name="lien_text" id="lien_text" type="text">
                        </div>

                        <input type="hidden" class="form-control" name="type" value="1" required>

                        <hr>

                        <?php $select_cat = $dbh->prepare("SELECT id, name_" . $code . "  as name FROM `bl_categories` WHERE `statut` = 1 order by name_" . $code . " ");
                            $select_cat->execute(); ?>
                        <select class="form-control mb-4" name="cat" id="cat" required>
                            <option value="0" disabled selected>2- <?= $_['champ_battle_2'] ?></option>
                            <?php if ($select_cat->rowCount() > 0) {
                                    while ($row_cat = $select_cat->fetch(PDO::FETCH_OBJ)) { ?>
                                    <option value="<?= $row_cat->id ?>"><?= $row_cat->name ?></option>
                            <?php }
                                } ?>
                        </select>

                        <input type="text" class="form-control mb-4" id="titleB" maxlength="50" placeholder='3- <?= $_['champ_battle_3'] ?>' name="titleB" required>
                        <input type="text" class="form-control mb-4" placeholder='4- <?= $_['champ_battle_4'] ?>' name="titleC">
                        <textarea name="info" id="info" class="form-control mb-4" placeholder="5- <?= $_['champ_battle_5'] ?>"></textarea>

                        <div id="errorLogBattle" class="text-center mb-3"></div>

                        <div class="sh-login__send mb-1">
                            <button type="submit" class="sh-btn mx-auto" id="bt_publier_battle"><?= $_['bt_battle_lancer'] ?></button>
                        </div>
                        <p class="m-auto text-center mt-0 mb-2"><small>*champs obligatoires</small></p>
                    </form>
                </div>
            </div>

            <div class="sh-upload__content invite" style="display:none;">
                <div class="sh-upload__form">
                    <form method="post" action="" name="form_save_battle_invite" id="form_save_battle_invite" enctype="multipart/form-data">
                        <p class="mb-3 text-center"><b><?= $_['titre_battle_inviter'] ?></b></p>

                        <div class="row">
                            <div class="col-lg-6 mt-3 mb-1">
                                <div class="sh-confirm__content">
                                    <div class="sh-confirm__form">
                                        <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 1*" name="email1" id="email1" required type="text">
                                        <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 3" name="email3" id="email3" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 mt-3 mb-1">
                                <div class="sh-confirm__content">
                                    <div class="sh-confirm__form">
                                        <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 2" name="email2" id="email2" type="text">
                                        <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 4" name="email4" id="email4" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">
                        <input class="form-control" name="id_battle" id="id_battle" value="" required type="hidden">
                        <input class="form-control" name="url_battle" id="url_battle" value="" required type="hidden">

                        <div class="sh-login__send mb-1">
                            <button type="submit" class="sh-btn" id="btn-partage-battle-invite"><?= $_['bt_battle_envoyer'] ?></button>
                        </div>
                        <p class="text-center mt-0 mb-3"><a href="javascript:void(0);" id="cancelSendInviteBattle" class="text-center"><?= $_['bt_battle_non'] ?></a></p>

                        <div class="text-center" id="errorLogPartageBattleInvite"></div>
                    </form>
                </div>
            </div>

        </div>

        <script>
            var lang = '<?= $code ?>';
            require(['app'], function() {
                require(['modules/main']);
                require(['modules/upload']);
            });
        </script>
    <?php } ?>
</div>