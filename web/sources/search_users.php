<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_battelike.php');
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis']?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  <?= $_['titre_search_friends']?>">
    <meta name="author" content="<?= $nameSite ?>">
    <title><?= $nameSite ?> | <?= $_['titre_search_friends']?></title>
   
    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_search_amis'] ?>" />
    <?php if($code == 'fr'){ 
		$footerEN = '/en/search-friends';?>
    	<link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
    <?php }else if($code == 'en'){ 
		$footerFR = '/fr/recherche-ami';?>
    	<link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
    <?php } ?>
    
    <?php include('required.php'); ?>
    <script>var page = 'search_amis';</script>
</head>
<body>
    
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container-fluid">
            <!--content head-->
            
            <div class="sh-content-head">
                <div class="sh-content-head__btns center-block text-center">
                	<h3 class="center-block">
                     	<?= $_['titre_search_friends']?>
                  	</h3>
                    <input type="text" class="quicksearch mt-2" style="width:250px; padding-left:12px;" placeholder="<?= $_['input_search_friends']?>" />
                </div>
            </div>
            
            <!--sections-->
            <div class="sh-section__wrap row tab-submit" style="min-height:100px;" id="users" > </div>
            
            <div class="sh-reponse" >
                <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                <div class="sh-login__content">
                    <p class="text-center"><b><?= $_['demande_friend_send']?></b></p>
                </div>
           	</div>
            
            <div class="sh-annuler" >
                <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                <div class="sh-login__content">
                    <p class="text-center"><b><?= $_['demande_friend_delete']?></b></p>
                </div>
           	</div>
      
        </div>
    </main>
    
    <?php include('footer.php'); ?>
    
    
    <script>
        require(['app'], function () {
            require(['modules/posts']);
			require(['modules/search_users']);
        });
    </script>
    
</body>
</html>