$(window).load(function() { 
	$("#status").fadeOut(); // will first fade out the loading animation
	$("#preloader").delay(100).fadeOut("slow"); // will fade out the white DIV that covers the website.
});


$( document ).ready(function() {
    
    
    //Custom Template Code 
    
    $('.show-submenu').click(function(){
       $(this).parent().find('.submenu').toggleClass('submenu-active'); 
       $(this).toggleClass('submenu-active');       
    });
	
	
	//connexion compte
	$('#logCompte').on('click', function() {
		
		$('#errorLog').css("display","none");
		
		var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   	 	var emailA = document.logFormP.email.value;	
				
		if((document.logFormP.email.value !='' && regex.test(emailA)) && document.logFormP.password.value !=''){
			
			$.ajax({
				type: "POST",
				url: "php/logUser.php",
				cache: true,
				data: $("#logFormP").serialize(),
				success: function(data){
					
					console.log(data);
					
					if(data =='non'){
						$('#errorLog').css("display","block");
						$('#errorLog').css("display","block");
						var f = document.getElementById('errorLog');
						f.innerHTML ="";
						f.innerHTML = "<strong class='color-red'>Aucun compte trouvé.</strong>";
						
					}else{
						
						if(data =='admin'){
							$('#logFormP').attr('action', 'admin-battles.php'); 
							$('#logFormP').submit();
						}
					
						
					}
				}
			});
			
		}else{
			
			if(document.logFormP.email.value ==''){ $('#email').css('border','1px solid #f00');}
			if(document.logFormP.password.value ==''){ $('#password').css('border','1px solid #f00');}
			
			$('#errorLog').css("display","block");
			var f = document.getElementById('errorLog');
			f.innerHTML ="";
			f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
			
			
		}
		return false;
		
	});
	
	
	$('#mdpEnvoi').click(function() {
		console.log('oui');
		$('#page-login').css('display','none');
		$('#page-oublie').css('display','block');
	});
	
	$('#mdpAnnule').click(function() {
		console.log('oui');
		$('#page-login').css('display','block');
		$('#page-oublie').css('display','none');
	});
	
	
	
	
	
	$('#envoiMail').on('click', function() {
			
		$('#errorLog22').css("display","none");
		
		var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   	 	var emailA = document.logFormP2.email.value;	
		
		if(document.logFormP2.email.value !='' && regex.test(emailA)){
			
			console.log("oui");
		
			var form = $('#logFormP2').get(0);
			var formData = new FormData(form);// get the form data
			// on envoi formData vers mail.php
	
			$.ajax({
				type: "POST",
				url: "php/forgotMdp.php",
				data : formData,
				processData: false,
				contentType: false,
				cache: false,
				success: function(data){
					console.log(data);
					if(data =="ok"){
						$('#errorLog22').css("display","block");
						var f = document.getElementById('errorLog22');
						f.innerHTML ="";
						f.innerHTML = "<strong class='blue'>Un mail vient de vous être envoyé.</strong>";
					}else{
						$('#errorLog22').css("display","block");
						var f = document.getElementById('errorLog22');
						f.innerHTML ="";
						f.innerHTML = "<strong class='color-red'>Email incorrect ou compte inexistant</strong>";
					}
				}
			});
			
		}else{
			
			$('#errorLog22').css("display","block");
			if(document.logFormP2.email.value ==''){ $('#email').css('border','1px solid #f00');}
			
		}
					
		return false;
	});
	
	
	$('#saveNewMP').on('click', function() {
		
		console.log("oui");
		
		var form = $('#logFormP').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveNewMdp.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Votre mot de passe a bien été réinitialisé. </strong>";
					
					setTimeout(function(){
						$('#logFormP').attr('action', 'index.php'); 
						$('#logFormP').submit();
					}, 500);
									
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>"+data+"</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	
	/* POP UP MDP*/
	
	if($('.simple-mdp-modal-content').length>0){
		mdpLog();
	}
	    
    function mdpLog(){
        $('.simple-mdp-modal-content').modal();      
    }; 
	
	
	$('#saveMdp').on('click', function() {
		
		console.log("oui");
		
		var form = $('#mdpForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveMdp.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Félicitations !</strong>";
					
					setTimeout(function(){
						$.modal.close();
					}, 500);
									
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>"+data+"</strong>";
				}
			}
		});

		return false;
	});
	
	
	$('#nonMdp').on('click', function() {
		
		console.log("oui");

		$.ajax({
			type: "POST",
			url: "php/nonMdp.php",
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				
				if(data =="ok"){
					
					$.modal.close();
									
				}
			}
		});

		return false;
	});
	
	
	$('#repOui').on('click', function() {
		
		console.log("oui");
		
		var form = $('#ouiForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveDesinscription.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					
					$('#btAction').css("display","none");
					var f = document.getElementById('messageOK');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Votre désinscription a bien été prise en compte.</strong>";
					
					//$.modal.close();				
				}else{
					var f = document.getElementById('messageOK');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>"+data+"</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	
	
	/* REPONSE */
	
	 $('.simple-login-modal').click(function() {
		var reponse = $('#reponse').val();
		var f = document.getElementById('showreponse');
		f.innerHTML ="";
		f.innerHTML = reponse;
		$('.simple-login-modal-content').modal();
	}); 
	
	
	
	
	
	//BATTLE
	
	$('.simple-suppclient-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_battle_supp').val(id);
		var f = document.getElementById('nameEntrSupp');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-suppclient-modal-content').modal();
	});
	
	
	
	$('#suppBattle').on('click', function() {
				
		var form = $('#form_battle_sup').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveBattle.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-battles.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	
	//POST
	$('.simple-suppPost-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_post_supp').val(id);
		var f = document.getElementById('nameEntrSupp');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-suppPost-modal-content').modal();
	});
	
	
	
	$('#suppPost').on('click', function() {
				
		var form = $('#form_post_sup').get(0);
		var formData = new FormData(form);// get the form data
	
		$.ajax({
			type: "POST",
			url: "php/savePost.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data !='non'){
					$.modal.close();				
					document.location.href="admin-edit-battle.php?id="+data;
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	//USER
	
	$('.simple-suppUser-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_user_supp').val(id);
		var f = document.getElementById('nameEntrSupp');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-suppUser-modal-content').modal();
	});
	
	
	
	$('#suppUser').on('click', function() {
				
		var form = $('#form_user_sup').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveUser.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-inscrits.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLogSupp');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	$('.simple-activeUser-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_user_active').val(id);
		var f = document.getElementById('nameEntrActive');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-activeUser-modal-content').modal();
	});
	
	
	
	$('#activeUser').on('click', function() {
				
		var form = $('#form_user_active').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveUser.php?type=active",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-inscrits.php";
				}else{
					$('#errorLogActive').css("display","block");
					var f = document.getElementById('errorLogActive');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	//CAT
	
	$('.simple-categorie-modal').click(function() {
		$('.simple-categorie-modal-content').modal();
	});
	
	$('#saveCat').on('click', function() {
				
		var form = $('#form_cat_sav').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveCat.php?type=ajout",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-categories.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	
	$('.simple-editCat-modal').click(function() {
		
		var id = $(this).data('id');
		var name = $(this).data('name');
		var visuel = $(this).data('visuel');
		
		$('#form_cat_upd #id').val(id);
		$('#form_cat_upd #name_fr').val(name);
		
		$('#form_cat_upd .visuel').attr('src', '../'+visuel)
		
		$('.simple-editCat-modal-content').modal();
	});
	
	$('#updCat').on('click', function() {
				
		var form = $('#form_cat_upd').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveCat.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-categories.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	
	$('.simple-suppCat-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_cat_supp').val(id);
		var f = document.getElementById('nameEntrSupp');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-suppCat-modal-content').modal();
	});
	
	
	
	$('#suppCat').on('click', function() {
				
		var form = $('#form_cat_sup').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveCat.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-categories.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	/* GRADE */
	$('.simple-editGrade-modal').click(function() {
		
		var id = $(this).data('id');
		var namefr = $(this).data('namefr');
		var nameen = $(this).data('nameen');
		var color = $(this).data('color');
		var maximum = $(this).data('max');
		var mininus = $(this).data('min');
		var statut = $(this).data('statut');
		
		$('#form_grade_upd #id').val(id);
		$('#form_grade_upd #namefr').val(namefr);
		$('#form_grade_upd #nameen').val(nameen);
		$('#form_grade_upd #color').val(color);
		$('#form_grade_upd #max').val(maximum);
		$('#form_grade_upd #min').val(mininus);
		$('#form_grade_upd #statut').val(statut);

		$('.simple-editGrade-modal-content').modal();
	});
	
	$('#editGrade').on('click', function() {
				
		var form = $('#form_grade_upd').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveGrade.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-grades.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	
	$('.simple-suppGrade-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_grade_supp').val(id);
		var f = document.getElementById('nameGradeSupp');
		f.innerHTML ="";
		f.innerHTML = name;
									
		$('.simple-suppGrade-modal-content').modal();
	});
	
	
	
	$('#suppGrade').on('click', function() {
				
		var form = $('#form_grade_sup').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveGrade.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-grades.php";
				}else{
					$('#errorLogSupp').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue.</strong>";
				}
			}
		});

		return false;
	});
	
	
	/* Fiche engnime */
	
	 $('.simple-egnime-modal').click(function() {
		 
		var id_egnime = $(this).data('id_egnime');
		var mois = $(this).data('mois');
		var egnime = $(this).data('egnime');
		var asso = $(this).data('asso');
		var montant = $(this).data('montant');
		
		$('#id_egnime').val(id_egnime);
		$('#mois').val(mois);
		$('#egnime').val(egnime);
		$('#asso').val(asso);
		$('#montant').val(montant);
		
		$('.simple-egnime-modal-content').modal();
	}); 
	
	
	$('.simple-egnime2-modal').click(function() {
		$('.simple-egnime2-modal-content').modal();
	});
	
	
	 $('.simple-suppEntreGame-modal').click(function() {
		 
		var id_egnime = $(this).data('id_egnime');
		var name = $(this).data('name');
		
		$('#id_game_entr_supp').val(id_egnime);
		var f = document.getElementById('nameEntrGameSupp');
		f.innerHTML ="";
		f.innerHTML = name;
		
		$('.simple-suppEntreGame-modal-content').modal();
	});  
	
	
	$('#modGameEntreprise').on('click', function() {
		
		console.log("oui");
	
		$.ajax({
			type: "POST",
			url: "php/saveEntrepEgnime.php?type=update",
			cache: true,
			data : $("#saveEntrepEgnime").serialize(),
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data+"&tab=1";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		return false;
	});
	
	$('#ajoutGameEntreprise').on('click', function() {
		
		console.log("oui");
	
		$.ajax({
			type: "POST",
			url: "php/saveEntrepEgnime.php?type=ajout",
			cache: true,
			data : $("#ajoutEntrepEgnime").serialize(),
			success: function(data){
				console.log(data);
				
				if(data !="non" && data !="deja"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data+"&tab=1";
					
				}else if(data =="deja"){
					
					$('#errorLogEgnimeLiaison').css("display","block");
					var f = document.getElementById('errorLogEgnimeLiaison');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Vous avez deja un game sur ce moi-ci.</strong>";
					
				} else{
					
					$('#errorLogEgnimeLiaison').css("display","block");
					var f = document.getElementById('errorLogEgnimeLiaison');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		return false;
	});
	
	$('#suppGameEntreprise').on('click', function() {
		
		console.log("oui");
	
		$.ajax({
			type: "POST",
			url: "php/saveEntrepEgnime.php?type=supp",
			cache: true,
			data : $("#GameEntrSuppForm").serialize(),
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data+"&tab=1";
				}else{
					$('#errorLogSuppGameEntreprise').css("display","block");
					var f = document.getElementById('errorLogSuppGameEntreprise');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une reeur est intervenue. Merci de recommencer.</strong>";
				}
			}
		});

		return false;
	});
        
		
		
		/* Fiche Asso */
	
	 $('.simple-asso-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		var site = $(this).data('site');
		var statut = $(this).data('statut');
		var desc = $(this).data('desc');
		var visuel = $(this).data('visuel');
		
		$('#id_asso').val(id);
		$('#name').val(name);
		$('#site').val(site);
		$('#statut').val(statut);
		$('#desc').val(desc);
		
		$('.visuel').attr('src', visuel)
		
		$('.simple-asso-modal-content').modal();
	});
	
	$('.simple-suppasso-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$('#id_asso_supp').val(id);
		var f = document.getElementById('nameSupp');
		f.innerHTML ="";
		f.innerHTML = name;
					
		//$('#nameSupp').innerHTML(name);
				
		$('.simple-suppasso-modal-content').modal();
	}); 
	
	
	$('.simple-asso2-modal').click(function() {
		$('.simple-asso2-modal-content').modal();
	}); 
	
	
	$('#saveAsso').on('click', function() {
				
		var form = $('#assoForm');
		
		for (i = 0; i < document.assoForm.elements.length; i++) {
		  if (document.assoForm.elements[i].type == 'file') {
			if (document.assoForm.elements[i].value == '') {
			  document.assoForm.elements[i].parentNode.removeChild(document.assoForm.elements[i]);
			}
		  }
		}

		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}
		
		if(formdata instanceof FormData && navigator.userAgent.match(/version\/11((\.[0-9]*)*)? .*safari/i)) {
			try {
				eval('for (var pair of formdata.entries()) {\
					if (pair[1] instanceof File && pair[1].name === \'\' && pair[1].size === 0) {\
						formdata.delete(pair[0]);\
					}\
				}');
			} catch(e) {}
		}

		
		$.ajax({
			type: "POST", 
			url: "php/saveAsso.php?type=update",
			data : formdata ? formdata : form.serialize(),
			cache : false,
			processData: false,
			contentType: false,
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-associations.php";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});
		
		return false;
	});
	
	$('#ajoutAsso').on('click', function() {
		
		var form = $('#assoForm2');
		
		for (i = 0; i < document.assoForm2.elements.length; i++) {
		  if (document.assoForm2.elements[i].type == 'file') {
			if (document.assoForm2.elements[i].value == '') {
			  document.assoForm2.elements[i].parentNode.removeChild(document.assoForm2.elements[i]);
			}
		  }
		}

		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}
		
		if(formdata instanceof FormData && navigator.userAgent.match(/version\/11((\.[0-9]*)*)? .*safari/i)) {
			try {
				eval('for (var pair of formdata.entries()) {\
					if (pair[1] instanceof File && pair[1].name === \'\' && pair[1].size === 0) {\
						formdata.delete(pair[0]);\
					}\
				}');
			} catch(e) {}
		}

		
		$.ajax({
			type: "POST", 
			url: "php/saveAsso.php?type=ajout",
			data : formdata ? formdata : form.serialize(),
			cache : false,
			processData: false,
			contentType: false,
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-associations.php";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	$('#suppAsso').on('click', function() {
		
		console.log("oui");
		
		var form = $('#suppForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveAsso.php?type=supp",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-associations.php";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		return false;
	});
	
	
	/* Fiche EGNIMES */
	
	 $('.simple-egnimes-modal').click(function() {
		 
		var id = $(this).data('id');
		var name = $(this).data('name');
		var reponse = $(this).data('reponse');
		var description = $(this).data('description');
		var mois = $(this).data('mois');
		var abonnement = $(this).data('abonnement');
		var statut = $(this).data('statut');
		
		var dateDebut = $(this).data('debut');
		var dateFin = $(this).data('fin');
		
		$('#id_egnime').val(id);
		$('#name').val(name);
		$('#reponse').val(reponse);
		$('#description').val(description);
		$('#mois').val(mois);
		$('#abonnement').val(abonnement);
		$('#statut').val(statut);
		
		$('#dateDebut').val(dateDebut);
		$('#dateFin').val(dateFin);
				
		$('.simple-egnimes-modal-content').modal({
            onShow : function() {
				$( "#dateDebut, #dateFin" ).datepicker(
					{ 	altField: "#datepicker",
						closeText: 'Fermer',
						prevText: 'Précédent',
						nextText: 'Suivant',
						currentText: 'Aujourd\'hui',
						monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
						monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
						dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
						dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
						dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
						weekHeader: 'Sem.',
						dateFormat: 'yy-mm-dd' }
				);
		}});
	}); 
	
	
	$('.simple-egnimes2-modal').click(function() {
		
		$('.simple-egnimes2-modal-content').modal({
            onShow : function() {
				$( "#dateDebut, #dateFin" ).datepicker(
					{ 	altField: "#datepicker",
						closeText: 'Fermer',
						prevText: 'Précédent',
						nextText: 'Suivant',
						currentText: 'Aujourd\'hui',
						monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
						monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
						dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
						dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
						dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
						weekHeader: 'Sem.',
						dateFormat: 'yy-mm-dd' }
				);
		}});
		
		
	}); 
	
	
	$('#ajoutEgnime').on('click', function() {
		
		console.log("oui");
		
		var form = $('#egnimeForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
			
		
		$.ajax({
			type: "POST",
			url: "php/saveEgnime.php?type=ajout",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="admin-egnimes.php";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		
		return false;
	});
	
	$('#saveEgnime').on('click', function() {
		
		console.log("oui");
		
		var form = $('#egnimeForm2').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
			
		
		$.ajax({
			type: "POST",
			url: "php/saveEgnime.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_egnimes.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		
		return false;
	});
	
	
		/* Fiche Indice */
		
	
	 $('.simple-indice-modal').click(function() {
		 
		 console.log('oui');
		 
		var id = $(this).data('id');
		var date = $(this).data('date');
		var numero = $(this).data('numero');
		var type = $(this).data('type');
		var equipe = $(this).data('equipe');
		var indice = $(this).data('indice');
		var lien = $(this).data('lien');
		
		$('#indiceForm #id_indice').val(id);
		$('#indiceForm #dateD').val(date);
		$('#indiceForm #numero').val(numero);
		$('#indiceForm #texte').val(lien);
		
		
		if(type =='1'){
			
            $('#indiceForm #type-1').prop('checked', true);
		
			$('#indiceForm #formAutre').css('display','none');
			$('#indiceForm #formImage').css('display','block');
			$('.visuel').attr('src', indice)
			
		}else if(type =='2' || type =='3'){
			
			if(type =='2'){
				$('#indiceForm #type-2').prop('checked', true);
			}else{
				$('#indiceForm #type-3').prop('checked', true);
			}
			
			$('#indiceForm #formImage').css('display','block');
			$('#indiceForm #formAutre').css('display','block');
			$('.visuel').attr('src', indice)
			$('#indiceForm #lien').val(lien);
			
		}else {
			$('#indiceForm #type-4').prop('checked', true);
			
			$('#indiceForm #formImage').css('display','block');
			$('#indiceForm #formAutre').css('display','none');
		}
		
		if(equipe =='1'){
            $('#indiceForm #equipe-1').prop('checked', true);
		}else if(equipe =='2'){
			$('#indiceForm #equipe-2').prop('checked', true);
		}else {
			$('#indiceForm #equipe-3').prop('checked', true);
		}
						
		$('.simple-indice-modal-content').modal();
	}); 
	
	
	$('.simple-indice2-modal').click(function() {
		
		$('.simple-indice2-modal-content').modal();
		
	}); 
	
	
	$('#ajoutIndice').on('click', function() {
		
		console.log("oui");
		
		var form = $('#indiceForm2').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
			
		
		$.ajax({
			type: "POST",
			url: "php/saveIndice.php?type=ajout",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_egnimes?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		
		return false;
	});
	
	$('#saveIndice').on('click', function() {
		
		console.log("oui");
		
		var form = $('#indiceForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
			
		
		$.ajax({
			type: "POST",
			url: "php/saveIndice.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_egnimes.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		
		return false;
	});
	
	
	/* SUPP Indice */
	
	 $('.simple-indiceSupp-modal').click(function() {
		 
		 console.log("oui");
		var id = $(this).data('id');
		var numero = $(this).data('numero');
		var date = $(this).data('date');
		var f = document.getElementById('showreponse');
		f.innerHTML ="";
		f.innerHTML = "N° "+numero+" : "+date;
		
		$('#id_ind').val(id);
		
		$('.simple-indiceSupp-modal-content').modal();
	}); 
	
	
	$('#suppIndice').on('click', function() {
		
		console.log("oui");
		var id_ind = $('#id_ind').val();
		var id_egn = $('#id_egn2').val();
		
		if(reponse !=''){
			$.ajax({
				type: "POST",
				url: "php/suppIndice.php?id="+id_ind+"&egnime="+id_egn,
				cache: true,
				success: function(data){
					console.log(data);
					if(data !="non"){
						$.modal.close();				
						document.location.href="admin-edit_egnimes?id="+data;
					}else{
						$('#errorLog').css("display","block");
						var f = document.getElementById('errorLog');
						f.innerHTML ="";
						f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
					}
				}
			});
			
		}else{
			
			$('#errorLog22').css("display","block");
			if(document.logFormP.email1.value ==''){ $('#email1').css('border','1px solid #f00');}
			
		}
					
		return false;
	});
	
	
	/* Fiche Profil */
	
	 $('.simple-profil-modal').click(function() {
		 
		var name = $(this).data('name');
		var prenom = $(this).data('prenom');
		var telephone = $(this).data('telephone');
		var love = $(this).data('love');
		var nolove = $(this).data('nolove');
		var visuel = $(this).data('visuel');
		
		$('#name').val(name);
		$('#prenom').val(prenom);
		$('#telephone').val(telephone);
		$('#love').val(love);
		$('#nolove').val(nolove);
		
		$('.visuel').attr('src', visuel)
		
		$('.simple-profil-modal-content').modal();
	}); 
	
	
	$('#saveProfil').on('click', function() {
		
		console.log("oui");
		
		var form = $('#profilForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
	
		$.ajax({
			type: "POST",
			url: "php/saveProfil.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data =="ok"){
					$.modal.close();				
					document.location.href="page-profile.php";
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner le champs réponse.</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	/* Fiche SALARIES / ADMIN */
	
	 $('.simple-salarie-modal').click(function() {
		 
		var id = $(this).data('id');
		var nom = $(this).data('nom');
		var prenom = $(this).data('prenom');
		var email = $(this).data('email');
		var groupe = $(this).data('groupe');
		
		$('#idSal').val(id);
		$('#nameSal2').val(nom);
		$('#preSal2').val(prenom);
		$('#emailSal2').val(email);
		$('#groupeSal2').val(groupe);
		
		var f = document.getElementById('nameSalarie');
		f.innerHTML ="";
		f.innerHTML = nom+" "+prenom;
				
		$('.simple-salarie-modal-content').modal();
	}); 
	
	
	$('.simple-salarie2-modal').click(function() {
		$('.simple-salarie2-modal-content').modal();
	}); 
	
	$('.simple-salarie3-modal').click(function() {
		 
		var id = $(this).data('id');
		var nom = $(this).data('nom');
		var prenom = $(this).data('prenom');
		
		$('#idSal3').val(id);
		var f = document.getElementById('nameSalarie3');
		f.innerHTML ="";
		f.innerHTML = nom+" "+prenom;
				
		$('.simple-salarie3-modal-content').modal();
	}); 
	
	$('.simple-salarie4-modal').click(function() {
		$('.simple-salarie4-modal-content').modal();
	});
	
	$('.simple-salarie5-modal').click(function() {
		 
		var id = $(this).data('id');
		var nom = $(this).data('nom');
		var prenom = $(this).data('prenom');
		
		$('#idSal5').val(id);
		var f = document.getElementById('nameSalarie5');
		f.innerHTML ="";
		f.innerHTML = nom+" "+prenom;
				
		$('.simple-salarie5-modal-content').modal();
	}); 
	
	
	$('#ajoutSal').on('click', function() {
		
		console.log("oui");
		
		var form = $('#salarieForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
			
		
		$.ajax({
			type: "POST",
			url: "php/saveSalarie.php?type=ajout",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				//console.log(data);
				if(data !="non"  &&  data !="identique"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
					
				}else if(data =="identique"){
					$('#errorLogSalAjout').css("display","block");
					var f = document.getElementById('errorLogSalAjout');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Deja inscrit dans une autre entreprise !</strong>";
				}else{
					$('#errorLogSalAjout').css("display","block");
					var f = document.getElementById('errorLogSalAjout');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	$('#saveSal').on('click', function() {
		
		console.log("oui");
		
		var form = $('#salarieForm2').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/saveSalarie.php?type=update",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				//console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	$('#delSal').on('click', function() {
				
		var form = $('#salarieForm3').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/saveSalarie.php?type=delete",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				//console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	
	$('#reacSal').on('click', function() {
				
		var form = $('#salarieForm5').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/saveSalarie.php?type=reactive",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				//console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	$('#listeSal').on('click', function() {
				
		var form = $('#salarieForm4').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/saveSalarie.php?type=liste",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLog').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	//GENERER EQUIPE
	$('.simple-generer-modal').click(function() {
		
		var egnime = $(this).data('egnime');
		$('#egnimeGenerer').val(egnime);
						
		$('.simple-generer-modal-content').modal();
	});
	
	$('#genererGroupe').on('click', function() {
				
		var form = $('#genererForm').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/tirage_au_sort_entreprise.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLogGenerer').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	
	
	/* PARTAGE */
	
	$('.simple-mail-modal').click(function() {
		$('.simple-mail-modal-content').modal();
	}); 
	
	$('.simple-dej-modal').click(function() {
		$('.simple-dej-modal-content').modal();
	});
	
	$('.simple-cafe-modal').click(function() {
		$('.simple-cafe-modal-content').modal();
	});
	
	$('#saveMailPartage').on('click', function() {
				
		var form = $('#mailForm').get(0);
		var formData = new FormData(form);// get the form data
		
		$('#encoursEnvoiMailPartage').css("display","block");
		var f = document.getElementById('encoursEnvoiMailPartage');
		f.innerHTML ="";
		f.innerHTML = '<i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom"></i><br>En cours d\'envoi. Merci de patienter ....';
	
		$.ajax({
			type: "POST",
			url: "php/email_partage.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				$('#encoursEnvoiMailPartage').css("display","none");
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="page-game.php";
				}else{
					$('#errorLogMailPartage').css("display","block");
					var f = document.getElementById('errorLogMailPartage');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	$('#saveCafePartage').on('click', function() {
				
		var form = $('#cafeForm').get(0);
		var formData = new FormData(form);// get the form data
		
		$('#encoursEnvoiCafePartage').css("display","block");
		var f = document.getElementById('encoursEnvoiCafePartage');
		f.innerHTML ="";
		f.innerHTML = '<i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom"></i><br>En cours d\'envoi. Merci de patienter ....';
	
		$.ajax({
			type: "POST",
			url: "php/email_partage.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				$('#encoursEnvoiCafePartage').css("display","none");
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="page-game.php";
				}else{
					$('#errorLogCafePartage').css("display","block");
					var f = document.getElementById('errorLogCafePartage');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	$('#saveDejPartage').on('click', function() {
				
		var form = $('#dejForm').get(0);
		var formData = new FormData(form);// get the form data
		
		$('#encoursEnvoiDejPartage').css("display","block");
		var f = document.getElementById('encoursEnvoiDejPartage');
		f.innerHTML ="";
		f.innerHTML = '<i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom"></i><br>En cours d\'envoi. Merci de patienter ....';
	
		$.ajax({
			type: "POST",
			url: "php/email_partage.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				$('#encoursEnvoiDejPartage').css("display","none");
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="page-game.php";
				}else{
					$('#errorLogDejPartage').css("display","block");
					var f = document.getElementById('errorLogDejPartage');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	
    /*Image Sliders*/

    //Note. Every image slider must be placed within the timeout function.//
    //Image sliders put a lot of load on mobile devices and slow the performance of other animations//
    //But adding a timeout event, even for a microsecond gives a great boost in performance (41% boost to be exact)
    
    setTimeout(function() {
        //Simple Slider
        
        var owl = $('.simple-slider');
        owl.owlCarousel({
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            items:1,
            loop:true,
            margin:5,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true
        });
        
        //Coverpage Slider

        $('.coverpage-slider').owlCarousel({
            loop:true,
            margin:-2,
            nav:false,
            dots:true,
            items:1
        });

        //Demo Slider inside Quotes

        $('.demo-slider').owlCarousel({
            loop:true,
            margin:200,
            nav:false,
            autoHeight:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                }
            }
        });

        $('.next-demo').click(function() {$('.demo-slider').trigger('next.owl.carousel');}); 
        $('.prev-demo').click(function() {$('.demo-slider').trigger('prev.owl.carousel');});
        
        //Homepage Slider No Transitions
        $('.homepage-slider-no-transition').owlCarousel({
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            loop:true,
            margin:5,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1081:{
                    items:3
                }
            }
        });
        
        //Homepage Slider With Transition
        $('.homepage-slider-transition').owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'fadeOut',
            animateIn: 'fadein',
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            items:1
        });
                
        
        //Homepage Slider With Transition 2
        $('.homepage-slider-transition-2').owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'slideOutDown',
            animateIn: 'slideInUp',
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            items:1
        });
                
        
        //Homepage Slider With Transition 2
        $('.homepage-slider-transition-3').owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'rollOut',
            animateIn: 'rollIn',
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            items:1
        });
        
        $('.next-home-slider').click(function() {$('.homepage-slider-no-transition, .homepage-slider-transition, .homepage-slider-transition-2, .homepage-slider-transition-3, .homepage-slider-no-transition').trigger('next.owl.carousel');}); 
        $('.prev-home-slider').click(function() {$('.homepage-slider-no-transition, .homepage-slider-transition, .homepage-slider-transition-2, .homepage-slider-transition-3, .homepage-slider-no-transition').trigger('prev.owl.carousel');});
        
        
        //Staff Slider No Transition
        $('.staff-slider-no-transition').owlCarousel({
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });
        
        //Staff Slider With Transition
        $('.staff-slider-transition').owlCarousel({
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
		
		
		$('.leader-slider-no-transition').owlCarousel({
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
		
		$('.fame-slider-no-transition').owlCarousel({
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
        
        $('.next-staff-slider').click(function() {$('.staff-slider-no-transition, .staff-slider-transition, .leader-slider-no-transition').trigger('next.owl.carousel');}); 
        $('.prev-staff-slider').click(function() {$('.staff-slider-no-transition, .staff-slider-transition, .leader-slider-no-transition').trigger('prev.owl.carousel');});
        
		
		$('.next-fame-slider').click(function() {$('.fame-slider-no-transition').trigger('next.owl.carousel');}); 
        $('.prev-fame-slider').click(function() {$('.fame-slider-no-transition').trigger('prev.owl.carousel');});
        
        
        //Quote Slider No Transition
        $('.quote-slider-no-transition').owlCarousel({
            autoHeight:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });        
        
        //Quote Slider No Transition
        $('.quote-slider-transition').owlCarousel({
            autoHeight:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            lazyLoad:true,
            loop:true,
            margin:10,
            nav:false,
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        
        $('.next-quote-slider').click(function() {$('.quote-slider-no-transition, .quote-slider-transition').trigger('next.owl.carousel');}); 
        $('.prev-quote-slider').click(function() {$('.quote-slider-no-transition, .quote-slider-transition').trigger('prev.owl.carousel');});
        
        //Placing the Dots if Needed
        function slider_dots(){
            var dots_width = (-($('.owl-dots').width()/2));
            $('.owl-dots').css('position', 'absolute');
            $('.owl-dots').css('left', '50%');
            $('.owl-dots').css('margin-left', dots_width);   
        }      
        slider_dots();

    }, 1);

	//Detect if iOS WebApp Engaged and permit navigation without deploying Safari
	(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")
    
    //Fast Click - Removing 300ms delay when clicking for instant response time
    
    $(function() {
        FastClick.attach(document.body);
    });
        
    //Lazy Load | Preloading Image

    $(function() {
        $(".preload-image").lazyload({
            threshold : 200,
            effect : "fadeIn"
        });
        $("img.lazy").show().lazyload();
    });
    
    //Page Chapters Activation
    
    $('.show-page-chapters, .hide-chapters').click(function(){
       $('.page-chapters').toggleClass('page-chapters-active'); 
    });
    
    $('.page-chapters a').click(function(){
        $('.page-chapters a').removeClass('active-chapter');
        $(this).addClass('active-chapter');
    });
    
    //Countdown timer

	var endDate = "June 7, 2015 15:03:25";
    $(function() {
        $('.countdown-class').countdown({
            date: "June 7, 2087 15:03:26"
        });
    });
    
    //Pie Charts 
    
    if ($('body').hasClass('has-charts')){
        var pieData = [
            {	value: 25,	color: "#c0392b", highlight: "#c0392b", label: "Red"			},
            {	value: 20,	color: "#27ae60", highlight: "#27ae60",	label: "Green"			},
            {	value: 15,	color: "#f39c12", highlight: "#f39c12",	label: "Yellow"			},
            {	value: 30,	color: "#2980b9", highlight: "#34495e",	label: "Dark Blue"		}
        ];
        var barChartData = {
            labels : ["One","Two","Three","Four","Five", "Six"],
            datasets : [{
                fillColor : "rgba(0,0,0,0.1)",
                strokeColor : "rgba(0,0,0,0.2)",
                highlightFill: "rgba(0,0,0,0.25)",
                highlightStroke: "rgba(0,0,0,0.25)",
                data : [20,10,40,30,10, 80]
            }]
        }
        window.onload = function(){
            var pie_chart_1 = document.getElementById("generate-pie-chart").getContext("2d");
            window.pie_chart_1 = new Chart(pie_chart_1).Pie(pieData);

            var bar_chart_1 = document.getElementById("generate-bar-chart").getContext("2d");
            window.bar_chart_1 = new Chart(bar_chart_1).Bar(barChartData);
        };
    }
	    
    //Tabs
    
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('active-tab');
		$('.tab-content').slideUp(200);

		$(this).addClass('active-tab');
		$("#"+tab_id).slideToggle(200);
		
	})
    
    //Accordion
    
    $('.accordion').find('.accordion-toggle').click(function(){
        //Expand or collapse this panel
        $(this).next().slideDown(250);
        $('.accordion').find('i').removeClass('rotate-180');
        $(this).find('i').addClass('rotate-180');

        //Hide the other panels
        $(".accordion-content").not($(this).next()).slideUp(200);
    });
        
    //Classic Toggles
    
    $('.toggle-title').click(function(){
        $(this).parent().find('.toggle-content').slideToggle(200); 
        $(this).find('i').toggleClass('rotate-toggle');
        return false;
    });
    
    //Notifications
    
    $('.static-notification-close').click(function(){
       $(this).parent().slideUp(200); 
        return false;
    });    
    
    $('.tap-dismiss').click(function(){
       $(this).slideUp(200); 
        return false;
    });
    
    //Modal Launchers
    
    $('.modal-close').click(function(){return false;});
    
	$('.simple-modal').click(function() {
		$('.simple-modal-content').modal();
	});
	
	$('.reponse').click(function() {
		var id = $(this).data('id');
		$('#modal-reponse'+id).modal();
		$('#reponseTable'+id).DataTable({
			"iDisplayLength": 10,
			"sPaginationType": "simple",
			"aaSorting": [[ 0, 'asc' ]],
			"aLengthMenu": [[ 10, 30, 50, 100,-1], [15, 30, 50, 100, "Tous"]],
			"autoWidth": true,
			"dom": 'Bfrtip',
			"buttons": [
				'excel'
			]
		});
	});
    
	$('.logs').click(function() {
		var id = $(this).data('id');
		$('#modal-logs'+id).modal();
		$('#logsTable'+id).DataTable({
			"iDisplayLength": 10,
			"sPaginationType": "simple",
			"aaSorting": [[ 0, 'desc' ]],
			"aLengthMenu": [[ 10, 30, 50, 100,-1], [15, 30, 50, 100, "Tous"]],
			"autoWidth": true,
			"dom": 'Bfrtip',
			"buttons": [
				'excel'
			]
		});
	});
	
	$('.equipe').click(function() {
		var id = $(this).data('id');
		$('#modal-equipe'+id).modal();
		$('#equipe'+id).DataTable({
			"iDisplayLength": 10,
			"sPaginationType": "simple",
			"aaSorting": [[ 0, 'asc' ]],
			"aLengthMenu": [[ 10, 30, 50, 100,-1], [10, 30, 50, 100, "Tous"]],
			"autoWidth": true,
			"dom": 'Bfrtip',
			"buttons": [
				'excel'
			]
		});
		
	});	
	
	
	$('.inaccessible').click(function() {
		$('#inaccessible').modal();		
	});	
	
	
	//GAGNANT
	$('.simple-gagnant-modal').click(function() {
		
		$.modal.close();
		$('#simple-gagnant').modal();
		
		var egnime = $(this).data('egnime');
		var equipe = $(this).data('equipe');
		
		var nom = $(this).data('nom');
		
		$('#egnimeGagnant').val(egnime);
		$('#equipeGagnant').val(equipe);
		
		var f = document.getElementById('equipeGagnante');
		f.innerHTML ="";
		f.innerHTML = nom;
		
		$('.simple-gagnant-modal-content').modal();
		
	});
	
	$('.simple-gagnant-modal').click(function() {
		
		$.modal.close();
		$('.simple-gagnant-modal-content').modal();
		
		var egnime = $(this).data('egnime');
		var equipe = $(this).data('equipe');
		
		var nom = $(this).data('nom');
		
		$('#egnimeGagnant').val(egnime);
		$('#equipeGagnant').val(equipe);
		
		var f = document.getElementById('equipeGagnante');
		f.innerHTML ="";
		f.innerHTML = nom;
		
	});
	
	
	$('#genererGagnant').on('click', function() {
				
		var form = $('#gagnantForm').get(0);
		var formData = new FormData(form);// get the form data
		
		$.ajax({
			type: "POST",
			url: "php/gagnant.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLogGenerer').css("display","block");
					var f = document.getElementById('errorLog');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Une erreur est intervenue. Merci de recommencer</strong>";
				}
			}
		});

		
		return false;
	});
	
	
	$('.simple-mailGagnant-modal').click(function() {
		var egnime = $(this).data('egnime');
		var reponse = $(this).data('reponse');
		var description = $(this).data('description');
		var gagnant = $(this).data('gagnant');
		var asso = $(this).data('asso');
		var lot = $(this).data('lot');
		var equipe = $(this).data('equipe');
		
		$('#idEnigme').val(egnime);
		$('#idEquipe').val(equipe);
		$('#mystere').val(reponse);
		$('#explication').val(description);
		$('#bravo').val(gagnant);
		
		if(lot==0){
			$('#lot').val(asso);
		}else{
			$('#lot').val(asso+' - '+lot+'€');
		}
		
		$('.simple-mailGagnant-modal-content').modal();
		
	}); 
	
	$('#saveMailGagnant').on('click', function() {
		
		console.log("oui");
		
		var form = $('#mailGagnantForm').get(0);
		var formData = new FormData(form);// get the form data
		// on envoi formData vers mail.php
		
		$('#encoursEnvoi').css("display","block");
		var f = document.getElementById('encoursEnvoi');
		f.innerHTML ="";
		f.innerHTML = '<i class="fa fa-spinner fa-spin fa-2x fa-fw margin-bottom"></i><br>En cours d\'envoi. Merci de patienter ....';
					
	
		$.ajax({
			type: "POST",
			url: "php/email_gagnant.php",
			data : formData,
			processData: false,
			contentType: false,
			cache: false,
			success: function(data){
				console.log(data);
				$('#encoursEnvoi').css("display","none");
				if(data !="non"){
					$.modal.close();				
					document.location.href="admin-edit_entreprises.php?id="+data;
				}else{
					$('#errorLogMail').css("display","block");
					var f = document.getElementById('errorLogMail');
					f.innerHTML ="";
					f.innerHTML = "<strong class='color-red'>Merci de renseigner tous les champs.</strong>";
				}
			}
		});

		return false;
	});
	
	
	
	$('.indice-modal').click(function() {
		var id = $(this).data('id');
		console.log(id);
		$('#indice-modal-'+id).modal();
	});
    
    //Sharebox Settings
        
    $('.show-share-bottom, .show-share-box').click(function(){
        $('.share-bottom').toggleClass('active-share-bottom'); 
        $.modal.close()
        return false;
    });    
    
    $('.close-share-bottom').click(function(){
       $('.share-bottom').removeClass('active-share-bottom'); 
        return false;
    });
    
    //Fixed Notifications

    //top
    $('.close-top-notification').click(function(){
       $('.top-notification').slideUp(200);
        return false;
    });
    
    $('.show-top-notification-1').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.top-notification-1').slideDown(200);
    });    
    
    $('.show-top-notification-2').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.top-notification-2').slideDown(200);
    });    
    
    $('.show-top-notification-3').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.top-notification-3').slideDown(200);
    });    
    
    //bottom
    $('.close-bottom-notification').click(function(){
       $('.bottom-notification').slideUp(200);
        clearTimeout(notification_timer);
        return false;
    });
    
    $('.show-bottom-notification-1').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.bottom-notification-1').slideDown(200);
        return false;
    });    
    
    $('.show-bottom-notification-2').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.bottom-notification-2').slideDown(200);
        return false;
    });    
    
    $('.show-bottom-notification-3').click(function(){
        $('.top-notification, .bottom-notification, .timeout-notification').slideUp(200);
        $('.bottom-notification-3').slideDown(200);
        return false;
    });
    
    //Timeout
    
    $('.timer-notification').click(function(){
        var notification_timer;
        notification_timer = setTimeout(function(){ $('.timeout-notification').slideUp(250); },5000);
    });
    
    //Switches
    
    $('.switch-1').click(function(){
       $(this).toggleClass('switch-1-on'); 
        return false;
    });
    
    $('.switch-2').click(function(){
       $(this).toggleClass('switch-2-on'); 
        return false;
    });
    
    $('.switch-3').click(function(){
       $(this).toggleClass('switch-3-on'); 
        return false;
    });
    
    $('.switch, .switch-icon').click(function(){
        $(this).parent().find('.switch-box-content').slideToggle(200); 
        $(this).parent().find('.switch-box-subtitle').slideToggle(200);
        return false;
    });
    
    //Reminders & Checklists & Tasklists
    
    $('.reminder-check-square').click(function(){
       $(this).toggleClass('reminder-check-square-selected'); 
        return false;
    });    
    
    $('.reminder-check-round').click(function(){
       $(this).toggleClass('reminder-check-round-selected'); 
        return false;
    });
    
    $('.checklist-square').click(function(){
       $(this).toggleClass('checklist-square-selected');
        return false;
    });    
    
    $('.checklist-round').click(function(){
       $(this).toggleClass('checklist-round-selected');
        return false;
    });
    
    $('.tasklist-incomplete').click(function(){
       $(this).removeClass('tasklist-incomplete'); 
       $(this).addClass('tasklist-completed'); 
    });    
    
    $('.tasklist-item').click(function(){
       $(this).toggleClass('tasklist-completed'); 
    });
    
    //Activity Item Toggle
    
    $('.activity-item').click(function(){
       $(this).find('.activity-item-detail').slideToggle(200); 
    });
    
    //Detecting Mobiles//
    
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    
    if(isMobile.any()) {
        //Settings for all mobiles
        $('head').append('<link />');
    } 
    
    if( !isMobile.any() ){
        $('.show-blackberry, .show-ios, .show-windows, .show-android').hide(0);
        $('show-no-detection').show(0);
        
        $('#content').bind('mousewheel', function(event) {
          event.preventDefault();
          var scrollTop = this.scrollTop;
          this.scrollTop = (scrollTop + ((event.deltaY * event.deltaFactor) * -2));
          //console.log(event.deltaY, event.deltaFactor, event.originalEvent.deltaMode, event.originalEvent.wheelDelta);
        });
        $("#content").css("overflow-y","hidden");
    }
    
    if(isMobile.Android()) {
        $('.show-android').show(0);
        $('.show-blackberry, .show-ios, .show-windows').hide(0);
    }
        
    if(isMobile.BlackBerry()) {
        $('.show-blackberry').show(0);
        $('.show-android, .show-ios, .show-windows').hide(0);
    }
        
    if(isMobile.iOS()) {
        $('.show-ios').show(0);
        $('.show-blackberry, .show-android, .show-windows').hide(0);
    }
        
    if(isMobile.Windows()) {
        $('.show-windows').show(0);
        $('.show-blackberry, .show-ios, .show-android').hide(0);
    }
    
    $('.back-to-top-badge, .back-to-top').click(function() {
		$('#content').animate({
			scrollTop:0
		}, 500, 'easeInOutQuad');
		return false;
	});
    
    //Show Back To Home When Scrolling
        
    $('#content').on('scroll', function () {
        var total_scroll_height = $('#content')[0].scrollHeight
        var inside_header = ($(this).scrollTop() <= 150);
        var passed_header = ($(this).scrollTop() >= 0); //250
        var footer_reached = ($(this).scrollTop() >= (total_scroll_height - ($(window).height() +100 )));
        
        if (inside_header == true) {
            $('.back-to-top-badge').removeClass('back-to-top-badge-visible');
        } else if (passed_header == true)  {
            $('.back-to-top-badge').addClass('back-to-top-badge-visible');
        } 
        if (footer_reached == true){            
            $('.back-to-top-badge').removeClass('back-to-top-badge-visible');
        }
    });
    
    //Make contianer fullscreen//    
         
    function create_paddings(){
        var no_padding = $(window).width();
        function mobile_paddings(){
            $('.content').css('padding-left', '20px');   
            $('.content').css('padding-right', '20px');   
            $('.container-fullscreen, .image-fullscreen').css('margin-left', '-21px');
            $('.container-fullscreen, .image-fullscreen').css('width', no_padding +2);    
        }
        
        function tablet_paddings(){
            $('.content').css('padding-left', '50px');   
            $('.content').css('padding-right', '50px');  
            $('.container-fullscreen, .image-fullscreen').css('margin-left', '-51px');
            $('.container-fullscreen, .image-fullscreen').css('width', no_padding +2);              
        }
        
        if($(window).width() < 766){
            mobile_paddings()
        }        
        if($(window).width() > 766){
            tablet_paddings()
        }
    }

    $(window).resize(function() {  
        create_paddings();
    });
    
    create_paddings();
    
    //Morph Headings
    
    $(".infinite-text").Morphext({
        // The [in] animation type. Refer to Animate.css for a list of available animations.
        animation: "flipInX",
        // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
        separator: "|",
        // The delay between the changing of each phrase in milliseconds.
        speed: 2000,
        complete: function () {
            // Called after the entrance animation is executed.
        }
    });
    
    //Set inputs to today's date by adding class set-day
    
    var set_input_now = new Date();
    var set_input_month = (set_input_now.getMonth() + 1);               
    var set_input_day = set_input_now.getDate();
    if(set_input_month < 10) 
        set_input_month = "0" + set_input_month;
    if(set_input_day < 10) 
        set_input_day = "0" + set_input_day;
    var set_input_today = set_input_now.getFullYear() + '-' + set_input_month + '-' + set_input_day;
    $('.set-today').val(set_input_today);
        
    
    //Portfolios and Gallerties
    
    $('.adaptive-one').click(function(){
        $('.portfolio-switch').removeClass('active-adaptive');
        $(this).addClass('active-adaptive');
        $('.portfolio-adaptive').removeClass('portfolio-adaptive-two portfolio-adaptive-three');
        $('.portfolio-adaptive').addClass('portfolio-adaptive-one');
        return false;
    });    
    
    $('.adaptive-two').click(function(){
        $('.portfolio-switch').removeClass('active-adaptive');
        $(this).addClass('active-adaptive');
        $('.portfolio-adaptive').removeClass('portfolio-adaptive-one portfolio-adaptive-three');
        $('.portfolio-adaptive').addClass('portfolio-adaptive-two'); 
        return false;
    });    
    
    $('.adaptive-three').click(function(){
        $('.portfolio-switch').removeClass('active-adaptive');
        $(this).addClass('active-adaptive');
        $('.portfolio-adaptive').removeClass('portfolio-adaptive-two portfolio-adaptive-one');
        $('.portfolio-adaptive').addClass('portfolio-adaptive-three'); 
        return false;
    });
    
    //Wide Portfolio
    
    $('.show-wide-text').click(function(){
        $(this).parent().find('.wide-text').slideToggle(200); 
        return false;
    });
    
    $('.portfolio-close').click(function(){
       $(this).parent().parent().find('.wide-text').slideToggle(200);
        return false;
    });
    
    $('.show-gallery, .show-gallery-1, .show-gallery-2, .show-gallery-3, .show-gallery-4, .show-gallery-5, .add-gallery a').swipebox();
    
    function apply_gallery_justification(){
        var screen_widths = $(window).width();
        if( screen_widths < 768){ 
            $('.gallery-justified').justifiedGallery({
                rowHeight : 70,
                maxRowHeight : 370,
                margins : 5,
                fixedHeight:false
            });
        };

        if( screen_widths > 768){
            $('.gallery-justified').justifiedGallery({
                rowHeight : 150,
                maxRowHeight : 370,
                margins : 5,
                fixedHeight:false
            });
        };
    };
    apply_gallery_justification();
    
    //Filterable Gallery
    
    var selectedClass = "";
    $(".filter-category").click(function(){
        $('.portfolio-filter-categories a').removeClass('selected-filter');
        $(this).addClass('selected-filter');
        selectedClass = $(this).attr("data-rel");
        $(".portfolio-filter-wrapper").show(250);
        $(".portfolio-filter-wrapper div").not("."+selectedClass).delay(100).hide(250);
        setTimeout(function() {
            $("."+selectedClass).show(250);
            $(".portfolio-filter-wrapper").show(250);
        }, 0);
    });
    
    //Fullscreen Map
    
    $('.map-text, .map-overlay').click(function(){
       $('.map-text, .map-overlay').fadeOut(200); 
       $('.deactivate-map').fadeIn(200); 
    });    
    
    $('.deactivate-map').click(function(){
       $('.map-text, .map-overlay').fadeIn(200); 
       $('.deactivate-map').fadeOut(200); 
    });
    
    function generate_map(){
        var map_width = $(window).width();
        var map_height = $(window).height();
        
        $('.map-fullscreen iframe').css('width', map_width);
        $('.map-fullscreen iframe').css('height', map_height);
    };
    generate_map();
    
    //-------------------Generate Cover Screen Elements--------------------//
    //Global Settings for Fullscreen Pages, PageApps and Coverscreen Slider//
    
    function align_cover_elements(){
        var cover_width = $(window).width();
        var cover_height = $(window).height();
        var cover_vertical = -($('.cover-center').height())/2;
        var cover_horizontal = -($('.cover-center').width())/2;
        
        $('.cover-screen').css('width', cover_width);
        $('.cover-screen').css('height', cover_height);
        $('.cover-screen .overlay').css('width', cover_width);
        $('.cover-screen .overlay').css('height', cover_height);
        
        $('.cover-center').css('margin-left', cover_horizontal);      
        $('.cover-center').css('margin-top', cover_vertical + 30);     
        $('.cover-left').css('margin-top', cover_vertical);   
        $('.cover-right').css('margin-top', cover_vertical);           
    };
    align_cover_elements();        
    
    //Resize Functions//
    
    $(window).resize(function(){
        apply_gallery_justification();  
        align_cover_elements();
        generate_map();
    });
    
    
    //Add To HomeScreen
    /*
    addToHomescreen({
        skipFirstVisit: false,
        detectHomescreen:true,
        maxDisplayCount: 140,
        startDelay:1,
        autostart:true,
        lifespan:0
    });
	*/
    
    //Swipebox Image Gallery//
    //SVG Usage is not recommended due to poor compatibility with older Android / Windows Mobile Devices//
    
	$(".swipebox").swipebox({
		useCSS : true, 
		hideBarsDelay : 3000 // 0 to always show caption and action bar
	});
    
    //Sidebar Activation for pages with proper functions
    
    if($('body').hasClass('dual-sidebar')){   dual_sidebar();   }
    if($('body').hasClass('left-sidebar')){   left_sidebar();   }
    if($('body').hasClass('right-sidebar')){  right_sidebar();  }
    if($('body').hasClass('no-sidebar')){     no_sidebar();     }
        
    function dual_sidebar(){
        var $div = $('<div />').appendTo('body');
        $div.attr('id', 'footer-fixed');
        $div.attr('class', 'not-active');
        var snapper = new Snap({
            element: document.getElementById('content'),
            elementMirror: document.getElementById('header-fixed'),
            elementMirror2: document.getElementById('footer-fixed'),
            disable: 'none',
            tapToClose: true,
            touchToDrag: true,
            maxPosition: 266,
            minPosition: -266
        });
        $('.close-sidebar').click(function(){
			$('.open-left-sidebar').css('display', 'block');
			snapper.close();});
        $('.open-left-sidebar').click(function() {
            //$(this).toggleClass('remove-sidebar');
            if( snapper.state().state=="left" ){
                snapper.close();
				$('.open-left-sidebar').css('display', 'block');
            } else {
                snapper.open('left');
				$('.open-left-sidebar').css('display', 'none');
            }
            return false;
        });	
        $('.open-right-sidebar').click(function() {
            //$(this).toggleClass('remove-sidebar');
            if( snapper.state().state=="right" ){
                snapper.close();
            } else {
                snapper.open('right');
            }
            return false;
        });
        snapper.on('open', function(){$('.back-to-top-badge').removeClass('back-to-top-badge-visible');});
    };    
    
    function left_sidebar(){
        var $div = $('<div />').appendTo('body');
        $div.attr('id', 'footer-fixed');
        $div.attr('class', 'not-active');
        var snapper = new Snap({
            element: document.getElementById('content'),
            elementMirror: document.getElementById('header-fixed'),
            elementMirror2: document.getElementById('footer-fixed'),
            disable: 'right',
            tapToClose: false,
            touchToDrag: false,
            maxPosition: 266,
            minPosition: -266
        });  
        $('.close-sidebar').click(function(){
			$('.open-left-sidebar').css('display', 'block');
			snapper.close();
		});
		
        $('.open-left-sidebar').click(function() {
            if( snapper.state().state=="left" ){
                snapper.close();
				$(this).css('display', 'block');

            } else {
                snapper.open('left');
				$(this).css('display', 'none');

            }
            return false;
        });	
        snapper.on('open', function(){$('.back-to-top-badge').removeClass('back-to-top-badge-visible');});
    };    
    
    function right_sidebar(){
        var $div = $('<div />').appendTo('body');
        $div.attr('id', 'footer-fixed');
        $div.attr('class', 'not-active');
        var snapper = new Snap({
            element: document.getElementById('content'),
            elementMirror: document.getElementById('header-fixed'),
            elementMirror2: document.getElementById('footer-fixed'),
            disable: 'left',
            tapToClose: true,
            touchToDrag: true,
            maxPosition: 266,
            minPosition: -266
        });     
        $('.close-sidebar').click(function(){snapper.close();});
        $('.open-right-sidebar').click(function() {
            //$(this).toggleClass('remove-sidebar');
            if( snapper.state().state=="right" ){
                snapper.close();
            } else {
                snapper.open('right');
            }
            return false;
        });
        snapper.on('open', function(){$('.back-to-top-badge').removeClass('back-to-top-badge-visible');});
    };     
        
    function no_sidebar(){
        var snapper = new Snap({
            element: document.getElementById('content'),
            elementMirror: document.getElementById('header-fixed'),
            elementMirror2: document.getElementById('footer-fixed'),
            disable: 'none',
            tapToClose: false,
            touchToDrag: false
        });        
    }; 
        
});
