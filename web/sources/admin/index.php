<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_admin.php');

$message ="";
if(isset($_GET['error']) && $_GET['error'] ==1){
	$message ="Pour des raisons de sécurité, vous ne pouvez accéder automatiquement au site qu'à partir du dernier email reçu de WAIT.IT";
}else if(isset($_GET['error']) && $_GET['error'] ==2){
	$message ="Vous avez été déconnecté. Merci de vous reconnecter pour accéder à votre espace.";
}

?>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<link rel="icon" type="image/png" href="images/splash/android-chrome-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
<link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
<link rel="shortcut icon" href="images/splash/favicon.ico" type="image/x-icon" /> 
    
<title>ADMINISTRATION REPUBLIKE</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="styles/style.css"           rel="stylesheet" type="text/css">
<link href="styles/framework.css"       rel="stylesheet" type="text/css">
<link href="styles/font-awesome.css"    rel="stylesheet" type="text/css">
<link href="styles/animate.css"         rel="stylesheet" type="text/css">

<!--<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jqueryui.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link href="https://www.waitit.io/moncompte/scripts/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="https://www.waitit.io/moncompte/scripts/DataTables/js/jquery.dataTables.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script> 
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script> 
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> 
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>  
  
<script type="text/javascript" src="scripts/framework-plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

</head>

<body class="no_sidebar"> 

<div id="preloader">
	<div id="status">
        <div class="preloader-logo"></div>
        <h3 class="center-text">Chargement...</h3>
    </div>
</div>
    
<div class="gallery-fix"></div> <!-- Important for all pages that have galleries or portfolios -->
    
<div id="header-fixed" class="header-style-1">
    <a class="header-logo" href="https://www.waitit.io"><img src="images/logo-republilke.png" width="30" alt="img"></a>
</div>
    
            
<div class="all-elements">
    <div class="snap-drawers">
        <div id="content" class="snap-content">
            <div class="content">
            <div class="header-clear"></div>
            <!--Page content goes here, fixed elements go above the all elements class-->        
                
                <div class="decoration"></div>
                
                <div id="page-login" class="page-login full-bottom">
                	<p class="center-text">ADMINISTRATION</p>
                	<form  method="post" action="" id="logFormP" name="logFormP" enctype="multipart/form-data">
                        
                        <div class="login-input">
                            <i class="fa fa-user"></i>
                            <input autocomplete="off" type="text" name="email" id="email" value="Email" onfocus="if (this.value=='Email') this.value = ''" onblur="if (this.value=='') this.value = 'Email'">
                        </div>
                        <div class="login-password">
                            <i class="fa fa-lock"></i>
                            <input type="password" autocomplete="off" name="password" id="password" value="password" onfocus="if (this.value=='password') this.value = ''" onblur="if (this.value=='') this.value = 'password'">
                        </div>
                        <!--<a href="javascript:void(0)" id="mdpEnvoi" class="login-forgot" style="width:100%;"><i class="fa fa-eye"></i>Mot de passe oublié</a>
                        <div class="clear"></div>-->
                        
                        <a href="javascript:void(0)" id="logCompte" class="login-button button button-small button-black button-fullscreen">Connexion</a>
                        <div class="clear"></div>
                        <div id="errorLog"></div>
                        <div class="decoration"></div>
                    </form>
                </div>
                
                <div id="page-oublie" class="page-login full-bottom" style="display:none;">
                    <form  method="post" action="" id="logFormP2" name="logFormP2" enctype="multipart/form-data">
                        <a href="#" class="page-login-logo"></a>
                        <div class="login-input">
                            <i class="fa fa-user"></i>
                            <input type="text" autocomplete="off" name="email" id="email" value="Email" onfocus="if (this.value=='Email') this.value = ''" onblur="if (this.value=='') this.value = 'Email'">
                        </div>
                        <a href="javascript:void(0)" id="mdpAnnule" class="login-forgot"><i class="fa fa-close"></i>Annuler</a>
                        <div class="clear"></div>
                        <a href="javascript:void(0)" id="envoiMail" class="login-button button button-small button-blue button-fullscreen full-bottom">Envoyer</a>
                        <div class="clear"></div>
                        <div id="errorLog22"></div>
                        <div class="decoration"></div>
                     </form>   
                </div>
                
                <div class="footer">
                    <p class="center-text">REPUBLIKE © 2018</p>
                </div>
                                
            <!-- End of entire page content-->
            
            <?php if($message  !='' ){ ?>
                <div class="simple-client-modal-content modal-content rounded-modal" id="popup" style="height:200px;">
                    <div class="login-modal-wrapper">
                        <p class="color-red-light center-text" style="margin-bottom:0;"><?php echo $message; ?></p>
                        <a href="javascript:void(0)" class="button-dark login-close modal-close" style="float:none; margin: 20px auto 0px;">FERMER</a>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>

            </div> 
        </div>
    </div>  
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a>
    
<script>

	if($('.simple-client-modal-content').length>0){
		mdpLog2();
	}
	    
    function mdpLog2(){
        $('#popup').modal();      
    }; 
</script>

</div>
</body>