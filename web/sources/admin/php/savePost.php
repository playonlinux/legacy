<?php
require_once ('mysql.inc.php');
require_once ('funct_admin.php');

securePost();
secureGet();

$now = date('Y-m-d');

if(isset($_SESSION['administration']) && $_SESSION['administration'] !='' ){
		
	$securite =$_SESSION['administration'];
	
	$sth = $dbh->prepare("SELECT * FROM `bl_admin` WHERE `password` = :securite limit 0,1");
	$sth->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
				
		$type = $_GET['type'];
		
		if ($type =='supp'){
			
			$id = intval($_POST['id_post_supp']);
			$id_battle = intval($_POST['id_battle']);
			
			$deleteReq = $dbh->prepare("DELETE FROM `bl_battle_posts` WHERE `id` = :id");
			$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReq->execute();
			
			$deleteReqLike = $dbh->prepare("DELETE FROM `bl_likes_post` WHERE `posts` = :id");
			$deleteReqLike->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReqLike->execute();
			
			echo $id_battle;
			
		}else{
			echo 'non';
		}
			
	}
}

?>