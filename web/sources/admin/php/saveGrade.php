<?php
require_once ('mysql.inc.php');
require_once ('funct_admin.php');

securePost();
secureGet();

$now = date('Y-m-d');

if(isset($_SESSION['administration']) && $_SESSION['administration'] !='' ){
		
	$securite =$_SESSION['administration'];
	
	$sth = $dbh->prepare("SELECT * FROM `bl_admin` WHERE `password` = :securite limit 0,1");
	$sth->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
				
		$type = $_GET['type'];
		
		if ($type =='ajout'){
			
			/*$name = $_POST['name'];
			$color = $_POST['color'];
			$min = $_POST['min'];
			$max = $_POST['max'];
			$statut = $_POST['statut'];
			$id_grade = $_POST['id'];
			
			$insertReq = $dbh->prepare("INSERT INTO `bl_categories` (`name_fr`, `name_en`, `name_es`,`statut`) 
			VALUES (:name_fr, :name_en, :name_es, :statut)");
			$insertReq->bindParam(':name_fr', $name_fr, PDO::PARAM_STR);
			$insertReq->bindParam(':name_en', $name_fr, PDO::PARAM_STR);
			$insertReq->bindParam(':name_es', $name_fr, PDO::PARAM_STR);
			$insertReq->bindParam(':statut', $statut, PDO::PARAM_STR);
			
			$insertReq->execute();
			
			echo "ok";*/
				
		}else if ($type =='update'){
			
			$namefr = $_POST['namefr'];
			$nameen = $_POST['nameen'];
			$color = $_POST['color'];
			$min = $_POST['min'];
			$max = $_POST['max'];
			$statut = $_POST['statut'];
			$id_grade = $_POST['id'];
			
			$updateReq = $dbh->prepare("UPDATE `bl_grade` SET `titre_fr` = :namefr,`titre_en` = :nameen,`points`= :minimum,`max`= :maximum,`color`= :color,`statut`= :statut WHERE id=:id_grade");
			$updateReq->bindParam(':id_grade', $id_grade, PDO::PARAM_STR);
			$updateReq->bindParam(':namefr', $namefr, PDO::PARAM_STR);
			$updateReq->bindParam(':nameen', $nameen, PDO::PARAM_STR);
			$updateReq->bindParam(':color', $color, PDO::PARAM_STR);
			$updateReq->bindParam(':minimum', $min, PDO::PARAM_STR);
			$updateReq->bindParam(':maximum', $max, PDO::PARAM_STR);
			$updateReq->bindParam(':statut', $statut, PDO::PARAM_STR);
			$updateReq->execute();
			
			echo "ok";
				
		}else if ($type =='supp'){
			
			$id = intval($_POST['id_grade_supp']);
			
			$deleteReq = $dbh->prepare("DELETE FROM `bl_grade` WHERE `id`= :id");
			$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReq->execute();
			
			echo "ok";
			
		}else{
			echo 'non';
		}
			
	}
}

?>