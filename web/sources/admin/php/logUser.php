<?php
require_once ('mysql.inc.php');
require_once ('funct_admin.php');

securePost();
secureGet();

$mail= $_POST['email'];
$password= $_POST['password'];

if ($password !='' && $mail != ''){
	
	$sth = $dbh->prepare('SELECT * FROM `bl_admin` WHERE email = :mail limit 0,1');
	$sth->bindParam(':mail', $mail, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row = $sth->fetch(PDO::FETCH_OBJ);
		$passRecup=$row->password;
		
		if (password_verify($password, $passRecup)) {
			
			echo 'admin';
			
			$_SESSION['administration'] =$passRecup;
			$_SESSION['type'] ='admin';
			
			
		}else{
			echo "non";
		}
	
	}else{
		
		echo 'non';

	}

}else{
	echo 'non';
}
	
?>