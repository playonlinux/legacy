<?php


function securePost() {
	if (isset($_POST)) {
	
		foreach ($_POST as $key => $value) {
			$_POST[$key] = htmlspecialchars(trim($value));
		}
	}
}

function secureGet() {
	if (isset($_GET)) {
	
		foreach ($_GET as $key => $value) {
			$_GET[$key] = htmlspecialchars(trim($value));
		}
	}
}

function name_user($id, $dbh) {
	
	$sth = $dbh->prepare("SELECT * FROM `bl_user` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$nom=$row_userResult->nom;	
		$prenom=$row_userResult->prenom;		
	}
		
	 
	return $nom.' '.$prenom;
}

/* BATTLE*/
function name_battle($id, $dbh) {
	
	$sth = $dbh->prepare("SELECT title FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$name = $row_userResult->title;
	}
	
	return  $name;
}

function url_battle($id, $dbh) {
	$url="";
	$sth = $dbh->prepare("SELECT url FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$url = $row_userResult->url;
	}
	
	return  $url;
}


/* POST */
function info_post($id, $dbh) {
	
	$sth = $dbh->prepare("SELECT * FROM `bl_battle_posts` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$token=$row_userResult->token;
		$user=$row_userResult->user;			
	}
		
	
	return  array($token, $user);
}

function photo_post_mail($id, $dbh) {
	
	$imagePartage='';
	$sth = $dbh->prepare("SELECT * FROM bl_battle_posts WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		
		$post=$row_userResult->post;
		$type=$row_userResult->type;
		
		if($type ==1){ 
			$imagePartage = ''.$urlSite.'/'.$post;
		}
    
		if($type ==2){ 
			
			list($video, $image) = video($post);
		
			if($image ==''){
				$imagePartage = ''.$urlSite.'/image/partage_video_republike.jpg';
			}else{ 
				$imagePartage = $image; }
		}
		
		if($type ==3){ 
			$imagePartage = ''.$urlSite.'/image/partage_texte_republike.jpg';
		}
                    					
	}
	
	return  $imagePartage;
}

function video($video) {
	
	$lien = "";
	$image = "";
	
	//echo $video;
	
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match)) {
		$video_id = $match[1];
		$video = 'https://www.youtube-nocookie.com/embed/'.$video_id.'?rel=0&amp;showinfo=0&amp;origin=https://www.republike.io';
		$image = "https://i.ytimg.com/vi/".$video_id."/hqdefault.jpg";
	}
	
	if(preg_match("@^(?:https://dai.ly/)?([^/]+)@i", $video))
	{
		$video = str_replace("https://dai.ly/", "https://www.dailymotion.com/embed/video/", $video);
	}
	
	if(preg_match("@^(?:https://www.dailymotion.com/video/)?([^/]+)@i", $video))
	{
		$video = str_replace("https://www.dailymotion.com/video/", "https://www.dailymotion.com/embed/video/", $video);
	}
	
	return array($video, $image); 
	//$video;
}


/* COMMENT */
function info_comment($id, $dbh) {
	
	$sth = $dbh->prepare("SELECT * FROM `bl_comments_post` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$message=$row_userResult->message;
		$user=$row_userResult->user;			
	}
		
	
	return  array($message, $user);
}



function no_special_character_v2($chaine){
 
    //  les accents
    $chaine=trim($chaine);
    /*$chaine= strtr($chaine,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ","aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
 */
  $chaine = str_replace(	array(
								'à', 'â', 'ä', 'á', 'ã', 'å',
								'î', 'ï', 'ì', 'í', 
								'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
								'ù', 'û', 'ü', 'ú', 
								'é', 'è', 'ê', 'ë', 
								'ç', 'ÿ', 'ñ', 'ý',
							),
							array(
								'a', 'a', 'a', 'a', 'a', 'a', 
								'i', 'i', 'i', 'i', 
								'o', 'o', 'o', 'o', 'o', 'o', 
								'u', 'u', 'u', 'u', 
								'e', 'e', 'e', 'e', 
								'c', 'y', 'n', 'y',
							),
							$chaine
						);
		$chaine = str_replace(	array(
								'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
								'Î', 'Ï', 'Ì', 'Í', 
								'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
								'Ù', 'Û', 'Ü', 'Ú', 
								'É', 'È', 'Ê', 'Ë', 
								'Ç', 'Ÿ', 'Ñ', 'Ý',
							),
							array(
								'A', 'A', 'A', 'A', 'A', 'A', 
								'I', 'I', 'I', 'I', 
								'O', 'O', 'O', 'O', 'O', 'O', 
								'U', 'U', 'U', 'U', 
								'E', 'E', 'E', 'E', 
								'C', 'Y', 'N', 'Y',
							),
							$chaine
						);
    //  les caracètres spéciaux (aures que lettres et chiffres en fait)
    $chaine = preg_replace('/([^.a-z0-9]+)/i', '-', $chaine);
    $chaine = strtolower($chaine);
 
    return $chaine;
 
}



?>