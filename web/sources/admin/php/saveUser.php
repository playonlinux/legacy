<?php
require_once ('mysql.inc.php');
require_once ('funct_admin.php');

securePost();
secureGet();

$now = date('Y-m-d');

if(isset($_SESSION['administration']) && $_SESSION['administration'] !='' ){
		
	$securite =$_SESSION['administration'];
	
	$sth = $dbh->prepare("SELECT * FROM `bl_admin` WHERE `password` = :securite limit 0,1");
	$sth->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
				
		$type = $_GET['type'];
		
		if ($type =='supp'){
			
			$id = intval($_POST['id_user_supp']);
			
			//desactivation compte
			$datesupp= date("Y-m-d");
			$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= '', `statut`='3', `date_supp`=:datesupp WHERE id = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->bindParam(':datesupp', $datesupp, PDO::PARAM_STR);
			$updateReq->execute();
			
			//desactivation battle
			$updateReq = $dbh->prepare("UPDATE `bl_battles` SET `statut`='3' WHERE user = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
			
			//desactivation post
			$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `statut`='2' WHERE user = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
						
			//selection des likes posts + desactivation like
			$postResult= $dbh->prepare("SELECT * FROM `bl_battle_posts` WHERE `user` = :id and `statut`='2'");
			$postResult->bindParam(':id', $id, PDO::PARAM_STR);
			$postResult->execute();
			
			if ($postResult->rowCount() > 0) {
				
				while ( $row_postResult = $userResult->fetch(PDO::FETCH_OBJ) ){ 
					$updateReq = $dbh->prepare("UPDATE `bl_likes_post` SET `statut`='2' WHERE user = :id ");
					$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
					$updateReq->execute();
				}
				
			}
			
			//desactivation amis
			$updateReq = $dbh->prepare("UPDATE `bl_user_friend` SET `statut`=2 WHERE (user=:id or friend = :id)");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
			
			//delete demande amis
			$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE (user=:id or qui = :id) and statut =1");
			$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReq->execute();
			
			echo "ok";
			
		}else if ($type =='active'){
			
			$id = intval($_POST['id_user_active']);
			
			//desactivation compte
			$datesupp= date("Y-m-d");
			$updateReq = $dbh->prepare("UPDATE `bl_user` SET `statut`='2', `date_supp`=null WHERE id = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
			
			//desactivation battle
			$updateReq = $dbh->prepare("UPDATE `bl_battles` SET `statut`='1' WHERE user = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
			
			//desactivation post
			$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `statut`='1' WHERE user = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
						
			//selection des likes posts + desactivation like
			$postResult= $dbh->prepare("SELECT * FROM `bl_battle_posts` WHERE `user` = :id and `statut`='1'");
			$postResult->bindParam(':id', $id, PDO::PARAM_STR);
			$postResult->execute();
			
			if ($postResult->rowCount() > 0) {
				
				while ( $row_postResult = $userResult->fetch(PDO::FETCH_OBJ) ){ 
					$updateReq = $dbh->prepare("UPDATE `bl_likes_post` SET `statut`='1' WHERE user = :id ");
					$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
					$updateReq->execute();
				}
				
			}
			
			//desactivation amis
			$updateReq = $dbh->prepare("UPDATE `bl_user_friend` SET `statut`=1 WHERE (user=:id or friend = :id)");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
			
			echo "ok";
			
		}else{
			echo 'non';
		}
			
	}
}

?>