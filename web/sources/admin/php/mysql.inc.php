<?php

ini_set("session.cookie_httponly", 1);
if (!isset($_SESSION)) {
    session_set_cookie_params(0, '/', '.' . $_SERVER['HTTP_HOST'], true, true);
    session_start();
}

$nameSite = "REPUBLIKE";
// $urlSite ="https://www.republike.io/";
$urlSite = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];

// DEV
if ($_SERVER['HTTP_HOST'] === 'preprod.republike.io') {
    // PREPROD
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'aj264163-001.privatesql';
    $db_name = 'republikedev18';
    $port = '35660';
} elseif ($_SERVER['HTTP_HOST'] === 'republike.io') {
    //PROD
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'aj264163-001.privatesql';
    $db_name = 'republike18';
    $port = '35660';
} else {
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'db';
    $db_name = 'republike18';
    $port = '3306';
}
try {
    $dbh = new PDO('mysql:host=' . $servername . ';port=' . $port . ';dbname=' . $db_name, $db_username, $db_password);
    $dbh->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET NAMES utf8");
} catch (PDOException $e) {
    die('Une erreur MySQL est arrivée: ' . $e->getMessage());
}
