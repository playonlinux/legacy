<?php
if(isset($_SESSION['administration']) && $_SESSION['administration'] !='' ){
	
	$securite =$_SESSION['administration'];
	
	$user = $dbh->prepare("SELECT * FROM `bl_admin` WHERE `password` = :securite limit 0,1");
	$user->bindParam(':securite', $securite, PDO::PARAM_STR);
	$user->execute();
	
	if ($user->rowCount() > 0) {
		
		$row_userResult = $user->fetch(PDO::FETCH_OBJ);
	
		$id_user = $row_userResult->id;
		$nom=$row_userResult->name;
		$email=$row_userResult->email;
			
	}else{
		session_start();
		session_unset(); 
		session_destroy(); 
		header('location: ./index.php');
	}
	
}else{
	session_start();
	session_unset(); 
	session_destroy(); 
	header('location: ./index.php');
}
?>