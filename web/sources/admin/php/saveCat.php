<?php
require_once ('mysql.inc.php');
require_once ('funct_admin.php');

securePost();
secureGet();

$now = date('Y-m-d');

if(isset($_SESSION['administration']) && $_SESSION['administration'] !='' ){
		
	$securite =$_SESSION['administration'];
	
	$sth = $dbh->prepare("SELECT * FROM `bl_admin` WHERE `password` = :securite limit 0,1");
	$sth->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
				
		$type = $_GET['type'];
		
		if ($type =='ajout'){
			
			$name_fr = $_POST['name_fr'];
			/*$name_en = $_POST['name_en'];
			$name_es = $_POST['name_es'];*/
			$statut = $_POST['statut'];
			
			$url_annonce =  trim(strtolower(str_replace(" ","-",no_special_character_v2($name_fr))));
			
			$insertReq = $dbh->prepare("INSERT INTO `bl_categories` (`name_fr`, `url_fr`, `name_en`, `url_en`, `name_es`, `statut`) 
			VALUES (:name_fr, :url_fr, :name_en, :url_en, :name_es, :statut)");
			
			$insertReq->bindParam(':name_fr', $name_fr, PDO::PARAM_STR);
			$insertReq->bindParam(':name_en', $name_fr, PDO::PARAM_STR);
			$insertReq->bindParam(':name_es', $name_fr, PDO::PARAM_STR);
			
			$insertReq->bindParam(':url_fr', $url_annonce, PDO::PARAM_STR);
			$insertReq->bindParam(':url_en', $url_annonce, PDO::PARAM_STR);
			
			$insertReq->bindParam(':statut', $statut, PDO::PARAM_STR);
			
			$insertReq->execute();
			$id_cat = $dbh->lastInsertId();
			
			$dossierEnregistrementImg = "images/cat/";
			// On récupére l'image de la vignette
			// Chemin de l'image par défaut
			$name_file = 'avatar.jpg';
			
			if(!empty($_FILES['image']['name']) && !empty($_FILES['image']['type']))
			{
				
				$tmp_file = $_FILES['image']['tmp_name'];
				
				$name_file = $_FILES['image']['name']; 
				$size_file = $_FILES['image']['size']; 
				$ext = end(explode('.',$name_file));
				
				$aleatoire = md5(time());
				
				$name_file = $aleatoire.'.'.rand(5, 9999).'.'.$ext;
				$name_file = str_replace(" ","-",$name_file);
				
				$cheminfichier = 'images/cat/'.$name_file;
				
				$type = mime_content_type($_FILES['image']['tmp_name']);
				$allowed_mime_types = array('image/gif','image/jpeg','image/png','image/bmp');
				
				$legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");
				
				$legalSize = "2000000";
				
				if(in_array($type, $allowed_mime_types) && ($size_file !=0 && $size_file < $legalSize) && in_array($ext, $legalExtensions)){
									
					if ( 0 < $_FILES['image']['error'] ) {
					
						echo 'Error: ' . $_FILES['image']['error'] . '<br>';
					}else {
						if(!move_uploaded_file($_FILES['image']['tmp_name'], '../../images/cat/'.$name_file)){
							echo "non";
						}else{
							$updateReq = $dbh->prepare("UPDATE `bl_categories` SET `visuel`= :cheminfichier WHERE `id` ='$id_cat'");
							$updateReq->bindParam(':cheminfichier', $cheminfichier, PDO::PARAM_STR);
							$rs = $updateReq->execute();
						}
					}
					
				}		
			}
		
			
			echo "ok";
				
		}else if ($type =='update'){
			
			$name_fr = $_POST['name_fr'];
			$statut = $_POST['statut'];
			$id_cat = $_POST['id'];
			
			$updateReq = $dbh->prepare("UPDATE `bl_categories` 
			SET `name_fr`= :name_fr, `name_en`= :name_en, `name_es`= :name_es, `statut`= :statut
			WHERE `id` =:id_cat");
			$updateReq->bindParam(':id_cat', $id_cat, PDO::PARAM_STR);
			$updateReq->bindParam(':name_fr', $name_fr, PDO::PARAM_STR);
			$updateReq->bindParam(':name_en', $name_fr, PDO::PARAM_STR);
			$updateReq->bindParam(':name_es', $name_fr, PDO::PARAM_STR);
			$updateReq->bindParam(':statut', $statut, PDO::PARAM_STR);
			$updateReq->execute();
			
			$dossierEnregistrementImg = "images/cat/";
			// On récupére l'image de la vignette
			// Chemin de l'image par défaut
			$name_file = 'avatar.jpg';
			
			if(!empty($_FILES['image']['name']) && !empty($_FILES['image']['type']))
			{
				$tmp_file = $_FILES['image']['tmp_name'];
				
				$name_file = $_FILES['image']['name']; 
				$size_file = $_FILES['image']['size']; 
				$ext = end(explode('.',$name_file));
				
				$aleatoire = md5(time());
				
				$name_file = $aleatoire.'.'.rand(5, 9999).'.'.$ext;
				$name_file = str_replace(" ","-",$name_file);
				
				$cheminfichier = 'images/cat/'.$name_file;
				
				$type = mime_content_type($_FILES['image']['tmp_name']);
				$allowed_mime_types = array('image/gif','image/jpeg','image/png','image/bmp');
				
				$legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");
				
				$legalSize = "2000000";
				
				if(in_array($type, $allowed_mime_types) && ($size_file !=0 && $size_file < $legalSize) && in_array($ext, $legalExtensions)){
									
					if ( 0 < $_FILES['image']['error'] ) {
					
						echo 'Error: ' . $_FILES['image']['error'] . '<br>';
					}else {
						if(!move_uploaded_file($_FILES['image']['tmp_name'], '../../images/cat/'.$name_file)){
							echo "non";
						}else{
							$updateReq = $dbh->prepare("UPDATE `bl_categories` SET `visuel`= :cheminfichier WHERE `id` ='$id_cat'");
							$updateReq->bindParam(':cheminfichier', $cheminfichier, PDO::PARAM_STR);
							$rs = $updateReq->execute();
						}
					}
					
				}		
			}
		
			
			echo "ok";
				
		}else if ($type =='supp'){
			
			$id = intval($_POST['id_cat_supp']);
			
			$deleteReq = $dbh->prepare("DELETE FROM `bl_categories` WHERE `id` = :id");
			$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReq->execute();
			
			echo "ok";
			
		}else{
			echo 'non';
		}
			
	}
}

?>