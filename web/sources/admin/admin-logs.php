<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_admin.php');

$page = "logs";

require_once ('php/admin_info.php');

// 1- recup game en cours pour cet user
$select_logs = $dbh->prepare("SELECT title, email, DATE_FORMAT(naissance, '%Y') as naissance, sexe, 
DATE_FORMAT(bl_logs.date, '%Y/%m/%d') as jour, DATE_FORMAT(bl_logs.date, '%H:%i:%s') as heure, page, bl_logs.info as info
FROM `bl_logs` 
inner join bl_logs_type on bl_logs_type.id  = bl_logs.type
inner join bl_user on bl_user.id  = bl_logs.user
order by bl_logs.date desc");
$select_logs->execute();

?>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black">


<link rel="icon" type="image/png" href="images/splash/android-chrome-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
<link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
<link rel="shortcut icon" href="images/splash/favicon.ico" type="image/x-icon" /> 
    
<title>Administration  - Liste des logs</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="styles/style.css"           rel="stylesheet" type="text/css">
<link href="styles/framework.css"       rel="stylesheet" type="text/css">
<link href="styles/font-awesome.css"    rel="stylesheet" type="text/css">
<link href="styles/animate.css"         rel="stylesheet" type="text/css">

<!--<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jqueryui.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link href="scripts/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="scripts/DataTables/js/jquery.dataTables.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script> 
   
<script type="text/javascript" src="scripts/framework-plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

</head>

<body class="left-sidebar" id="client"> 

<?php include ('header.php'); ?>
            
<div class="all-elements">
    <div class="snap-drawers">
    
        <?php include ('menu_left_admin.php'); ?>
        
        <div id="content" class="snap-content">
            <div class="content">
            <div class="header-clear"></div>
            <!--Page content goes here, fixed elements go above the all elements class-->        
             
             <div class="heading-style-1 container half-bottom">
                    <a href="#"><i class="fa fa-bookmark"></i></a>
                    <h4>Liste des logs</h4>
                    <div class="heading-block bg-night-dark"></div>
                    <div class="heading-decoration bg-night-dark"></div>
             </div>
             
            <div class="decoration"></div>
            
            <p>
                <span class="highlighted bg-red-dark color-white">INFOS :</span> 
                <span>"bienvenue" ou "Welcome" = Home Page</span>
             </p>
                        
            <div class="container no-bottom">
            
                <div class="container">
                    <table cellspacing='0' width="100%" class="default table">
                    	<thead>
                            <tr>
                                <th class="table-title">DATE</th>
                                <th class="table-title">HEURE</th>
                                <th class="table-title">EMAIL</th>
                                <th class="table-title">AGE</th>
                                <th class="table-title">SEXE</th>
                                <th class="table-title">LANGUE</th>
                                <th class="table-title">PAGE</th>
                                <th class="table-title">INFOS</th>
                                <th class="table-title">LOGS / ACTIONS</th>
                            </tr>
                        </thead>
                        <?php if ($select_logs->rowCount() > 0) {
								while ( $row_inscrits = $select_logs->fetch(PDO::FETCH_OBJ) ){ 
								$url = explode("/", $row_inscrits->page);
								if($url[5] !=''){ $url[5] = '/'.$url[5];}
								?>
								<tr>
									<td><?= $row_inscrits->jour ?></td>
                                    <td><?= $row_inscrits->heure ?></td>
									<td><?= $row_inscrits->email ?></td>
                                    <td><?php echo date('Y') - $row_inscrits->naissance; ?></td>
                                    <td><?php  if($row_inscrits->sexe ==2){ echo 'H'; }else if($row_inscrits->sexe ==1){ echo 'F'; }else{ echo 'A';} ?></td>
									<td><?= $url[3] ?></td>
                                    <td><?= $url[4].$url[5] ?></td>
                                    <td><?= $row_inscrits->info ?></td>
                                    <td><?= $row_inscrits->title ?></td>
								</tr>
							<?php } ?>
                        <?php }else{ ?>
                        	<tr>
                                <td colspan="7">
                                 <p>
                                    <span class="highlighted color-blue">
                                        Aucun logs.
                                    </span>
                                 </p>
                                
                                </td>
                            </tr>
                                                            
                     <?php } ?>
                        
                    
                     </table>
                </div>

            </div>
            
                   
            <div class="decoration"></div>
             <?php include ('footer.php'); ?>            
            
        </div>
    </div>  
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i></a>
</div>
    
</div>

<script>

$(document).ready(function() {
	
	$('.table').DataTable({
		"iDisplayLength": 50,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, 'desc' ]],
		"aLengthMenu": [[ 50, 100,-1], [50, 100, "Tous"]],
		"autoWidth": true,
		"dom": 'Bfrtip',
		"buttons": [
			'excel'
		]
	});
	
});

</script>

</body>