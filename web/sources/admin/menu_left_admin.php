<div class="snap-drawer snap-drawer-left">        
    <div class="sidebar-header-left">
        <a href="#" class="name_user"><i class="fa fa-user"></i> <? echo $nom; ?></a>
        <a class="close-sidebar" href="#"><i class="fa fa-close"></i></a>
    </div>      

    <p class="sidebar-divider">ADMINISTRATION</p>
    
    <div class="sidebar-menu">
    
    	<a class="menu-item <?php if($page =='battles'){ echo 'menu-item-active'; } ?>" href="admin-battles.php">
            <i class="fa fa-bookmark"></i>
            <em>Batailles </em>
            <i class="fa fa-circle"></i>
        </a>
        
        <a class="menu-item <?php if($page =='inscrits'){ echo 'menu-item-active'; } ?>" href="admin-inscrits.php">
            <i class="fa fa-user"></i>
            <em>Inscrits</em>
            <i class="fa fa-circle"></i>
        </a> 
        
        <a class="menu-item <?php if($page =='categories'){ echo 'menu-item-active'; } ?>" href="admin-categories.php">
            <i class="fa fa-cog"></i>
            <em>Thèmes</em>
            <i class="fa fa-circle"></i>
        </a> 
        
        <a class="menu-item <?php if($page =='grades'){ echo 'menu-item-active'; } ?>" href="admin-grades.php">
            <i class="fa fa-cog"></i>
            <em>Grades</em>
            <i class="fa fa-circle"></i>
        </a>
        
        <a class="menu-item <?php if($page =='signaler'){ echo 'menu-item-active'; } ?>" href="admin-signaler.php">
            <i class="fa fa-cog"></i>
            <em>Contenus signalés</em>
            <i class="fa fa-circle"></i>
        </a> 
        
        <a class="menu-item <?php if($page =='logs'){ echo 'menu-item-active'; } ?>" href="admin-logs.php">
            <i class="fa fa-cog"></i>
            <em>Logs</em>
            <i class="fa fa-circle"></i>
        </a>
        
       
        <a class="menu-item" href="php/deconnexion.php">
            <i class="fa fa-sign-out "></i>
            <em>Deconnexion</em>
            <i class="fa fa-circle "></i>
        </a>
        
    </div>
    
    
    <p class="sidebar-footer">Administration REPUBLIKE - © <?php echo date('Y'); ?></p>
    
</div>