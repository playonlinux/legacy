<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_admin.php');

$page = "grades";

require_once ('php/admin_info.php');

// 1- recup game en cours pour cet user
$select_cats = $dbh->prepare("SELECT * FROM `bl_grade` order by id asc");
$select_cats->execute();


?>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black">


<link rel="icon" type="image/png" href="images/splash/android-chrome-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
<link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
<link rel="shortcut icon" href="images/splash/favicon.ico" type="image/x-icon" /> 
    
<title>Administration - Liste des Grades</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="styles/style.css"           rel="stylesheet" type="text/css">
<link href="styles/framework.css"       rel="stylesheet" type="text/css">
<link href="styles/font-awesome.css"    rel="stylesheet" type="text/css">
<link href="styles/animate.css"         rel="stylesheet" type="text/css">

<!--<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jqueryui.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link href="scripts/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="scripts/DataTables/js/jquery.dataTables.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script> 
   
<script type="text/javascript" src="scripts/framework-plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>


</head>

<body class="left-sidebar" id="client"> 

<?php include ('header.php'); ?>
            
<div class="all-elements">
    <div class="snap-drawers">
    
        <?php include ('menu_left_admin.php'); ?>
        
        <div id="content" class="snap-content">
            <div class="content">
            <div class="header-clear"></div>
            <!--Page content goes here, fixed elements go above the all elements class-->        
             
             <div class="heading-style-1 container half-bottom">
                    <a href="#"><i class="fa fa-question"></i></a>
                    <h4>Liste des Grades</h4>
                    <div class="heading-block bg-night-dark"></div>
                    <div class="heading-decoration bg-night-dark"></div>
             </div>
             
            <div class="decoration"></div>
                        
            <div class="container no-bottom">
            	<!--<a class="button button-black button-round simple-categorie-modal" href="javascript:void(0)" ><i class="fa fa-plus"></i> Créer un nouveau thème</a>-->
                
                <div class="container">
                    <table cellspacing='0' width="100%" class="default table">
                    	<thead>
                        <tr>
                            <th class="table-title">ORDRE</th>
                            <th class="table-title">COLOR</th>
                            <th class="table-title">TITRE FR</th>
                            <th class="table-title">TITRE EN</th>
                            <th class="table-title">POINTS MIN.</th>
                            <th class="table-title">POINTS MAX.</th>
                            <th class="table-title">STATUT</th>
                            <th class="table-title">ACTIONS</th>
                        </tr>
                        </thead>
                        <?php if ($select_cats->rowCount() > 0) {
								while ( $row_cat = $select_cats->fetch(PDO::FETCH_OBJ) ){ 
								?>
								<tr>
									<td><?= $row_cat->id ?></td>
                                    <td> <span style="background-color:<?= $row_cat->color ?>; color:#ffff; padding:5px;"><?= $row_cat->color ?></span></td>
                                    <td><?= $row_cat->titre_fr ?></td>
                                    <td><?= $row_cat->titre_en ?></td>
                                    <td><?= $row_cat->points ?></td>
                                    <td><?= $row_cat->max ?></td>
									<td><?php if( $row_cat->statut ==1) { echo '<span class="green">ONLINE</span>';}else{echo '<span class="red">OFFLINE</span>';} ?></td>
									<td>
                                    <a class="button button-blue simple-editGrade-modal" href="javascript:void(0)" 
                                            data-id= "<?= $row_cat->id ?>"
                                            data-namefr= "<?= $row_cat->titre_fr ?>" 
                                            data-nameen= "<?= $row_cat->titre_en ?>"
                                            data-color= "<?= $row_cat->color ?>"
                                            data-min= "<?= $row_cat->points ?>"
                                            data-max= "<?= $row_cat->max ?>"
                                            data-statut= "<?= $row_cat->statut ?>"
                                        > <span class="fa fa-edit"></span> </a>
                                        
                                        <a class="button button-red simple-suppGrade-modal" href="javascript:void(0)" 
                                            data-id= "<?= $row_cat->id ?>"
                                            data-name= "<?= $row_cat->titre_fr ?>"
                                        > <span class="fa fa-trash"></span> </a>
									</td>
								</tr>
							<?php } ?>
                        <?php }else{ ?>
                        	<tr>
                                <td colspan="7">
                                 <p>
                                    <span class="highlighted color-blue">
                                        Aucun grade.
                                    </span>
                                 </p>
                                
                                </td>
                            </tr>
                                                            
                     <?php } ?>
                        
                    
                     </table>
                    
                    </div>
                
                </div>
                
            </div>            
            
            <div class="simple-editGrade-modal-content modal-content rounded-modal">
                <div class="login-modal-wrapper">
                    <h4>Modifier Grade</h4>
                    
                    <form method="post" action="" enctype="multipart/form-data" name="form_grade_upd" id="form_grade_upd">
                    
                        <div class="icon-field">
                            <i class="fa fa-briefcase"></i>
                            <input class="text-field green-field" required placeholder="Nom" value="" name="namefr" id="namefr"  type="text">
                        </div>
                        
                        <div class="icon-field">
                            <i class="fa fa-briefcase"></i>
                            <input class="text-field green-field" required placeholder="Nom" value="" name="nameen" id="nameen"  type="text">
                        </div>
                        <div class="icon-field">
                            <h6>Couleur du cercle *</h6>
                        </div>
                        
                        <div class="icon-field">
                            <i class="fa fa-eyedropper"></i>
                            <input class="text-field green-field" required placeholder="Color" value="" name="color" id="color"  type="color">
                        </div>
                        
                        <div class="icon-field">
                            <h6>Points*</h6>
                        </div>
                        
                        <div class="icon-field">
                            <i class="fa fa-minus"></i>
                            <input class="text-field green-field" required placeholder="Minimum" value="" name="min" id="min"  type="number">
                        </div>
                        
                        <div class="icon-field">
                            <i class="fa fa-plus"></i>
                            <input class="text-field green-field" required placeholder="Maximum" value="" name="max" id="max"  type="number">
                        </div>
                        
                                              
                        <div class="icon-field">
                            <h6>Statut*</h6>
                            <select class="select-field green-field" name="statut" id="statut">
                                <option value="1">Online</option>
                                <option value="2">Offline</option>
                            </select>
                        </div>
                        
                        <input required name="id" id="id"  type="hidden">
                        
                        <p id="errorLogUp"></p>
                        <a href="javascript:void(0)" id="editGrade" class="button-green login-button">MODIFIER</a>
                        <a href="javascript:void(0)" class="button-red login-close modal-close">ANNULER</a>
                    
                    </form>
                    <div class="clear"></div>
                </div>
            </div>
            
            
            <div class="simple-suppGrade-modal-content modal-content rounded-modal">
                    <div class="login-modal-wrapper">
                        <h4>SUPPRIMER CE GRADE : <p class="center-text color-green" id="nameGradeSupp"></p></h4>
                        <p class="center-text color-red-dark">!! Attention, c'est définitif !!</p>
                        
                        <form method="post" action="" enctype="multipart/form-data" name="form_grade_sup" id="form_grade_sup">
                              
                            <input required name="id_grade_supp" id="id_grade_supp"  type="hidden">
                            
                            <p id="errorLogSupp"></p>
                        
                            <a href="javascript:void(0)" id="suppGrade" class="button-red login-button">SUPPRIMER</a>
                            <a href="javascript:void(0)" class="button-dark login-close modal-close">ANNULER</a>
                        
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
                
                   
            <div class="decoration"></div>
                
           <?php include ('footer.php'); ?>

            
        </div>
    </div>  
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i></a>
</div>
    
</div>

<script>

$(document).ready(function() {
	
	$('.table').DataTable({
		"iDisplayLength": 50,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, 'asc' ]],
		"aLengthMenu": [[ 50, 100,-1], [50, 100, "Tous"]],
		"autoWidth": true,
		"dom": 'Bfrtip',
		"buttons": [
			'excel'
		]
	});
	
});

</script>

</body>