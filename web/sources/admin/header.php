<div id="preloader">
    <div id="status">
        <div class="preloader-logo"></div>
        <h3 class="center-text">Chargement...</h3>
    </div>
</div>

<div class="gallery-fix"></div> <!-- Important for all pages that have galleries or portfolios -->

<div id="header-fixed" class="header-style-1">
    <a class="header-1 open-left-sidebar" href="#"><i class="fa fa-navicon"></i></a>
    <a class="header-logo" href="#"><img src="/images/logo-republilke.png" alt="img"></a>

</div>