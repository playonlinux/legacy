<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_admin.php');

$page = "battles";

require_once ('php/admin_info.php');

// 1- BATTLE

$select_battle = $dbh->prepare("SELECT *, 
(SELECT count(distinct(user)) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_user, 
(SELECT count(id) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_post,
(SELECT sum(likes) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_like
FROM `bl_battles`
WHERE id=:id and `statut` ='1' limit 0,1");
$select_battle->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
$select_battle->execute();

$row_battle = $select_battle->fetch(PDO::FETCH_OBJ);

// 2- POSTS
$select_posts = $dbh->prepare("SELECT *
FROM `bl_battle_posts`
WHERE `statut` ='1' and battle = :id order by date desc ");
$select_posts->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
$select_posts->execute();

?>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black">


<link rel="icon" type="image/png" href="images/splash/android-chrome-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
<link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
<link rel="shortcut icon" href="images/splash/favicon.ico" type="image/x-icon" /> 
    
<title>Adminsitration  - Liste des posts</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="styles/style.css"           rel="stylesheet" type="text/css">
<link href="styles/framework.css"       rel="stylesheet" type="text/css">
<link href="styles/font-awesome.css"    rel="stylesheet" type="text/css">
<link href="styles/animate.css"         rel="stylesheet" type="text/css">

<!--<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jqueryui.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link href="scripts/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="scripts/DataTables/js/jquery.dataTables.js"></script> 

<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script> 
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script> 
   
<script type="text/javascript" src="scripts/framework-plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

</head>

<body class="left-sidebar" id="client"> 

<?php include ('header.php'); ?>
            
<div class="all-elements">
    <div class="snap-drawers">
    
        <?php include ('menu_left_admin.php'); ?>
        
        <div id="content" class="snap-content">
            <div class="content">
            <div class="header-clear"></div>
            <!--Page content goes here, fixed elements go above the all elements class-->        
             
             <div class="heading-style-1 container half-bottom">
                    <a href="#"><i class="fa fa-bookmark"></i></a>
                    <h4>Liste des posts pour : "<?= $row_battle->title ?>"</h4>
                    <div class="heading-block bg-night-dark"></div>
                    <div class="heading-decoration bg-night-dark"></div>
             </div>
             
            <div class="container no-bottom">
                
                <div class="one-half-responsive">
                    <ul class="font-icon-list">
                        <li><i class="fa fa-bookmark"></i><?= $row_battle->title ?></li>
                        <li><i class="fa fa-users"></i><?= $row_battle->nb_user ?> participant(s)</li>
                        <li><i class="fa fa-bullhorn"></i><?= $row_battle->nb_post ?> post(s)</li>
                        <li><i class="fa fa-heart"></i><?= $row_battle->nb_like ?> like(s)</li>
                    </ul>
                </div>
                
               
            </div>
            
            
            <div class="decoration"></div>
                        
            <div class="container no-bottom">
            
                <div class="container">
                    <table cellspacing='0' width="100%" class="default table">
                    	<thead>
                        <tr>
                            <th class="table-title">DATE</th>
                            <th class="table-title">TITRE</th>
                            <th class="table-title">USER</th>
                            <th class="table-title">POST</th>
                            <th class="table-title">LIKE</th>
                            <th class="table-title">STATUT</th>
                            <th class="table-title">ACTIONS</th>
                        </tr>
                        </thead>
                        
                        <?php if ($select_posts->rowCount() > 0) {
									while ( $row_posts = $select_posts->fetch(PDO::FETCH_OBJ) ){ 
								?>
								<tr>
									<td><?= $row_posts->date ?></td>
									<td><?= $row_posts->title ?></td>
									<td><?= name_user($row_posts->user, $dbh) ?></td>
									<td>
									<?php if($row_posts->type ==1){ ?>
                                         <a target="_blank" href="../single_post.php?id=<?= $row_posts->token ?>">
                                         	<img src="../<?= $row_posts->post ?>" width="50" alt="">
                                         </a>
                                    <?php } ?>
									
									<?php if($row_posts->type ==4){ ?>
                                            <a target="_blank" href="../single_post.php?id=<?= $row_posts->token ?>">
                                         		<img src="../<?= $row_posts->post ?>" width="50" alt="">
                                         	</a>
                                    <?php } ?>
                                    
                                    <?php if($row_posts->type ==2){ ?>
                                            <a target="_blank" href="../single_post.php?id=<?= $row_posts->token ?>"><?= $row_posts->post ?></a>
                                    <?php } ?>
                                    
                                    <?php if($row_posts->type ==3){ ?>
                                            <a target="_blank" href="../single_post.php?id=<?= $row_posts->token ?>">
												<?= mb_strimwidth($row_posts->post, 0, 50, " [...]") ?>
                                            </a>
                                    <?php } ?>
									                                    
                                    </td>
                                    <td><?= $row_posts->likes ?></td>
									<td><?php if( $row_posts->statut ==1) { echo '<span class="green">ONLINE</span>';}else{echo '<span class="red">OFFLINE</span>';} ?></td>
									<td>
                                    <a class="button button-black" target="_blank" href="../single_post.php?id=<?= $row_posts->token ?>"><span class="fa fa-eye"></span> </a>
                                    
									<a class="button button-red simple-suppPost-modal" href="javascript:void(0)" 
									data-id= "<?= $row_posts->id ?>"
									data-name= "<?= $row_posts->title ?>"
									> <span class="fa fa-trash"></span> </a>
									</td>
								</tr>
							<?php } ?>
                        <?php }else{ ?>
                        	<tr>
                                <td colspan="6">
                                 <p>
                                    <span class="highlighted color-blue">
                                        Aucune battle.
                                    </span>
                                 </p>
                                
                                </td>
                            </tr>
                                                            
                     <?php } ?>
                        
                    
                     </table>
                </div>

                <div class="simple-suppPost-modal-content modal-content rounded-modal">
                    <div class="login-modal-wrapper">
                        <h4>SUPPRIMER CE POST : <p class="center-text color-green" id="nameEntrSupp"></p></h4>
                        <p class="center-text color-red-dark">!! Attention, c'est définitif et vous perdez toutes les informations et son historique  !!</p>
                        
                        <form method="post" action="" enctype="multipart/form-data" name="form_post_sup" id="form_post_sup">
                              
                            <input required name="id_post_supp" id="id_post_supp" type="hidden">
                            <input required name="id_battle" id="id_battle"  value ="<?= $_GET['id'] ?>" type="hidden">
                            
                            <p id="errorLogSupp"></p>
                        
                            <a href="javascript:void(0)" id="suppPost" class="button-black login-button">SUPPRIMER</a>
                            <a href="javascript:void(0)" class="button-dark login-close modal-close">ANNULER</a>
                        
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
                
                
            </div>
            
                   
            <div class="decoration"></div>
             <?php include ('footer.php'); ?>

            
            
        </div>
    </div>  
    <a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i></a>
</div>
    
</div>

<script>

$(document).ready(function() {
	
	$('.table').DataTable({
		"iDisplayLength": 50,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, 'desc' ]],
		"aLengthMenu": [[ 50, 100,-1], [50, 100, "Tous"]],
		"autoWidth": true,
		"dom": 'Bfrtip',
		"buttons": [
			'excel'
		]
	});
	
});

</script>

</body>