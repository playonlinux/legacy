<?php
require_once('php/mysql.inc.php');
if (empty($_SESSION["mes_welcome"])) {
    $_SESSION["mes_welcome"] = time() + 365 * 24 * 3600;
    // setcookie ("mes_welcome", 'popup', time() + 365*24*3600, '/', '.republike.io', true, true);
}
require_once('php/funct_battelike.php');
$page = "accueil";
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Welcome">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Welcome</title>

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_bienvenue'] ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/welcome'; ?>
        <link rel="alternate" hreflang="en" href="/en/welcome" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/bienvenue'; ?>
        <link rel="alternate" hreflang="fr" href="/fr/bienvenue" />
    <?php } ?>

    <?php include('required.php'); ?>

    <?php if (!empty($_SESSION['securite'])) { ?>
        <?php if (user_cat($_SESSION['id_user'], $dbh) == 1) { ?>
            <script>
                var page = 'connecte';
            </script>
        <?php } else { ?>
            <script>
                var page = 'nocat';
            </script>
        <?php } ?>
    <?php } else { ?>
        <script>
            var page = 'accueil';
        </script>
    <?php } ?>

    <script>
        var lang = '<?= $code ?>';
    </script>
</head>

<body>
    <div class="bg-img-home" style="color: #ffffff;">
        <div class="container">
            <div class="topnav">
                <?php include('header.php'); ?>
                <div class="container-fluid">
                    <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 160px;">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <span class="text-left mb-1 home-title"><?= $_['bigtitre_index'] ?></span>
                            </div>
                            <div class="col-sm-12">
                                <h5 class="text-left mb-4" style="color:#ffffff;"><?= $_['titre_index'] ?></h5>
                            </div>
                        </div>
                        <?php include('lancerBattle.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <main style="padding-top:0px;">
        <div class="sh-content-head" style="padding-top:0;">
            <div class="sh-content-head__btns center-block text-center hidden-xs">
                <div id="sorts" class="sh-content-head__control mt-2">
                    <?php if (!empty($_SESSION['securite'])) { ?>
                        <?php if (user_cat($_SESSION['id_user'], $dbh) == 1) { ?>
                            <a href="javascript:void(0)" data-filter="category" class="filter-button sh-btn-icon switcher active"><?= $_['bt_cat']; ?></a>
                            <a href="javascript:void(0)" data-sort-by="users" class="sort-button sh-btn-icon switcher"><?= $_['bt_user']; ?></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" data-sort-by="users" class="sort-button sh-btn-icon switcher active"><?= $_['bt_user']; ?></a>
                        <?php } ?>
                    <?php } else { ?>
                        <a href="javascript:void(0)" data-sort-by="users" class="sort-button sh-btn-icon switcher active"><?= $_['bt_user']; ?></a>
                    <?php } ?>

                    <?php if (!empty($_SESSION['securite'])) { ?>
                        <a href="javascript:void(0)" data-filter="friends" class="filter-button sh-btn-icon switcher"><?= $_['bt_amis']; ?></a>
                    <?php } ?>

                    <a href="javascript:void(0)" data-sort-by="date" class="sort-button sh-btn-icon switcher"><?= $_['bt_date']; ?></a>
                </div>
            </div>
            <select id="sorts" class="switcher sh-content-head__control visible-xs col-sm-12 form-control2 mt-2">
                <?php if (!empty($_SESSION['securite'])) { ?>
                    <?php if (user_cat($_SESSION['id_user'], $dbh) == 1) { ?>
                        <option data-filter="category" class="filter-button active"><?= $_['bt_cat']; ?></option>
                        <option data-sort-by="users" class="sort-button"><?= $_['bt_user']; ?></option>
                    <?php } else { ?>
                        <option data-sort-by="users" class="sort-button active"><?= $_['bt_user']; ?></option>
                    <?php } ?>
                <?php } else { ?>
                    <option data-sort-by="users" class="sort-button active"><?= $_['bt_user']; ?></option>
                <?php } ?>

                <?php if (!empty($_SESSION['securite'])) { ?>
                    <option data-filter="friends" class="filter-button"><?= $_['bt_amis']; ?></option>
                <?php } ?>

                <option data-sort-by="date" class="sort-button"><?= $_['bt_date']; ?></option>

            </select>
        </div>

        <!--sections-->
        <div class="container" style="background-color:#eeeeee; padding: 30px;">
            <div class="row tab-submit" id="defis"></div>
        </div>
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/home']);
            <?php if (empty($_SESSION["mes_welcome"])) { ?> require(['modules/home_message']);
            <?php } ?>
        });
    </script>

    <?php if (empty($_SESSION["mes_welcome"])) { ?>
        <div class="sh-message">
            <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
            <div class="sh-login__content text-center">
                <p class="intro">
                    <b>Vous adorez créer ou dénicher des contenus (photos, vidéos, textes, musiques) ?<br>
                        Vous voulez les partager autour de vous ?</b><br><br>
                </p>
                <p class="intro text-left">
                    Sur <b>REPUBLIKE</b>, vous pouvez :<br>
                    • Lancer des batailles de contenus sur tous les sujets ;<br>
                    • Publier des contenus, que ce soient les vôtres ou ceux des autres ;<br>
                    • Liker les meilleures propositions et les faire gagner ;<br>
                    • Tester vos créations ;<br>
                    • Inviter vos amis à participer et à vous soutenir ;<br>
                    • Défier d'autres fans...
                </p>
                <div class="sh-login__send mt-4 mb-1">
                    <a href="#" class="sh-login__btn-signup sh-btn"><?= $_['bt_connecter']; ?></a>
                </div>
                <a class="" href="/<?= $code ?>/<?= $_['url_condition_1'] ?>"><?= $_['bt_decouvrez']; ?></a>
            </div>
        </div>
    <?php } ?>

</body>

</html>