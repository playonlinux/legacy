<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_battelike.php');

if(isset($_SESSION['securite']) && $_SESSION['securite'] !='' ){
	
}else{
	session_start();
	session_unset(); 
	session_destroy(); 
	header('Location: /index.php');
}	
?>

<!DOCTYPE html>
<html lang="fr_FR">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Mon compte">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | 404</title>
    
    <link rel="icon" type="image/png" href="images/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
    <link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="images/splash/favicon.ico" type="image/x-icon" /> 

    <!-- STYLESHEET -->
    <!-- FONTS -->
    <!-- Muli -->
    <link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">

    <!-- icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/icons/fontawesome/css/font-awesome.min.css"/>

    <link rel="stylesheet" href="fonts/icons/sharehub/style.css"/>

    <!-- Vendor -->
    <!-- Custom -->
    <link rel="stylesheet" href="vendor/magnificPopup/dist/magnific-popup.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet"/>

    <!-- JAVA SCRIPT -->
    <!-- require -->
    <script data-main="js/app" src="vendor/require/require.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="vendor/datePicker/css/datepicker.css"/>
</head>
<body>
   <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container-fluid">
            <!--content head-->
            <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="row">
                    
                    <div class="col-lg-12 text-center mt-4 mb-4">
                      <h2 class="center-block" style="font-size: 44px;"><b class="rose"><?= $_['titre_404'] ?></b></h2>
                	</div>
                    <div class="col-lg-12 text-center mt-1 mb-4">
                      <p class="center-block"><?= $_['sstitre_404'] ?></p>
                	</div>
                </div>
            </div>
        </div>
    </main>

        <?php include('footer.php'); ?>

    <script>
        require(['app'], function () {
            require(['modules/posts']);
        });
    </script>
</body>
</html>