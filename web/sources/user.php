<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

secureGet();

if (!empty(($_GET['id']))) {
    $url_user = $_GET['id'];
    $select_user = $dbh->prepare(
        "SELECT id, points, photo, connexion, likes, like_coins, statut, sexe, info, ville, nom, prenom, email,
            DATE_FORMAT(date, '%m/%Y') AS date_format,
            (SELECT COUNT(id) FROM bl_battles WHERE user = U.id) AS nb_battles, 
            (SELECT COUNT(id) FROM bl_battle_posts WHERE user = U.id) AS nb_post,
            (SELECT COUNT(id) FROM bl_battle_posts WHERE user = U.id AND trophee != 0) AS nb_trophees,
            (SELECT COUNT(id) FROM bl_user_favoris WHERE user = U.id) AS nb_favoris,
            (SELECT SUM(likes) FROM bl_battle_posts WHERE user = U.id) AS nb_like,
            DATE_FORMAT(naissance, '%d/%m/%Y') AS naissance_format
        FROM bl_user U
        WHERE url = :id
        LIMIT 0,1
    "
    );
    $select_user->bindParam(':id', $url_user, PDO::PARAM_STR);
    $select_user->execute();

    $row_user = $select_user->fetch(PDO::FETCH_OBJ);
    $id_user = $row_user->id;

    if ($id_user != '') {

        //theme
        $select_theme = $dbh->prepare("SELECT cat FROM `bl_user_cat` WHERE user = :id");
        $select_theme->bindParam(':id', $id_user, PDO::PARAM_STR);
        $select_theme->execute();
        $mes_themes = array();
        if ($select_theme->rowCount() > 0) {
            while ($row_theme = $select_theme->fetch(PDO::FETCH_OBJ)) {
                $mes_themes[] = $row_theme->cat;
            }
        }

        /* Classement */
        $select_grade = $dbh->prepare("SELECT `points`, `titre_" . $code . "` AS name FROM bl_grade WHERE `points` > :point LIMIT 1");
        $select_grade->bindParam(':point', $row_user->points, PDO::PARAM_STR);
        $select_grade->execute();
        $row_gradeSupp = $select_grade->fetch(PDO::FETCH_OBJ);
        $nbPointGrade = $row_gradeSupp->points;
        $nomPointGrade = $row_gradeSupp->name;


        // $demandeAmis = false;
        $demanderAmi = false;
        $amis = false;

        $select_demande = $dbh->prepare("SELECT 1 FROM `bl_user_demande_friend` WHERE user = :user AND qui = :ami AND `statut` = '1' LIMIT 0,1");
        $select_demande->bindParam(':user', $id_user, PDO::PARAM_STR);
        $select_demande->bindParam(':ami', $_SESSION['id_user'], PDO::PARAM_STR);
        $select_demande->execute();

        $demandeAmis = $select_demande->rowCount() > 0;

        $select_demande = $dbh->prepare(
            "   SELECT DATE_FORMAT(date, '%d-%m-%Y') AS date_demande 
            FROM `bl_user_demande_friend`
            WHERE user = :ami AND qui = :user AND `statut` = '1'
            LIMIT 0,1
        "
        );
        $select_demande->bindParam(':user', $id_user, PDO::PARAM_STR);
        $select_demande->bindParam(':ami', $_SESSION['id_user'], PDO::PARAM_STR);
        $select_demande->execute();

        if ($select_demande->rowCount() > 0) {
            $row_demande = $select_demande->fetch(PDO::FETCH_OBJ);
            $demanderAmi = true;
            $demanderAmi_date = $row_demande->date_demande;
        }

        $select_amis = $dbh->prepare(
            "   SELECT DATE_FORMAT(date, '%d/%m/%Y') AS date_val
            FROM `bl_user_friend`
		    WHERE (user = :ami OR friend = :ami) AND (user = :user OR friend = :user) 
            LIMIT 0,1
        "
        );
        $select_amis->bindParam(':ami', $id_user, PDO::PARAM_STR);
        $select_amis->bindParam(':user', $_SESSION['id_user'], PDO::PARAM_STR);
        $select_amis->execute();

        if ($select_amis->rowCount() > 0) {
            $row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
            $date_val = $row_amis->date_val;
            $amis = true;
        }
    } else {

        $id_user = $_GET['id'];
        $select_user = $dbh->prepare("SELECT url FROM bl_user WHERE id = :id LIMIT 0,1");
        $select_user->bindParam(':id', $id_user, PDO::PARAM_STR);
        $select_user->execute();

        $row_user = $select_user->fetch(PDO::FETCH_OBJ);
        $url_user = $row_user->url;

        if (!empty($url_user)) {
            header("Location: /" . $code . "/" . $_['url_user'] . "/" . $url_user);
        } elseif (!empty($_SESSION['securite'])) {
            header("Location:/" . $code . "/" . $_['url_bienvenue']);
        } else {
            header('Location: /');
        }
    }
} elseif (!empty($_SESSION['securite'])) {
    header("Location:/" . $code . "/" . $_['url_bienvenue']);
} else {
    header('Location: /');
}
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Mon compte">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | <?= $row_user->prenom ?> <?= $row_user->nom ?></title>

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= $url_user ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/citizen/' . $_GET['id']; ?>
        <link rel="alternate" hreflang="en" href="/en/citizen/<?= $url_user ?>" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/citoyen/' . $_GET['id']; ?>
        <link rel="alternate" hreflang="fr" href="/fr/citoyen/<?= $url_user ?>" />
    <?php } ?>

    <?php include('required.php'); ?>
    <script>
        var page = "user";
        var parametre = 0;
    </script>
    <?php if (isset($_GET['index']) && $_GET['index'] == 'trophee') { ?>
        <script>
            var onglet = "trophee";
            var user = '<?= $row_user->id ?>';
        </script>
    <?php } else { ?>
        <script>
            var onglet = "";
        </script>
    <?php } ?>

    <script>
        var trophees = <?= !empty($_GET['trophees']) ? 1 : 0 ?>;
    </script>
</head>

<body>
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main style="padding-top: 110px;">
        <div class="container" style="background-color:#e6e9f1;">
            <!--content head-->
            <div class="row">

                <div class="sh-head-user col-lg-3 col-sm-12 col-3">
                    <div class="sh-head-user__content offset-1">
                        <?php
                        $progess = (!empty($_SESSION['securite'])) ? get_user_progress($id_user) : '';
                        $color = (!empty($_SESSION['securite'])) ? grade_user_color((points_user($id_user, $dbh)), $dbh) : '#000000';
                        $colorClass = (!empty($_SESSION['securite'])) ? get_grade_color_class(points_user($id_user, $dbh)) : '';
                        ?>
                        <div class="text-center sh-head-user__image sh-head-user__profil col-lg-2 col-md-2 col-4">
                            <span class="sh-section__user__image" style="border-color:<?= $color ?>; 
                        background-image:url(/<?= $row_user->photo; ?>);">
                            </span>
                        </div>
                        <div class="row mt-3 mb-3">
                            <div class="col-12">
                                <b style="font-size:2rem"><?= $row_user->nom . ' ' . $row_user->prenom ?></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12"><?= grade_user_between(points_user($id_user, $dbh), $code, $dbh); ?></div>
                            <div class="col-sm-8 col-11 w3-container">
                                <div class="col-12 w3-light-grey w3-round-xlarge" style="padding: 0">
                                    <div class="w3-container w3-round-xlarge <?= $colorClass ?>" style="width:<?= $progess + 15 ?>%;padding-left:<?= ($progess) ? $progess - 10 : 1 ?>%"><b><?= $progess ?>%</b></div>
                                </div>
                            </div>
                            <div class="col-1" style="font-size: smaller; padding-left:15px;"><?= get_user_next_grade($id_user); ?></div>

                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="sh-head-user__info col-lg-12 col-md-9 col-9 offset-1 mt-4 mb-4">
                                    <div class="sh-head-user__info-head mb-0">
                                        <div class="sh-head-user__name">

                                            <?php if (!$demandeAmis && !$amis) {

                                                if (!empty($_SESSION['securite'])) { ?>
                                                    <?php if (!$demanderAmi) { ?>
                                                        <a id="btn-friend" data-friend="<?= $row_user->id; ?>" data-user="<?= $_SESSION['id_user']; ?>" href="javascript:void(0)" class="sh-btn sh-btn-rose btn-friend_search mt-3"><?= $_['bt_devenir'] ?></a>
                                                        <div class="sh-reponse">
                                                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                                                            <div class="sh-login__content">
                                                                <p class="text-center"><b><?= $_['demande_friend_send'] ?></b></p>
                                                            </div>
                                                        </div>
                                                    <?php } else { ?>
                                                        <strong class='rose center-block mt-3 mb-2'>Invitation envoyée le <?= $demanderAmi_date ?></strong>
                                                        <a id="btn-annule-friend" data-friend="<?= $id_user ?>" data-user="<?= $_SESSION['id_user'] ?>" href="javascript:void(0)" class="sh-btn sh-btn-rose btn-friend_annule"><?= $_['bt_annulerami'] ?></a>
                                                        <div class="sh-annuler">
                                                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                                                            <div class="sh-login__content">
                                                                <p class="text-center"><b><?= $_['demande_friend_delete'] ?></b></p>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <a id="demande_amis" href="javascript:void(0)" class="sh-btn sh-btn-rose btn-upload_btn-signup mt-3"><?= $_['bt_devenir'] ?></a>
                                                <?php } ?>

                                            <?php } ?>

                                            <?php if ($demandeAmis && !$amis) {
                                                if (!empty($_SESSION['securite'])) { ?>

                                                    <a id="btn-yes" data-friend="<?= $row_user->id; ?>" data-user="<?= $_SESSION['id_user']; ?>" href="javascript:void(0)" class="sh-btn btn-yes mt-3">Accepter</a>
                                                    <a id="btn-no" data-friend="<?= $row_user->id; ?>" data-user="<?= $_SESSION['id_user']; ?>" href="javascript:void(0)" class="sh-btn btn-no mt-3">Ignorer</a>

                                                    <div class="sh-yes">
                                                        <div class="sh-upload__logo"><a href="./" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                                                        <div class="sh-login__content">
                                                            <p class="text-center"><b><?= $_['demande_friend_yes'] ?></b></p>
                                                        </div>
                                                    </div>

                                                    <div class="sh-no">
                                                        <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                                                        <div class="sh-login__content">
                                                            <p class="text-center"><b><?= $_['demande_friend_no'] ?></b></p>
                                                        </div>
                                                    </div>

                                                <?php } else { ?>
                                                    <a id="demande_yes" href="javascript:void(0)" class="sh-btn btn-upload_btn-signup mt-3">Accepter</a>
                                                    <a id="demande_no" href="javascript:void(0)" class="sh-btn btn-upload_btn-signup mt-3">Ignorer</a>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($amis) {
                                                if (!empty($_SESSION['securite'])) { ?>
                                                    <p class="mb-0"><?= $_['ami_depuis'] ?> <?= $date_val; ?></p>
                                                    <a id="btn-annule-friend" data-friend="<?= $id_user ?>" data-user="<?= $_SESSION['id_user'] ?>" href="javascript:void(0)" class="sh-btn sh-btn-rose btn-supp-friend"><?= $_['bt_neplusetreami'] ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12 col-sm-12 offset-1 mt-5 mb-5">
                    <div class="mb-3">
                        <span class="profil mt-5 mb-0">
                            <!-- <div class="sh-btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['nb_points'] ?>"><i class="fa fa-circle-thin"></i><span><?= $row_user->points ?> points</span></div> -->
                            <div class="sh-btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['nb_likes2'] ?>"><i class="fa fa-heart"></i><span><?= $row_user->likes  ?></span></div>
                            <div class="sh-btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['nb_likecoins'] ?>"><i class="repu-coins"></i><span><?= $row_user->like_coins  ?></span></div>
                        </span>
                    </div>
                    <p><?= $row_user->info ?></p>
                    <div class="row mt-4 mb-2">
                        <div class="col-lg-12 col-md-12">
                            <p class="mb-3"><i class="fa fa-map-marker"></i> <?= $row_user->ville ?></p>
                            <p class="mb-3"><i class="fa fa-calendar-check-o"></i> <?= $_['menu_left_compte1'] ?> <?= $row_user->date_format ?></p>
                        </div>
                    </div>

                    <div class="sh-head-user__info-communautaire row mt-3 mb-0 ttg-grid-padding--none">
                        <?php

                        $select_amis = $dbh->prepare(
                            "   SELECT friend AS user_id FROM bl_user_friend WHERE user = :id AND statut = 1
                            UNION 
                                SELECT user FROM bl_user_friend WHERE friend = :id AND statut = 1
                            "
                        );

                        $select_amis->bindParam(':id', $id_user, PDO::PARAM_STR);
                        $select_amis->execute();

                        if ($select_amis->rowCount() > 0) { ?>

                            <div class="col-lg-12 col-md-12 mb-2">
                                <p><i class="fa fa-user-o"></i> <?= $_['menu_left_compte2'] ?> (<?= $select_amis->rowCount() ?>)</p>
                            </div>

                            <?php
                                while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) {

                                    ?>
                                <!--section-->
                                <div class="col-lg-1 col-md-1 col-sm-1 col-1">
                                    <div class="sh-section-amis">
                                        <div class="sh-section__content">
                                            <? if ($row_amis->user_id == $_SESSION['id_user']) { ?>
                                                <a data-toggle="tooltip" data-placement="top" data-original-title="<?= speudo_user($row_amis->user_id, $dbh) ?>" href="/<?= $code ?>/<?= $_['url_compte'] ?>">
                                                    <div class="sh-section__image">
                                                        <span class="sh-section__user__image" style="border-color:<?= grade_user_color(points_user($row_amis->user_id, $dbh), $dbh) ?>;background-image:url(<?= '/' . photo_user($row_amis->user_id, $dbh) ?>)"></span>
                                                    </div>
                                                </a>

                                            <? } else { ?>
                                                <a data-toggle="tooltip" data-placement="top" data-original-title="<?= speudo_user($row_amis->user_id, $dbh) ?>" href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_amis->user_id, $dbh) ?> ">
                                                    <div class="sh-section__image">
                                                        <span class="sh-section__user__image" style="border-color:<?= grade_user_color(points_user($row_amis->user_id, $dbh), $dbh) ?>;background-image:url(<?= '/' . photo_user($row_amis->user_id, $dbh) ?>)"></span>
                                                    </div>
                                                </a>
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>
                        <? }
                        } ?>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#fff;">
                    <div class="sh-content-head header-battle">
                        <div class="sh-content-head__btns text-center hidden-xs">
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2 active" data-id="defis" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte31'] ?> (<?= $row_user->nb_battles ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="posts" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte32'] ?> (<?= $row_user->nb_post ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="trophees" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte33'] ?> (<?= $row_user->nb_trophees ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="favoris" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte34'] ?> (<?= $row_user->nb_favoris ?>)</a>
                        </div>
                        <select class="sh-content-head__btns visible-xs menu col-sm-12 form-control2">
                            <option class="active" data-id="defis" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte31'] ?> (<?= $row_user->nb_battles ?>)</option>
                            <option data-id="posts" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte32'] ?> (<?= $row_user->nb_post ?>)</option>
                            <option data-id="trophees" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte33'] ?> (<?= $row_user->nb_trophees ?>)</option>
                            <option data-id="favoris" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte34'] ?> (<?= $row_user->nb_favoris ?>)</option>
                        </select>
                    </div>

                    <div>
                        <div class="row tab-content" id="defis" data-user="<?= $row_user->id ?>"></div>
                        <div class="row tab-content" id="posts" data-user="<?= $row_user->id ?>" style="display:none;"></div>
                        <div class="row tab-content" id="trophees" data-user="<?= $row_user->id ?>" style="display:none;"></div>
                        <div class="row tab-content" id="favoris" data-user="<?= $row_user->id ?>" style="display:none;"></div>

                        <!-- SUPP POST -->
                        <div class="sh-supp">
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-login__content">
                                <div class="sh-login__form text-center mt-1 mb-5">
                                    <?= $_['supp_contenu1'] ?> <br>"<b><span id="postName"></span></b>" ?
                                    <br><br><?= $_['supp_contenu2'] ?>
                                </div>
                                <div class="sh-login__send">
                                    <input type="hidden" name="id_post" id="id_post" required>
                                    <button type="submit" class="sh-btn" id="suppPost"><?= $_['bt_confirm'] ?></button>
                                </div>
                            </div>
                        </div>

                        <div class="sh-popup-post">
                            <div class="sh-popup__content" id="contenu_post">
                            </div>
                        </div>
                        <!-- edit POST -->
                        <div class="sh-editPost">
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-upload__content">
                                <div class="sh-upload__form">
                                    <form method="post" name="form_edit_post" id="form_edit_post" enctype="multipart/form-data">
                                        <p><?= $_['edit_contenu'] ?></p>
                                        <input class="form-control mt-3" placeholder="<?= $_['edit_titre'] ?>" name="name_post" id="name_post" type="text">
                                        <input type="hidden" name="id_post" id="id_post" required>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn mx-auto" id="editPost"><?= $_['bt_edit'] ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- edit Battle -->
                        <div class="sh-editBattle">
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-upload__content">
                                <div class="sh-upload__form">
                                    <form method="post" name="form_edit_battle" id="form_edit_battle" enctype="multipart/form-data">
                                        <p><?= $_['edit_catbattle'] ?></p>
                                        <?php $select_cat = $dbh->prepare("SELECT id, name_" . $code . " as name  FROM `bl_categories` WHERE `statut` = '1' order by name_" . $code . " ");
                                        $select_cat->execute(); ?>
                                        <select class="form-control mt-3 mb-4" name="cat_battle">
                                            <?php if ($select_cat->rowCount() > 0) {
                                                while ($row_cat = $select_cat->fetch(PDO::FETCH_OBJ)) { ?>
                                                    <option value="<?= $row_cat->id ?>"><?= $row_cat->name ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                        <p><?= $_['edit_titrebattle'] ?></p>
                                        <input class="form-control mt-3" placeholder="<?= $_['champ_battle_3'] ?>" name="name_battle" type="text">
                                        <p><?= $_['edit_descbattle'] ?></p>
                                        <textarea name="info_battle" id="info_battle" class="form-control mb-4" placeholder="<?= $_['champ_battle_5'] ?>"></textarea>
                                        <input type="hidden" name="id_battle" id="id_battle" required>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn mx-auto" id="editBattle"><?= $_['bt_edit'] ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/profil']);
        });
    </script>
</body>

</html>