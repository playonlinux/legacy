<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');
$page = "hall";
//logs
if (isset($_SESSION['id_user'])) {
    save_log_ou($_SESSION['id_user'], 28, $adr, $dbh);
}
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Hall of fame">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Hall of fame</title>

    <link rel="canonical" href="/<?= $code ?>/hall-of-fame" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/hall-of-fame'; ?>
        <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/hall-of-fame'; ?>
        <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
    <?php } ?>

    <?php include('required.php'); ?>
    <script>
        var number = 10;
        var page = 'hall';
    </script>

</head>

<body>

    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container">
            <!--content head-->

            <div class="sh-content-head">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h1 class="center-block"><b>HALL OF FAME</b></h1>
                    </div>
                </div>
            </div>

            <div class="sh-content-head" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="row">
                    <div class="col-lg-12 text-center mt-1 mb-5">
                        <p class="center-block table-responsive">
                            <table class="table hall" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?= $_['bt_participants'] ?></th>
                                        <th>Grade</th>
                                        <th>Points</th>
                                        <th><img src="/images/icons/gold-number.svg" class="icons-50"></th>
                                        <th><img src="/images/icons/silver-number.svg" class="icons-50"></th>
                                        <th><img src="/images/icons/bronze-number.svg" class="icons-50"></th>
                                        <th><i class="fa fa-trophy"></i></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php $select_users = $dbh->prepare("SELECT id, points, `or`, argent, bronze FROM `bl_user` WHERE statut < 3 ORDER BY `bl_user`.`points` DESC ");
                                    $select_users->execute();
                                    if ($select_users->rowCount() > 0) {
                                        $i = 1;
                                        while ($row_users = $select_users->fetch(PDO::FETCH_OBJ)) { ?>

                                            <tr <?php if ($row_users->id == $_SESSION['id_user']) {
                                                            echo 'class="user"';
                                                        } ?> id="post_<?= $row_users->id ?>">
                                                <td class="chiffre <?php if ($i < 4) {
                                                                                echo 'ordre"';
                                                                            } ?>"><?= $i ?></td>
                                                <td>
                                                    <a href="<?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_users->id) { ?>
											                        /<?= $code ?>/<?= $_['url_compte'] ?>
                                                        <?php } else { ?>
                                                                    /<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_users->id, $dbh) ?>
                                                        <?php } ?>
                                                        ">
                                                        <div class="sh-section__image">
                                                            <span class="sh-section__user__image" style="border-color:<?= grade_user_color($row_users->points, $dbh) ?>; background-image:url(/<?= photo_user($row_users->id, $dbh) ?>)"></span>
                                                        </div>
                                                        <?= prenom_user($row_users->id, $dbh) ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <img src="<?= get_user_avatar($row_users->id); ?>" style="height:70px; width:70px;vertical-align:super">
                                                </td>
                                                <td class="chiffre"><?= $row_users->points ?></span></td>
                                                <td class="chiffre"><?= $row_users->or ?></td>
                                                <td class="chiffre"><?= $row_users->argent ?></td>
                                                <td class="chiffre"><?= $row_users->bronze ?></td>
                                                <td>
                                                    <a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_users->id, $dbh) ?>/<?= $_['views']['trophies'] ?>" class="sh-btn sh-btn-rose center-block mt-3">
                                                        <?= $_['bt_trophees'] ?>
                                                    </a>
                                                </td>
                                            </tr>
                                    <? $i++;
                                        }
                                    } ?>


                                </tbody>
                            </table>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/hall']);
        });
    </script>

</body>

</html>