<?php
require_once('./database.inc.php');

$now = date('Y-m-d');

$userFinal = $dbh->prepare("SELECT * FROM `bl_user` WHERE `statut` =1 and ADDDATE(date, 7) <= $now ");
$userFinal->execute();

if ($userFinal->rowCount() > 0) {

	while ($row_userFinal = $userFinal->fetch(PDO::FETCH_OBJ)) {

		$id_user = $row_userFinal->id;
		$prenom = $row_userFinal->prenom;
		$email = $row_userFinal->email;
		$password = $row_userFinal->password;


		//desactivation compte
		$datesupp = date("Y-m-d");
		$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= '', `statut`='3', `date_supp`=:datesupp WHERE id = :id ");
		$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
		$updateReq->bindParam(':datesupp', $datesupp, PDO::PARAM_STR);
		$updateReq->execute();

		//desactivation battle
		$updateReq = $dbh->prepare("UPDATE `bl_battles` SET `statut`='3' WHERE user = :id ");
		$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
		$updateReq->execute();

		//desactivation post
		$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `statut`='2' WHERE user = :id ");
		$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
		$updateReq->execute();

		//selection des likes posts + desactivation like
		$postResult = $dbh->prepare("SELECT * FROM `bl_battle_posts` WHERE `user` = :id and `statut`='2'");
		$postResult->bindParam(':id', $id_user, PDO::PARAM_STR);
		$postResult->execute();

		if ($postResult->rowCount() > 0) {

			while ($row_postResult = $userResult->fetch(PDO::FETCH_OBJ)) {
				$updateReq = $dbh->prepare("UPDATE `bl_likes_post` SET `statut`='2' WHERE user = :id ");
				$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
				$updateReq->execute();
			}
		}

		//desactivation amis
		$updateReq = $dbh->prepare("UPDATE `bl_user_friend` SET `statut`=2 WHERE (user=:id or friend = :id)");
		$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
		$updateReq->execute();

		//delete demande amis
		$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE (user=:id or qui = :id) and statut =1");
		$deleteReq->bindParam(':id', $id_user, PDO::PARAM_STR);
		$deleteReq->execute();

		save_log($id_user, '16', $dbh);

		//email nouveau contenu si par detenteur de la battle
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="https://www.republike.io" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
										
											<p style="color:#777777;">Bonjour ' . $prenom . ',<br><br>
											Vous compte a été supprimé mais vous nous manquez déjà !
											<br><br>
											Vous serez toujours bienvenu sur Republike.<br /><br /></p>
											
											<table width="250" border="0" cellspacing="0" cellpadding="0" style=" background-color:#ff0000; color:#fff;">
												  <tr align="center" height="40">
													<td align="center" height="40">
														<a target="_blank" href="' . $urlSite . '/"
														style="text-decoration:none; color:#fff; font-size:14px; line-height:22px;"> 
															PAGE D\'ACCUEIL
														</a>
													</td>
												</tr>
											  </table><br />
											
											<p style="font-size:10px; color:#777777;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br />
											<b>Pour paramètrer vos alertes, rendez-vous dans "Mon compte / Paramètres".</b><br /><br />
											© 2019 REPUBLIKE</p>
																								
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';

		$to = $email;
		//$to='dev@mkael-group.com';
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader('Votre compte va être supprimé ☹');

		if ($to != '') {
			mail($to, $sujet, $msg, $headers);
		}
	}
}

//function
function save_log($id_user, $log, $dbh)
{

	$dbh->exec("INSERT INTO `bl_logs` (`user`, `type`) 
		VALUES ('$id_user', '$log')");
}
