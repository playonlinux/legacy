<?php
require_once('./database.inc.php');

$now = date('Y-m-d');

$userConnexion = $dbh->prepare(
	"	SELECT distinct(`user`) as user_id,
		(SELECT ADDDATE(max(DATE_FORMAT(date, '%Y-%m-%d')), INTERVAL 7 DAY) as date  FROM `bl_connexion` where bl_connexion.`user` = user_id) as date2
		FROM `bl_connexion`
		ORDER BY `date2` ASC
	"
);
$userConnexion->execute();

if ($userConnexion->rowCount() > 0) {

	while ($row_userConnexion = $userConnexion->fetch(PDO::FETCH_OBJ)) {

		$date2 = $row_userConnexion->date2;
		$user = $row_userConnexion->user_id;

		// if ($date2 !== $now) {
		// 	return;
		// }

		save_log($user, '8', $dbh);

		$prenom = prenom_user($user, $dbh);
		$email = email_user($user, $dbh);

		//email nouveau contenu si par detenteur de la battle
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif;">
			<tbody>
				<tr>
					<td height="73" align="center" valign="top">
						<table width="400" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
									<tr>
									<td height="58" valign="middle" style="padding:9px">
										<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr align="center">
												<td valign="middle" style="padding:0px 9px">
												<a href="https://www.republike.io" target="_blank">
													<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
												</a>
												</td>
											</tr>
										</tbody>
										</table>
									</td>
									</tr>
									</tbody>
								</table>
							</td>
							</tr>
							</tbody>
						</table>
						<table align="center" width="400" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
								<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr align="center">
											<td width="400" height="214" align="center">
											<p>Bonjour ' . $prenom . ',<br><br>
											N\'abandonnez pas la lutte !
											<br><br></p>
												
											  <table width="250" border="0" cellspacing="0" cellpadding="0" style="background-color:#ff0000; color:#FFFFFF;">
												  <tr align="center" height="50">
													<td align="center" height="50">
														<a target="_blank" href="' . $urlSite . '" 
														style="text-decoration:none; color:#FFFFFF; font-size:14px; line-height:20px;"> 
														ME CONNECTER
														</a>
													</td>
												</tr>
											  </table><br />
												
												<p style="font-size:10px;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br /><br />
												© 2018 REPUBLIKE</p>
																									
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>';

		$to = $email;
		//$to='dev@mkael-group.com';
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader('Le pouvoir est en jeu !');
		var_export($sujet);
		if ($to != '') {
			mail($to, $sujet, $msg, $headers);
		}
	}
}

//function
function save_log($id_user, $log, $dbh)
{

	$dbh->exec("INSERT INTO `bl_logs` (`user`, `type`) 
		VALUES ('$id_user', '$log')");
}

function prenom_user($id, $dbh)
{

	$sth = $dbh->prepare("SELECT * FROM `bl_user` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$id_user = $row_userResult->id;
		$prenom = $row_userResult->prenom;
	}

	return $prenom;
}

function email_user($id, $dbh)
{

	$sth = $dbh->prepare("SELECT * FROM `bl_user` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$id_user = $row_userResult->id;
		$email = $row_userResult->email;
	}

	return $email;
}
