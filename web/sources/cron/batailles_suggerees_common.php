<?php

// require_once('./database.inc.php');
require_once('../php/mysql.inc.php');

$defaultMail = (!empty($_POST['adminEmail'])) ? $_POST['adminEmail'] : '';

function run($frequency, $codeLog)
{
	global $dbh, $urlSite, $defaultMail, $_;

	$query = $dbh->prepare("SELECT id, code, prenom, email FROM `bl_user` WHERE `statut` = 2");
	$query->execute();

	if (!$query->rowCount()) {
		return false;
	}

	while ($row = $query->fetch(PDO::FETCH_OBJ)) {

		$id_user = $row->id;
		$code = $row->code;
		$prenom = $row->prenom;
		$email = $row->email;

		require '../php/lang_' . $code . '.php';

		list($liste_battles, $objet) = battle_sugg($id_user, $code, $dbh);

		if (alert_user(9, $id_user, $dbh) != $frequency) {
			continue;
		}

		save_log($id_user, $codeLog, $dbh);

		//email nouveau contenu si par detenteur de la battle
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
			<tbody>
				<tr>
					<td height="73" align="center" valign="top">
						<table width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
									<tr>
									<td height="58" valign="middle" style="padding:9px">
										<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr align="center">
												<td valign="middle" style="padding:0px 9px">
												<a href="' . $urlSite . '" target="_blank">
													<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
													src="' . $urlSite . '/images/logo-republilke-entier.png"  
													style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
												</a>
												</td>
											</tr>
										</tbody>
										</table>
									</td>
									</tr>
									</tbody>
								</table>
							</td>
							</tr>
							</tbody>
						</table>
						<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
								<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr align="center">
											<td width="420" height="214" align="center">
											
											<p style="color:#777777;">' . $_['mailing']['hello']($prenom) . ',<br><br>
												' . $_['mailing']['battle']['suggested']['body'] . ' 
												<br><br>
												' . $liste_battles . '<br />
												</p>
												
												<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																									
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>';

		$to = (!empty($defaultMail)) ? $defaultMail : $email;
		//$to='dev@mkael-group.com';
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader($objet);

		if (!empty($to) && !empty($sujet)) {
			mail($to, $sujet, $msg, $headers);
		}
	}
}

//function
function save_log($id_user, $log, $dbh)
{

	$dbh->exec("INSERT INTO `bl_logs` (`user`, `type`) 
		VALUES ('$id_user', '$log')");
}

function alert_user($alert, $id_user, $dbh)
{

	$query = $dbh->prepare("SELECT `opt` FROM `bl_user_alerts` WHERE `user` =:user and `alert` = :alert");
	$query->bindParam(':user', $id_user, PDO::PARAM_STR);
	$query->bindParam(':alert', $alert, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount() > 0) {

		$row = $query->fetch(PDO::FETCH_OBJ);
		$opt = $row->opt;
	}

	return $opt;
}

function battle_sugg($id_user, $code, $dbh)
{

	global $urlSite, $_;

	$sujet = '';
	$battle = '';

	$select_amis = $dbh->prepare("SELECT cat FROM `bl_user_cat` WHERE `user` =:user ORDER BY RAND() LIMIT 0, 4");
	$select_amis->bindParam(':user', $id_user, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$i = 0;
		$sujet = '';
		while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) {
			$id_battle = recherche_battle($row_amis->cat, $dbh);
			if (!empty($id_battle)) {
				$imagePartage[$i] = photo_battle_mail($id_battle, $dbh);
				$sujet .= name_cat($row_amis->cat, $dbh) . ',';
				$url_battle[$i] = url_battle($id_battle, $dbh);
				$i++;
			}
		}

		if ($i != 0) {
			$battle = '<table width="420" border="0" cellspacing="0" cellpadding="0">
			  <tr valign="top">
				<td align="center" width="205">
					<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_battle'][$code] . '/' . $url_battle[0] . '" style="text-decoration:none; border:none;"> 
						<img src="' . $imagePartage[0] . '" width="90%" style="display:block; border:none; margin-bottom: 5px;"> 
					</a>
					<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_battle'][$code] . '/' . $url_battle[2] . '" style="text-decoration:none;border:none;"> 
						<img src="' . $imagePartage[2] . '"width="90%" style="display:block; border:none;"> 
					</a>
				</td>
				<td  align="center" width="205">
					<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_battle'][$code] . '/' . $url_battle[1] . '" style="text-decoration:none;border:none;"> 
						<img src="' . $imagePartage[1] . '" width="90%" style="display:block; border:none; margin-bottom: 5px;"> 
					</a>
					
					<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_battle'][$code] . '/' . $url_battle[3] . '" style="text-decoration:none;border:none;"> 
						<img src="' . $imagePartage[3] . '" width="90%" style="display:block; border:none;"> 
					</a>
				</td>
			  </tr>
			</table>';

			$sujet .= ' ' . $_['mailing']['battle']['suggested']['subject'];
		}
	}

	return array($battle, $sujet);
}

function url_battle($id, $dbh)
{
	$url = "";
	$sth = $dbh->prepare("SELECT url FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$url = $row_userResult->url;
	}

	return  $url;
}

function recherche_battle($cat, $dbh)
{

	$id_battle = 0;
	$select_amis = $dbh->prepare("SELECT id FROM `bl_battles` WHERE `category` =:cat ORDER BY RAND() limit 0,1");
	$select_amis->bindParam(':cat', $cat, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		$id_battle = $row_amis->id;
	}

	return $id_battle;
}

function photo_battle_mail($id_battle, $dbh)
{

	global $urlSite;
	$imagePartage = '';
	$sth = $dbh->prepare("SELECT * FROM bl_battle_posts WHERE battle = :id and statut = 1 order by likes DESC limit 0,1");
	$sth->bindParam(':id', $id_battle, PDO::PARAM_STR);
	$sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$post = $row_userResult->post;
		$type = $row_userResult->type;

		if ($type == 1) {
			$imagePartage = '' . $urlSite . '/' . $post;
		}

		if ($type == 2) {

			list($video, $image) = video($post);

			if ($image == '') {
				$imagePartage = '' . $urlSite . '/images/partage_video_republike.jpg';
			} else {
				$imagePartage = $image;
			}
		}

		if ($type == 3) {
			$imagePartage = '' . $urlSite . '/images/partage_texte_republike.jpg';
		}
	}

	return  $imagePartage;
}

function name_cat($cat, $dbh)
{

	$name_cat = "";
	$select_amis = $dbh->prepare("SELECT name_fr FROM `bl_categories` WHERE `id` =:cat limit 0,1");
	$select_amis->bindParam(':cat', $cat, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		$name_cat = $row_amis->name_fr;
	}

	return $name_cat;
}

function video($video)
{

	$lien = "";
	$image = "";

	//echo $video;

	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match)) {
		$video_id = $match[1];
		$video = 'https://www.youtube-nocookie.com/embed/' . $video_id . '?rel=0&amp;showinfo=0&amp;origin=https://www.republike.io';
		$image = "https://i.ytimg.com/vi/" . $video_id . "/hqdefault.jpg";
	}

	if (preg_match("@^(?:https://dai.ly/)?([^/]+)@i", $video)) {
		$video = str_replace("https://dai.ly/", "https://www.dailymotion.com/embed/video/", $video);
	}

	if (preg_match("@^(?:https://www.dailymotion.com/video/)?([^/]+)@i", $video)) {
		$video = str_replace("https://www.dailymotion.com/video/", "https://www.dailymotion.com/embed/video/", $video);
	}

	return array($video, $image);
	//$video;
}
