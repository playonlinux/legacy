<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
  <script src="../vendor/jquery/jquery.min.js"></script>
</head>

<body>
  <main>
    <input type="email" id="email" placeholder="Enter email to redirect to">
    <?php
    $entries = [
      ['label' => 'Bataille 1 jour', 'action' => 'batailles_suggerees_jour.php'],
      ['label' => 'Bataille 1 semaine', 'action' => 'batailles_suggerees_semaine.php'],
      ['label' => 'Bataille 1 mois', 'action' => 'batailles_suggerees_mois.php'],
      ['label' => 'Relance connexion', 'action' => 'relanceConnexion.php', 'attributes' => 'disabled'],
      ['label' => 'Relance Finalisation', 'action' => 'relanceFinalisation.php', 'attributes' => 'disabled'],
      ['label' => 'Relance Jour 1', 'action' => 'relanceJour_un.php', 'attributes' => 'disabled'],
      ['label' => 'Relance Jour 6', 'action' => 'relanceJour_six.php', 'attributes' => 'disabled'],
      ['label' => 'Relance Jour 7', 'action' => 'relanceJour_sept.php', 'attributes' => 'disabled']
    ];

    foreach ($entries as $entry) {
      ?>
      <button data-action="<?= $entry['action'] ?>" <?= $entry['attributes'] ?>><?= $entry['label'] ?></button>
    <?php
    }
    ?>
  </main>
  <script>
    $(document).ready(function() {
      $('button').click(function() {
        const url = $(this).data('action');
        const params = {
          adminEmail: $("#email").val()
        };
        $.post({
          url: url,
          data: $.param(params),
          success: function(data) {},
          error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
              console.log("Not connect.\n Verify Network.");
            } else if (jqXHR.status == 404) {
              console.log("Requested page not found. [404]");
            } else if (jqXHR.status == 500) {
              console.log("Internal Server Error [500].");
            } else if (exception === "parsererror") {
              console.log(jqXHR.responseText);
            } else if (exception === "timeout") {
              console.log("Time out error.");
            } else if (exception === "abort") {
              console.log("Ajax request aborted.");
            } else {
              console.log("Uncaught Error.\n" + jqXHR.responseText);
            }
          }
        });
      });
    });
  </script>
</body>

</html>