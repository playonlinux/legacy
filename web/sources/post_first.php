<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

$id = $_GET['id'];
$token = $_GET['token'];

if ($token != '' && $id != '0') {

    $sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE id = :id and password = :token limit 0,1');
    $sth->bindParam(':id', $id, PDO::PARAM_STR);
    $sth->bindParam(':token', $token, PDO::PARAM_STR);
    $rs = $sth->execute();

    if ($sth->rowCount() > 0) {

        $row = $sth->fetch(PDO::FETCH_OBJ);
        $nom = $row->nom;
        $prenom = $row->prenom;
        $email = $row->email;
        $id_user = $row->id;
        $statut = $row->statut;
    }
}
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |   Confirmation d'inscription">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Postez votre premier contenu</title>

    <?php include('required.php'); ?>

</head>

<body>
    <!-- HEADER -->
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container">

            <!--sections-->
            <form method="post" action="" name="form_save_theme" id="form_save_theme" enctype="multipart/form-data">
                <input type="hidden" class="form-control" autocomplete="off" name="user" value="<?= $id ?>" required>
                <input type="hidden" class="form-control" autocomplete="off" name="token" value="<?= $token ?>" required>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12 mt-3 mb-3">
                                <h4 class="center-block">
                                    <b><?= $_['user']['are-you'] ?></b>
                                </h4>
                            </div>
                        </div>
                        <div class="sh-section__edit-post">
                            <div class="row">

                                <?php
                                $genders = [
                                    [
                                        'type' => 'women',
                                        'path' => 'women',
                                        'value' => 1,
                                        'checked' => ($_GET['gender'] == 1) ? 'checked' : ''
                                    ],
                                    [
                                        'type' => 'man',
                                        'path' => 'men',
                                        'value' => 2,
                                        'checked' => ($_GET['gender'] == 2) ? 'checked' : ''
                                    ]
                                ];

                                foreach ($genders as $gender) { ?>
                                    <div class="sh-section__item col-6">
                                        <div class="sh-section" style="background:transparent">
                                            <div class="sh-section__content">
                                                <div class="sh-section__image" style="display: flex;flex-direction: column;">
                                                    <label class="sh-checkbox-cat">
                                                        <input type="radio" name="gender[]" id="<?= $gender['type'] ?>" value="<?= $gender['value'] ?>" <?= $gender['checked'] ?>>
                                                        <span style="width: 60%; transform: translate(11%, -2%);">R</span>
                                                        <img src="/images/icons/<?= $gender['path'] ?>/svg/serf.svg" style="width: 30%; height: 30%">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12 mt-3 mb-3">
                                <h4 class="center-block">
                                    <b><?= $_['my-account']['notif-email'] ?></b>
                                </h4>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="sh-section__edit-post">
                            <div class="row">
                                <?php
                                $select_alerts = $dbh->prepare("SELECT titre_" . $code . " alert_name, opt1, opt5 FROM `bl_alerts` WHERE `statut` = 1 ORDER BY id ASC");
                                $select_alerts->execute();

                                if ($select_alerts->rowCount() > 0) {
                                    $num_alert = 1;
                                    while ($row_alerts = $select_alerts->fetch(PDO::FETCH_OBJ)) { ?>
                                        <div class="col-lg-12 mb-3">
                                            <div class="sh-confirm__form">
                                                <div class="row">
                                                    <div class="col-6" style="font-size:1.8rem;"><?= $row_alerts->alert_name ?></div>
                                                    <div class="col-6 text-right">
                                                        <label class="switch">
                                                            <input type="checkbox" class="chk_alert" name="alert_<?= $num_alert ?>" id="alert_<?= $num_alert ?>" <?= ($option == $row_alerts->opt1) ? "checked" : "" ?>>
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                <? $num_alert++;
                                    }
                                } ?>
                            </div>

                        </div>
                    </div>
                    <br>
                    <hr>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12 mt-3 mb-3">
                                    <h4 class="center-block">
                                        <b><?= $_['categories']['select'] ?></b>
                                    </h4>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <br>
                            <!--section-->
                            <div class="sh-section__edit-post">
                                <!--section-->
                                <div class="row">
                                    <?php $select_posts = $dbh->prepare("SELECT * FROM `bl_categories` WHERE `statut` = '1' order by name_fr asc ");
                                    $select_posts->execute();

                                    if ($select_posts->rowCount() > 0) {
                                        while ($row_posts = $select_posts->fetch(PDO::FETCH_OBJ)) { ?>
                                            <!--section-->
                                            <div class="sh-section__item col-lg-2 col-md-4 col-sm-4 col-6">
                                                <div class="sh-section">

                                                    <div class="sh-section__content">
                                                        <div class="sh-section__image">
                                                            <label class="sh-checkbox-cat">
                                                                <input type="checkbox" class="chk-categories" name="cat[]" id='<?= $row_posts->id ?>' value="<?= $row_posts->id ?>">
                                                                <span>R</span>

                                                                <?php if ($row_posts->visuel != '') { ?>
                                                                    <img src="<?= $row_posts->visuel ?>" alt="<?= $row_posts->name_fr ?>">
                                                                <?php } else { ?>
                                                                    <img src="images/logo-republilke.png" alt="<?= $row_posts->name_fr ?>">
                                                                <?php } ?>

                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="sh-section__footer text-center">
                                                        <p class="text-center" style="width:100%;"><?= $row_posts->name_fr ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                    <? }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>

            <script>
                require(['app'], function() {
                    require(['modules/main']);
                    require(['modules/signup']);
                });
            </script>
        </div>
    </main>

    <!-- FOOTER -->
    <?php include('footer.php'); ?>
</body>

</html>