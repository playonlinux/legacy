<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  <meta name="description" content="<?= $nameSite ?> |  Conditions générales">
  <meta name="author" content="battlelike.com">
  <title><?= $nameSite ?> | Conditions générales</title>

  <link rel="canonical" href="/<?= $code ?>/terms-of-service" />
  <?php if ($code == 'fr') {
    $footerEN = '/en/terms-of-service'; ?>
    <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
  <?php } else if ($code == 'en') {
    $footerFR = '/fr/conditions-generales'; ?>
    <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
  <?php } ?>

  <?php include('required.php'); ?>

</head>

<body>
  <?php include('header.php'); ?>
  <main style="padding-top: 13rem;">
    <div class="container">
      <h1 style="text-align:center">
        <strong>CONDITIONS GENERALES D’UTILISATION</strong>
      </h1>
      <br>
      <h2 style="text-align:justify">
        <strong>PREAMBULE</strong>
      </h2>
      <br>
      <br>
      <h3 style="text-align:justify">
        <strong>Editeur et directeur de la publication</strong>
      </h3>
      <br>
      <br>
      <p style="text-align:justify">
        Le directeur de publication est Etienne de Sainte Marie.
      </p>
      <p style="text-align:justify">
        L’Editeur peut être contacté par email à l’adresse suivante :
        <a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a>.
      </p>
      </strong>
      <br>
      <br>
      <h3 style="text-align:justify">
        <strong>Hébergeur</strong>
      </h3>
      <br>
      </strong>
      <br>
      <p style="text-align:justify">
        Le Site est hébergé par la société OVH SAS, sis 2 rue Kellermann (BP 80157
        - 59053 Roubaix Cedex 1). L’hébergeur est joignable par téléphone au numéro
        suivant : +33 8 20 69 87 65
      </p>
      <p style="text-align:justify">
        Les présentes CGU sont exclusivement soumises au droit français et ont pour
        objet de définir les conditions d’utilisation du Site. Les CGU sont
        accessibles à tout moment par un lien direct en bas de chaque page du Site.
      </p>
      <p style="text-align:justify">
        L’Editeur édite le Site pour y héberger les contenus publiés par les
        utilisateurs inscrits sur le Site (ci-après, les <strong>Utilisateurs</strong>). Avant d’utiliser le Site, nous recommandons
        aux Utilisateurs de lire attentivement et de comprendre les présentes CGU
        qu’ils devront avoir accepté pour pouvoir créer leur compte personnel sur
        le Site (le <strong>Compte</strong>) et interagir avec le Site. Sans
        acceptation de leur par des présentes CGU, les Utilisateurs ne pourront ni
        s’inscrire sur le Site, ni publier un quelconque contenu notamment.
      </p>
      <p style="text-align:justify">
        Un « contenu » se définit comme (i) tout texte (y compris les liens vers
        des fichiers de texte), (ii) toute photo (y compris les liens vers des
        fichiers photo), (iii) toute image (y compris les liens vers des fichiers
        image), (iv) tout enregistrement audio (y compris les liens vers des
        fichiers audio), (v) toute vidéo (y compris les liens vers des fichiers
        vidéo), (iv) toute combinaison audiovisuelle (y compris les liens vers des
        fichiers audiovisuels), et (v) plus généralement, toute donnée enregistrée
        sur le Site.
      </p>
      <p style="text-align:justify">
        La signature électronique de l’Utilisateur, preuve de son acceptation des
        présentes, consiste à cocher la case "J'ai lu et accepte les conditions
        générales d’utilisation du Site" lors de son inscription sur le Site. Cette
        acceptation ne peut être que pleine et entière. Toute adhésion sous réserve
        est considérée comme nulle et non avenue.
      </p>
      <p style="text-align:justify">
        <a name="_GoBack"></a>
        Le Site est formellement interdit aux moins de 13 ans. La date de naissance
        de chaque Utilisateur lui est demandée lors de son inscription sur le Site,
        laquelle inscription ne peut aboutir si la date de naissance qu’il
        renseigne indique qu’il a moins de 13 ans.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 1 – Description du service</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Le Site est une plateforme électronique permettant aux Utilisateurs de
        lancer des concours de contenu sur des thématiques qu’ils choisissent. Dans
        ce cadre, les Utilisateurs peuvent (i) publier des contenus, et/ou (ii)
        apprécier positivement des contenus, y compris les leurs, en leur
        attribuant des « likes » (ci-après les <strong>Likes</strong>).
      </p>
      <p style="text-align:justify">
        Les Likes sont au cœur du Site puisqu’ils déterminent le classement de
        chaque contenu par rapport aux autres, les contenus recevant plus de Likes
        apparaissant avant les contenus recevant moins de Likes.
      </p>
      <p style="text-align:justify">
        Les contenus ayant reçu un même nombre de Likes y sont classés entre eux
        par ordre chronologique, les contenus plus récents apparaissant avant les
        contenus moins récents.
      </p>
      <p style="text-align:justify">
        Chaque Utilisateur dispose d’un certain nombre de Likes gratuits. Il peut
        également recevoir des Likes supplémentaires fonction de ses interactions
        avec le Site (voir sur le Site, les Lois de la République).
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 2 – Inscription au Site et Compte de l’Utilisateur</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Pour s’inscrire sur le Site et créer son Compte, chaque visiteur doit avoir
        13 ans ou plus. Il doit remplir un formulaire d’inscription. Une fois son
        inscription finalisée, l’Utilisateur dispose d’un identifiant de connexion
        (login et mot de passe) lui permettant d’accéder à son Compte et reçoit un
        courrier électronique de validation envoyé à l’adresse de messagerie
        électronique indiquée par lui.
      </p>
      <p style="text-align:justify">
        Il appartient à chaque Utilisateur de choisir un mot de passe suffisamment
        sécurisé pour éviter tout accès aux données personnelles de son Compte
        et/ou toute usurpation d'identité par un tiers. L’Editeur ne pourra être
        tenu responsable d'un tel accès causé par une sécurisation insuffisante du
        mot de passe. Il est recommandé aux Utilisateurs de choisir un mot de passe
        d'au moins 8 caractères contenant des lettres, des chiffres et des
        caractères spéciaux.
      </p>
      <p style="text-align:justify">
        Les Utilisateurs s’engagent à ne pas enregistrer d’informations fausses
        (qu’elles soient erronées et/ou mensongères), attentatoires aux droits des
        tiers (usurpation d’identité etc.) et/ou contraires à la loi, à l’ordre
        public et/ou aux bonnes mœurs. Chaque Utilisateur s'engage également à
        mettre à jour son Compte et à y apporter sans délai toute modification
        nécessaire concernant ses informations. L’Utilisateur est informé et
        accepte que les informations saisies aux fins de création ou de mise à jour
        de son Compte vaillent preuve de son identité et de son âge. Les
        informations saisies par l’Utilisateur l’engagent dès leur validation.
      </p>
      <p style="text-align:justify">
        Chaque Utilisateur sera seul responsable des conséquences pouvant résulter
        de la fourniture d'informations invalides (qu’elles soient erronées et/ou
        mensongères), attentatoires aux droits des tiers et/ou contraires à la loi,
        à l’ordre public et/ou aux bonnes mœurs. L’Editeur ne jouant qu'un rôle
        d'intermédiaire technique passif pour la mise en ligne desdites
        informations, il ne contrôle pas ces informations.
      </p>
      <p style="text-align:justify">
        L’Utilisateur enregistré est seul autorisé à utiliser son Compte. Il n’y
        a accès qu’après avoir renseigné son identifiant de connexion.
      </p>
      <p style="text-align:justify">
        L’Utilisateur est seul responsable des informations qui lui permettent
        d’accéder à son Compte (identifiant de connexion), lesquelles sont
        strictement personnelles, confidentielles et ne doivent jamais être
        communiquées à un tiers. A cet égard, l’Utilisateur négligeant s’engage à
        indemniser l’Editeur de tout préjudice subi par ce dernier suite à
        l’utilisation frauduleuse par un tiers du Compte de l’Utilisateur.
        L’Utilisateur s’engage en outre à informer l’Editeur de toute utilisation
        frauduleuse de son Compte, et ce dès qu’il en a connaissance.
      </p>
      <p style="text-align:justify">
        L’ouverture du Compte est entièrement gratuite.
      </p>
      <p style="text-align:justify">
        L’Utilisateur s’engage à ne pas créer d’autres Comptes ni à utiliser le
        Compte d’un tiers.
      </p>
      <p style="text-align:justify">
        L’Editeur peut, à tout moment, supprimer un Compte qui lui paraitrait
        contrevenir aux dispositions ci-dessus, sans délais ni indemnisation
        d’aucune sorte.
      </p>
      <p style="text-align:justify">
        L’Editeur informe les Utilisateurs qu’ils disposent d'un droit d'accès, de
        rectification et de suppression de leurs données personnelles dans le cadre
        de la réglementation en vigueur. Les Utilisateurs peuvent à tout moment
        accéder et modifier en ligne les informations associées à leur profil dès
        que leur inscription est finalisée. Pour plus de détails, voir <em><strong><a href="#article6">Article 6</a></strong></em> ci-dessous.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 3 – Acceptation des CGU</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        En tant que de besoin, il est rappelé que l’acceptation des présentes CGU
        est un prérequis à l’ouverture d’un Compte et à l’utilisation du Site.
      </p>
      <p style="text-align:justify">
        Les Utilisateurs ne peuvent ni utiliser le Site ni accepter les présentes
        CGU si les lois de leur pays de résidence ou du pays à partir duquel ils
        accèdent au Site ou l’utilisent leur interdisent de recevoir ou d'utiliser
        le Site ou en prohibent la réception ou l’utilisation.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 4 – Modification des CGU </strong>
      </h3>
      <br>
      <p style="text-align:justify">
        L’Editeur se réserve le droit de modifier périodiquement les présentes CGU,
        pour les adapter aux modifications législatives et réglementaires, ou
        améliorer les fonctionnalités proposées dans le cadre du Site. Par
        conséquent, les Utilisateurs doivent consulter régulièrement ces CGU pour
        être informés des modifications effectuées. En cas de modification des CGU,
        la version modifiée des CGU est seule publiée. Continuer d’utiliser le Site
        une fois publiées les CGU modifiées vaut pour l’Utilisateur acceptation
        desdites CGU modifiées.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 5 – Résiliation</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Tout Utilisateur peut, à tout moment, résilier le contrat qui, de part
        l’acceptation des présentes CGU, le lie à l’Editeur. Pour ce faire, il lui
        suffit de (i) supprimer son Compte (commande disponible sur son Compte), ou
        (ii) notifier sa demande de résiliation en envoyant un email à <u><a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a></u>.
        Ladite résiliation entraine la fermeture sans délai du Compte de
        l’Utilisateur concerné, lequel peut également demander à ce que ne soient
        plus visibles sur le Site les contenus publiés par lui.
      </p>
      <p>
        L’Editeur peut à tout moment résilier le contrat d’un Utilisateur,
        notamment si :
      </p>
      <ul>
        <li>
          <p>
            ce dernier ne respecte pas l'un quelconque des termes des présentes
            CGU, et/ou
          </p>
        </li>
        <li>
          <p>
            l’Editeur y est contraint en vertu de la loi, et/ou
          </p>
        </li>
        <li>
          <p>
            l’Editeur décide de fermer le Site.
          </p>
        </li>
      </ul>
      <br>
      <h3 id="article6" style="text-align:justify">
        <strong>Article 6 – Cookies et données personnelles</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Les dispositions concernant le traitement des données personnelles des
        Utilisateurs, leur finalité ainsi que les cookies figurent sur une page
        distincte du Site, intitulée « Confidentialité et Cookies » accessible sur
        chaque page du Site et/ou en cliquant sur le lien suivant : Confidentialité
        et Cookies.
      </p>
      <p style="text-align:justify">
        Les Utilisateurs disposent de droits d'opposition, d'accès et de
        rectification des données les concernant. Les Utilisateurs peuvent exercer
        à tout moment ce droit via leur Compte ou en envoyant un email à <u><a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a></u>. Lors
        de la clôture d’un Compte sur le Site, les données sont conservées de
        manière confidentielle à des fins de preuve et d'archivage, pour une durée
        fixée par l'Éditeur, et au maximum pour la durée de prescription de toutes
        les actions civiles en vertu de l'article 2262 du Code Civil.
      </p>
      <p style="text-align:justify">
        Le présent <em><strong><a href="#article6">Article 6</a></strong></em> restera en vigueur
        nonobstant tous les cas d'expiration ou de résiliation des présentes.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>
          Article 7 – Conformité à la loi et autres obligations des Utilisateurs
        </strong>
      </h3>
      <br>
      <p style="text-align:justify">
        <u>
          Il appartient à l'Utilisateur de publier des contenus ne risquant pas
          de heurter la sensibilité de ceux qui peuvent y avoir accès.
        </u>
        <u>
          Les contenus étant ouverts à toutes catégories d’Utilisateurs ayant 13
          ans ou plus, les contenus publiés doivent impérativement être adaptés à
          ce large public.
        </u>
      </p>
      <p style="text-align:justify">
        Dans tous les cas, lorsqu’un Utilisateur fournit et/ou diffuse du contenu
        sur le Site, il doit s’assurer du respect des dispositions légales et
        réglementaires en vigueur, ainsi que du respect de l’ordre public et des
        bonnes mœurs.
      </p>
      <p style="text-align:justify">
        Chaque Utilisateur accepte notamment de ne pas, et s’engage à ne pas, sans
        que cette liste soit limitative :
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            Enfreindre, délibérément ou non, toute législation ou
            réglementation locale, régionale, nationale ou internationale ;
          </p>
        </li>
      </ul>
      <ul>
        <li>
          <p style="text-align:justify">
            Rendre disponible (par téléchargement, publication, partage,
            commentaire et/ou par tout autre moyen) tout contenu qui soit
            illégal, contrefaisant, injurieux, menaçant, violent, harcelant,
            diffamatoire, insultant, obscène, pornographique, haineux, raciste,
            homophobe et/ou, plus généralement, qui porterait atteinte aux
            droits des tiers et/ou au respect de la vie privée.
          </p>
        </li>
      </ul>
      <ul>
        <li>
          <p style="text-align:justify">
            Rendre disponible (par téléchargement, publication, partage,
            commentaire et/ou par tout autre moyen) tout contenu contraire au
            droit d'auteur, au droit des marques ou au droit des personnes.
          </p>
        </li>
      </ul>
      <ul>
        <li>
          <p style="text-align:justify">
            Rendre disponible (par téléchargement, publication, partage,
            commentaire et/ou par tout autre moyen) toute publicité et/ou
            message à caractère commercial ;
          </p>
        </li>
      </ul>
      <ul>
        <li>
          <p style="text-align:justify">
            Se connecter au Site via le Compte d’un tiers pour lequel
            l’Utilisateur n’est titulaire d’aucun droit.
          </p>
        </li>
      </ul>
      <p style="text-align:justify">
        En cas de doute concernant la licéité d’un contenu et/ou si la publication
        de ce contenu risque de violer la loi, l’ordre public, les bonnes mœurs
        et/ou plus généralement l’une des dispositions qui précèdent, il est
        vivement conseillé à l’Utilisateur de demander l’assistance juridique d’un
        expert ou, à défaut, de ne pas publier ce contenu.
      </p>
      <p style="text-align:justify">
        L’Editeur, en sa qualité d'hébergeur des contenus mis en ligne par les
        Utilisateurs, ne peut être tenu pour responsable desdits contenus, sauf
        dans le cas où (i) il aurait été́ dûment informé d’un contenu illicite au
        sens de la législation en vigueur, et (ii) il n’aurait pas agi promptement
        pour le retirer. L’Editeur ne jouant qu'un rôle d'intermédiaire technique
        passif pour la mise en ligne des contenus édités par les Utilisateurs, ne
        contrôle aucun de ces contenus avant leur mise en ligne, ni n’en a
        connaissance.
      </p>
      <p style="text-align:justify">
        Chaque utilisateur peut donc, et est encouragé à, nous signaler tout
        contenu qu’il estimerait abusif au regard de ce qui précède. Pour ce faire,
        il lui suffit d’aller dans un bloc contenu et cliquer sur le pictogramme «
        Signaler ce contenu ».
      </p>
      <p style="text-align:justify">
        Après avoir cliqué sur le bouton « Signalez un contenu », l’utilisateur
        requérant devra :
      </p>
      <p style="text-align:justify">
        - indiquer le motif pour lequel il souhaiterait voir le contenu retiré, et
      </p>
      <p style="text-align:justify">
        - donner sur lui les indications suivantes : nom, prénom, adresse du
        domicile et numéro de téléphone pour le joindre.
      </p>
      <p style="text-align:justify">
        Dans tous les cas, l’Editeur se réserve le droit discrétionnaire de
        supprimer du Site tout contenu, sans motif, ni préavis ni indemnité́
        d’aucune sorte, et/ou d’informer les pouvoirs publics.
      </p>
      <p style="text-align:justify">
        L’Utilisateur est seul responsable des relations qu’il pourra nouer avec
        les autres Utilisateurs et des informations qu’il pourra leur communiquer
        dans le cadre de l’utilisation du Site. Il lui appartient d’exercer la
        prudence et le discernement appropriés dans ces relations et
        communications. L’Utilisateur s’engage en outre, dans ses échanges avec les
        autres Utilisateurs, à respecter les règles usuelles de politesse et de
        courtoisie.
      </p>
      <p style="text-align:justify">
        L’Utilisateur garantit l’Editeur (et son directeur de la publication)
        contre toutes plaintes, réclamations, actions et/ou revendications que
        l’Editeur (et/ou son directeur de la publication) pourrait subir du fait de
        la violation par l’Utilisateur des présentes CGU. L’Utilisateur s’engage
        notamment à indemniser l’Editeur (et/ou son directeur de la publication) de
        tout préjudice que ce dernier subirait, et à lui payer tous les frais,
        charges et/ou condamnations que ce dernier pourrait avoir à supporter du
        fait de la violation par l’Utilisateur des présentes CGU.
      </p>
      <p style="text-align:justify">
        Les Utilisateurs s’engagent à : (i) s’interdire toute action de nature à
        interrompre, suspendre, ralentir ou empêcher la continuité des services
        assurés par le Site, (ii) ne pas s’introduire, ni tenter de s’introduire,
        dans les systèmes de l’Editeur, (iii) ne pas détourner des ressources
        système du Site, (iv) s’interdire toute action de nature à imposer une
        charge disproportionnée sur les infrastructures du Site, (v) ne pas porter
        atteinte aux mesures de sécurité et d’authentification du Site, (vi)
        s’interdire tout acte de nature à porter atteinte aux droits et intérêts
        financiers, commerciaux et/ou moraux de l’Editeur ou des autres
        Utilisateurs (notamment en terme d’image et de réputation), (vii)
        s’interdire toute action visant à détourner des Utilisateurs du Site ou à
        faire la promotion de services concurrents du Site, et (viii) plus
        généralement, ne pas contrevenir aux dispositions des présentes CGU.
      </p>
      <p style="text-align:justify">
        Chaque Utilisateur s'engage à agir de bonne foi dans le cadre des
        présentes.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 8 – Notifications aux Utilisateurs</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Les Utilisateurs acceptent que l’Editeur, via le Site, leur adresse des
        notifications par courriel, SMS ou tout autre moyen.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 9 – Responsabilité de l’Editeur</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        L’Editeur s’engage à fournir les services du Site avec diligence et selon
        les règles de l’art, étant précisé qu’il pèse sur lui une obligation de
        moyens, à l’exclusion de toute obligation de résultat, ce que les
        Utilisateurs reconnaissent et acceptent expressément.
      </p>
      <p style="text-align:justify">
        L’Editeur ne sera responsable que des dommages directs exclusivement
        imputables à une faute contractuelle commise par lui. Sauf faute grave ou
        lourde de sa part, l’Editeur ne pourra être tenu responsable d'un dommage
        éventuellement subi par un Utilisateur au titre de l'utilisation du Site ou
        de l'impossibilité d'utiliser tout ou partie du Site. L’Editeur ne pourra
        davantage être tenu responsable d'un éventuel dysfonctionnement, panne,
        retard ou interruption de l'accès au réseau Internet. L’utilisation du Site
        implique la connaissance et l’acceptation par les Utilisateurs des
        caractéristiques et des limites de l’Internet, et notamment celles
        relatives aux performances techniques, aux temps de réponse pour consulter
        ou transférer des informations, et aux risques inhérents à toute connexion
        et transmission sur Internet. L’Editeur ne sera pas responsable des
        éventuels dommages causés par cette connexion Internet, du fait notamment
        de la transmission d'un virus informatique, d'un ver, d'une bombe
        temporelle, d'un cheval de Troie, d'une bombe logique ou de toute autre
        forme de routine de programmation conçue pour endommager, détruire ou
        détériorer de toute autre manière la ou les fonctionnalités d'un terminal
        informatique ou entraver le bon fonctionnement de celui-ci. A cet égard,
        l'Utilisateur reconnait qu'il est de sa responsabilité́ d'installer des
        anti-virus et des logiciels de sécurité́ appropriés sur son matériel
        informatique et tout autre dispositif afin de le protéger contre tout
        bogue, virus ou autre routine de programmation de cet ordre.
      </p>
      <p style="text-align:justify">
        L’Editeur décline toute responsabilité en cas de perte éventuelle des
        informations accessibles dans le Compte de l’Utilisateur, celui-ci devant
        en sauvegarder une copie et ne pouvant prétendre à aucun dédommagement à ce
        titre.
      </p>
      <p style="text-align:justify">
        Pour des raisons techniques (maintenance notamment), légales, commerciales
        et/ou d’opportunité, l’Editeur se réserve la faculté discrétionnaire de
        suspendre, d'interrompre, de fermer et/ou de faire évoluer, à tout moment,
        tout ou partie du Site. Pour des raisons techniques, légales, commerciales
        et/ou d’opportunité, l’Editeur se réserve également la faculté
        discrétionnaire d'enlever ou de modifier, à tout moment, tout contenu. Il
        est entendu que de telles interventions ne pourront en aucun cas engager la
        responsabilité de l'Editeur, ni donner lieu à des indemnités ou à des
        dommages et intérêts au profit d'un Utilisateur.
      </p>
      <p style="text-align:justify">
        En tant que de besoin, il est rappelé que :
      </p>
      <p style="text-align:justify">
        - l’Editeur, en sa qualité d'hébergeur des contenus mis en ligne par les
        Utilisateurs, ne peut être tenu pour responsable desdits contenus, sauf
        dans le cas où (i) il aurait été́ dûment informé d’un contenu illicite au
        sens de la législation en vigueur, et (ii) il n’aurait pas agi promptement
        pour le retirer, et
      </p>
      <p style="text-align:justify">
        - l’Editeur (i) ne jouant qu'un rôle d'intermédiaire technique passif pour
        la mise en ligne des contenus édités par les Utilisateurs, ne contrôle
        aucun de ces contenus avant leur mise en ligne, ni n’en a connaissance, et
        (ii) se réserve le droit discrétionnaire de supprimer du Site tout contenu
        dont il aurait connaissance, sans préavis ni indemnité́ d’aucune sorte.
      </p>
      <p style="text-align:justify">
        L’Editeur ne pourra en aucun cas être tenu pour responsable de la
        disponibilité technique des sites internet et des applications mobiles
        exploités par des tiers (y compris ses éventuels partenaires) auxquels
        l’Utilisateur pourrait avoir accès par l'intermédiaire du Site. L’Editeur
        n'endosse aucune responsabilité au titre des contenus, publicités, produits
        et/ou services disponibles sur de tels sites et applications mobiles tiers
        dont il est rappelé qu’ils sont régis par leurs propres conditions
        d’utilisation. L’Editeur n'est pas non plus responsable des transactions
        intervenues entre l’Utilisateur et un quelconque tiers vers lequel
        l’Utilisateur serait orienté par l'intermédiaire du Site.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 10 – Propriété intellectuelle </strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Le Site et la technologie sous-jacente sont la propriété exclusive de
        l’Editeur, lequel est titulaire des droits de propriété intellectuelle
        concernés. Sont également couverts les marques, les noms de domaine, les
        noms commerciaux, les logos et tout autre signe distinctif relatif au Site,
        tels que les graphismes, les photographies, les animations, les vidéos et
        les textes, lesquels ne peuvent être reproduits, utilisés ou représentés
        sans l'autorisation expresse de l’Editeur sous peine de poursuites
        judiciaires.
      </p>
      <p style="text-align:justify">
        Nonobstant ce qui précède, les contenus publiés par chaque Utilisateur sur
        le Site sont et restent la propriété de chaque Utilisateur, sous réserve
        des licences suivantes concédées par chaque Utilisateur :
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            Chaque Utilisateur concède à l’Editeur une licence non-exclusive,
            transférable, susceptible de sous-licence, et gratuite, pour le
            monde entier, et pour la durée de l'inscription de l’Utilisateur,
            d'utiliser, de reproduire, représenter, publier, mettre à
            disposition, communiquer, modifier, adapter, afficher, traduire sur
            le Site et sur tous autres supports (notamment sur tout support
            physique ou numérique, dans tout communiqué ou dossier de presse ou
            financier, support de présentation, matériel promotionnel et/ou
            publicitaire, site Internet et/ou page de réseaux sociaux), par
            tous moyens, tout ou partie du contenu publié par ledit Utilisateur
            sur le Site, sans limitation dans le nombre d'exemplaires, à des
            fins de stockage, de publicité, de promotion, de marketing, de
            communication, de relations publiques et/ou pour les besoins de la
            mise en place de partenariats ou sponsors avec les partenaires de
            l'Editeur. L’Utilisateur reconnaît que toute utilisation de son
            contenu effectuée par l’Editeur préalablement à sa désinscription,
            à la suppression ou à la clôture de son Compte, ne pourra être
            remise en cause par l’Utilisateur.
          </p>
        </li>
      </ul>
      <ul>
        <li>
          <p style="text-align:justify">
            Chaque Utilisateur concède également aux autres Utilisateurs une
            licence non-exclusive, personnelle, non-transférable, insusceptible
            de sous-licence, pour le monde entier, et pour la durée de
            l'inscription de l’Utilisateur, de reproduire et de représenter
            tout ou partie du contenu publié par ledit Utilisateur sur le Site,
            à des fins strictement privées et non commerciales.
          </p>
        </li>
      </ul>
      <p style="text-align:justify">
        Il est interdit de reproduire, représenter et/ou exploiter de quelque autre
        manière que ce soit tout ou partie du Site et/ou des contenus du Site,
        et/ou des lignes de code sous-jacentes, sans l'autorisation préalable de
        l'Editeur.
      </p>
      <p style="text-align:justify">
        Par exception à ce qui précède, les Utilisateurs sont autorisés à
        reproduire (y compris en téléchargeant ou en imprimant) et représenter tout
        ou partie du Site, à des fins strictement personnelles et privées.
      </p>
      <p style="text-align:justify">
        Notamment, les Utilisateurs sont expressément autorisés à reproduire et/ou
        représenter et/ou partager tout ou partie du Site et des contenus du Site
        sur les réseaux sociaux qu’ils utilisent.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 11 - Stipulations générales</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Toute notification envoyée à l’Editeur doit se faire en envoyant un email à <u><a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a></u>.
      </p>
      <p style="text-align:justify">
        Les mentions légales et le préambule des présentes CGU font partie
        intégrante des présentes CGU et ont valeur obligatoire entre les parties,
        ainsi que tous les contenus auxquels renvoient les présentes CGU par lien
        hypertexte ou autrement.
      </p>
      <p style="text-align:justify">
        Dans l'hypothèse où une ou plusieurs modalités figurant dans les présentes
        seraient considérées comme illégales, inopposables ou inapplicables par une
        décision de justice, les autres stipulations des présentes resteront en
        vigueur, à la condition que l'économie générale des présentes ne s'en
        trouve pas significativement modifiée.
      </p>
      <p style="text-align:justify">
        La tolérance de l’Editeur à un manquement aux présentes n'altérera
        aucunement ses droits et actions à l'encontre de tout autre manquement
        similaire et/ou ultérieur.
      </p>
      <p style="text-align:justify">
        Chaque Utilisateur accepte que les droits et obligations issus des
        présentes puissent être librement et de plein droit cédés par l’Editeur à
        un tiers en cas de cession d'actifs, de fusion ou d'acquisition.
      </p>
      <p style="text-align:justify">
        Le présent article restera en vigueur nonobstant tous les cas d'expiration
        ou de résiliation des présentes.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 12 - Langue</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        En cas de contestation sur la signification d'un terme ou d'une disposition
        des présentes CGU, la langue d'interprétation est la langue française.
      </p>
      <p style="text-align:justify">
        Les CGU (et/ou un résumé des CGU) peuvent être disponibles en d’autres
        langues sur le Site, mais à titre indicatif seulement, et seulement pour en
        faciliter la lecture et la compréhension par les Utilisateurs non
        francophones. Ces traductions n’ont pas de valeur légale et, en cas de
        contradiction ou de contestation, la langue d’interprétation sera la langue
        française et les CGU de référence seront la version française des présentes
        CGU.
      </p>
      <p style="text-align:justify">
        Le présent article restera en vigueur nonobstant tous les cas d'expiration
        ou de résiliation des présentes.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 13 - Loi applicable et juridiction compétente</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Les présentes CGU sont régies et interprétées conformément au droit
        français.
      </p>
      <p style="text-align:justify">
        Les tribunaux français auront compétence exclusive pour se prononcer sur
        tous les litiges susceptibles de naître entre les parties relatifs à
        l'exécution des présentes CGU.
      </p>
      <p style="text-align:justify">
        Les litiges intervenant entre Utilisateurs doivent être réglés entre
        Utilisateurs. L’Editeur ne sera en aucun cas requis d'intervenir ou de
        régler ledit litige.
      </p>
      <p style="text-align:justify">
        Le présent article restera en vigueur nonobstant tous les cas d'expiration
        ou de résiliation des présentes.
      </p>
      <br>
      <br>
    </div>
  </main>
  <?php include('footer.php'); ?>
</body>

</html>