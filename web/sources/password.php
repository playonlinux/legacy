<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

secureGet();

if (!empty($_GET['token'])) {

    $token = base64_decode($_GET['token']);
    $id_user = intval($_GET['id']);

    $select_user = $dbh->prepare("SELECT 1 FROM `bl_user` WHERE `token` = :token AND id= :id LIMIT 0,1");
    $select_user->bindParam(':token', $token, PDO::PARAM_STR);
    $select_user->bindParam(':id', $id_user, PDO::PARAM_STR);
    $select_user->execute();
}

if (($select_user && !$select_user->rowCount()) || empty($token)) {
    session_start();
    session_unset();
    session_destroy();
    header('Location: /index.php');
    exit;
}


?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Réinitialisation de mot de passe">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Réinitialisation de mot de passe</title>

    <?php include('required.php'); ?>

</head>

<body>

    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="sh-login__password">
            <div class="sh-upload__logo"><a href="./" class="sh-logo"><img src="images/logo-republilke-entier.png" alt="REPUBLIKE"></a></div>
            <div class="sh-login__content">
                <p class="text-center"><b>RÉINITIALISER VOTRE MOT DE PASSE</b></p>
                <div class="sh-login__form">
                    <form method="post" action="" id="form_save_mdp" name="form_save_mdp" enctype="multipart/form-data">
                        <div class="sh-input-copy">
                            <input type="password" id="pass" name="pass" class="form-control" placeholder='Nouveau mot de passe*' autocomplete="off" min="8" required>
                            <a href="#" class="show-password"><i class="fa fa-eye"></i></a>
                        </div>

                        <div class="sh-input-copy">
                            <input type="password" id="pass_confirm" name="pass_confirm" class="form-control" placeholder='Confirmer nouveau mot de passe*' min="8" autocomplete="off" required>
                            <a href="#" class="show-password"><i class="fa fa-eye"></i></a>
                        </div>

                        <small>*Champs obligatoire - minimum 8 caratères</small>
                        <div class="sh-login__send mt-4">
                            <input required value="<?= $token; ?>" name="token" id="token" type="hidden">
                            <input required value="<?= $id_user; ?>" name="id" id="id" type="hidden">
                            <button type="submit" class="sh-btn mx-auto" id="bt_save">Enregistrer</button>
                        </div>
                        <div id="errorLog" class="text-center"></div>
                    </form>

                    <script>
                        require(['app'], function() {
                            require(['modules/main']);
                            require(['modules/signup']);
                        });
                    </script>

                </div>
            </div>
        </div>
    </main>

    <?php include('footer.php'); ?>

</body>

</html>