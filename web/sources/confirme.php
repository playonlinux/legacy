<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

$id = $_GET['id'];
$token = $_GET['token'];

if ($token != '' && $id != '0') {

    $sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE id = :id and password = :token limit 0,1');
    $sth->bindParam(':id', $id, PDO::PARAM_STR);
    $sth->bindParam(':token', $token, PDO::PARAM_STR);
    $rs = $sth->execute();

    if ($sth->rowCount() > 0) {

        $row = $sth->fetch(PDO::FETCH_OBJ);
        $nom = $row->nom;
        $prenom = $row->prenom;
        $email = $row->email;
        $id_user = $row->id;
        $statut = $row->statut;
        $code = $row->code;
    } else {
        header('Location: /index.php');
    }
}
?>
<!DOCTYPE html>
<html lang="fr_FR">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |   Confirmation d'inscription">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Confirmation d'inscription</title>

    <?php include('required.php'); ?>

</head>

<body>
    <!-- HEADER -->
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container">

            <div class="sh-content-head sh-content-head__flex-off">
                <div class="row">

                    <div class="col-lg-12 text-center">
                        <div class="sh-logo__home"><img src="images/logo-republilke-entier.png" alt="REPUBLIKE"></div>
                    </div>

                    <div class="col-lg-12 text-center">
                        <h4 class="center-block mt-3">
                            <?php if ($statut == 1) { ?>
                                Votre inscription est presque terminée. <br>Nous avons encore besoin de quelques informations.
                            <?php } else { ?>
                                Votre inscription a été validée. <br>Postez vos meilleurs contenus et batailles ou invitez vos amis pour gagner des LIKECOINS.
                            <?php } ?>
                        </h4>
                    </div>
                    <?php if ($statut != 1) { ?>
                        <div class="col-lg-12 text-center mt-5">
                            <a class="sh-btn center-block" href="/<?= $code ?>/<?= $_['url_bienvenue'] ?>"> PARTICIPER</a>
                        </div>
                    <?php } ?>
                </div>
            </div>


            <?php if ($statut == 1) { ?>
                <!--sections BEST-->
                <hr>

                <!--sections-->
                <form method="post" action="" name="form_save_confirm" id="form_save_confirm" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12 mt-3 mb-3">
                                    <h4 class="center-block">
                                        <b>1/ Sélectionnez vos thèmes favoris (5 minimum)</b>
                                    </h4>
                                </div>
                            </div>
                            <!--section-->
                            <div class="sh-section__edit-post">
                                <!--section-->
                                <div class="row">
                                    <?php $select_posts = $dbh->prepare("SELECT * FROM `bl_categories` WHERE `statut` = '1' order by name_fr asc ");
                                    $select_posts->execute();

                                    if ($select_posts->rowCount() > 0) {
                                        while ($row_posts = $select_posts->fetch(PDO::FETCH_OBJ)) { ?>
                                            <!--section-->
                                            <div class="sh-section__item col-lg-2 col-md-4">
                                                <div class="sh-section">

                                                    <div class="sh-section__content">
                                                        <div class="sh-section__image">
                                                            <label class="sh-checkbox-cat">
                                                                <input type="checkbox" name="cat[]" id='<?= $row_posts->id ?>' value="<?= $row_posts->id ?>">
                                                                <span>R</span>

                                                                <?php if ($row_posts->visuel != '') { ?>
                                                                    <img src="<?= $row_posts->visuel ?>" alt="<?= $row_posts->name_fr ?>">
                                                                <?php } else { ?>
                                                                    <img src="images/logo-republilke.png" alt="<?= $row_posts->name_fr ?>">
                                                                <?php } ?>

                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="sh-section__footer text-center">
                                                        <p class="text-center" style="width:100%;"><?= $row_posts->name_fr ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <? }
                                    } ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 mt-3 mb-3">
                                    <h4 class="center-block">
                                        <b>2/ Complétez votre profil</b>
                                    </h4>
                                </div>
                            </div>

                            <!--section-->
                            <div class="sh-section__edit-post">
                                <!--section-->
                                <div class="row">
                                    <div class="col-lg-4 offset-lg-1 mt-3 mb-0">
                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">
                                                <label class="sh-radio"> <input type="radio" name="sexe" value="1" checked> <span></span>
                                                    <p>Femme</p>
                                                </label>
                                                <label class="sh-radio"> <input type="radio" name="sexe" value="2"> <span></span>
                                                    <p>Homme</p>
                                                </label>
                                                <label class="sh-radio mb-4"> <input type="radio" name="sexe" value="3"> <span></span>
                                                    <p>Autre</p>
                                                </label>
                                                <input type="text" class="form-control" autocomplete="off" placeholder='Nom*' name="nom" id="nom_u" value="<?= $nom ?>" required>
                                                <input type="text" class="form-control" autocomplete="off" placeholder='Prénom*' name="prenom" id="prenom_u" value="<?= $prenom ?>" required>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 offset-lg-1 mt-3 mb-0">
                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">

                                                <input type="email" id="email_u" class="form-control" autocomplete="off" placeholder='Email*' name="email_u" value="<?= $email ?>" required>
                                                <input type="text" class="form-control" autocomplete="off" placeholder='Date de naissance* - 01/01/1970' name="naissance" id="naissance" required>
                                                <input type="text" class="form-control" autocomplete="off" placeholder='Pays*' name="ville" id="ville" required>

                                                <input type="hidden" class="form-control" autocomplete="off" name="user" value="<?= $id ?>" required>
                                                <input type="hidden" class="form-control" autocomplete="off" name="token" value="<?= $token ?>" required>

                                                <div class="sh-confirm__send">
                                                    <button type="submit" class="sh-btn" id="bt_confirm">Enregistrer</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mt-2 mb-5">
                                        <div id="errorSaveConfirm" class="text-center"></div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            <?php } ?>
            <script>
                require(['app'], function() {
                    require(['modules/main']);
                    require(['modules/signup']);
                });
            </script>
        </div>
    </main>

    <!-- FOOTER -->
    <?php include('footer.php'); ?>
</body>

</html>