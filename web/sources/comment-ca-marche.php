<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');
?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="<?= $nameSite ?> | <?= $_['sstitre_comment_meta'] ?>">
	<meta name="author" content="battlelike.com">
	<title><?= $nameSite ?> | <?= $_['sstitre_comment'] ?></title>
	<link rel="canonical" href="/<?= $code ?><?= $_['url_condition_1'] ?>" />
	<?php if ($code == 'fr') {
		$footerEN = '/en/laws-of-the-republike'; ?>
		<link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
	<?php } else if ($code == 'en') {
		$footerFR = '/fr/lois-de-la-republike'; ?>
		<link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
	<?php } ?>

	<?php include('required.php'); ?>
	<script>
		var page = "compte";
	</script>
</head>
<?php
$array = [
	[
		'img' => '/images/icons/crossed-swords.svg',
		'class' => 'icons-50',
		'message' => $_['how-it-works']['launch']
	],
	[
		'img' => '/images/icons/bow.svg',
		'class' => 'icons-50',
		'message' => $_['how-it-works']['publish']
	],
	[
		'img' => '/images/icons/liked-pink.svg',
		'class' => 'icons-50',
		'message' => $_['how-it-works']['like']
	],
	[
		'img' => '/images/icons/friends.svg',
		'class' => 'icons-50',
		'message' => $_['how-it-works']['invite']
	],
	[
		'img' => '/images/icons/share.svg',
		'class' => 'icons-50',
		'message' => $_['how-it-works']['share']
	]
];

?>

<body>
	<div class="bg-img" style="color: #ffffff;">
		<div class="container">
			<div class="topnav">
				<? include('header.php'); ?>
				<div class="container-fluid">
					<div class="sh-content-head how-it-works">
						<h3 class="how-it-works-title"><?= $_['how-it-works']['title']; ?></h3>

						<div class="how-it-works-grids">
							<ul class="how-it-works-list">
								<?php
								foreach ($array as $entry) {
									?>
									<li>
										<img src="<?= $entry['img'] ?>" class="<?= $entry['class'] ?>">
										<p><?= $entry['message'] ?></p>
									</li>
								<?php
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<main class="container">
		<article style="background-color:#eeeeee; padding: 30px;">
			<div class="how-it-works-grids">
				<ul class="how-it-works-laws">
					<li class="text-center visible-sm">
						<img src="/images/icons/balance.svg" class="how-it-works-balance">
					</li>
					<li class="how-it-works-laws-title text-center visible-sm">
						<?= $_['how-it-works']['laws-of-republike'] ?>
					</li>
					<li>
						<div class="how-it-works-grids">
							<ul class="how-it-works-text">
								<li>
									<aside>I</aside>
								</li>
								<li>
									<div class='title'>
										<?= $_['how-it-works']['quest']['title'] ?>
									</div>

									<div class="content">
										<?= $_['how-it-works']['quest']['content'] ?>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li class="how-it-works-balance text-center hidden-sm">
						<img src="/images/icons/balance.svg">
					</li>
					<li class="visible-sm">
						<div class="how-it-works-grids">
							<ul class="how-it-works-text">
								<li>
									<aside>II</aside>
								</li>
								<li>
									<div class='title'>
										<?= $_['how-it-works']['points']['title'] ?>
									</div>
									<div class="content">
										<?= $_['how-it-works']['points']['content'] ?>
										<div class="points">
											<div>
												<img src="/images/icons/gold-number.svg" class="icons">
												<span><b>9</b> <small>points</small></span>
											</div>
											<div>
												<img src="/images/icons/silver-number.svg" class="icons">
												<span><b>3</b> <small>points</small></span>
											</div>
											<div>
												<img src="/images/icons/bronze-number.svg" class="icons">
												<span><b>1</b> <small>points</small></span>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="how-it-works-grids">
							<ul class="how-it-works-text">
								<li>
									<aside>III</aside>
								</li>
								<li>
									<div class='title'>
										<?= $_['how-it-works']['currency']['title'] ?>
									</div>
									<div class="content">
										<?= $_['how-it-works']['currency']['content'] ?>
									</div>
								</li>
							</ul>
						</div>
					<li class="hidden-sm">
						<div class="how-it-works-grids">
							<ul class="how-it-works-text">
								<li>
									<aside>II</aside>
								</li>
								<li>
									<div class='title'>
										<?= $_['how-it-works']['points']['title'] ?>
									</div>
									<div class="content">
										<?= $_['how-it-works']['points']['content'] ?>
										<div class="points">
											<div>
												<img src="/images/icons/gold-number.svg" class="icons">
												<span><b>9</b> <small>points</small></span>
											</div>
											<div>
												<img src="/images/icons/silver-number.svg" class="icons">
												<span><b>3</b> <small>points</small></span>
											</div>
											<div>
												<img src="/images/icons/bronze-number.svg" class="icons">
												<span><b>1</b> <small>points</small></span>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li class="how-it-works-laws-title text-center hidden-sm">
						<?= $_['how-it-works']['laws-of-republike'] ?>
					</li>
					<li>
						<div class="how-it-works-grids">
							<ul class="how-it-works-text">
								<li>
									<aside>IV</aside>
								</li>
								<li>
									<div class='title'>
										<?= $_['how-it-works']['votes']['title'] ?>
									</div>
									<div class="content">
										<?= $_['how-it-works']['votes']['content'] ?>
									</div>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</article>
		<article>
			<div class="how-it-works-grids">
				<ul class="how-it-works-accumulate">
					<li class="text-right">
						<img src="/images/icons/coins.svg" class="how-it-works-coins">
					</li>
					<li class="how-it-works-laws-title text-center">
						<?= $_['how-it-works']['accumulate']['title'] ?>
					</li>
					<!-- <li class="text-left">
						<img src="/images/icons/purse.svg" class="how-it-works-coins">
					</li> -->
				</ul>
			</div>
			<div class="how-it-works-grids">
				<ul class="how-it-works-points">
					<?php
					foreach ([10, 50, 100, 200] as $point) {
						?>
						<li>
							<div class='title'>
								<?= $_['how-it-works']['accumulate'][$point]['title'] ?>
							</div>
							<div class="content">
								<?= $_['how-it-works']['accumulate'][$point]['content'] ?>
							</div>
						</li>
					<?php
					}
					?>
				</ul>
			</div>
		</article>
		<article style="background-color:#eeeeee; padding: 30px;">
			<div class="how-it-works-laws-title text-center">
				<?= $_['how-it-works']['levels']['title'] ?>
			</div>
			<div class="how-it-works-grids">
				<ul class="how-it-works-ranks">
					<li>
						<img src="/images/icons/gold-number.svg" class="icons-50">
						<span><b>9</b> <small>points</small></span>
					</li>
					<li>
						<img src="/images/icons/silver-number.svg" class="icons-50">
						<span><b>3</b> <small>points</small></span>
					</li>
					<li>
						<img src="/images/icons/bronze-number.svg" class="icons-50">
						<span><b>1</b> <small>points</small></span>
					</li>
				</ul>
			</div>

			<?php

			$array = [
				'first-line' => [],
				'second-line' => []
			];
			$query = $dbh->prepare("SELECT titre_$code AS grade, points FROM bl_grade WHERE statut = 1");
			$query->execute();
			$mod = 0;
			if ($query->rowCount() > 0) {
				while ($row = $query->fetch(PDO::FETCH_OBJ)) {

					$gender = ((++$mod) % 2 === 1) ? 'women' : 'men';
					$grade = grade_user_between($row->points, 'en', $dbh);
					$grade = implode('-', array_map(function ($item) {
						return trim(strtolower($item));
					}, explode(' ', trim(strtolower($grade)))));

					if ($mod <= 6) {
						$array['first-line'][] = [
							'grade' => $row->grade,
							'img' => "/images/icons/$gender/svg/$grade.svg",
							'points' => $row->points
						];
					} elseif ($row->points < 180) {
						$array['second-line'][] = [
							'grade' => $row->grade,
							'img' => "/images/icons/$gender/svg/$grade.svg",
							'points' => $row->points
						];
					} else {
						$array['second-line'][] = [
							'grade' => $row->grade,
							'img' => "/images/logo-republilke.png",
							'points' => $row->points
						];
					}
				}
			}
			?>
			<div class="how-it-works-grids">
				<?php
				foreach ($array as $class => $avatars) {
					?>
					<ul class="how-it-works-gallery <?= $class ?> text-center">
						<?php
							foreach ($avatars as $avatar) {
								?>
							<li>
								<?= $avatar['grade'] ?>
								<img src=<?= $avatar['img'] ?> class="icons-50">
								<b><?= $avatar['points'] ?></b>
							</li>
						<?php
							}
							?>
					</ul>
				<?php
				}
				?>
			</div>
		</article>
	</main>
	<?php include('footer.php'); ?>
</body>