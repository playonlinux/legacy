<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  <meta name="description" content="<?= $nameSite ?> |  Terms of service">
  <meta name="author" content="battlelike.com">
  <title><?= $nameSite ?> | Terms of service</title>

  <link rel="canonical" href="/<?= $code ?>/terms-of-service" />
  <?php if ($code == 'fr') {
    $footerEN = '/en/terms-of-service'; ?>
    <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
  <?php } else if ($code == 'en') {
    $footerFR = '/fr/conditions-generales'; ?>
    <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
  <?php } ?>

  <?php include('required.php'); ?>
</head>

<body>
  <?php include('header.php'); ?>
  <main style="padding-top: 13rem;">
    <div class="container">
      <h1 style="text-align:center">
        <strong>TERMS OF SERVICE</strong>
      </h1>
      <br>
      <h2 style="text-align:justify">
        <strong>PREAMBLE</strong>
      </h2>
      <br>
      <br>
      <h3 style="text-align:justify">
        <strong>Publisher and Publishing Director</strong>
      </h3>
      <br>
      <p style="text-align:justify">
      </p>
      <br>
      <p style="text-align:justify">
        The Publisher can be contacted by email at the following address:
        <a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a>.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Host</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The Site is hosted by OVH SAS, located at 2 rue Kellermann (BP 80157 -
        59053 Roubaix Cedex 1). The host is reachable by phone at the following
        number: +33 8 20 69 87 65
      </p>
      <p style="text-align:justify">
        These Terms are exclusively subject to French law and are intended to
        define the conditions of use of the Site. The Terms are accessible at
        any time by a direct link at the bottom of each page of the Site.
      </p>
      <p style="text-align:justify">
        The Publisher edits the Site to host the content published by users
        registered on the Site (hereinafter, the Users). Before using the Site,
        we recommend that Users carefully read and understand these Terms that
        they must have accepted in order to create their personal account on
        the Site (the Account) and interact with the Site. Without acceptance
        of them by these Terms, Users may not register on the Site, or publish
        any particular content.
      </p>
      <p style="text-align:justify">
        "Content" is defined as (i) any text (including links to text files),
        (ii) any photo (including links to photo files), (iii) any image
        (including links to image files), (iv) any audio recording (including
        links to audio files), (v) any video (including links to video files),
        (iv) any audiovisual combination (including links to audiovisual
        files), and (v) more generally, any data recorded on the Site.
      </p>
      <p style="text-align:justify">
        The electronic signature of the User, proof of his acceptance of the
        present, is to check the box "I have read and accept the terms and
        conditions of use of the Site" when registering on the Site. This
        acceptance can only be full and complete. Any membership subject to
        reservation is considered null and void.
      </p>
      <p style="text-align:justify">
        The Site is formally prohibited to children under 13 years old. The
        date of birth of each User is requested when registering on the Site,
        which registration can not succeed if the date of birth that he
        indicates indicates that he is under 13 years.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 1 - Description of the service</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The Website is an electronic platform allowing Users to launch content
        competitions on themes that they choose. In this context, Users may (i)
        publish content, and / or (ii) positively value content, including their
        own, by giving them "likes" (hereinafter the "Likes").
      </p>
      <p style="text-align:justify">
        Likes are at the heart of the Site since they determine the ranking of each
        content compared to others, the content receiving more Likes appearing
        before the content receiving less Likes.
      </p>
      <p style="text-align:justify">
        The contents having received the same number of Likes are classified
        therein in chronological order, the more recent contents appearing before
        the less recent contents.
      </p>
      <p style="text-align:justify">
        Each User has a number of free Likes. He may also receive additional Likes
        depending on his interactions with the Site (see the Site, the Laws of the
        Republic).
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 2 - Registration to the Website and User Account</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        To register on the Site and create an Account, each visitor must be 13
        years of age or over. He must fill out a registration form. Once
        registration is finalized, the User has a login (login and password) to
        access his Account and receives a validation email sent to the e-mail
        address indicated by him.
      </p>
      <p style="text-align:justify">
        It is up to each User to choose a sufficiently secure password to avoid any
        access to the personal data of his Account and / or any impersonation by a
        third party. The Publisher can not be held responsible for such access
        caused by insufficient security of the password. Users are recommended to
        choose a password of at least 8 characters containing letters, numbers and
        special characters.
      </p>
      <p style="text-align:justify">
        Users undertake not to record any false information (whether false and / or
        misleading), detrimental to the rights of third parties (impersonation
        etc.) and / or contrary to the law, to the order public and / or morality.
        Each User also undertakes to update his Account and to make any necessary
        changes to his Account without delay. The User is informed and agrees that
        the information entered for the purpose of creating or updating his Account
        is proof of his identity and age. The information entered by the User
        commits him as soon as it is validated.
      </p>
      <p style="text-align:justify">
        Each User shall be solely responsible for the consequences that may result
        from the provision of invalid information (whether erroneous and / or
        misleading) that is prejudicial to the rights of third parties and / or
        contrary to law, public order and / or morality. As the Publisher plays
        only a passive technical intermediary role for the posting of said
        information, it does not control this information.
      </p>
      <p style="text-align:justify">
        The Registered User is the only one authorized to use his Account. It only
        has access after filling in its login ID.
      </p>
      <p style="text-align:justify">
        The User is solely responsible for information that allows him to access
        his Account (login ID), which are strictly personal, confidential and must
        never be communicated to a third party. In this regard, the neglecting User
        undertakes to indemnify the Publisher for any loss suffered by the latter
        as a result of fraudulent use by a third party of the User's Account. The
        User further undertakes to inform the Publisher of any fraudulent use of
        his Account as soon as he becomes aware of it.
      </p>
      <p style="text-align:justify">
        Opening the Account is completely free.
      </p>
      <p style="text-align:justify">
        The User agrees not to create other Accounts or to use the Account of a
        third party.
      </p>
      <p style="text-align:justify">
        The Publisher may, at any time, delete an Account that appears to violate
        the above provisions without delay or compensation of any kind.
      </p>
      <p style="text-align:justify">
        The Publisher informs Users that they have a right to access, rectify and
        delete their personal data within the framework of the regulations in
        force. Users may at any time access and modify the information associated
        with their profile online as soon as their registration is finalized. For
        more details, see <em><strong><a href="#article6">Article 6</a></strong></em> below.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 3 - Acceptance of Terms</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        If necessary, it is reminded that the acceptance of these Terms is a
        prerequisite for the opening of an Account and the use of the Site.
      </p>
      <p style="text-align:justify">
        Users may neither use the Site nor accept these Terms if the laws of their
        country of residence or the country from which they access or use the Site
        prohibit them from receiving or using the Site or prohibiting their
        receipt. or use.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 4 - Modification of the Terms</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The Publisher reserves the right to periodically modify these TOS, to adapt
        them to the legislative and regulatory changes, or to improve the features
        offered in the context of the Site. Therefore, Users must regularly consult
        these Terms to be informed of the changes made. In case of modification of
        the Terms, the modified version of the Terms is only published. Continuing
        to use the Site once published the amended Terms of Use is valid for the
        User's acceptance of the modified Terms.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 5 - Termination</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Any User may, at any time, terminate the agreement which, by accepting
        these Terms, binds him to the Publisher. To do so, it is sufficient for him
        (i) to delete his Account (order available on his Account), or (ii) to
        notify his request for termination by sending an email to
        <a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a>. Such termination results in the closure of the
        Account of the User concerned without delay, who may also request that the
        contents published by him be no longer visible on the Site.
      </p>
      <p style="text-align:justify">
        The Publisher may at any time terminate the contract of a User, in
        particular if:
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            the latter does not comply with any of the terms of these Terms,
            and / or
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            the Publisher is obliged by law, and / or
          </p>
        </li>
      </ul>
      <p style="text-align:justify">
        The Publisher decides to close the Site.
      </p>
      <br>
      <h3 id="article6" style="text-align:justify">
        <strong>Article 6 - Cookies and personal data</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The provisions concerning the processing of users' personal data, their
        purpose as well as cookies appear on a separate page of the Site, entitled
        "Confidentiality and Cookies" accessible on each page of the Site and / or
        by clicking on the following link: Confidentiality and Cookies .
      </p>
      <p style="text-align:justify">
        Users have rights of opposition, access and rectification of data
        concerning them. Users may exercise this right at any time via their
        Account or by sending an email to <a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a>. When closing an
        Account on the Site, the data is kept confidential for evidentiary and
        archiving purposes, for a period of time fixed by the Publisher, and at
        most for the limitation period of all actions under article 2262 of the
        Civil Code.
      </p>
      <p style="text-align:justify">
        This <em><strong><a href="#article6">Article 6</a></strong></em> shall remain in force notwithstanding any expiration or
        termination of this Agreement.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>
          Article 7 - Compliance with the law and other obligations of Users
        </strong>
      </h3>
      <br>
      <p style="text-align:justify">
        It is the User's responsibility to publish content that does not run the
        risk of offending those who may have access to it. Since the content is
        open to all categories of users aged 13 or over, published content must be
        adapted to this broad audience.
      </p>
      <p>
        In any case, when a User provides and / or distributes content on the Site,
        he must ensure compliance with legal and regulatory provisions in force, as
        well as respect for public order and morality.
      </p>
      <p>
        Each User agrees not to, and agrees not to, without this list being
        exhaustive:
      </p>
      <p>
        - To violate, deliberately or not, any local, regional, national or
        international legislation or regulations;
      </p>
      <p>
        - Make available (by downloading, publishing, sharing, commenting and / or
        by any other means) any content that is illegal, counterfeit, abusive,
        threatening, violent, harassing, defamatory, insulting, obscene,
        pornographic, hateful, racist, homophobic and / or, more generally, that
        would infringe the rights of third parties and / or the respect of the
        private life.
      </p>
      <p>
        - Make available (by downloading, publishing, sharing, commenting and / or
        by any other means) any content that is contrary to copyright, trademark
        law or the rights of individuals.
      </p>
      <p>
        - Make available (by downloading, publishing, sharing, commenting and / or
        by any other means) any advertising and / or commercial message;
      </p>
      <p>
        - Connect to the Site via the Account of a third party for which the User
        has no rights.
      </p>
      <p>
        In case of doubt concerning the legality of a content and / or if the
        publication of this content may violate the law, public order, morality and
        / or more generally one of the preceding provisions, it is strongly advised
        the User to seek the legal assistance of an expert or, failing that, not to
        publish this content.
      </p>
      <p>
        The Publisher, as a host of the content posted online by the Users, can not
        be held responsible for the said content, except in the event that (i) it
        has been duly informed of illegal content within the meaning of the
        legislation, and (ii) he would not have acted promptly to withdraw it. The
        Publisher plays only a role of passive technical intermediary for the
        putting on line of the contents edited by the Users, does not control any
        of these contents before their putting on line, nor does it know about it.
      </p>
      <p>
        Each user can therefore, and is encouraged to, report any content that he
        considers abusive in light of the above. To do this, simply go to a block
        content and click on the pictogram "Report this content".
      </p>
      <p>
        After clicking on the "Report Content" button, the requesting user will:
      </p>
      <p>
        - indicate the reason why he would like to see the content removed, and
      </p>
      <p>
        - give him the following details: surname, first name, home address and
        telephone number to reach him.
      </p>
      <p>
        In any case, the Publisher reserves the discretionary right to remove from
        the Site any content, without cause, notice or indemnity of any kind, and /
        or inform the public authorities.
      </p>
      <p>
        The User is solely responsible for the relations he may establish with
        other Users and information that he may communicate to them in connection
        with the use of the Site. It is up to him to exercise proper prudence and
        discernment in these relations and communications. The User agrees in
        addition, in his exchanges with other Users, to respect the usual rules of
        politeness and courtesy.
      </p>
      <p>
        The User warrants the Publisher (and its Publication Director) against all
        complaints, claims, actions and / or claims that the Publisher (and / or
        its Publisher) may suffer as a result of the breach by the Publisher. User
        of these Terms. The User agrees in particular to indemnify the Publisher
        (and / or its director of the publication) for any prejudice that the
        latter would suffer, and to pay him all the costs, charges and / or
        convictions that the latter might have to bear because of the violation by
        the User of these Terms.
      </p>
      <p>
        Users undertake to: (i) refrain from any action likely to interrupt,
        suspend, slow down or prevent the continuity of the services provided by
        the Site, (ii) not to break in or attempt to break in, in the Publisher's
        systems, (iii) not divert system resources from the Site, (iv) refrain from
        any action likely to impose a disproportionate burden on the Site's
        infrastructure, (v) not undermine the measures security and authentication
        of the Site, (vi) prohibit any act likely to affect the financial,
        commercial and / or moral rights and interests of the Publisher or other
        Users (particularly in terms of image and reputation), (vii) refrain from
        any action aimed at diverting Users from the Site or promoting services
        competing with the Site, and (viii) more generally, not contravening the
        provisions of these Terms.
      </p>
      <p>
        Each User undertakes to act in good faith in the context of these presents.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 8 - User Notifications</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        Users agree that the Publisher, via the Site, sends them notifications by
        email, SMS or any other means.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 9 - Responsibility of the Publisher</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The Publisher undertakes to provide the services of the Site with due
        diligence and according to the rules of the art, being specified that it
        weighs on him an obligation of means, to the exclusion of any obligation of
        result, what Users recognize and accept expressly.
      </p>
      <p style="text-align:justify">
        The Publisher will be liable only for direct damages exclusively
        attributable to a contractual fault committed by him. Except for gross
        negligence or gross negligence on the part of the Publisher, the Publisher
        may not be held liable for any damage that may be suffered by a User in
        connection with the use of the Site or the impossibility of using all or
        part of the Site. The Publisher can not be held responsible for any
        malfunction, failure, delay or interruption of access to the Internet. The
        use of the Site implies the knowledge and acceptance by Users of the
        characteristics and limitations of the Internet, and in particular those
        relating to technical performance, response times to consult or transfer
        information, and to the risks inherent in any connection and transmission
        over the Internet. The Publisher will not be responsible for any damage
        caused by this Internet connection, especially because of the transmission
        of a computer virus, a worm, a time bomb, a Trojan, a bomb logic or any
        other form of programming routine designed to damage, destroy or otherwise
        impair the functionality or functionality of a computer terminal or hinder
        the proper functioning of the latter. In this regard, the User acknowledges
        that it is his responsibility to install anti-virus and appropriate
        security software on his computer hardware and other devices to protect
        against any bug, virus or other programming routine of this order.
      </p>
      <p style="text-align:justify">
        The Publisher declines all responsibility in the event of any loss of the
        information available in the User's Account, which must save a copy thereof
        and can not claim any compensation in this respect.
      </p>
      <p style="text-align:justify">
        For technical reasons (including maintenance), legal, commercial and / or
        opportunity, the Publisher reserves the discretion to suspend, interrupt,
        close and / or change, at any time, all or part of of the site. For
        technical, legal, commercial and / or opportunity reasons, the Publisher
        also reserves the discretion to remove or modify, at any time, any content.
        It is understood that such interventions can not in any way engage the
        responsibility of the Publisher, nor give rise to compensation or damages
        for the benefit of a User.
      </p>
      <p style="text-align:justify">
        Where necessary, it is recalled that:
      </p>
      <p style="text-align:justify">
        - the Publisher, in its capacity as host of the content posted online by
        Users, can not be held responsible for such content, except in the event
        that (i) it has been duly informed of illegal content within the meaning of
        the legislation in force, and (ii) he would not have acted promptly to
        withdraw it, and
      </p>
      <p style="text-align:justify">
        - The Publisher (i) playing only a role of passive technical intermediary
        for the putting on line of the contents edited by the Users, does not
        control any of these contents before their putting on line, nor does it
        know about it, and (ii) reserves the right to remove from the Site any
        content of which it is aware without notice or compensation of any kind.
      </p>
      <p style="text-align:justify">
        The Publisher may in no way be held responsible for the technical
        availability of websites and mobile applications operated by third parties
        (including potential partners) to which the User may access through the
        Site. The Publisher does not endorse any liability for the content,
        advertising, products and / or services available on such third-party sites
        and mobile applications which are reminded that they are governed by their
        own terms of use. The Publisher is also not responsible for transactions
        between the User and any third party to which the User is directed through
        the Site.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 10 - Intellectual property</strong>
      </h3>
      <br>
      <p style="text-align:justify">
        The Site and the underlying technology are the exclusive property of the
        Publisher, which holds the intellectual property rights concerned. Also
        covered are trademarks, domain names, trade names, logos and any other
        distinctive sign relating to the Site, such as graphics, photographs,
        animations, videos and texts, which can not be reproduced, used or
        represented without the express permission of the Publisher or face legal
        proceedings.
      </p>
      <p>
        Notwithstanding the foregoing, the content published by each User on the
        Site is and remains the property of each User, subject to the following
        licenses granted by each User:
      </p>
      <p>
        - Each User grants to the Publisher a non-exclusive, transferable,
        sublicensable, and free license, for the entire world, and for the duration
        of the registration of the User, to use, to reproduce, represent, publish,
        make available, communicate, modify, adapt, display, translate on the Site
        and in any other medium (in particular on any physical or digital medium,
        in any press release or press or financial file, presentation support,
        promotional material and / or advertising, website and / or social
        networking page), by any means, all or part of the content published by
        said User on the Site, without limitation in the number of copies, for
        purposes of storage, advertising , promotion, marketing, communication,
        public relations and / or for the purposes of setting up partnerships or
        sponsors with the Publisher's partners. The User acknowledges that any use
        of its content by the Publisher prior to unsubscription, deletion or
        closure of his Account, can not be questioned by the User.
      </p>
      <p>
        - Each User also grants to other Users a non-exclusive, personal,
        non-transferable, non-sublicensable license for the entire world, and for
        the duration of the User's registration, to reproduce and represent any or
        part of the content published by said User on the Site, for strictly
        private and non-commercial purposes.
      </p>
      <p>
        It is prohibited to reproduce, represent and / or otherwise exploit all or
        part of the Site and / or the contents of the Site, and / or underlying
        lines of code, without the prior permission of the Editor.
      </p>
      <p>
        As an exception to the foregoing, Users are authorized to reproduce
        (including downloading or printing) and to represent all or part of the
        Site, for strictly personal and private purposes.
      </p>
      <p>
        In particular, Users are expressly authorized to reproduce and / or
        represent and / or share all or part of the Website and the contents of the
        Site on the social networks they use.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 11 - General provisions</strong>
      </h3>
      <br>
      <p>
        Any notification sent to the Publisher must be done by sending an email to
        <a target="_blank" href="mailto:contact@republike.io">contact@republike.io</a>.
      </p>
      <p>
        The legal notices and the preamble of these Terms and Conditions are an
        integral part of these Terms and are binding between the parties, as well
        as all content referred to herein by hypertext link or otherwise.
      </p>
      <p>
        In the event that one or more terms contained herein are considered
        illegal, unenforceable or unenforceable by a court decision, the other
        provisions herein will remain in effect, provided that the general scheme
        of these terms and conditions does not apply. found not significantly
        changed.
      </p>
      <p>
        The Publisher's tolerance of a breach of these Terms shall not affect its
        rights and actions against any other similar and / or subsequent breach.
      </p>
      <p>
        Each User agrees that the rights and obligations arising from these Terms
        and Conditions may be freely and automatically assigned by the Publisher to
        a third party in the event of asset disposals, mergers or acquisitions.
      </p>
      <p>
        This section will remain in effect notwithstanding any expiration or
        termination of this Agreement.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 12 - Language</strong>
      </h3>
      <br>
      <p>
        In case of dispute over the meaning of a term or a provision of these
        Terms, the language of interpretation is the French language.
      </p>
      <p>
        The Terms of Use (and / or a summary of the Terms of Use) may be available
        in other languages ​​on the Site, but for information purposes only, and
        only for ease of reading and understanding by non-French-speaking Users.
        These translations have no legal value and, in the event of contradiction
        or dispute, the language of interpretation will be the French language and
        the reference GCU will be the French version of these Terms.
      </p>
      <p>
        This section will remain in effect notwithstanding any expiration or
        termination of this Agreement.
      </p>
      <br>
      <h3 style="text-align:justify">
        <strong>Article 13 - Applicable Law and Jurisdiction</strong>
      </h3>
      <br>
      <p>
        These Terms are governed by and construed in accordance with French law.
      </p>
      <p>
        French courts will have exclusive jurisdiction to rule on all disputes that
        may arise between the parties relating to the execution of these Terms.
      </p>
      <p>
        Disputes between Users must be settled between Users. The Publisher will
        under no circumstances be required to intervene or settle the dispute.
      </p>
      <p>
        This section will remain in effect notwithstanding any expiration or
        termination of this Agreement.
      </p>
      <br>
      <br>
    </div>
  </main>
  <?php include('footer.php'); ?>
</body>

</html>