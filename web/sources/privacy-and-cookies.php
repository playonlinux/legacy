<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  <meta name="description" content="<?= $nameSite ?> |  Privacy and cookies">
  <meta name="author" content="battlelike.com">
  <title><?= $nameSite ?> | Privacy and cookies</title>

  <link rel="canonical" href="/<?= $code ?>/privacy-and-cookies" />
  <?php if ($code == 'fr') {
    $footerEN = '/en/privacy-and-cookies'; ?>
    <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
  <?php } else if ($code == 'en') {
    $footerFR = '/fr/confidentialite-et-cookies'; ?>
    <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
  <?php } ?>

  <?php include('required.php'); ?>

</head>

<body>
  <?php include('header.php'); ?>
  <main style="padding-top: 13rem;">
    <div class="container">
      <h1 style="text-align:center">
        <strong>Privacy and Cookies</strong>
      </h1>
      <br>
      <p style="text-align:justify">
        Republike wishes to inform you in a clear and transparent way of the
        personal data which can be collected during your navigation on the site, as
        well as the purpose of their treatment.
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>1. Your personal account data</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        When you register (registration form) and at any time on your personal
        account, you can complete / modify:
      </p>
      <p style="text-align:justify">
        - your surname, first name, age, sex and email address, and
      </p>
      <p style="text-align:justify">
        - other information to refine your personal profile (eg profile picture,
        country of residence).
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>2. Your navigation data</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        During your navigation, the site may also collect the following information
        about you:
      </p>
      <p style="text-align:justify">
        - Frequency and duration of navigation;
      </p>
      <p style="text-align:justify">
        - Published content, shared content, Liké content, reported content, etc. ;
      </p>
      <p style="text-align:justify">
        - Pages visited etc.
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>3. Purposes of the treatment</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        The processing of these data serves the following purposes:
      </p>
      <p style="text-align:justify">
        - Authenticate site users, administer the site and optimize the user
        experience through site customization;
      </p>
      <p style="text-align:justify">
        - Allow the site to contact the users to, in particular, promote and
        optimize the use of the site;
      </p>
      <p style="text-align:justify">
        - Establish anonymous statistical analyzes measuring, in particular, the
        audience and the qualification of the site audience in order to improve its
        services;
      </p>
      <p style="text-align:justify">
        - Establish anonymous behavioral analyzes for sale, which analyzes can
        notably study the correlations between profiles and behaviors;
      </p>
      <p style="text-align:justify">
        - Customize advertising on the site according to the profile of each user
        (targeted advertising based on his personal data and / or his browsing
        data) in order to display only the advertising content likely to interest
        him and / or allow website partners to send offers directly to users
        registered in the site database.
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>4. Cookies</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        The above data may in particular be collected using cookies, whether
        registered by us (proprietary cookies) and / or by third parties
        (third-party cookies). Third-party cookies are, for example, those set up
        by an advertising agency or agency to, in particular, (i) display
        advertisements that are relevant to you according to your interests, and
        (ii) limit the number of times the same advertisement will be used.
        addressed, and (iii) measure the effectiveness of an advertising campaign.
        Share links to Facebook, Twitter and other social networks allow you to
        share content found on Republike on said social networks. When you use
        these share buttons, a third-party cookie is (or will be) installed by the
        relevant social network.
      </p>
      <p style="text-align:justify">
        A cookie (or cookie) is a set of data stored on the hard drive of your
        device (computer, tablet, mobile) through your browser software during the
        consultation of an online service. Cookies registered (or that will be
        registered) by us (proprietary cookies) or by third parties (third-party
        cookies) when you visit our site recognize the device you are using. These
        cookies, far from causing any damage to your device, make it easier to find
        your configuration preferences, to pre-fill certain fields and to adapt the
        content of our services.
      </p>
      <p style="text-align:justify">
        You alone (e) choose if you want to have cookies stored on your device,
        knowing that you can easily refuse and / or delete the registration of all
        or part of these cookies. However, it is important to note that any setting
        that you can undertake will be likely to alter the fluidity of your
        navigation on Republike as well as access to some of the proposed features.
        To set your cookies, you can configure your browser software so that
        cookies are saved in your device or, conversely, they are rejected. Some
        browsers allow you to define rules to manage cookies site by site. For the
        management of cookies, the configuration of each browser is different. It
        is usually described in the help menu of your browser, which will let you
        know how to change your wishes.
      </p>
      <p style="text-align:justify">
        Note that this procedure will not prevent the display of contextual
        advertising (also called semantic advertising) on ​​the websites you visit.
        It will only block targeted and personalized advertising based on your
        personal data.
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>5. Modification of our privacy policy</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        <a name="_GoBack"></a>
        We may occasionally modify this Privacy Policy. When necessary, we will
        notify you and / or seek your agreement. We advise you to regularly check
        this page to be aware of any changes or updates to our privacy policy.
      </p>
      <br>
      <br>
    </div>
  </main>
  <?php include('footer.php'); ?>
</body>

</html>