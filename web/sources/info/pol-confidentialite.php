<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once ($root.'/php/mysql.inc.php');
require_once ($root.'/php/funct_battelike.php');


?>

<!DOCTYPE html>
<html lang="fr_FR">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Mon compte">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | politique de confidentialité</title>
    
    <link rel="icon" type="image/png" href="/images/splash/android-chrome-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" sizes="196x196" href="images/splash/apple-touch-icon-196x196.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/splash/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/splash/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/splash/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/splash/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/splash/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/splash/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/splash/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/splash/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="images/splash/apple-touch-icon-57x57.png">  
    <link rel="icon" type="image/png" href="images/splash/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="images/splash/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/splash/favicon-16x16.png" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo $root.'/';?>images/splash/favicon.ico" type="image/x-icon" /> 

    <!-- STYLESHEET -->
    <!-- FONTS -->
    <!-- Muli -->
    <link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">

    <!-- icon -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $root.'/';?>fonts/icons/fontawesome/css/font-awesome.min.css"/>

    <link rel="stylesheet" href="<?php echo $root.'/';?>fonts/icons/sharehub/style.css"/>

    <!-- Vendor -->
    <!-- Custom -->
    <link rel="stylesheet" href="<?php echo $root.'/';?>vendor/magnificPopup/dist/magnific-popup.css" type="text/css" />
    <link href="/css/style.css" rel="stylesheet"/>

    <!-- JAVA SCRIPT -->
    <!-- require -->
    <script data-main="<?php echo $root.'/';?>js/app" src="<?php echo $root.'/';?>vendor/require/require.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo $root.'/';?>vendor/datePicker/css/datepicker.css"/>
</head>
<body>
   <?php include($root.'/header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container-fluid">
            <!--content head-->
            <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 5px;">
            	<br><br><br>
                <div class="row container">
					<br><br>

					<h1>politique de confidentialite</h1>

					<h2>Confidentialité et Cookies</h2>

					<br><br><h3>1. Qui collecte vos données personnelles ?</h3>


					<p>

					- Responsable de traitement - Les données que vous renseignez sont collectées par la société REPUBLIKE, en cours d’immatriculation, représentée par son Président, Etienne Marcotte de Sainte Marie. En attendant l’immatriculation de la société, Etienne Marcotte de Sainte Marie est responsable du traitement.  
					Pour joindre le responsable du traitement, envoyez un email à : XXXX
					- Base légale - Les données sont traitées sur la base de votre consentement lors de votre inscription sur la Plateforme. Toutefois, vous pouvez vous y opposer à tout moment soit en vous désinscrivant de la Plateforme soit en nous envoyant un email à XXXXX
					</p>

					<br><br><h3>2. Quelles données sont traitées ?</h3>
					<p>
					-	Vos données d’inscription et de profil - Civilité, votre nom, prénom, adresse électronique et toutes les autres informations que vous enregistrerez sur la Plateforme pour compléter votre profil (ex. photo de profil)
					-	Vos données de navigation - Lors de votre navigation, la Plateforme pourra également collecter les informations suivantes vous concernant :
					o	Fréquence et durée de navigation ; 
					o	Contenus publiés, contenus partagés, contenus Likés, contenus signalés etc. ;
					o	Pages visités etc. 
					</p>

					<br><br><h3>3. Qui sont les destinataires de vos données personnelles ?</h3>
					<p>
					- En interne - Vos données sont destinées et exploitées par l’équipe qui gère la Plateforme, sous la responsabilité du responsable de traitement.   
					- En externe - Nous pouvons faire appel à des prestataires, appelés aussi sous-traitants, pour nous aider à travailler (ex : hébergement des données).
					Pour des raisons de sécurité nationale ou de lutte contre la fraude, on se doit aussi de communiquer vos données à la demande des autorités habilitées comme des juges, les services de police...
					</p>

					<br><br><h3>4. A quoi servent vos données personnelles ?</h3>
					<p>
					Le traitement de ces données répond aux finalités suivantes :
					- Authentifier les utilisateurs de la Plateforme, administrer la Plateforme et optimiser l’expérience utilisateur à travers la personnalisation de la Plateforme ; 
					- Permettre à la Plateforme de contacter les utilisateurs pour, notamment, promouvoir et optimiser l’utilisation de la Plateforme ; 
					- Etablir des analyses statistiques anonymes mesurant notamment l’audience et la qualification de l’audience de la Plateforme afin d’améliorer ses services ;
					- Etablir des analyses comportementales anonymes destinées à la vente, lesquelles analyses pourront notamment étudier les corrélations entre des profils et des comportements ; 
					- Personnaliser la publicité sur la Plateforme en fonction du profil de chaque utilisateur (publicité ciblée en fonction de ses données personnelles et/ou de ses données de navigation) afin de n’afficher que les contenus publicitaires susceptibles de l’intéresser et/ou permettre aux partenaires de la Plateforme d’adresser des offres directement aux utilisateurs enregistrés dans la base de donnée de la Plateforme. 
					</p>
					<br><br><h3>5. Cookies et liens de partage sur les réseaux sociaux</h3>
					<p>
					Les données ci-dessus pourront notamment être collectées à l’aide de cookies, qu’ils soient enregistrés par nous (cookies propriétaires) et/ou par des tiers (cookies tiers). Les cookies tiers sont par exemple ceux qu’installent une régie ou une agence publicitaire pour notamment (i) afficher des publicités pertinentes pour vous en fonction de vos centres d'intérêts, (ii) limiter le nombre de fois où une même publicité vous sera adressée, et (iii) mesurer l'efficacité d'une campagne publicitaire. Les liens de partage vers Facebook, Twitter et les autres réseaux sociaux vous permettent quant à eux de partager des contenus trouvés sur la Plateforme sur lesdits réseaux sociaux. Lorsque vous utilisez ces boutons de partage, un cookie tiers est (ou sera) installé par le réseau social concerné.

					Un cookie (ou témoin de connexion) est un ensemble de données stocké sur le disque dur de votre terminal (ordinateur, tablette, mobile) par le biais de votre logiciel de navigation à l'occasion de la consultation d'un service en ligne. Les cookies enregistrés (ou qui seront enregistrés) par nous (cookies propriétaires) ou par des tiers (cookies tiers) lorsque vous visitez notre site reconnaissent l'appareil que vous êtes en train d'utiliser. Ces cookies, loin de causer un quelconque dommage à votre appareil, permettent par exemple de plus facilement retrouver vos préférences de configuration, de pré-remplir certains champs et d'adapter le contenu de nos services.

					Vous seul(e) choisissez si vous souhaitez avoir des cookies enregistrés sur votre appareil, sachant que vous pouvez facilement refuser et/ou supprimer l'enregistrement de tout ou partie de ces cookies. Toutefois, il est important de noter que tout paramétrage que vous pouvez entreprendre sera susceptible d’altérer la fluidité de votre navigation sur la Plateforme ainsi que l'accès à certaines des fonctionnalités proposées. Pour paramétrer vos cookies, vous pouvez configurer votre logiciel de navigation de manière à ce que des cookies soient enregistrés dans votre terminal ou, au contraire, qu'ils soient rejetés. Certains navigateurs vous permettent de définir des règles pour gérer les cookies site par site. Pour la gestion des cookies, la configuration de chaque navigateur est différente. Elle est généralement décrite dans le menu d'aide de votre navigateur, qui vous permettra de savoir de quelle manière modifier vos souhaits. 

					Notez que cette procédure n'empêchera pas l'affichage des publicités contextuelles (dites aussi publicités sémantiques) sur les sites Internet que vous visitez. Elle ne bloquera que les publicités ciblées et personnalisées en fonction de vos données personnelles.
					</p>
					<br><br><h3>6. Pendant combien de temps conservez-vous mes données ?</h3>
					<p>
					- Durée - 3 ans suivant votre dernière interaction (log) sur la Plateforme.
					- Si vous avez exercé votre droit d’accès à vos données et que vous nous avez communiqué une copie de votre pièce d’identité, nous pourrons la conserver pendant 1 an à compter de sa réception par nos services.
					- Délais légaux - Au terme de ces délais, nous pourrons conserver vos données jusqu’à l’expiration à la fois des délais de prescription légaux et ceux prévus par les différentes obligations de conservation imposés par la réglementation.
					</p>
					<br><br><h3>7. Quels sont mes droits sur mes données ? </h3>
					<p>
					Vous pouvez demander à :
					- accéder à vos données
					- les modifier en cas d’erreur (ex : erreur dans l’orthographe de votre nom…) ou de changement (d’adresse, de nom…)
					- ce qu’on ne traite plus vos données (droit d’opposition) ou a ce qu’on en limite le traitement (droit de limitation) ; le traitement de vos données étant nécessaire à votre utilisation de la Plateforme, l’exercice de votre droit d’opposition ou de limitation aboutira à votre désinscription de la Plateforme 
					- la portabilité de vos données.
					- Sort de vos données après votre décès ; vous pouvez décider, dès maintenant, du sort de vos données personnelles après votre décès… un peu comme un testament numérique. Vous donnez vos directives sur leur conservation, leur effacement et leur communication, et vous pouvez également désigner une personne en charge de leur exécution. Lorsque ces directives sont générales, elles peuvent être confiées à un tiers de confiance certifié par la CNIL. S’il s’agit de directives particulières, elles peuvent également nous être confiées en tant que Responsable de traitement. Vous restez libre de modifier ou de révoquer ces instructions, à tout moment, sur simple demande. En l'absence de directives données de votre vivant, vos héritiers auront la possibilité, une fois que vous serez décédés, d'exercer certains droits.
					</p>
					<br><br><h3>8. Comment exercer vos droits ?</h3>
					<p>
					- Envoyez votre demande par email à contact@republike.io , avec une copie de votre carte d’identité portant la signature du titulaire. Nous vous répondrons dans un délai d’un mois maximum à compter de la réception de la demande.
					- Réclamations - En cas de désaccord persistant concernant vos données, vous avez la possibilité de saisir la CNIL (3 place de Fontenoy 75007 Paris,
					https://www.cnil.fr/fr/vous-souhaitez-contacter-la-cnil - 01 53 73 22 22).
					</p>
<br><br>
<br><br>
<br><br>


                </div>
            </div>
        </div>
    </main>

        <?php include($root.'/footer.php'); ?>


</body>
</html>