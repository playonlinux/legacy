<?php
require_once ('php/mysql.inc.php');
require_once ('php/funct_battelike.php');

$class = "col-lg-4 col-md-4 col-sm-12";

$select_battles = $dbh->prepare("SELECT *, bl_battles.id as id_battle, bl_battles.statut as statut_battle, bl_battles.info as info_battle, bl_battles.url as url_battle, 
(SELECT count(distinct(user)) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_user, 
(SELECT sum(likes) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_like,
(SELECT id FROM bl_battle_posts WHERE battle = bl_battles.id and statut = 1 order by likes DESC limit 0,1 ) as id_post
FROM `bl_battles`
inner join bl_user on bl_user.id = bl_battles.user
inner join bl_user_cat on bl_user_cat.cat = bl_battles.category
WHERE bl_battles.`statut` <'2' and bl_user_cat.user ='".$_SESSION['id_user']."' order by rand() limit 0,6");
$select_battles->execute();

//SELECT * FROM `bl_user_cat` WHERE `user` =:user and `cat` =:cat


?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> | Félicitations !">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Félicitations !</title>
    
    <?php include('required.php'); ?>
    <script>var page = 'fin';</script>
    
</head>
<body>
	<!-- HEADER -->
	<?php include('header.php'); ?>

	<!-- MAIN -->
    <main>
        <div class="container">
        
         	<div class="sh-content-head sh-content-head__flex-off">
                <div class="row">
                	<div class="col-lg-12 text-center"> 
                   		 <div class="sh-logo__home"><img src="images/logo-republilke-entier.png" alt="REPUBLIKE"></div>
                    </div>
                    
                    <div class="col-lg-12 text-center"> 
                        <h4 class="center-block">
                            FÉLICITATIONS ! Votre inscription est désormais terminée. <br>Vous avez remporté 300 LIKECOINS.
                        </h4>
                    </div>
                    
                    <div class="col-lg-12 text-center mt-4"> 
                        <h5 class="center-block">
                            <strong>Choisissez une bataille ci-dessous, postez votre premier contenu et gagnez encore 1 000 LIKECOINS !</strong><br>
                        	<a href="/<?= $code ?>/<?= $_['url_bienvenue'] ?>"><u>Passez</u></a>
                        </h5>
                    </div>
                </div>
            </div>
            
             <!--sections BEST-->
            
            <div class="sh-section__wrap row tab-submit" id="defis" style="min-height:100px;">
                
         		<?php if ($select_battles->rowCount() > 0) { 
                            
                    while ( $row_battles = $select_battles->fetch(PDO::FETCH_OBJ) ){ 
                        list($post, $type) = photo_post($row_battles->id_post, $dbh); 
                       
                        $class_cat = '';
                        if(isset($_SESSION['securite']) && $_SESSION['securite'] !=''){ 
                            $cat = recherche_cat($_SESSION['id_user'], $row_battles->category, $dbh);
                            if($cat==1){
                                $class_cat = ' cat ';
                            }
                        }
                        $class_statut = 'open';
                        if($row_battles->statut_battle ==2){ 
                            $class_statut = "closed";
                        } 
                        
                        ?>
                        <!--section-->
                        <div id="battle_<?= $row_battles->id_battle ?>" class="sh-section__item <?= $class ?> <?= $class_cat ?> <?= $class_statut ?> wow fadeInUp">
                            <div class="sh-section">
                                <div class="sh-section__content">
                                
                                    <a class="link" href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $row_battles->url_battle ?>"></a>
                                                    
                                    <?php if($type ==1){ ?>
                                    <div class="sh-section__media">
                                        <div class="element text-center" style="background-image:url(/<?= $post ?>); background-size: cover; background-position: center;"></div>
                                     </div>
                                    <?php //$imagePartage = $urlSite.$post; } ?>
                                    
                                    <?php if($type ==2){ list($video, $image) = video($post) ?>
                    
										<?php if($image ==''){ ?>
                                            <div class="sh-section__media">
                                                <div class="element text-center" style="background-image:url(/images/partage_video_republike.jpg); background-size: cover;">
                                                    <iframe height="300px" width="100%" src="<?= $video ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                             </div> 
                                        <?php //$imagePartage = $urlSite.'image/partage_video_republike.jpg';
                                        
                                        }else{ ?>
                                        
                                            <div class="sh-section__media">
                                                <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('https://i.imgur.com/TxzC70f.png') no-repeat;"></div>
                                                <div class="element text-center" style="background-image:url(<?= $image ?>); background-size: cover; background-position: center;"></div>
                                             </div>
                                             
                                        <?php //$imagePartage = $image; }?>
                                    <?php  } ?>
                    
                                 
                                    
                                    <?php if($type ==3){ ?>
                                        <div class="sh-section__media">
                                            <div class="element text-center">
                                                <p class="text-muted mb-2"><?= mb_strimwidth($post, 0, 70, "[...]") ?> </p>                                                
                                            </div>
                                         </div>
                                    <?php //$imagePartage = $urlSite.'image/partage_texte_republike.jpg'; } ?>
                                    
                                    <p>
                                        <?= $row_battles->title ?>
                                        <span class="date" style="visibility: hidden;"><?= $row_battles->id_battle ?></span>
                                    </p>
                                </div>
                                
                            </div>
                        </div>
                        
                    <?php }
                  }else{
                    echo 'null';
                } ?>
            </div>

            
            <script>
			require(['app'], function () {
				require(['modules/main']);
				require(['modules/signup']);
				require(['modules/felicitations']);
			});
			</script>
        </div>
    </main>

	<!-- FOOTER -->
    <?php include('footer.php'); ?>
</body>
</html>