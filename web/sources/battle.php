<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

secureGet();
if (!empty($_GET['id'])) {

    $select_battle = $dbh->prepare("SELECT id, title, url, user, info, vues, category,
        (SELECT count(distinct(user)) FROM bl_battle_posts WHERE battle = bl_battles.id) AS nb_user, 
        (SELECT count(id) FROM bl_battle_posts WHERE battle = bl_battles.id AND statut=1) AS nb_post,
        (SELECT sum(likes) FROM bl_battle_posts WHERE battle = bl_battles.id AND statut=1) AS nb_like,
        (SELECT id FROM bl_battle_posts WHERE battle = bl_battles.id AND statut=1 ORDER BY likes DESC LIMIT 0,1 ) AS id_post
	FROM `bl_battles`
	WHERE url = :id AND statut < 3 LIMIT 0,1");
    $select_battle->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
    $select_battle->execute();

    if ($select_battle->rowCount()) {
        $row_battle = $select_battle->fetch(PDO::FETCH_OBJ);
        $id_battle = $row_battle->id;
        $url_battle = $row_battle->url;
    }

    if (!empty($id_battle)) {

        list($post, $type) = photo_post($row_battle->id_post, $dbh);

        if (in_array($type, [1, 4])) {
            $imagePartage = $post;
        }
        if ($type == 2) {
            $imagePartage = 'images/partage_video_republike.jpg';
        }
        if ($type == 3) {
            $imagePartage = 'images/partage_texte_republike.jpg';
        }
        list($width, $height, $type, $attr) = getimagesize($imagePartage);

        //update vues
        $updateReq = $dbh->prepare("UPDATE `bl_battles` SET `vues`= vues + 1 WHERE id = :id");
        $updateReq->bindParam(':id', $id_battle, PDO::PARAM_STR);
        $updateReq->execute();

        //POPUP POST

        if (!empty($_GET['token'])) {
            $query = $dbh->prepare("SELECT id FROM `bl_battle_posts` WHERE `statut` ='1' AND token = :token LIMIT 0,1");
            $query->bindParam(':token', $_GET['token'], PDO::PARAM_STR);
            $query->execute();

            $row_post = $query->fetch(PDO::FETCH_OBJ);
            $id_post_popup = $row_post->id;
        }
    } else {

        $select_battle = $dbh->prepare("SELECT id, title, url, user, info, vues, category,
            (SELECT COUNT(DISTINCT(user)) FROM bl_battle_posts WHERE battle = bl_battles.id) AS nb_user, 
            (SELECT COUNT(id) FROM bl_battle_posts WHERE battle = bl_battles.id) AS nb_post,
            (SELECT SUM(likes) FROM bl_battle_posts WHERE battle = bl_battles.id) AS nb_like,
            (SELECT id FROM bl_battle_posts WHERE battle = bl_battles.id ORDER BY likes DESC LIMIT 0,1 ) AS id_post
		FROM `bl_battles`
		WHERE id = :id AND statut < 3 LIMIT 0,1");
        $select_battle->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
        $select_battle->execute();

        if ($select_battle->rowCount()) {
            $row_battle = $select_battle->fetch(PDO::FETCH_OBJ);
            $id_battle = $row_battle->id;
            $url_battle = $row_battle->url;
        }
        if (!empty($id_battle)) {
            header("Location:/" . $code . "/" . $_['url_battle'] . "/" . $url_battle . "");
            exit;
        } elseif (!empty($_SESSION['securite'])) {
            header("Location:/" . $code . "/" . $_['url_bienvenue']);
            exit;
        } else {
            header('Location: /');
            exit;
        }
    }

    $select_share = $dbh->prepare("SELECT COUNT(*) AS nb_shares FROM bl_logs WHERE `type` = 6 AND page = :page");
    $select_share->bindParam(':page', $id_battle, PDO::PARAM_STR);
    $select_share->execute();

    $row_share = $select_share->fetch(PDO::FETCH_OBJ);
} elseif (!empty($_SESSION['securite'])) {
    header("Location:/" . $code . "/" . $_['url_bienvenue']);
} else {
    header('Location: /');
}
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> | <?= $row_battle->title ?>">
    <meta name="author" content="<?= $nameSite ?>">
    <title><?= $nameSite ?> | <?= $row_battle->title ?></title>

    <meta property="og:title" content="<?= $nameSite ?> | <?= $row_battle->title ?>" />
    <meta property="og:site_name" content="<?= $nameSite; ?>" />
    <meta property="og:description" content="<?= $_['page_battle'] ?> <?= $row_battle->title ?>" />
    <meta property="og:image" content="<?= $imagePartage; ?>" />
    <meta property="og:image:width" content="<?= $width; ?>" />
    <meta property="og:image:height" content="<?= $height; ?>" />
    <meta property="og:image:alt" content="<?= $row_battle->title ?>" />
    <meta property="og:url" content="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $_GET['id'] ?>" />

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $_GET['id'] ?>" />
    <?php
    if ($code == 'fr') {
        $footerEN = '/en/battle/' . $_GET['id']; ?>
        <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
    <?php
    } else if ($code == 'en') {
        $footerFR = '/fr/bataille/' . $_GET['id'];
        ?>
        <link rel="alternate" hreflang="fr" href="<?= $footerFR ?> ?>" />
    <?php
    }
    ?>

    <?php include('required.php'); ?>

    <script>
        var battle = '<?= $id_battle ?>';
        var page = '<?= !empty($_GET["page"]) ? "battlePost" : "battle" ?>';
        var post = '<?= !empty($_GET["token"]) ? $id_post_popup : "" ?>';
        var lang = '<?= $code ?>';
        var url_bienvenue = '<?= $_['url_bienvenue'] ?>';
    </script>
</head>

<body>
    <div class="bg-img" style="color: #ffffff;">
        <div class="container">
            <div class="topnav">
                <? include('header.php'); ?>
                <div class="container-fluid">
                    <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 160px;">
                        <div class="row mb-5" style="width:-webkit-fill-available;">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="row">
                                    <div class="col-12">
                                        <span class="battle-title"><?= $row_battle->title ?></span>
                                        <?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battle->user) { ?>
                                            <span style="cursor:pointer">
                                                <img src="/images/icons/edit-grey.svg" data-toggle="tooltip" data-placement="left" title="<?= $_['modif_battle'] ?>" href="javascript:void(0)" id="edit-b_<?= $row_battle->id ?>" data-id="<?= $row_battle->id ?>" data-name="<?= $row_battle->title ?>" data-info="<?= $row_battle->info ?>" data-category="<?= $row_battle->category ?>" class="editBattle sh-section__btn-link icons-20">
                                            </span>
                                        <?php } ?>
                                        <?php
                                        if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battle->user && $row_battle->nb_post <= 1) {
                                            ?>
                                            <span style="cursor:pointer">
                                                <img src="/images/icons/trash-grey.svg" data-toggle="tooltip" data-placement="top" data-battle="<?= $row_battle->id ?>" data-title="<?= $row_battle->title ?>" title="Supprimer ce contenu" class="sh-battle__trash icons-20">
                                            </span>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if (!empty($row_battle->info)) {
                                        ?>
                                        <div class="col-12 battle-info">
                                            <p><?= $row_battle->info ?></p>
                                        </div>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 mt-5 mb-5">
                                <?php
                                if (!empty($_SESSION['securite'])) {
                                    ?>
                                    <img src="/images/icons/<?= $code; ?>/participate-button-left.svg" class="btn-post participate-button">

                                <?php
                                } else {
                                    ?>

                                    <img src="/images/icons/<?= $code; ?>/participate-button-left.svg" class="btn-upload_btn-signup participate-button">
                                <?php
                                }
                                ?>
                            </div>
                            <?php
                            $title = speudo_user($row_battle->user, $dbh) . ' ' . grade_user_between(points_user($row_battle->user, $dbh), $code, $dbh);
                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battle->user) {
                                $href = "/" . $code . "/" . $_['url_compte'];
                            } else {
                                $href = "/" . $code . "/" . $_['url_user'] . "/" . url_user($row_battle->user, $dbh);
                            }
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-12">

                                <span class="redirect-avatar" data-location="<?= $href ?>" style="display:inline-flex; vertical-align:bottom;cursor:pointer">
                                    <span class="sh-section__avatar icons-35" style="border-color:<?= grade_user_color(points_user($row_battle->user, $dbh), $dbh) ?>; background-image:url(/<?= photo_user($row_battle->user, $dbh) ?>)"></span>
                                </span>
                                <span class="">
                                    <img src="/images/icons/friends.svg" class="icons-35">
                                    <span><?= $row_battle->nb_user; ?></span>
                                </span>
                                <span>
                                    <img src="/images/icons/bow.svg" class="icons-35">
                                    <span><?= $row_battle->nb_post; ?></span>
                                </span>

                                <span>
                                    <img src="/images/icons/eye.svg" class="icons-35">
                                    <span><?= $row_battle->vues; ?></span>
                                </span>
                                <span id="battle-header-likes">
                                    <img src="/images/icons/liked-pink.svg" class="icons-35">
                                    <span><?= $row_battle->nb_like; ?></span>
                                </span>
                                <span style="cursor:pointer">
                                    <?php if (!empty($_SESSION['securite'])) { ?>

                                        <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-partage-battle icons-35" data-battle="<?= $row_battle->id ?>" data-url="<?= $row_battle->url ?>" data-title="<?= $row_battle->title ?>" data-image="/<?= $imagePartage; ?>">
                                    <?php } else { ?>
                                        <!-- TODO: Add automatic redirection after login process -->
                                        <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-upload_btn-signup icons-35" data-battle="<?= $row_battle->id  ?>">
                                    <?php } ?>
                                    <span id="share_battle_<?= $row_battle->id ?>"><?= $row_share->nb_shares; ?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MAIN -->
    <main class="container">
        <!--sections-->
        <div class="sh-section__wrap mur row mt-3">

            <!--section-->
            <?php
            $query = $dbh->prepare(
                "   SELECT id, title, type, token, statut, user, post, vues,
                    (SELECT CONCAT(U.prenom, ' ', U.nom) FROM bl_user U WHERE U.id = BBP.user) AS username, 
                    -- (SELECT COUNT(distinct(user)) FROM bl_likes_post BLP WHERE BLP.posts = BBP.id) AS nb_users, 
                    SUM(likes) AS nb_likes
                FROM bl_battle_posts BBP
                WHERE `statut` = 1 AND battle =  :id
                GROUP BY id, title, type, token, statut, user, post, vues
                ORDER BY nb_likes DESC
            "
            );
            $query->bindParam(':id', $id_battle, PDO::PARAM_STR);
            $query->execute();

            if ($query->rowCount() > 0) {
                $rank = 1;
                $laurels = [
                    1 => ['title' => 'or', 'class' => ['rank' => 'rank gold-rank', 'border' => 'gold-border']],
                    2 => ['title' => 'argent', 'class' => ['rank' => 'rank silver-rank', 'border' => 'silver-border']],
                    3 => ['title' => 'bronze', 'class' => ['rank' => 'rank bronze-rank', 'border' => 'bronze-border']]
                ];

                while ($row_post = $query->fetch(PDO::FETCH_OBJ)) {

                    $select_share = $dbh->prepare("SELECT COUNT(*) AS nb_shares FROM bl_logs WHERE `type` = 31 AND page = :page");
                    $select_share->bindParam(':page', $row_post->id, PDO::PARAM_STR);
                    $select_share->execute();

                    $row_share = $select_share->fetch(PDO::FETCH_OBJ);
                    ?>

                    <div class="sh-section__item col-lg-4 col-md-4 col-sm-12 col-sm-6" id="post_<?= $row_post->id ?>">
                        <div class="sh-section box <?= ($rank < 4) ? $laurels[$rank]['class']['border'] : '' ?>">

                            <div class="<?= ($rank < 4) ? $laurels[$rank]['class']['rank'] : '' ?>">
                                <div class="sh-section__head">
                                    <?php
                                            $title = speudo_user($row_post->user, $dbh) . ' ' . grade_user_between(points_user($row_post->user, $dbh), $code, $dbh);
                                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user) {
                                                $href = "/" . $code . "/" . $_['url_compte'];
                                            } else {
                                                $href = "/" . $code . "/" . $_['url_user'] . "/" . url_user($row_post->user, $dbh);
                                            }
                                            ?>

                                    <span class="redirect-avatar" data-location="<?= $href ?>">
                                        <span class="sh-section__avatar icons-35" style="border-color:<?= grade_user_color(points_user($row_post->user, $dbh), $dbh) ?>; background-image:url(/<?= photo_user($row_post->user, $dbh) ?>)"></span>
                                        <span><?= strtoupper($row_post->username) ?> </span>
                                    </span>

                                    <?php
                                            if ($rank >= 4) {
                                                ?>
                                        <div class="medaillePost" data-toggle="tooltip" data-placement="top" data-original-title="Classement <?= ucfirst($laurels[$rank]['title']) ?>">
                                            <div class="chiffre" style="background-color:#FFFFFF; border-radius: 50%">
                                                <?= $rank ?><span>/<?= $row_battle->nb_post ?></span>
                                            </div>
                                        </div>
                                    <?php
                                            }
                                            ?>
                                </div>
                                <div class="sh-section__content">
                                    <?php
                                            switch ($row_post->type) {
                                                case 1: {
                                                        $imagePartage = $row_post->post;
                                                        ?>
                                                <div class="sh-section__image">
                                                    <div class="element text-center">
                                                        <img src="/<?= $row_post->post ?>" alt="<?= $row_post->title ?>">
                                                    </div>
                                                </div>
                                            <?php
                                                            break;
                                                        }
                                                    case 2: {
                                                            list($video, $image) = video($row_post->post);
                                                            $imagePartage = ($image) ? $image : '/images/partage_video_republike.jpg';
                                                            ?>
                                                <div class="sh-section__media">
                                                    <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('https://i.imgur.com/TxzC70f.png') no-repeat;"></div>
                                                    <img class="element" src="<?= $imagePartage; ?>">
                                                </div>
                                            <?php
                                                            break;
                                                        }
                                                    case 3: {
                                                            $imagePartage = '/images/partage_texte_republike.jpg';
                                                            ?>
                                                <div class="sh-section__media">
                                                    <div class="element text-center">
                                                        <p class="text-muted mb-2"><?= mb_strimwidth($row_post->post, 0, 70, "[...]") ?> </p>
                                                    </div>
                                                </div>
                                        <?php
                                                        break;
                                                    }
                                            }

                                            ?>
                                        <div class="overlay">
                                            <a class="btn-popup-post link" id="btn-popup-post_<?= $row_post->id ?>" href="javascript:void(0)" data-id="<?= $row_post->id ?>" data-token="<?= $row_post->token ?>" data-ordre="<?= $rank ?>"></a>
                                            <img src="/images/icons/search.svg" style="width: 100%; height: 100%">
                                            </a>
                                        </div>
                                </div>

                                <div>
                                    <div style="padding:10px 0">
                                        <h3 title="<?= $row_post->title ?>" data-toggle="tooltip" data-placement="top" class="title">
                                            <?= mb_strimwidth($row_post->title, 0, 40, "[...]") ?>
                                            <h3>
                                    </div>
                                </div>
                            </div>
                            <div class="sh-section__footer">
                                <div>
                                    <span>
                                        <img src="/images/icons/eye.svg" class="icons-30">
                                        <span id="view_post_<?= $row_post->id ?>"><?= $row_post->vues; ?></span>
                                    </span>
                                </div>
                                <div style="padding-bottom:10px">
                                    <span style="cursor:pointer">
                                        <?php
                                                if (!empty($_SESSION['securite'])) {
                                                    ?>
                                            <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" id="like_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?= ($row_post->statut == 1) ? "likeSave" : "inactif"; ?> sh-section__btn-like sh-btn-icon icons-30">
                                        <?php
                                                } else {
                                                    ?>
                                            <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" data-id="<?= $row_post->id ?>" class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon icons-30">
                                        <?php
                                                }
                                                ?>
                                        <span><?= $row_post->nb_likes; ?></span>
                                    </span>
                                    <?php
                                            if (!empty($_SESSION['securite'])) {
                                                ?>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon icons-30" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" data-image="<?= $imagePartage ?>">
                                            <span id="share_post_<?= $row_post->id ?>"><?= $row_share->nb_shares; ?></span>
                                        </span>

                                    <?php
                                            } else {
                                                ?>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon icons-30" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" data-image="<?= $imagePartage ?>">
                                            <span><?= $row_share->nb_shares; ?></span>
                                        </span>
                                        </span>
                                    <?php
                                            }
                                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user) {
                                                ?>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/edit-grey.svg" data-toggle="tooltip" data-placement="left" title="<?= $_['modif_contenu'] ?>" href="javascript:void(0)" id="edit_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" data-name="<?= $row_post->title ?>" class="editPost sh-section__btn-link icons-30">
                                        </span>
                                    <?php
                                            }
                                            if (!empty($_SESSION['securite']) && $_SESSION['id_user'] != $row_post->user) {
                                                ?>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" title="<?= $_['signaler_contenu'] ?>" class="sh-post__signaler icons-20">
                                        </span>
                                    <?php
                                            }
                                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user) {
                                                ?>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/trash-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" title="Supprimer ce contenu" class="sh-post__trash icons-30">
                                        </span>
                                    <?php
                                            }
                                            ?>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                    $rank++;
                }
            }
            ?>
        </div>
        <div class="sh-popup-post">
            <div class="sh-popup__content" id="contenu_post">
            </div>
        </div>
        <div class="sh-upload">
            <div class="sh-upload__logo">
                <a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a>
            </div>
            <div class="sh-upload__content save">
                <p class="text-center"><b><?= $_['post_contenu'] ?></b></p>
                <div class="sh-upload__form">
                    <form method="post" action="" name="form_save_post" id="form_save_post" enctype="multipart/form-data">

                        <div class="sh-head-user__upload mx-auto" style="width:255px;">
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_1'] ?>" class="sh-btn-icon btn-type active" data-id="1"><i class="fa fa-file-image-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_2'] ?>" class="sh-btn-icon btn-type" data-id="2"><i class="fa fa-file-video-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_3'] ?>" class="sh-btn-icon btn-type" data-id="4"><i class="fa fa-file-audio-o"></i></div>
                            <div data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['pop_post_4'] ?>" class="sh-btn-icon btn-type" data-id="3"><i class="fa fa-file-text-o"></i></div>
                        </div>


                        <div id="formImage">
                            <p class="small"><?= $_['champ_type1'] ?> : (JPG, GIF, PNG)</p>
                            <input type="file" name="image" id="image" class="form-control inputfile" />
                            <label for="image">
                                <i class="fa fa-upload">&nbsp;<?= $_['upload']['image'] ?></i>
                            </label>
                        </div>

                        <div id="formSon" style="display:none;">
                            <p class="small"><?= $_['champ_type2'] ?> : (MP3)</p>
                            <input type="file" name="son" id="son" class="form-control inputfile" />
                            <label for="son">
                                <i class="fa fa-upload">&nbsp;<?= $_['upload']['sound'] ?></i>
                            </label>
                        </div>

                        <div id="formLien" style="display:none;">
                            <p class="small"><?= $_['champ_type3'] ?> : (Youtube, Vimeo, Dailymotion)</p>
                            <input class="form-control" placeholder="<?= $_['champs_post_1'] ?>" name="lien" id="lien" type="text">
                        </div>

                        <div id="formText" style="display:none;">
                            <p class="small"><?= $_['champ_type4'] ?></p>
                            <textarea name="text" id="text" class="form-control" placeholder="<?= $_['champs_post_2'] ?>"></textarea>
                            <input class="form-control mt-1" placeholder="<?= $_['placehoder2_type4'] ?>" name="lien_text" id="lien_text" type="text">
                        </div>

                        <input type="text" class="form-control mt-4" placeholder='<?= $_['champs_post_3'] ?>' name="title">


                        <input type="hidden" class="form-control" name="type" value="1" required>
                        <input type="hidden" class="form-control" name="battle" value="<?= $id_battle ?>" required>
                        <input type="hidden" class="form-control" name="url_battle" value="<?= $_GET['id'] ?>" required>

                        <input type="hidden" class="form-control" name="code" value="<?= $code ?>" required>

                        <hr>
                        <div class="sh-login__send">
                            <button type="submit" class="sh-btn sh-btn-rose mx-auto" id="bt_publier"><?= $_['bt_publier'] ?></button>
                        </div>
                        <div id="errorLogPost" class="text-center mb-3"></div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/battle']);
            require(['modules/upload']);

        });
    </script>
    <?php if (!empty($_GET['token'])) { ?>
        <script>
            require(['app'], function() {
                require(['modules/battle_popup']);
            });
        </script>
    <?php } ?>
</body>

</html>