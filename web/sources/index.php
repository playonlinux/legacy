<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');
$page = "index";

if (!empty($_SESSION['securite'])) {
    header("Location:/" . $code . "/" . $_['url_bienvenue']);
}
?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  Welcome">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Welcome</title>

    <link rel="canonical" href="/<?= $code ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en'; ?>
        <link rel="alternate" hreflang="en" href="/en" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr'; ?>
        <link rel="alternate" hreflang="fr" href="/fr" />
    <?php } ?>

    <?php include('required.php'); ?>

    <?php if (!empty($_SESSION['securite'])) { ?>
        <?php if (user_cat($_SESSION['id_user'], $dbh) == 1) { ?>
            <script>
                var page = 'connecte';
            </script>
        <?php } else { ?>
            <script>
                var page = 'nocat';
            </script>
        <?php } ?>
    <?php } else { ?>
        <script>
            var page = 'accueil';
        </script>
    <?php } ?>
</head>

<body style=" background-image:url(/images/bg_home-republike.jpg);">
    <!-- MAIN -->
    <main style="padding: 0">
        <div class="container-fluid">
            <!--content head-->
            <div class="sh-logo__index mb-5 mt-5"><img src="/images/logo-republilke-entier.png" alt="REPUBLIKE"></div>
            <div class="sh-content-head" style="padding-bottom: 5px; padding-top: 10px;">
                <div class="row">
                    <div class="col-lg-12 text-center mb-4 mt-4 wow bounceInDown" data-wow-delay="600ms">
                        <h1 class="big text-center center-block"><?= $_['bigtitre_index'] ?></h1>
                    </div>
                    <div class="col-lg-12 text-center mb-5 mt-2 wow bounceInDown" data-wow-delay="600ms">
                        <h2 class="big text-center center-block"><?= $_['titre_index'] ?></h2>
                    </div>
                    <div class="col-lg-12 text-center mb-5 mt-1 wow bounceInDown" data-wow-delay="600ms">
                        <i class="repu-laurels" style="font-size: 90px; margin:0 auto;"></i>
                    </div>
                    <div class="col-lg-12 text-center mb-5 mt-1 wow fadeIn" data-wow-delay="800ms">
                        <a href="/<?= $code ?>/<?= $_['url_bienvenue'] ?>" class="sh-btn big center-block"><?= $_['play'] ?></a>
                    </div>

                </div>
            </div>

        </div>
        <div class="container mb-5 mt-5">
            <!--content head-->
            <div class="sh-content-features" style="padding-bottom: 5px; padding-top: 10px;">
                <div class="row mb-5 mt-5">
                    <div class="col-lg-3 col-md-3 col-sm-3 text-center wow fadeInUp" data-wow-delay="1100ms">
                        <h4 class="text-center center-block"><b><?= $_['bloc_1'] ?></b></h4>
                        <i class="fa fa-search center-block mb-3 mt-3" style="font-size: 60px;"></i>
                        <p class="text-center center-block"><?= $_['txt_bloc_1'] ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 text-center wow fadeInUp" data-wow-delay="1100ms">
                        <h4 class="text-center center-block"><b><?= $_['bloc_2'] ?></b></h4>
                        <i class="fa fa-bullhorn center-block mb-3 mt-3" style="font-size: 60px;"></i>
                        <p class="text-center center-block"><?= $_['txt_bloc_2'] ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 text-center wow fadeInUp" data-wow-delay="1100ms">
                        <h4 class="text-center center-block"><b><?= $_['bloc_3'] ?></b></h4>
                        <i class="fa fa-heart-o center-block mb-3 mt-3" style="font-size: 60px;"></i>
                        <p class="text-center center-block"><?= $_['txt_bloc_3'] ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 text-center wow fadeInUp" data-wow-delay="1100ms">
                        <h4 class="text-center center-block"><b><?= $_['bloc_4'] ?></b></h4>
                        <i class="repu-laurels center-block mb-0 mt-0" style="font-size: 55px;"></i>
                        <p class="text-center center-block"><?= $_['txt_bloc_4'] ?></p>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <script>
        require(['app'], function() {
            require(['modules/index']);
        });
    </script>
</body>

</html>