<?php

define('NB_MAX_LIKES', 1000);
define('NB_MAX_LIKES_PER_CONTENT', 10);

$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

if (!empty($_GET['code']) && ($_GET['code'] == 'en') || !empty($_POST['code']) && ($_POST['code'] == 'en')) {
	$code = 'en';
} elseif (!empty($_GET['code']) && ($_GET['code'] == 'fr') || !empty($_POST['code']) && ($_POST['code'] == 'fr')) {
	$code = 'fr';
} elseif (!empty($_SESSION['code'])) {
	$code = $_SESSION['code'];
} elseif ($lang != 'fr') {
	$code = 'en';
} else {
	$code = 'fr';
}

$_SESSION['code'] = $code;
require_once('lang_' . strtolower($code) . '.php');
$_['currentFR'] = 'fr';
$_['currentEN'] = 'en';

date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, strtolower($code) . '_' . strtoupper($code) . '.UTF8');
// header("Location: $index",TRUE,301);

//Adresse précedent
$adr = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';

function sanitize(&$input)
{
	if (isset($input)) {
		foreach ($input as $key => &$value) {
			if (!is_array($value)) {
				$input[$key] = htmlspecialchars(trim($value));
			} else {
				sanitize($value);
			}
		}
	}
}

function securePost()
{
	global $_POST;
	sanitize($_POST);
}

function secureGet()
{
	global $_GET;
	sanitize($_GET);
}

function save_connexion($id_user, $dbh)
{

	$query = $dbh->prepare("INSERT INTO `bl_connexion` (`user`) VALUES (:id_user)");
	$query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$query->execute();
}

//logs
function save_log($id_user, $logType, $dbh)
{

	$query = $dbh->prepare("INSERT INTO `bl_logs` (`user`, `type`) VALUES (:id_user, :logType)");
	$query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$query->bindParam(':logType', $logType, PDO::PARAM_STR);
	$query->execute();
}

function save_log_ou($id_user, $logType, $logMsg, $dbh)
{

	$query = $dbh->prepare("INSERT INTO `bl_logs` (`user`, `type`, `page`) VALUES (:id_user, :logType, :logMsg)");
	$query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$query->bindParam(':logType', $logType, PDO::PARAM_STR);
	$query->bindParam(':logMsg', $logMsg, PDO::PARAM_STR);
	$query->execute();
}

function save_log_ou_info($id_user, $logType, $logMsg, $info, $dbh)
{

	$query = $dbh->prepare("INSERT INTO `bl_logs` (`user`, `type`, `page`, `info`) VALUES (:id_user, :logType, :logMsg, :info)");
	$query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$query->bindParam(':logType', $logType, PDO::PARAM_STR);
	$query->bindParam(':logMsg', $logMsg, PDO::PARAM_STR);
	$query->bindParam(':info', $info, PDO::PARAM_STR);
	$query->execute();
}


function save_alert($id_user, $dbh)
{

	//delete
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_alerts` WHERE `user` = :id");
	$deleteReq->bindParam(':id', $id_user, PDO::PARAM_STR);
	$deleteReq->execute();

	$id = $id_user;

	//insert cat
	$alert = 1;
	$opt = 2;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 2;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 3;
	$opt = 1;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 4;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 5;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 6;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 7;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 8;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();

	$alert = 9;
	$opt = 3;
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $alert, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $opt, PDO::PARAM_STR);
	$insertReq->execute();
}


function save_points($id_user, $points, $dbh)
{

	$query = $dbh->prepare("INSERT INTO `bl_battlecoins` (`user`, `points`) VALUES (:id_user, :points)");
	$query->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$query->bindParam(':points', $points, PDO::PARAM_STR);
	$query->execute();
}


function save_notifs($id_user, $message, $dbh)
{


	$insertReq = $dbh->prepare("INSERT INTO `bl_notifications` (`user`, `notif`) 
	VALUES (:id_user, :message)");
	$insertReq->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$insertReq->bindParam(':message', $message, PDO::PARAM_STR);

	$insertReq->execute();
}

function video($video)
{

	$lien = "";
	$image = "";

	//echo $video;

	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match)) {
		$video_id = $match[1];
		$video = 'https://www.youtube-nocookie.com/embed/' . $video_id . '?rel=0&amp;showinfo=0&amp;origin=https://www.republike.io';
		$image = "https://i.ytimg.com/vi/" . $video_id . "/hqdefault.jpg";
	}

	if (preg_match("@^(?:https://dai.ly/)?([^/]+)@i", $video)) {
		$video = str_replace("https://dai.ly/", "https://www.dailymotion.com/embed/video/", $video);
	}

	if (preg_match("@^(?:https://www.dailymotion.com/video/)?([^/]+)@i", $video)) {
		$video = str_replace("https://www.dailymotion.com/video/", "https://www.dailymotion.com/embed/video/", $video);
	}

	return array($video, $image);
	//$video;
}


function alert_user($alert, $user, $dbh)
{

	$sth = $dbh->prepare("SELECT `opt` FROM `bl_user_alerts` WHERE `user` =:user and `alert` = :alert");
	$sth->bindParam(':user', $user, PDO::PARAM_STR);
	$sth->bindParam(':alert', $alert, PDO::PARAM_STR);

	$sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$opt = $row_userResult->opt;
	}

	return $opt;
}


function grade_user_color($points, $dbh)
{

	$sth = $dbh->prepare("SELECT bl_grade.color as color 
	FROM bl_grade
	WHERE :points BETWEEN `points` AND `max`");
	$sth->bindParam(':points', $points, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$color = $row_userResult->color;
	}

	return $color;
}

function grade_user_between($points, $code, $dbh)
{

	$sth = $dbh->prepare("SELECT bl_grade.titre_" . $code . " as titre
	FROM bl_grade
	WHERE :points BETWEEN `points` AND `max`");
	$sth->bindParam(':points', $points, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$grade = $row_userResult->titre;
	}

	return $grade;
}

function get_user_avatar($id)
{
	global  $dbh;
	$points = points_user($id, $dbh);
	$grade = grade_user_between($points, 'en', $dbh);
	$grade = implode('-', array_map(function ($item) {
		return trim(strtolower($item));
	}, explode(' ', trim(strtolower($grade)))));

	$gender = gender_user($id);

	return "/images/icons/{$gender}/svg/{$grade}.svg";
}

function get_user_next_grade($id)
{
	global  $dbh, $code;
	$points = points_user($id, $dbh);
	$sth = $dbh->prepare("SELECT `max` 
		FROM bl_grade 
		WHERE :points BETWEEN points AND `max`
		ORDER BY points ASC
		LIMIT 1");
	$sth->bindParam(':points', $points, PDO::PARAM_STR);
	$sth->execute();

	if (!$sth->rowCount()) {
		return '';
	}
	$row = $sth->fetch(PDO::FETCH_OBJ);

	$points =  $row->max + 1;

	$sth = $dbh->prepare("SELECT bl_grade.titre_" . $code . " as titre
		FROM bl_grade 
		WHERE points = :points 
		ORDER BY points ASC
		LIMIT 1");
	$sth->bindParam(':points', $points, PDO::PARAM_STR);
	$sth->execute();

	if (!$sth->rowCount()) {
		return '';
	}

	$row = $sth->fetch(PDO::FETCH_OBJ);
	return $row->titre;
}

$gradeToClass = [
	'serf' => 'w3-black',
	'apprentice' => 'w3-yellow',
	'freeman' => 'w3-orange',
	'bourgeois' => 'w3-green',
	'knight' => 'w3-light-blue',
	'consul' => 'w3-indigo',
	'senator' => 'w3-purple',
	'chancellor' => 'w3-brown',
	'dictator' => 'w3-deep-purple',
	'enlightened-dictator' => 'w3-red',
	'imperator' => 'w3-sand'
];
function get_grade_color_class($points)
{
	global $dbh, $gradeToClass;
	$grade = grade_user_between($points, 'en', $dbh);
	$grade = implode('-', array_map(function ($item) {
		return trim(strtolower($item));
	}, explode(' ', trim(strtolower($grade)))));

	return $gradeToClass[$grade];
}

function get_user_progress($id)
{
	global $dbh;
	$points = points_user($id, $dbh);
	$sth = $dbh->prepare(
		"	SELECT points, `max` 
			FROM bl_grade 
			WHERE :points BETWEEN points AND `max`
			ORDER BY points ASC
			LIMIT 1
		"
	);
	$sth->bindParam(':points', $points, PDO::PARAM_STR);
	$sth->execute();

	$row = $sth->fetch(PDO::FETCH_OBJ);

	return min(ceil(($points - $row->points) * 100 / ($row->max - $row->points)), 100);
}

function photo_post($id, $dbh)
{

	$sth = $dbh->prepare("SELECT post, `type` FROM `bl_battle_posts` WHERE `id` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$sth->execute();
	if ($sth->rowCount() > 0) {
		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$post = $row_userResult->post;
		$type = $row_userResult->type;
	}


	return array($post, $type);
}


function photo_post_mail($id, $dbh)
{
	global $urlSite;
	$imagePartage = '';
	$sth = $dbh->prepare("SELECT post, `type` FROM bl_battle_posts WHERE `token` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$post = $row_userResult->post;
		$type = $row_userResult->type;

		if ($type == 1) {
			$imagePartage = $urlSite . '/' . $post;
		}

		if ($type == 2) {

			list($video, $image) = video($post);

			if ($image == '') {
				$imagePartage = $urlSite . '/images/partage_video_republike.jpg';
			} else {
				$imagePartage = $image;
			}
		}

		if ($type == 3) {
			$imagePartage = $urlSite . '/images/partage_texte_republike.jpg';
		}
	}

	return  $imagePartage;
}

function titre_post_mail($id, $dbh)
{

	$sth = $dbh->prepare("SELECT title FROM `bl_battle_posts` WHERE `token` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$title = $row_userResult->title;
	}


	return  $title;
}



function photo_battle_mail($id, $dbh)
{
	global $urlSite;
	$imagePartage = '';
	$sth = $dbh->prepare("SELECT post, `type` FROM bl_battle_posts WHERE battle = :id and statut = 1 order by likes DESC limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$post = $row_userResult->post;
		$type = $row_userResult->type;

		if ($type == 1) {
			$imagePartage = $urlSite . '/' . $post;
		}

		if ($type == 2) {

			list($video, $image) = video($post);

			if ($image == '') {
				$imagePartage = $urlSite . '/images/partage_video_republike.jpg';
			} else {
				$imagePartage = $image;
			}
		}

		if ($type == 3) {
			$imagePartage = $urlSite . '/images/partage_texte_republike.jpg';
		}
	}

	return  $imagePartage;
}

function info_post($id, $dbh)
{

	$sth = $dbh->prepare("SELECT token, user FROM `bl_battle_posts` WHERE `id` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$token = $row_userResult->token;
		$user = $row_userResult->user;
	}


	return  array($token, $user);
}


function speudo_user($id, $dbh)
{
	$sth = $dbh->prepare("
		SELECT IFNULL(speudo, CONCAT(prenom, ' ', nom)) as pseudo
		FROM `bl_user` 
		WHERE `id` = :id
		LIMIT 0,1
	");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row = $sth->fetch(PDO::FETCH_OBJ);

		return $row->pseudo;
	}

	return false;
}

function gender_user($id)
{

	global $_SESSION, $dbh;
	$sth = $dbh->prepare("
		SELECT CASE WHEN sexe = 1 THEN 'women' ELSE 'men' END as gender 
		FROM `bl_user` 
		WHERE `id` = :id 
		LIMIT 0,1
	");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row = $sth->fetch(PDO::FETCH_OBJ);

		return $row->gender;
	}

	return false;
}

function prenom_user($id, $dbh)
{

	$query = $dbh->prepare("SELECT prenom FROM `bl_user` WHERE `id` = :id limit 0,1");
	$query->bindParam(':id', $id, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount() === 0) {
		return false;
	}
	$row = $query->fetch(PDO::FETCH_OBJ);
	return $row->prenom;
}

function email_user($id, $dbh)
{

	$query = $dbh->prepare("SELECT email FROM `bl_user` WHERE `id` = :id limit 0,1");
	$query->bindParam(':id', $id, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount() === 0) {
		return false;
	}
	$row = $query->fetch(PDO::FETCH_OBJ);
	return $row->email;
}


function code_user_by_id($id)
{
	global $dbh;

	$query = $dbh->prepare("SELECT code FROM `bl_user` WHERE `id` = :id LIMIT 0,1");
	$query->bindParam(':id', $id, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount() === 0) {
		return false;
	}

	$row = $query->fetch(PDO::FETCH_OBJ);
	return $row->code;
}
function code_user_by_email($email)
{
	global $dbh;

	$query = $dbh->prepare("SELECT code FROM `bl_user` WHERE `email` = :email LIMIT 0,1");
	$query->bindParam(':email', $email, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount() === 0) {
		return false;
	}

	$row = $query->fetch(PDO::FETCH_OBJ);
	return $row->code;
}


function points_user($id, $dbh)
{

	$sth = $dbh->prepare("SELECT points FROM `bl_user` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);

		$points = $row_userResult->points;
	}

	return $points;
}


function url_user($id, $dbh)
{

	$sth = $dbh->prepare("SELECT url FROM `bl_user` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$url = $row_userResult->url;
	}

	return $url;
}


function name_battle($id, $dbh)
{

	$sth = $dbh->prepare("SELECT title FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$name = $row_userResult->title;
	}

	return  $name;
}

function statut_battle($id, $dbh)
{
	$statut = 1;
	$sth = $dbh->prepare("SELECT statut FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$statut = $row_userResult->statut;
	}

	return  $statut;
}

function url_battle($id, $dbh)
{
	$url = "";
	$sth = $dbh->prepare("SELECT url FROM `bl_battles` WHERE `id` = :id limit 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$url = $row_userResult->url;
	}

	return  $url;
}


function photo_user($id, $dbh)
{

	$sth = $dbh->prepare("SELECT photo FROM `bl_user` WHERE `id` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		$photo = $row_userResult->photo;
	}


	return  $photo;
}


function name_user($id, $dbh)
{


	$sth = $dbh->prepare("SELECT CONCAT(prenom, ' ', nom) AS username FROM `bl_user` WHERE `id` = :id LIMIT 0,1");
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$rs = $sth->execute();

	if ($sth->rowCount() > 0) {

		$row_userResult = $sth->fetch(PDO::FETCH_OBJ);
		return $row_userResult->username;
	}

	return;
}

function existe_user($email, $dbh)
{
	$sth = $dbh->prepare("SELECT 1 FROM `bl_user` WHERE `email` = :email LIMIT 0,1");
	$sth->bindParam(':email', $email, PDO::PARAM_STR);
	$rs = $sth->execute();
	return $sth->rowCount() > 0;
}



function mon_ami($id, $ami, $sexe, $code, $dbh)
{

	$amis = "";

	$select_amis = $dbh->prepare(
		"	SELECT DATE_FORMAT(date, '%d/%m/%Y') AS date_demande 
			FROM `bl_user_friend`
			WHERE (user=:user AND friend = :ami) OR (user=:ami AND friend = :user) 
			LIMIT 0,1
		"
	);
	$select_amis->bindParam(':ami', $ami, PDO::PARAM_STR);
	$select_amis->bindParam(':user', $id, PDO::PARAM_STR);

	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		if ($sexe == 1) {
			$amis = "Amie";
		} else {
			$amis = "Ami";
		}
		if ($code == "en") {
			$amis = "Friend since " . $row_amis->date_demande;
		} else {
			$amis = $amis . " depuis le " . $row_amis->date_demande;
		}
	}

	return $amis;
}


function demande_ami($id, $ami, $dbh)
{

	$amis = "";

	$select_amis = $dbh->prepare(
		"	SELECT DATE_FORMAT(date, '%d-%m-%Y') AS date_demande 
			FROM `bl_user_demande_friend`
			WHERE user = :user AND qui = :ami AND statut = 1 
			LIMIT 0,1
		"
	);
	$select_amis->bindParam(':ami', $ami, PDO::PARAM_STR);
	$select_amis->bindParam(':user', $id, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		$amis = "Invitation envoyée le " . $row_amis->date_demande;
	}

	return $amis;
}

function recherche_ami($id, $ami, $dbh)
{

	$select_amis = $dbh->prepare(
		"   SELECT friend AS user_id FROM bl_user_friend WHERE user = :user AND statut = 1
		UNION 
			SELECT user FROM bl_user_friend WHERE friend = :ami AND statut = 1
	"
	);
	$select_amis->bindParam(':ami', $ami, PDO::PARAM_STR);
	$select_amis->bindParam(':user', $id, PDO::PARAM_STR);
	$select_amis->execute();

	return ($select_amis->rowCount() > 0);
}

function recherche_cat($id, $cat, $dbh)
{

	$select_amis = $dbh->prepare("SELECT 1 FROM `bl_user_cat` WHERE `user` = :user AND `cat` = :cat LIMIT 0,1");
	$select_amis->bindParam(':cat', $cat, PDO::PARAM_STR);
	$select_amis->bindParam(':user', $id, PDO::PARAM_STR);
	$select_amis->execute();

	return ($select_amis->rowCount() > 0);
}

function user_cat($id, $dbh)
{

	$select_amis = $dbh->prepare("SELECT 1 FROM `bl_user_cat` WHERE `user` = :user LIMIT  0,1");
	$select_amis->bindParam(':user', $id, PDO::PARAM_STR);
	$select_amis->execute();

	return ($select_amis->rowCount() > 0);
}

function pluralize($count, $text)
{
	return $count . (($count == 1) ? (" $text") : (" ${text}s"));
}

function ago($datetime)
{
	global $code;

	if (is_string($datetime)) {
		$datetime = new DateTime($datetime);
	}
	$interval = date_create('now')->diff($datetime);
	$suffix = ($interval->invert ? ' ago' : '');
	$prefix = ($interval->invert ? ' Il y a ' : '');

	$callbacks = [
		'fr' => function ($interval) use ($prefix) {
			if ($interval->y >= 1) return $prefix . pluralize($interval->y, 'an');
			if ($interval->m >= 1) return $prefix . $interval->m . ' mois';
			if ($interval->d >= 1) return $prefix . pluralize($interval->d, 'jour');
			if ($interval->h >= 1) return $prefix . pluralize($interval->h, 'heure');
			if ($interval->i >= 1) return $prefix . pluralize($interval->i, 'minute');
			return $prefix . pluralize($interval->s, 'seconde');
		},
		'en' => function ($interval) use ($suffix) {
			if ($interval->y >= 1) return pluralize($interval->y, 'year') . $suffix;
			if ($interval->m >= 1) return pluralize($interval->m, 'month') . $suffix;
			if ($interval->d >= 1) return pluralize($interval->d, 'day') . $suffix;
			if ($interval->h >= 1) return pluralize($interval->h, 'hour') . $suffix;
			if ($interval->i >= 1) return pluralize($interval->i, 'minute') . $suffix;
			return pluralize($interval->s, 'second') . $suffix;
		}
	];

	return $callbacks[$code]($interval);
}

function alert_option($option, $dbh)
{

	$cat_ok = 0;
	$select_amis = $dbh->prepare("SELECT `titre_fr` FROM `bl_alert_options` WHERE `id` =:option LIMIT 0,1");
	$select_amis->bindParam(':option', $option, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		$cat_ok = $row_amis->titre_fr;
	}

	return $cat_ok;
}

function alert_option_user($user, $alert, $dbh)
{

	$option = 0;
	$select_amis = $dbh->prepare("SELECT opt FROM `bl_user_alerts` WHERE `user` = :user AND `alert`= :alert");
	$select_amis->bindParam(':user', $user, PDO::PARAM_STR);
	$select_amis->bindParam(':alert', $alert, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		$row_amis = $select_amis->fetch(PDO::FETCH_OBJ);
		$option = $row_amis->opt;
	}

	return $option;
}

function fav_user($user, $post, $dbh)
{

	$favResult = $dbh->prepare("SELECT 1 FROM `bl_user_favoris` WHERE `user` = :user AND `post` = :post LIMIT 0,1");
	$favResult->bindParam(':user', $user, PDO::PARAM_STR);
	$favResult->bindParam(':post', $post, PDO::PARAM_STR);
	$favResult->execute();

	return ($favResult->rowCount() > 0);
}

function is_first_post($user, $dbh)
{

	$query = $dbh->prepare("SELECT COUNT(*) AS nb_posts FROM `bl_battle_posts` WHERE `user` = :user");
	$query->bindParam(':user', $user, PDO::PARAM_STR);
	$query->execute();

	if ($query->rowCount()) {
		$row = $query->fetch(PDO::FETCH_OBJ);
		return $row->nb_posts;
	}

	return false;
}


function logUser($email, $password)
{
	global $dbh, $code, $_, $adr;

	if (empty($email) || empty($password)) {
		return false;
	}

	$sth = $dbh->prepare('SELECT `password`, prenom, photo, id, likes, like_coins, sexe FROM `bl_user` WHERE email = :email AND statut !=3 LIMIT 0,1');
	$sth->bindParam(':email', $email, PDO::PARAM_STR);
	$rs = $sth->execute();

	if (!$sth->rowCount()) {
		return false;
	}

	$row = $sth->fetch(PDO::FETCH_OBJ);

	$passRecup = $row->password;
	$prenom = $row->prenom;
	$photo = $row->photo;
	$id_user = $row->id;
	$likes = $row->likes;
	$like_coins = $row->like_coins;
	$gender = $row->sexe;

	if (!password_verify($password, $passRecup)) {
		return false;
	}
	session_start();
	$_SESSION['securite'] = $passRecup;
	$_SESSION['prenom'] = $prenom;
	$_SESSION['photo'] = $photo;
	$_SESSION['id_user'] = $id_user;
	$_SESSION['likes'] = $likes;
	$_SESSION['likeCoins'] = $like_coins;
	$_SESSION['gender'] = $gender;

	if (!empty($_POST["remember"])) {
		setcookie("member_login", $_POST["email"], time() + 365 * 24 * 3600, '/', '.' . $_SERVER['HTTP_HOST'], true, false);
		setcookie("member_pwd", $_POST["password"], time() + 365 * 24 * 3600, '/', '.' . $_SERVER['HTTP_HOST'], true, false);
	} else {

		if (isset($_COOKIE["member_login"])) {
			setcookie("member_login", $_POST["email"], time() - 3600);
		}
		if (isset($_COOKIE["member_pwd"])) {
			setcookie("member_pwd", $_POST["password"], time() - 3600);
		}
	}

	//verifie si connexion 12h
	$userConnexion = $dbh->prepare("SELECT DATE_FORMAT( ADDDATE(max(date), INTERVAL 12 HOUR), '%Y-%m-%d %k') as date  
	FROM `bl_connexion` where bl_connexion.`user` = '$id_user' limit 0,1");
	$userConnexion->execute();

	if ($userConnexion->rowCount() > 0) {

		$row_userConnexion = $userConnexion->fetch(PDO::FETCH_OBJ);
		$date2 = $row_userConnexion->date;
		$date = explode(" ", $date2);

		$heure2 = $date[1];
		$now2 = $date[0];

		$heure = date('H');
		$now = date('Y-m-d');

		if ($heure2 == $heure && $now2 == $now) {
			/*** likesCoins ***/
			$likesCoins = 50;
			$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins`= `like_coins` + $likesCoins WHERE `id` = :id");
			$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
			$updateReq->execute();
			/*** likesCoins ***/

			//notif
			$message = '<div class="sh-notif__ligne">
				<a href="/' . $code . '/' . $_['url_compte'] . '" class="sh-comments__notif sh-avatar">
					<img src="/' . $photo . '" alt="' . $prenom . '">
				</a>
				<div>
					<a href="/' . $code . '/' . $_['url_compte'] . '" >Bravo, vous gagnez 50 LIKECOINS !</a>
					<span>[date]</span>
				</div>
			</div>';
			save_notifs($id_user, $message, $dbh);
		}
	}

	//insertion traceur
	save_connexion($id_user, $dbh);

	//insertion log connexion
	save_log_ou($id_user, 21, $adr, $dbh);

	//UPDATE token connexion
	$tokenUser = password_hash($prenom . date('YmdHHis'), PASSWORD_DEFAULT);
	$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= :token WHERE id = :id_user");
	$updateReq->bindParam(':token', $tokenUser, PDO::PARAM_STR);
	$updateReq->bindParam(':id_user', $id_user, PDO::PARAM_STR);
	$updateReq->execute();

	return true;
}

function no_special_character($chaine)
{

	//  les accents
	$chaine = trim($chaine);
	/*$chaine= strtr($chaine,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ","aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"); */
	$chaine = str_replace(
		array(
			'à', 'â', 'ä', 'á', 'ã', 'å',
			'î', 'ï', 'ì', 'í',
			'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
			'ù', 'û', 'ü', 'ú',
			'é', 'è', 'ê', 'ë',
			'ç', 'ÿ', 'ñ', 'ý',
		),
		array(
			'a', 'a', 'a', 'a', 'a', 'a',
			'i', 'i', 'i', 'i',
			'o', 'o', 'o', 'o', 'o', 'o',
			'u', 'u', 'u', 'u',
			'e', 'e', 'e', 'e',
			'c', 'y', 'n', 'y',
		),
		$chaine
	);

	$chaine = str_replace(
		array(
			'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
			'Î', 'Ï', 'Ì', 'Í',
			'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø',
			'Ù', 'Û', 'Ü', 'Ú',
			'É', 'È', 'Ê', 'Ë',
			'Ç', 'Ÿ', 'Ñ', 'Ý',
		),
		array(
			'A', 'A', 'A', 'A', 'A', 'A',
			'I', 'I', 'I', 'I',
			'O', 'O', 'O', 'O', 'O', 'O',
			'U', 'U', 'U', 'U',
			'E', 'E', 'E', 'E',
			'C', 'Y', 'N', 'Y',
		),
		$chaine
	);

	$chaine = str_replace(
		array(
			',', '?'
		),
		'',
		$chaine
	);

	//  les caracètres spéciaux (aures que lettres et chiffres en fait)
	$chaine = preg_replace('/([^.a-z0-9]+)/i', '-', $chaine);
	$chaine = strtolower($chaine);

	return $chaine;
}
