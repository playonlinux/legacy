<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

$where = "";
$where2 = "";
$limit = "order by nom desc limit 0,100";
//limit 0,20

$class = "col-lg-3 col-md-4 col-6";

if (!empty($_POST['mots'])) {
	$name = $_POST['mots'];
	$mots = explode(' ', $name); //En separe l'expression en mots cles

	$k = 1;
	$where .= "and ";

	foreach ($mots as $mot) {
		if ($k == count($mots)) {
			$where .= "(`bl_user`.`speudo` LIKE '%" . $mot . "%' or `bl_user`.`nom` LIKE '%" . $mot . "%' or `bl_user`.`prenom` LIKE '%" . $mot . "%')";
		} else {
			$where .= "(`bl_user`.`speudo` LIKE '%" . $mot . "%' or `bl_user`.`nom` LIKE '%" . $mot . "%' or `bl_user`.`prenom` LIKE '%" . $mot . "%') AND ";
		}
		$k++;
	}

	$limit = "order by nom desc";
	$class = "col-lg-3 col-md-4 col-6";
}

if (!empty($_POST['cat'])) {
	$cat = $_POST['cat'];
	$where = " and `bl_battles`.`category` = '$cat'";
	$class = "col-lg-3 col-md-4 col-6";
	$limit = "order by nom desc limit 0,100";
}

if (!empty($_POST['number'])) {
	$number = $_POST['number'];
	$limit = " order by nom desc limit 0,$number";
	$class = "col-lg-3 col-md-4 col-6";
}
$exclu = "";
if (!empty($_SESSION['securite'])) {
	$exclu = " and id != " . $_SESSION['id_user'] . " ";
}

$select_users = $dbh->prepare("SELECT * FROM `bl_user`
WHERE bl_user.`statut` < '3' $exclu $where $limit");
$select_users->execute();

if (!$select_users->rowCount()) {
	echo 'null';
	exit;
}
while ($row_users = $select_users->fetch(PDO::FETCH_OBJ)) { ?>
	<!--section-->

	<div class="sh-section__item <?= $class ?>" id="user_<?= $row_users->id ?>">

		<div class="sh-section box">
			<div class="sh-section__content">
				<a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_users->id, $dbh) ?> ">
					<div class="sh-section__media">

						<span class="sh-section__user__image" style="border-color:<?php echo grade_user_color(points_user($row_users->id, $dbh), $dbh); ?>; background-image:url(<?= '/' . photo_user($row_users->id, $dbh) ?>)"></span>
					</div>
				</a>
				<p class="text-center name">
					<?= name_user($row_users->id, $dbh) ?><br />
					<span class="grade" style="color:<?php echo grade_user_color(points_user($row_users->id, $dbh), $dbh); ?>"><?php echo grade_user_between(points_user($row_users->id, $dbh), $code, $dbh); ?></span>
				</p>
			</div>
			<div class="sh-section__footer text-center">
				<?php if (!empty($_SESSION['securite'])) {
						$mon_amis = mon_ami($_SESSION['id_user'],  $row_users->id, $row_users->sexe, $code,  $dbh);
						$demande_amis = demande_ami($_SESSION['id_user'],  $row_users->id, $dbh);
						?>
					<?php if (!empty($demande_amis)) { ?>
						<a id="btn-annule-friend-<?= $row_users->id; ?>" data-friend="<?= $row_users->id ?>" data-user="<?= $_SESSION['id_user'] ?>" href="javascript:void(0)" class="sh-btn center-block btn-friend_annule">
							<?= $_['bt_annulerami'] ?></a>
					<?php } elseif (empty($mon_amis)) { ?>
						<a id="btn-friend-<?= $row_users->id; ?>" data-friend="<?= $row_users->id; ?>" data-user="<?= $_SESSION['id_user']; ?>" href="javascript:void(0)" class="sh-btn center-block btn-friend_search">
							<?= $_['bt_devenir'] ?></a>
					<?php } else { ?>
						<p class='rose center-block mt-4 mb-3'><?= $mon_amis ?></p>
					<?php } ?>
				<?php } else { ?>
					<a id="demande_amis" href="javascript:void(0)" class="sh-btn center-block btn-upload_btn-signup mt-3"> <?= $_['bt_devenir'] ?></a>
				<?php } ?>
			</div>
		</div>
	</div>

<?php } ?>