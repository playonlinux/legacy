<?php
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

securePost();
secureGet();

if(isset($_SESSION['securite']) && $_SESSION['securite'] !='' ){
	
	$securite =$_SESSION['securite'];
	
	$userResult= $dbh->prepare("SELECT * FROM `bl_user` WHERE `password` = :securite limit 0,1");
	$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();
	
	if ($userResult->rowCount() > 0) {
		
		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user=$row_userResult->id;
			
		//POST
		$id_post = $_POST['id_post'];
		
		$favResult= $dbh->prepare("SELECT * FROM `bl_user_favoris` WHERE `user` = :user and `post` = :post limit 0,1");
		$favResult->bindParam(':user', $user, PDO::PARAM_STR);
		$favResult->bindParam(':post', $id_post, PDO::PARAM_STR);
		$favResult->execute();
		
		//echo $favResult->rowCount();
		
		if ($favResult->rowCount() == 0) {
			
			$insertReq = $dbh->prepare("INSERT INTO `bl_user_favoris` (`user`, `post`) VALUES (:user, :post)");
			$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
			$insertReq->bindParam(':post', $id_post, PDO::PARAM_STR);
			$insertReq->execute();
			echo 'save';
			
			
		}else{
			
			$deleteReq= $dbh->prepare("DELETE FROM `bl_user_favoris` WHERE `user` = :user and `post` = :post");
			$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
			$deleteReq->bindParam(':post', $id_post, PDO::PARAM_STR);
			$deleteReq->execute();
			echo 'remove';
			
		}
		
	}else{
		echo 'non';
	}
			
}

?>