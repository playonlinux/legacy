<?php

require_once('./mysql.inc.php');
require_once('./funct_battelike.php');


securePost();

$lastName = $_POST['last_name'];
$firstName = $_POST['first_name'];
$email = $_POST['email'];
try {
	$birthday = (new DateTime($_POST['birthday']))->format('Y-m-d');
} catch (Exception $e) {
	$birthday = null;
}
$facebookId = $_POST['id'];
$accessToken = $_POST['access_token'];
$data = file_get_contents(html_entity_decode($_POST['picture']));
$photo =  'images/avatars/' . hash("sha256", $_POST['picture'] . time());
file_put_contents('../' . $photo, $data, LOCK_EX);

switch ($_POST['gender']) {
	case 'female': {
			$gender = 1;
			break;
		}
	case 'male': {
			$gender = 2;
			break;
		}
	default: {
			$gender = 3;
			break;
		}
}

$tokenUser = password_hash($firstName . date('YmdHHis'), PASSWORD_DEFAULT);
$url =  trim(strtolower(str_replace(" ", "-", no_special_character($lastName . '.' . $firstName))));

// Verification si le faccebookId existe dans la BDD
$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE facebook_id = :facebook_id limit 0,1');
$sth->bindParam(':facebook_id', $facebookId, PDO::PARAM_STR);
$sth->execute();

$firstTime = ($sth->rowCount() === 0);

if ($firstTime) {
	// Verification si l'adresse mail existe dans la BDD
	$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE email = :email limit 0,1');
	$sth->bindParam(':email', $email, PDO::PARAM_STR);
	$sth->execute();
}

$firstTime = ($sth->rowCount() == 0);
if ($firstTime) { //CAD que le mail nest pas dans la BDD    
	//nouvelle entrée dans la bdd
	$insertReq = $dbh->prepare(
		" INSERT INTO `bl_user`(`code`, `nom`, `prenom`, `speudo`, `email`, `password`, `photo`, `sexe`, `naissance`, `url`,`token`,`likes`,`statut`,`facebook_id`) 
				VALUES (:code, :nom, :prenom , :speudo, :email, :password, :photo, :gender, :birthday, :url, :token, '200','2', :facebook_id)"
	);
} else {
	$rowUser = $sth->fetch(PDO::FETCH_OBJ);
	$insertReq = $dbh->prepare(
		"	UPDATE `bl_user` SET 
					`code`=:code,
					`nom`=:nom, 
					`prenom`=:prenom , 
					`speudo`=:speudo,
					`email`=:email,
					`photo`=:photo,
					`sexe`=:gender,
					`naissance`=:birthday,
					`url`=:url,
					`token`=:token,
					`statut`='2',
					`facebook_id`=:facebook_id 
				WHERE email = :email"
	);
}


$insertReq->bindParam(':code', $code, PDO::PARAM_STR);
$insertReq->bindParam(':nom', $lastName, PDO::PARAM_STR);
$insertReq->bindParam(':prenom', $firstName, PDO::PARAM_STR);
$insertReq->bindParam(':speudo', $firstName, PDO::PARAM_STR);
$insertReq->bindParam(':email', $email, PDO::PARAM_STR);
if ($firstTime) {
	$insertReq->bindParam(':password', $tokenUser, PDO::PARAM_STR);
}
$insertReq->bindParam(':photo', $photo, PDO::PARAM_STR);
$insertReq->bindParam(':gender', $gender, PDO::PARAM_STR);
$insertReq->bindParam(':birthday', $birthday, PDO::PARAM_STR);
$insertReq->bindParam(':url', $url, PDO::PARAM_STR);
$insertReq->bindParam(':token', $tokenUser, PDO::PARAM_STR);
$insertReq->bindParam(':facebook_id', $facebookId, PDO::PARAM_STR);

$insertReq->execute();

$id_user = ($firstTime) ? $dbh->lastInsertId() : $rowUser->id;

//save_log($id_user, '4', $dbh);

//insertion traceur
save_connexion($id_user, $dbh);

//creation des alerts
save_alert($id_user, $dbh);

$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE facebook_id = :facebook_id and statut !=3 limit 0,1');
$sth->bindParam(':facebook_id', $facebookId, PDO::PARAM_STR);
$rs = $sth->execute();

$row = $sth->fetch(PDO::FETCH_OBJ);

$passRecup = $row->password;
$firstName = $row->prenom;
$photo = $row->photo;
$id_user = $row->id;
$likes = $row->likes;
$like_coins = $row->like_coins;
$tokenUser = $row->token;
$email = $row->email;


$_SESSION['token'] = $tokenUser;
$_SESSION['securite'] = $passRecup;
$_SESSION['prenom'] = $firstName;
$_SESSION['photo'] = $photo;
$_SESSION['id_user'] = $id_user;
$_SESSION['likes'] = $likes;
$_SESSION['likeCoins'] = $like_coins;
$_SESSION["email"] = $email;
$_SESSION["password"] = 'NULL';
$_SESSION['fb_access_token'] = $accessToken;
$_SESSION['gender'] = $gender;

setcookie("member_login", $_SESSION["email"], time() + 365 * 24 * 3600, '/', '.' . $_SERVER['HTTP_HOST'], true, true);
setcookie("member_pwd", $_SESSION["password"], time() + 365 * 24 * 3600, '/', '.' . $_SERVER['HTTP_HOST'], true, true);
setcookie("securite", $_SESSION["securite"], time() + 365 * 24 * 3600, '/', '.' . $_SERVER['HTTP_HOST'], true, true);

if ($firstTime) {
	echo json_encode([
		'isFirstTime' => $firstTime,
		'code' => $_SESSION['code'],
		'idUser' => $id_user,
		'token' => $tokenUser
	]);
}
