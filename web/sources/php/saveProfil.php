<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

$id = $_SESSION['id_user'];
$token = $_SESSION['securite'];

if (empt($id) || empty($token)) {
	exit;
}

$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE id = :id and password = :token  limit 0,1');
$sth->bindParam(':id', $id, PDO::PARAM_STR);
$sth->bindParam(':token', $token, PDO::PARAM_STR);
$rs = $sth->execute();

if ($sth->rowCount() > 0) {
	echo 'non';
	exit;
}

$type = (!empty($_POST['type'])) ? $_POST['type'] : $_GET['type'];


if ($type == "profil") {

	$nom = $_POST['nom'];
	$prenom = $_POST['prenom'];
	$speudo = "";
	$mail = $_POST['email'];

	$sexe = $_POST['sexe'];
	$ville = $_POST['ville'];

	$info = $_POST['info'];

	$naissance = $_POST['naissance'];
	$naissance = explode('/', $naissance);
	$naissance = $naissance[2] . '-' . $naissance[1] . '-' . $naissance[0];


	$updateReq = $dbh->prepare("UPDATE `bl_user` SET `nom`=:nom, `prenom`=:prenom, `speudo`=:speudo, `email`=:mail,
		`sexe`=:sexe, `naissance`=:naissance, `ville`=:ville,`info`=:info
		WHERE id = :id ");
	$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
	$updateReq->bindParam(':nom', $nom, PDO::PARAM_STR);
	$updateReq->bindParam(':prenom', $prenom, PDO::PARAM_STR);
	$updateReq->bindParam(':speudo', $speudo, PDO::PARAM_STR);
	$updateReq->bindParam(':mail', $mail, PDO::PARAM_STR);
	$updateReq->bindParam(':sexe', $sexe, PDO::PARAM_STR);
	$updateReq->bindParam(':naissance', $naissance, PDO::PARAM_STR);
	$updateReq->bindParam(':ville', $ville, PDO::PARAM_STR);
	$updateReq->bindParam(':info', $info, PDO::PARAM_STR);

	$updateReq->execute();


	$dossierEnregistrementImg = "images/avatars/";
	// On récupére l'image de la vignette
	// Chemin de l'image par défaut
	$name_file = 'avatar.jpg';

	if (!empty($_FILES['photo']['name']) && !empty($_FILES['photo']['type'])) {

		$tmp_file = $_FILES['photo']['tmp_name'];

		$name_file = $_FILES['photo']['name'];
		$size_file = $_FILES['photo']['size'];
		$ext = end(explode('.', $name_file));

		$aleatoire = md5(time());

		$name_file = $aleatoire . '.' . rand(5, 9999) . '.' . $ext;
		$name_file = str_replace(" ", "-", $name_file);

		$cheminfichier = 'images/avatars/' . $name_file;

		$type = mime_content_type($_FILES['photo']['tmp_name']);
		$allowed_mime_types = array('image/gif', 'image/jpeg', 'image/png', 'image/bmp');

		$legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");

		$legalSize = "2000000";

		if (in_array($type, $allowed_mime_types) && ($size_file != 0 && $size_file < $legalSize) && in_array($ext, $legalExtensions)) {

			if (0 < $_FILES['photo']['error']) {

				echo 'Error: ' . $_FILES['photo']['error'] . '<br>';
			} else {
				if (!move_uploaded_file($_FILES['photo']['tmp_name'], '../images/avatars/' . $name_file)) {
					echo "non";
				} else {
					$updateReq = $dbh->prepare("UPDATE `bl_user` SET `photo`= :cheminfichier WHERE `id` ='$id'");
					$updateReq->bindParam(':cheminfichier', $cheminfichier, PDO::PARAM_STR);
					$rs = $updateReq->execute();

					$_SESSION['photo'] = $cheminfichier;
				}
			}
		}
	}

	$_SESSION['prenom'] = $prenom;

	echo "ok";
} else if ($type == "mdp") {

	if (isset($_POST['pass']) && $_POST['pass'] != '') {

		if (isset($_POST['pass_confirm']) && $_POST['pass_confirm'] != '') {

			if ($_POST['pass_confirm'] == $_POST['pass']) {

				$passwordOK = password_hash($_POST['pass'], PASSWORD_DEFAULT);

				$updateReq = $dbh->prepare("UPDATE `bl_user` SET `password`= :passwordOK WHERE `id` = :id");
				$updateReq->bindParam(':passwordOK', $passwordOK, PDO::PARAM_STR);
				$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
				$updateReq->execute();

				$_SESSION['securite'] = $passwordOK;

				echo 'ok';
			} else {
				echo 'Mots de passe non identiques !';
			}
		} else {
			echo 'Merci de remplir tous les champs !';
		}
	}
} else if ($type == "cat") {

	if (isset($_POST['catok']) && $_POST['catok'] != '') {
		//delete
		$deleteReq = $dbh->prepare("DELETE FROM `bl_user_cat` WHERE `user` = :id");
		$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
		$deleteReq->execute();

		//insert cat
		$catok = explode(',', $_POST['catok']);
		foreach ($catok as $cat) {
			$insertReq = $dbh->prepare("INSERT INTO `bl_user_cat`(`user`, `cat`) VALUES (:id, :cat)");
			$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
			$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);

			$insertReq->execute();
		}

		echo 'ok';
	} else {
		echo 'non';
	}
} else if ($type == "notif") {

	// if(($_POST['alert_1']) && $_POST['alert_1'] !='') {
	// 	//delete
	// 	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_alerts` WHERE `user` = :id");
	// 	$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
	// 	$deleteReq->execute();

	//insert cat
	for ($i = 1; $i < 10; $i++) {

		if (!empty($_POST['alert_' . $i])) {

			$deleteReq = $dbh->prepare("DELETE FROM `bl_user_alerts` WHERE `user` = :id AND alert = :alert AND opt = :opt");
			$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
			$deleteReq->bindParam(':alert', $i, PDO::PARAM_STR);
			$deleteReq->bindParam(':opt', $_POST['alert_' . $i], PDO::PARAM_STR);
			$deleteReq->execute();


			$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
			$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
			$insertReq->bindParam(':alert', $i, PDO::PARAM_STR);
			$insertReq->bindParam(':opt', $_POST['alert_' . $i], PDO::PARAM_STR);
			$insertReq->execute();
		}
	}

	echo 'ok';

	// }else{
	// 	echo 'non';
	// }

} else if ($type == "supp") {

	//desactivation compte
	$datesupp = date("Y-m-d");
	$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= '', `statut`='3', `date_supp`=:datesupp WHERE id = :id ");
	$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
	$updateReq->bindParam(':datesupp', $datesupp, PDO::PARAM_STR);
	$updateReq->execute();

	//desactivation battle
	$updateReq = $dbh->prepare("UPDATE `bl_battles` SET `statut`='3' WHERE user = :id ");
	$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
	$updateReq->execute();

	//desactivation post
	$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `statut`='2' WHERE user = :id ");
	$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
	$updateReq->execute();

	//selection des likes posts + desactivation like
	$postResult = $dbh->prepare("SELECT * FROM `bl_battle_posts` WHERE `user` = :id and `statut`='2'");
	$postResult->bindParam(':id', $id, PDO::PARAM_STR);
	$postResult->execute();

	if ($postResult->rowCount() > 0) {

		while ($row_postResult = $userResult->fetch(PDO::FETCH_OBJ)) {
			$updateReq = $dbh->prepare("UPDATE `bl_likes_post` SET `statut`='2' WHERE user = :id ");
			$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
			$updateReq->execute();
		}
	}

	//desactivation amis
	$updateReq = $dbh->prepare("UPDATE `bl_user_friend` SET `statut`=2 WHERE (user=:id or friend = :id)");
	$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
	$updateReq->execute();

	//delete demande amis
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE (user=:id or qui = :id) and statut =1");
	$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
	$deleteReq->execute();

	save_log($id, '12', $dbh);

	session_start();
	session_unset();
	session_destroy();

	echo 'ok';
}
