<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (!empty($_SESSION['securite'])) {

	$securite = $_SESSION['securite'];

	$userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
	$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
	// $userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
	// $userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();

	if ($userResult->rowCount() > 0) {

		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $row_userResult->id;

		if ($_POST['type'] == 'post') {
			//POST
			$id_post = $_POST['id_post'];
			$name_post = $_POST['name_post'];

			$updatePost = $dbh->prepare("UPDATE `bl_battle_posts` SET `title`=:name_post WHERE `user` =:user and `id` =:id_post");
			$updatePost->bindParam(':id_post', $id_post, PDO::PARAM_STR);
			$updatePost->bindParam(':user', $user, PDO::PARAM_STR);
			$updatePost->bindParam(':name_post', $name_post, PDO::PARAM_STR);
			$updatePost->execute();
			save_log($user, '10', $dbh);
		} elseif ($_POST['type'] == 'battle') {
			//POST
			$id_battle = $_POST['id_battle'];
			$name_battle = $_POST['name_battle'];
			$info_battle = $_POST['info_battle'];
			$cat_battle = $_POST['cat_battle'];

			$updatePost = $dbh->prepare("UPDATE `bl_battles` SET `category`= :cat_battle, `title`=:name_battle, `info`=:info_battle WHERE `user` =:user and `id` =:id_battle");
			$updatePost->bindParam(':id_battle', $id_battle, PDO::PARAM_STR);
			$updatePost->bindParam(':user', $user, PDO::PARAM_STR);
			$updatePost->bindParam(':name_battle', $name_battle, PDO::PARAM_STR);
			$updatePost->bindParam(':info_battle', $info_battle, PDO::PARAM_STR);
			$updatePost->bindParam(':cat_battle', $cat_battle, PDO::PARAM_STR);

			$updatePost->execute();
			save_log($user, '11', $dbh);
		}

		echo 'oui';
	} else {
		echo 'non';
	}
} else {

	echo 'non';
}
