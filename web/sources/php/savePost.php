<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (empty($_SESSION['securite'])) {
	echo 'non';
	exit;
}

$request = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$request->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
$request->execute();

if (!$request->rowCount()) {
	echo 'non';
	exit;
}

$row = $request->fetch(PDO::FETCH_OBJ);
$user = $row->id;

$title = $_POST['title'];
$battle = intval($_POST['battle']);
$type = intval($_POST['type']);

$post = '';
$lien_post = '';

if (!empty($_POST['text'])) {
	$post = $_POST['text'];
	if (!empty($_POST['lien_text'])) {
		$lien_post = $_POST['lien_text'];
	}
}

if (!empty($_POST['lien'])) {
	$post = $_POST['lien'];
}

$token = md5(time());

$insertReq = $dbh->prepare("INSERT INTO `bl_battle_posts` (`battle`, `user`, `title`, `type`, `post`, `lien_post`, `token`) 
		VALUES (:battle, :user, :title, :type, :post, :lien_post, :token)");
$insertReq->bindParam(':battle', $battle, PDO::PARAM_STR);
$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
$insertReq->bindParam(':title', $title, PDO::PARAM_STR);
$insertReq->bindParam(':type', $type, PDO::PARAM_STR);
$insertReq->bindParam(':post', $post, PDO::PARAM_STR);
$insertReq->bindParam(':lien_post', $lien_post, PDO::PARAM_STR);
$insertReq->bindParam(':token', $token, PDO::PARAM_STR);

$insertReq->execute();
$id_post = $dbh->lastInsertId();

$dossierEnregistrementImg = "images/posts/";
// On récupére l'image de la vignette
// Chemin de l'image par défaut
$name_file = 'avatar.jpg';

if (!empty($_FILES['image']['name']) && !empty($_FILES['image']['type'])) {
	// IMAGE principale //
	// on test si le fichier est uploadĂŠ
	$tmp_file = $_FILES['image']['tmp_name'];
	$size_file = $_FILES['image']['size'];
	// renommons le fichier pour plus de sécurité

	$name_file = $_FILES['image']['name'];
	$ext = end(explode('.', $name_file));

	$aleatoire = md5(time());

	$name_file = $aleatoire . '.' . rand(5, 9999) . '.' . $ext;
	$name_file = str_replace(" ", "-", $name_file);

	$cheminfichier = 'images/posts/' . $name_file;


	$type = mime_content_type($_FILES['image']['tmp_name']);
	$allowed_mime_types = array('image/gif', 'image/jpeg', 'image/png', 'image/bmp');

	$legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");

	$legalSize = "10000000";

	if (in_array($type, $allowed_mime_types) && ($size_file != 0 && $size_file < $legalSize) && in_array($ext, $legalExtensions)) {

		if (0 < $_FILES['image']['error']) {
			echo 'Error: ' . $_FILES['image']['error'] . '<br>';
		} else {

			if (!move_uploaded_file($_FILES['image']['tmp_name'], '../images/posts/' . $name_file)) { } else {

				$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `post`= :cheminfichier WHERE `id` ='$id_post'");
				$updateReq->bindParam(':cheminfichier', $cheminfichier, PDO::PARAM_STR);
				$updateReq->execute();
			}
		}
	}
}


//on verifie si premier post

if (is_first_post($user, $dbh)) {

	/*** likesCoins ***/
	$likesCoins = 1000;
	$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins`= like_coins + $likesCoins WHERE id = :id");
	$updateReq->bindParam(':id', $user, PDO::PARAM_STR);
	$updateReq->execute();
	/*** likesCoins ***/


	/*** on recupere les likes du USER ***/
	$userLikesResult = $dbh->prepare("SELECT like_coins FROM `bl_user` WHERE id = :id limit 0,1");
	$userLikesResult->bindParam(':id', $user, PDO::PARAM_STR);
	$userLikesResult->execute();

	if ($userLikesResult->rowCount() > 0) {
		$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
		$mesLikeCoins = $row_userLikesResult->like_coins;
		$_SESSION['likeCoins'] = $mesLikeCoins;
	}
}

//mail user bataille
$userBattle = $dbh->prepare("SELECT user FROM `bl_battles`  WHERE `id`= :battle limit 0,1");
$userBattle->bindParam(':battle', $battle, PDO::PARAM_STR);
$userBattle->execute();
save_log($user, '2', $dbh);

if ($userBattle->rowCount() > 0) {

	$row_userBattle = $userBattle->fetch(PDO::FETCH_OBJ);
	$userB = $row_userBattle->user;

	if ($user != $userB) {


		$email = email_user($userB, $dbh);

		if (empty($email)) {
			echo 'non';
			exit;
		}
		$prenom = prenom_user($userB, $dbh);
		$title_b = name_battle($battle, $dbh);

		//celui qui post
		$amis = prenom_user($user, $dbh);
		$amis_photo = photo_user($user, $dbh);

		/*** likesCoins ***/
		$likesCoins = 100;
		$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins`= `like_coins` + $likesCoins WHERE `id` = :id");
		$updateReq->bindParam(':id', $userB, PDO::PARAM_STR);
		$updateReq->execute();
		/*** likesCoins ***/

		//email nouveau contenu si par detenteur de la battle
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		$imagePartage = photo_post_mail($token, $dbh);

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
				<tbody>
					<tr>
						<td height="73" align="center" valign="top">
							<table width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
								<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
										<tbody>
										<tr>
										<td height="58" valign="middle" style="padding:9px">
											<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr align="center">
													<td valign="middle" style="padding:0px 9px">
													<a href="' . $urlSite . ' target="_blank">
														<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
														src="' . $urlSite . '/images/logo-republilke-entier.png"  
														style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
													</a>
													</td>
												</tr>
											</tbody>
											</table>
										</td>
										</tr>
										</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
									<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
										
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											  <tr align="center">
												<td width="420" height="214" align="center">
												
													<p class="color:#777777;">' . $_['mailing']['hello']($prenom) . ',<br><br>
													' . $_['mailing']['content']['post']['body'] . ' :<br /> <b>' . $title_b . '</b>.
													<br> <br />
														<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_contenu'] . '/' . $token . '" style="text-decoration:none;"> 
															<img src="' . $imagePartage . '" width="250"> 
														</a><br /><br />
													</p>
													
													<p style="font-size:10px; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																										
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
				</table>';

		$to = $email;
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader($_['mailing']['content']['post']['subject']);

		if (alert_user(2, $userB, $dbh) == 1) {
			mail($to, $sujet, $msg, $headers);
		}

		//notif
		$message = '<div class="sh-notif__ligne">
						<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '" class="sh-comments__notif sh-avatar">
						<img src="/' . $amis_photo . '" alt="' . $amis . '"></a>
						<div>
							<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '">' . $_['mailing']['content']['post']['subject'] . '</a>
							<span>[date]</span>
						</div>
					</div>';
		save_notifs($userB, $message, $dbh);

		echo 'oui';
	}
}
