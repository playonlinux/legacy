<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();

if (!empty($_SESSION['securite'])) {

	$securite = $_SESSION['securite'];

	$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
	$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();

	if ($userResult->rowCount() > 0) {

		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $_SESSION['id_user'];
		// $user = $row_userResult->id;

		//POST
		$id_battle = $_POST['id_battle'];

		try {
			$dbh->beginTransaction();
			$query = $dbh->prepare("DELETE FROM bl_likes_post WHERE posts IN (SELECT id FROM bl_battle_posts WHERE battle = :id_battle)");
			$query->bindParam(':id_battle', $id_battle, PDO::PARAM_STR);
			$query->execute();

			$query = $dbh->prepare("DELETE FROM `bl_battle_posts` WHERE `battle` = :id_battle");
			$query->bindParam(':id_battle', $id_battle, PDO::PARAM_STR);
			$query->execute();

			$query = $dbh->prepare("DELETE FROM `bl_battles` WHERE `id` = :id_battle");
			$query->bindParam(':id_battle', $id_battle, PDO::PARAM_STR);
			$query->execute();

			save_log($user, '36', $dbh);

			$dbh->commit();
			echo 'oui';
		} catch (Exception $e) {
			$dbh->rollBack();
			echo 'non';
		}
	} else {
		echo 'non';
	}
} else {

	echo 'non';
}
