<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (!empty($_SESSION['securite'])) {

	$securite = $_SESSION['securite'];

	$userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
	$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();

	if ($userResult->rowCount() > 0) {

		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $row_userResult->id;

		// Pictos Home
		if (isset($_POST['value']) && $_POST['value'] == '.cat') {
			save_log_ou($user, '22', $adr, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == '.ami') {
			save_log_ou($user, '23', $adr, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == 'users') {
			save_log_ou($user, '25', $adr, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == 'date') {
			save_log_ou($user, '24', $adr, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == 'recherche') {
			$info =  urldecode(base64_decode($_POST['val']));
			save_log_ou_info($user, '26', $adr, $info, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == 'partage') {
			$info = $_POST['val'];
			save_log_ou_info($user, '30', $adr, $info, $dbh);
		} else if (isset($_POST['value']) && $_POST['value'] == 'partContenu') {
			$info = $_POST['val'];
			save_log_ou_info($user, '32', $adr, $info, $dbh);
		}
		echo "oui";
	} else {
		echo 'non';
	}
}
