<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();

if (empty($_POST['token'])) {
	echo 'non';
	exit;
}

if (empty($_POST['pass']) || empty($_POST['pass_confirm'])) {
	echo $_['password']['empty_fields'];
	exit;
}
if ($_POST['pass_confirm'] != $_POST['pass']) {
	echo $_['password']['pass_not_equals'];
	exit;
}

$token = $_POST['token'];
$id_user = intval($_POST['id']);

$sth = $dbh->prepare("SELECT 1 FROM `bl_user` WHERE `token` = :token and id = :id_user limit 0,1");
$sth->bindParam(':token', $token, PDO::PARAM_STR);
$sth->bindParam(':id_user', $id_user, PDO::PARAM_STR);
$sth->execute();

if (!$sth->rowCount()) {
	echo $_['password']['user_not_found'];
	exit;
}

$password = password_hash($_POST['pass'], PASSWORD_DEFAULT);

$updateReq = $dbh->prepare("UPDATE `bl_user` SET `password`= :password, `token`= null WHERE `id` = :id_user");
$updateReq->bindParam(':password', $password, PDO::PARAM_STR);
$updateReq->bindParam(':id_user', $id_user, PDO::PARAM_STR);
$updateReq->execute();

//insertion traceur
//save_log($id_user, 16, $dbh);

echo 'ok';
