<?php
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

securePost();
secureGet();

if(isset($_SESSION['securite']) && $_SESSION['securite'] !='' ){
	
	$securite =$_SESSION['securite'];
	
	$userResult= $dbh->prepare("SELECT * FROM `bl_user` WHERE `password` = :securite limit 0,1");
	$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();
	
	if ($userResult->rowCount() > 0) {
		
		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user=$row_userResult->id;
			
		$like_coins = intval($_POST['like_coins']);
		$likes = intval($_POST['likes']);		
		
		//Mise à jour de la demande				
		$updateReq = $dbh->prepare("UPDATE `bl_user` SET `likes` = `likes` + :likes,`like_coins` = `like_coins` - :like_coins WHERE `id` =:user");
		$updateReq->bindParam(':user', $user, PDO::PARAM_STR);
		$updateReq->bindParam(':likes', $likes, PDO::PARAM_STR);
		$updateReq->bindParam(':like_coins', $like_coins, PDO::PARAM_STR);
		$updateReq->execute();
		
		$userResult= $dbh->prepare("SELECT likes, like_coins FROM `bl_user` WHERE `password` = :securite limit 0,1");
		$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
		$rs = $userResult->execute();
		if ($userResult->rowCount() > 0) {
			
			$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
			$nb_likes=$row_userResult->likes;
			$nb_like_coins=$row_userResult->like_coins;
		}
		
		$_SESSION['likes'] =$nb_likes;
		$_SESSION['likeCoins'] =$nb_like_coins;
		save_log($user, '7', $dbh);
		
		echo 'oui';
		
	}else{
		echo 'non';
	}
			
}

?>