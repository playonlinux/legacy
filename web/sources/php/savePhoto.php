<?php
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

securePost();
secureGet();
$id= $_SESSION['id_user'];

$dossierEnregistrementImg = "images/avatars/";
// On récupére l'image de la vignette
// Chemin de l'image par défaut
$name_file = 'avatar.jpg';

if(!empty($_FILES['photo']['name']) && !empty($_FILES['photo']['type']))
{
  
  $tmp_file = $_FILES['photo']['tmp_name'];
  
  $name_file = $_FILES['photo']['name']; 
  $size_file = $_FILES['photo']['size']; 
  $ext = end(explode('.',$name_file));
  
  $aleatoire = md5(time());
  
  $name_file = $aleatoire.'.'.rand(5, 9999).'.'.$ext;
  $name_file = str_replace(" ","-",$name_file);
  
  $cheminfichier = 'images/avatars/'.$name_file;
  
  $type = mime_content_type($_FILES['photo']['tmp_name']);
  $allowed_mime_types = array('image/gif','image/jpeg','image/png','image/bmp');
  
  $legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");
  
  $legalSize = "2000000";
  
  if(in_array($type, $allowed_mime_types) && ($size_file !=0 && $size_file < $legalSize) && in_array($ext, $legalExtensions)){
            
    if ( 0 < $_FILES['photo']['error'] ) {
    
      echo 'Error: ' . $_FILES['photo']['error'] . '<br>';
    }else {
      if(!move_uploaded_file($_FILES['photo']['tmp_name'], '../images/avatars/'.$name_file)){
        echo "non";
      }else{
        $updateReq = $dbh->prepare("UPDATE `bl_user` SET `photo`= :cheminfichier WHERE `id` =:id");
        $updateReq->bindParam(':cheminfichier', $cheminfichier, PDO::PARAM_STR);
        $updateReq->bindParam(':id', $id, PDO::PARAM_STR);
        $rs = $updateReq->execute();
        
        $_SESSION['photo'] =$cheminfichier;
      }
    }
    
  }		
}

?>