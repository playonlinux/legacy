<?php
// ini_set("session.cookie_httponly", 1);
if (empty($_SESSION)) {
    // 1 semaine 
    session_set_cookie_params(604800, '/', '.' . $_SERVER['HTTP_HOST'], true, true);
    session_start();
}

$nameSite = "REPUBLIKE";
// $urlSite ="https://www.republike.io/";

// DEV
if (in_array($_SERVER['HTTP_HOST'], ['preprod.republike.io', 'www.preprod.republike.io'])) {
    // PREPROD
    $urlSite = 'https://' . $_SERVER['HTTP_HOST'];
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'aj264163-001.privatesql';
    $db_name = 'republikedev18';
    $port = '35660';
    ini_set('display_errors', '1');
    error_reporting(E_ALL);
    $_['secure'] = true;
} elseif (in_array($_SERVER['HTTP_HOST'], ['republike.io', 'www.republike.io'])) {
    //PROD
    $urlSite = 'https://' . $_SERVER['HTTP_HOST'];
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'aj264163-001.privatesql';
    $db_name = 'republike18';
    $port = '35660';

    $_['secure'] = true;
} else {
    $urlSite = 'http://' . $_SERVER['HTTP_HOST'];
    $db_username = 'republike00118';
    $db_password = '94K2pkFN5qdN';
    $servername = 'db';
    $db_name = 'republike18';
    $port = '3306';
    ini_set('display_errors', '1');
    error_reporting(E_ALL);
    $_['secure'] = false;
}
try {
    $dbh = new PDO('mysql:host=' . $servername . ';port=' . $port . ';dbname=' . $db_name, $db_username, $db_password);
    $dbh->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->exec("SET NAMES utf8");
} catch (PDOException $e) {
    die('Une erreur MySQL est arrivée: ' . $e->getMessage());
}
