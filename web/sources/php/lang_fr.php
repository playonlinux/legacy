<?php
// Locale
$_['code']						= 'fr-FR';
$_['code1']						= 'fr';
$_['codeBis']					= 'fr_FR';
$_['direction']					= 'ltr';
$_['date_format_short']			= 'd/m/Y';
$_['date_format_short2']		= '%d %M %Y';
$_['date_format_long']			= 'l j F Y';
$_['time_format']				= 'H:i:s';

$_['play'] = 'JOUER';

//lien
$_['url_bienvenue']				= 'bienvenue';
$_['url_battle']				= 'bataille';
$_['url_contenu']				= 'contenu';
$_['url_theme']					= 'theme';
$_['url_user']					= 'citoyen';
$_['url_compte']				= 'mon-compte';
$_['url_search']				= 'recherche';
$_['url_search_amis']			= 'recherche-amis';
$_['views']['settings'] = 'parametres';
$_['views']['trophies'] = 'trophees';
// $_['views']['friend-request'] = 'demande-ami';
//Header
$_['menu_inviter']				= 'Inviter des amis';
$_['menu_notif']				= 'Notifications';
$_['menu_hall']					= 'Hall of fame';

$_['lang1']						= 'Français';
$_['lang2']						= 'Anglais';

$_['menu_condition_1']			= 'Comment ça marche';
$_['url_condition_1']			= 'lois-de-la-republike';
$_['menu_condition_2']			= 'FAQ';
$_['menu_condition_3']			= 'Mentions légales';
$_['menu_condition_4']			= 'CGU';
$_['menu_condition_5']			= 'CGV';
$_['menu_condition_6']			= 'Déconnexion';
$_['footer']['privacy']			= 'Confidentialité et Cookies';
$_['url_terms_of_service'] 	= 'conditions-generales';
$_['url_privacy'] 	= 'confidentialite-et-cookies';

$_['search']					= 'Rechercher par titre, mots clés, thèmes, utilisateur ...';

//Footer
$_['baseline']					= 'Forme d\'organisation politique dans laquelle les détenteurs du pouvoir l\'exercent en vertu d\'un mandat conféré par le corps social.';


//Accueil
$_['bigtitre_index']			= 'Prouvez que vous détenez<br><b>les meilleurs contenus du monde</b>';
$_['titre_index']				= 'Batailles de contenus, tous thèmes, tous supports';
$_['ss_index']					= 'Batailles en cours';


$_['bloc_1']					= 'CHERCHEZ';
$_['txt_bloc_1']				= 'Trouvez des contenus exceptionnels';
$_['bloc_2']					= 'DEFIEZ';
$_['txt_bloc_2']				= 'Confrontez-les <br>à ceux des autres';
$_['bloc_3']					= 'VOTEZ';
$_['txt_bloc_3']				= 'Faites gagner <br>les meilleurs';
$_['bloc_4']					= 'PROGRESSEZ';
$_['txt_bloc_4']				= 'Gagnez des batailles <br>et passez les niveaux';

$_['bt_cat']					= 'Batailles suggérées';
$_['bt_user']					= 'Batailles les plus populaires';
$_['bt_amis']					= 'Batailles d\'amis';
$_['bt_date']					= 'Batailles les plus récentes';

$_['bt_decouvrez']				= 'Lois de la Republike';

$_['discover_my_battles'] = 'DECOUVRIR MES BATAILLES';
$_['by'] = 'PAR';


//BATAILLES / CONTENUS
$_['nb_participants']			= 'Nombre de citoyens';
$_['nb_vues']					= 'Nombre de vues';
$_['partager']					= 'Partager';
$_['partager_amis']				= 'Partager avec vos amis';
$_['modif_battle']				= 'Modifier cette bataille';
$_['battle_close']				= 'Bataille terminée';

$_['modif_contenu']				= 'Modifier ce contenu';
$_['supp_contenu']				= 'Supprimer ce contenu';
$_['like_contenu']				= 'Liker le contenu';
$_['fav_contenu']				= 'Ajouter aux favoris';
$_['signaler_contenu']			= 'Signaler ce contenu';
$_['signaler_comment']			= 'Signaler ce commentaire';

$_['page_battle']				= 'Bataille :';

$_['nb_contenus']				= 'Nombre de contenus';
$_['nb_likes']					= 'Nombre de likes';

$_['return_battle']				= 'Retour à la bataille';
$_['comment']					= 'Commentaires';
$_['msg_comment']				= 'Laissez votre commentaire ici (140 caractères max)*';
$_['bt_comment_envoyer']		= 'Envoyer';
$_['bt_entry']					= 'Participer';


//MODIF CONTENUS
$_['edit_contenu']				= 'Modifiez ou entrez votre titre, #motscles (Optionnel)';
$_['edit_titre']				= 'Entrez votre titre, #motscles (Optionnel)';


//MODIF BATTLE
$_['edit_catbattle']			= 'Modifiez le thème de votre bataille*';
$_['edit_titrebattle']			= 'Modifiez le nom de votre bataille';
$_['edit_descbattle']			= 'Modifiez le descriptif de votre bataille';

//DELETE POST
$_['supp_contenu1']				= 'Souhaitez-vous supprimer ce contenu ';
$_['supp_contenu2']				= 'Attention, vous perdez vos likes gagnés !';

$_['bt_edit']					= 'Valider';
$_['bt_confirm']				= 'Confirmer';

//popup bataille
$_['bt_battle']					= '<b>Lancer une bataille</b>';
$_['champ_battle_1']			= 'Insérez un contenu*';
$_['champ_battle_2']			= 'Choisissez votre thème*';
$_['champ_battle_3']			= 'Donnez un nom à votre bataille (max. 50 caractères)*';
$_['champ_battle_4']			= 'Donnez un titre à votre contenu (Optionnel)';
$_['champ_battle_5']			= 'Décrivez votre bataille (Optionnel)';

$_['champ_type1']				= 'Image';
$_['champ_type2']				= 'Son';
$_['champ_type3']				= 'Lien';
$_['placehoder_type3']			= 'Lien de la vidéo';
$_['champ_type4']				= 'Texte';
$_['placehoder_type4']			= 'Ecrivez votre texte ici';
$_['placehoder2_type4']			= 'Lien (Optionnel)';

$_['bt_battle_lancer']			= 'Lancer votre bataille';

$_['titre_battle_inviter']		= 'Inviter des amis (Optionnel)';
$_['ami_battle_email']			= 'E-mail ami';
$_['bt_battle_envoyer']			= 'Envoyer';

$_['bt_battle_non']				= 'Non, pas maintenant';

//popup contenu
$_['bt_contenu']				= 'PARTICIPER';
$_['post_contenu']				= 'POSTEZ VOTRE CONTENU';

$_['pop_post_1']				= 'Uploader une image';
$_['pop_post_2']				= 'Ajouter un lien video';
$_['pop_post_3']				= 'Uploader un mp3';
$_['pop_post_4']				= 'Ajouter un texte';

$_['champs_post_1']				= 'Lien de la vidéo* : ';
$_['champs_post_2']				= 'Ecrivez votre texte ici*';
$_['champs_post_3']				= 'Entrez votre titre, #motscles (Optionnel)';

$_['bt_publier']				= 'Publier';

//Connexion

$_['creer_compte']				= 'CRÉER UN COMPTE';
$_['compte_nom']				= 'Nom de famille*';
$_['compte_prenom']				= 'Prénom*';
$_['compte_email']				= 'Email*';
$_['compte_mdp']				= 'Mot de passe* : min. 8 caractères';
$_['bt_inscription']			= 'Inscription';
$_['bt_connecter']				= 'Connectez-vous';
$_['un_compte']					= 'Vous avez un compte ?';

$_['compte_felicitation']		= 'BRAVO, VOUS ÊTES EN LICE ET VOUS AVEZ GAGNÉ 200 Likes !';
$_['compte_inviter']			= 'Invitez vos amis à jouer avec vous et obtenez jusqu\'à 200 LikeCoins par invité.';


$_['me_connecter']				= 'ME CONNECTER';
$_['connection_email']			= 'Email*';
$_['connection_mdp']			= 'Mot de passe*';
$_['bt_souvenir']				= 'Se souvenir de moi';
$_['se_connecter']				= 'Se connecter';
$_['mdp_oublie']				= 'Mot de passe oublié ?';
$_['bt_creer_compte']			= 'Inscrivez-vous';
$_['pas_compte']				= 'Vous n\'avez pas de compte ?';
$_['login_facebook']			= 'Connexion avec Facebook';


$_['mdp_oublier']				= 'MOT DE PASSE OUBLIÉ';
$_['mes_mdp']					= 'Un mail va vous être envoyé pour réinitialiser votre mot de passe';
$_['mdp_email']					= 'Email*';
$_['bt_envoyer']				= 'Envoyer';
$_['bt_annuler']				= 'Annuler';
$_['categories']['select'] = 'Sélectionnez vos thèmes favoris (5 minimum)';
$_['user']['are-you'] = 'Êtes-vous';

//PARTAGE BATAILLE
$_['partage_battle']			= 'Partager cette bataille';
$_['ou_envoyer']				= 'Ou l\'envoyer à';
$_['pas_amis']					= 'Vous n\'avez encore aucun ami !';
$_['bt_partager']				= 'PARTAGER';
$_['envoyer_a']					= 'Envoyer à';

$_['bravo_partage']				= 'BRAVO, VOS PARTAGES ONT BIEN ÉTÉ ENVOYÉS !';


//PARTAGE CONTENU
$_['partage_contenu']			= 'Partager ce contenu';


//INVITATION AMI
$_['inviter_amis']				= 'INVITER DES AMIS À REJOINDRE REPUBLIKE';
$_['bt_inviter_amis']			= 'Inviter des amis';
$_['ou']						= 'ou';
$_['bt_trouver_amis']			= 'Trouver des amis';

$_['bravo_invitation']			= 'BRAVO, VOS INVITATIONS SONT BIEN PARTIES !';


//CONVERTIR LikeCoins
$_['convertir_likecoins']		= 'Convertissez vos LikeCoins pour pouvoir jouer';
$_['bt_convertir_maintenant']	= 'Convertir maintenant';


//SIGNALER CONTENU
$_['titre_signaler']			= 'Signaler ce contenu';
$_['type_signaler1']			= 'Contenu hors sujet';
$_['type_signaler2']			= 'Contenu protégé';
$_['type_signaler3']			= 'Autre';
$_['msg_signaler']				= 'Laissez votre message ici (140 caractères max)*';
$_['bt_signaler']				= 'Signaler';

$_['bravo_signalement']			= 'MERCI. VOTRE SIGNALEMENT A BIEN ÉTÉ ENVOYÉ !';



//THEMES
$_['titre_theme']				= 'Thème :';
$_['result_theme']				= 'Résultat(s) pour le thème :';

$_['bt_batailles']				= 'Batailles';
$_['bt_contenus']				= 'Contenus';

$_['bt_aleatoire']				= 'Trier aléatoirement';
$_['bt_nblikes']				= 'Trier par nombre de like';


//RECHERCHE
$_['titre_search']				= 'Recherche';
$_['result_search']				= 'Résultat(s) pour :';

$_['bt_participants']			= 'Citoyens';

//RECHERCHE AMIS
$_['titre_search_friends']		= 'Trouver des amis';
$_['input_search_friends']		= 'Rechercher par prénom, nom ...';
$_['demande_friend_send']		= 'VOTRE DEMANDE A BIEN ÉTÉ ENVOYÉE !';
$_['demande_friend_delete']		= 'L\'INVITATION A BIEN ÉTÉ ANNULÉE !';
$_['demande_friend_yes']		= 'FÉLICITATIONS !';
$_['demande_friend_no']		= 'DOMMAGE !';

//MON COMPTE // USER
$_['titre_compte']				= 'Mon compte';

$_['nb_points']					= 'Nombres de points';
$_['nb_likes2']					= 'Nombre de Likes';
$_['nb_likecoins']				= 'Nombre de LikeCoins';

$_['bt_convertir']				= 'Convertir';
$_['compte_convertir']			= 'Convertir des LikeCoins en Likes';

$_['popup_convertir']			= 'CONVERTIR LikeCoins EN Likes';
$_['finaliser_convertir']		= 'Finalisez votre inscription pour convertir vos LikeCoins et pouvoir jouer.';
$_['bt_finaliser']				= 'Finaliser maintenant';
$_['bravo_convertir']			= 'BRAVO, VOUS AVEZ DES NOUVEAUX Likes !';

$_['menu_compte1']				= 'ma communauté';
$_['menu_compte2']				= 'mon activité';
$_['menu_compte3']				= 'Paramètres';

$_['ssmenu_compte11']			= 'mes amis';
$_['ssmenu_compte12']			= 'mes invitations en cours';
$_['ssmenu_compte13']			= 'Inviter des amis';

$_['ssmenu_compte21']			= 'Profil';
$_['ssmenu_compte22']			= 'Thèmes';
$_['ssmenu_compte23']			= 'Notifications';

$_['ssmenu_compte31']			= 'Batailles';
$_['ssmenu_compte32']			= 'Contenus';
$_['ssmenu_compte33']			= 'Trophées';
$_['ssmenu_compte34']			= 'Favoris';

$_['bt_deconnexion']			= 'Me déconnecter';

$_['menu_left_compte1']			= 'Inscrit depuis le ';
$_['menu_left_compte2']			= 'Amis';
$_['menu_left_compte3']			= 'Invitations';


//USER
$_['menu_user1']				= 'ses amis';
$_['menu_user2']				= 'ses thèmes';
$_['menu_user3']				= 'ses batailles';
$_['menu_user4']				= 'ses contenus';
$_['menu_user5']				= 'ses trophées';

$_['ami_depuis']				= 'Ami depuis le ';

$_['bt_devenir']				= 'Devenir ami';
$_['bt_neplusetreami']			= 'Ne plus être ami';
$_['bt_annulerami']				= 'Annuler la demande';
$_['voir_profil']				= 'Voir son profil';

//FORM PROFIL
$_['titre_form']				= 'MON PROFIL';
$_['form_sexe1']				= 'Femme';
$_['form_sexe2']				= 'Homme';
$_['form_sexe3']				= 'Autre';

$_['form_nom']					= 'Nom';
$_['form_prenom']				= 'Prénom';

$_['form_photo']				= 'Photo';
$_['form_photo1']				= 'Poids max : 200ko - JPG, GIF, PNG, BMP';

$_['form_email']				= 'E-mail';
$_['form_bio']					= 'Bio';
$_['form_naissance']			= 'Naissance';
$_['form_pays']					= 'Pays';

$_['titre_form_mdp']			= 'MODIFIER MON MOT DE PASSE';
$_['form_mdp1']					= 'Mot de passe (minimum 8 caratères)';
$_['form_mdp2']					= 'Confirmation mot de passe';

$_['titre_form_supp']			= 'SUPPRIMER MON COMPTE';
$_['sstitre_form_supp']			= 'Si vous supprimez votre compte, personne ne verra vos batailles, contenus ou autre information sur votre profil Republike.';
$_['sstitre_form_supp2']		= 'Attention, vous êtes sur le point de supprimer votre compte. Souhaitez-vous vraiment supprimer votre compte sur Republike ?';
$_['bt_form_supp']				= 'Supprimer';
$_['bt_form_supp2']				= 'oui, je confirme';
$_['mes_form_supp']				= 'Votre compte a été supprimé :-( Dans quelques secondes, vous allez être redirigé vers la page d\'accueil.';

$_['upload']['avatar'] = 'Importer un avatar';
$_['upload']['image'] = 'Importer une image';
$_['upload']['sound'] = 'Importer un son';

//FORM CAT
$_['titre_form_cat']			= 'MES THÈMES';

//PAGE 404
$_['titre_404']					= 'OUPS ! PAGE INTROUVABLE';
$_['sstitre_404']				= 'La page que vous recherchez semble introuvable. Si le problème persiste, signalez le à contact@republike.io 
ou postez une nouvelle bataille sur les sites les plus buggés et vous pourriez gagner une surprise ;-) ';

//PAGE HALL OF FAME
$_['bt_trophees']				= 'Ses trophées';
$_['nb_or']						= 'Nombres d\'Or';
$_['nb_argent']					= 'Nombres d\'Argent';
$_['nb_bronze']					= 'Nombres de Bronze';

//COMMENT CA MARCHE
$_['titre_comment']				= 'Comment ça marche ?';
$_['sstitre_comment']			= 'Sur Republike vous pouvez';
$_['sstitre_comment_meta']		= 'Lois de la Republike';

$_['titre_comment']				= 'TITRE';
$_['points_comment']			= 'POINTS';
$_['vendre_comment']			= 'A VENDRE';
$_['prix_comment']				= 'PRIX';
$_['suite_comment']				= 'La suite … à venir';

$_['bloc_comment1']				= 'Vous prétendez détenir des merveilles (image, photo, vidéo, musiques, etc…) sur un sujet ?<br>
								  Vous voulez montrer au monde que vous êtes le meilleur ou simplement partager passions et trouvailles ?';

$_['how-it-works']['title'] = '<b>Sur <b class="majR">R</b>epublike vous pouvez</b>';
$_['how-it-works']['launch'] = '<b>Lancer des batailles de contenus</b>'; //<br><small>Sur vos sujets favoris</small>';
$_['how-it-works']['publish'] = '<b>Poster vos meilleurs contenus</b>'; //<br><small>Pour gagner des Batailles</small>';
$_['how-it-works']['like'] = '<b>Liker vos contenus favoris</b>'; //<br><small>Pour les défendre</small>';
$_['how-it-works']['invite'] = '<b>Inviter fans et amis</b>'; //<br><small>&Agrave; participer et/ou à vous soutenir</small>';
$_['how-it-works']['share'] = '<b>Les batailles sont ouvertes au monde entier</b>'; //<br><small>Tout le monde peut voir vos contenus, les évaluer et les défier</small>';
$_['how-it-works']['laws-of-republike'] = '<b>LOIS DE LA <b class="majR">R</b>EPUBLIKE</b>';

$_['how-it-works']['quest']['title'] = 'LA QUÊTE';
$_['how-it-works']['quest']['content'] = 'Devenez <b>EMPEREUR</b> de <b class="majR">R</b>epublike et prouvez que vous êtes le <b>Maître des Contenus</b>. Pour atteindre ce <b>but suprême</b>, vous allez devoir <a href="#" class="how-it-works-link">traverser les niveaux</a> de <b class="majR">R</b>epublike en gagnants des points.';

$_['how-it-works']['points']['title'] = 'GAGNER DES POINTS';
$_['how-it-works']['points']['content'] = 'Participez à des Batailles en postant vos <b>meilleurs contenus</b>. Dès qu’un contenu atteint <b>100 £ikes</b>, la Bataille est terminée. Les 3 meilleurs contenus recoivent des médailles (or, argent, bronze) et <b>gagnent des points</b>';

$_['how-it-works']['currency']['title'] = 'MONNAIE';
$_['how-it-works']['currency']['content'] = 'La <b>monnaie</b>  de la <b class="majR">R</b>epublike est le <b>£ikeCoin</b>. Pour continuer à jouer vous devez <a href="#" class"how-it-works-link">cumuler des £ikeCoins</a> pour pouvoir acheter des <b>£ikes</b> et continuer de voter.';

$_['how-it-works']['votes']['title'] = 'VOTES';
$_['how-it-works']['votes']['content'] = 'Vous pouvez voter pour un même contenu <b>jusqu\'à 10 fois</b>. Liker l\'un de vos contenus <b>coûtera 100 £ikeCoins par like</b>. Liker le contenu d\'un concitoyen <b>rapporte 10 £ikeCoins par like</b>. <b>Acheter 1 £ike coûte 10 £ikeCoins</b>.';

$_['how-it-works']['accumulate']['title'] = '<b><span style="color:#fe585c">Cumuler</span><br> des £ikeCoins</b>';

$_['how-it-works']['accumulate']['10']['title'] = '<b>10<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['10']['content'] = 'Un citoyen like un de vos contenus<sup>*</sup>. Vous liker le contenu d\'un autre citoyen<sup>*</sup>. <small><i><sup>*</sup> 10 £ikeCoins par like</i></small>';

$_['how-it-works']['accumulate']['50']['title'] = '<b>50<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['50']['content'] = 'Partager des contenus ou des Batailles en dehors de la  <b class="majR">R</b>epublike . Reconnexion après 12 heures';

$_['how-it-works']['accumulate']['100']['title'] = '<b>100<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['100']['content'] = 'Pour chaque nouveau citoyen postant un premier contenu sur une Bataille que vous avez lancée';

$_['how-it-works']['accumulate']['200']['title'] = '<b>200<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['200']['content'] = 'Inviter un ami à rejoindre la <b class="majR">R</b>epublike (Invitation acceptée)';

$_['how-it-works']['levels']['title'] = '<span class="title">LES NIVEAUX</span><small> de la </small> <b class="majR">R</b>epublike';

$_['mailing']['hello'] = function ($prenom) {
	return 'Salut' . (!empty($prenom)) ? ' ' . $prenom : '';
};

$_['mailing']['user']['signup']['body']  = 'Il ne vous reste qu\'à cliquer sur le lien pour valider votre inscription. <br/><br />A la clé, <b>300 LikeCoins</b> !
<br><br><em>A défaut, votre compte sera supprimé dans 7 jours.</em><br/><br/>';
$_['mailing']['user']['signup']['subject'] = 'Finaliser votre inscription';

$_['mailing']['user']['invite']['body'] = function ($prenom) {
	return $prenom . ' souhaite vous inviter à rejoindre Republike.<br><br>La plateforme où poster vos meilleurs contenus et les confrontez aux autres.';
};
$_['mailing']['user']['invite']['subject'] = function ($prenom) {
	return $prenom . ' souhaite vous inviter sur Republike';
};


$_['mailing']['user']['battle']['body'] = function ($prenom) {
	return $prenom . ' vous recommande cette bataille !';
};
$_['mailing']['user']['battle']['subject'] = function ($prenom) {
	return $prenom . ' vous recommande cette bataille !';
};
$_['mailing']['user']['post']['body'] = function ($prenom) {
	return $prenom . ' vous recommande ce contenu !';
};
$_['mailing']['user']['post']['subject'] = function ($prenom) {
	return $prenom . ' vous recommande ce contenu !';
};

$_['mailing']['user']['invite']['discover'] = 'DÉCOUVRIR';

$_['mailing']['user']['friend-request']['body'] = function ($prenom) {
	return $prenom . ' vous a envoyé une demande d\'ami.<br />Découvrez son profil.';
};
$_['mailing']['user']['friend-request']['subject'] = function ($prenom) {
	return $prenom . ' vous a envoyé une invitation';
};

$_['mailing']['battle']['lanch']['body'] = function ($prenom) {
	return 'Votre ami ' . $prenom . ' lance une nouvelle bataille';
};
$_['mailing']['battle']['lanch']['subject'] = function ($prenom) {
	return $prenom . ' lance une bataille';
};


$_['mailing']['content']['post']['body'] = 'Un nouveau contenu a été posté sur votre bataille';
$_['mailing']['content']['post']['subject'] = 'Bravo, vos batailles plaisent !';


$_['mailing']['content']['overtook']['body'] = function ($message, $title, $isFriend) {
	return 'Attention ! '  . ($isFriend) ? 'Votre ami ' : '' . $message . ' vient de vous passer devant sur la bataille : ' . $title . ' !';
};
$_['mailing']['content']['overtook']['subject'] = 'Votre position est menacée';

$_['mailing']['battle']['winners']['first']['subject'] = function ($battleName) {
	return 'Vous avez gagné la bataille ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['first']['body'] = function ($battleName) {
	return 'Bravo, vous gagnez la bataille <b>' . $battleName . '</b> et vous remportez 9 points !';
};
$_['mailing']['battle']['winners']['second']['subject'] = function ($battleName) {
	return 'Vous terminez deuxième de la bataille ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['second']['body'] = function ($battleName) {
	return 'Bravo, vous terminez deuxième de la bataille <b>' . $battleName . '</b> et vous remportez 3 points !';
};
$_['mailing']['battle']['winners']['thrid']['subject'] = function ($battleName) {
	return 'Vous terminez troisième de la bataille ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['thrid']['body'] = function ($battleName) {
	return 'Bravo, vous terminez troisième de la bataille <b>' . $battleName . '</b> et vous remportez 1 point !';
};


$_['mailing']['battle']['suggested']['subject'] = 'et autres batailles qui pourraient vous intéresser...';
$_['mailing']['battle']['suggested']['body'] = 'Ces batailles peuvent vous intéresser : ';

$_['mailing']['footer'] = function ($urlSite) {
	$message = 'Cet email est automatisé. Pour toutes questions, écrivez-nous à <a href="mailto:contact@republike.io">contact@republike.io.</a><br />';
	$message .= (!empty($urlSite)) ? '<b>Pour paramètrer vos alertes, rendez-vous <a href="' . $urlSite . '/fr/mon-compte/parametres">ici</a></b><br /><br />' : '';
	$message .= '© 2019 REPUBLIKE';
	return $message;
};


$_['bloc_comment2']				= 'Sur <b class="majR">R</b>epublike, vous pouvez :<br><br />
								<b>Lancer des Batailles</b> de contenus sur vos sujets favoris<br>
								<b>Publier vos contenus</b> sur n\'importe quelle Bataille<br>
								<b>Liker</b> vos contenus préférés pour les pousser ou les défendre<br>
								<b>Inviter</b> fans et amis à participer ou à vous soutenir<br />
								<b>Les batailles sont ouvertes au monde entier ; chacun peut voir vos contenus, les évaluer et les défier</b>';

$_['bloc_comment3']				= '<b class="rose">Loi 1 :</b> Votre objectif est de gagner des batailles de contenus pour franchir les niveaux et devenir Imperator de la <span class="majR">R</span>epublike<br />
								<b class="rose">Loi 2 :</b> Dès qu\'un contenu obtient <b>100 Likes</b>, la bataille est terminée ; les trois contenus les plus likés reçoivent des médailles (or, argent, bronze)<br /> 
								<b class="rose">Loi 3 :</b> Les médailles vous attribuent des points qui vous permettent de franchir les niveaux <b><u>Titres et prestige</u></b><br />
								<b class="rose">Loi 4 :</b> La monnaie de la <span class="majR">R</span>epublike est le LikeCoins <b><u>Comment gagner des LikeCoins</u></b><br />
								<b class="rose">Loi 5 :</b> Vous pouvez liker vos contenus préférés, ceux des autres, mais les vôtres aussi ! Cependant ATTENTION : </b><br />
								I. Votre stock de Likes est limité, <br />
								II. Vous pouvez liker un même contenu jusqu\'à 10 fois,<br />
								III. Liker un de vos contenus vous coûte (100 LikeCoins),<br />
								IV.	Alors que liker le contenu d\'un autre vous rapporte (10 LikeCoins)<br />
								<b class="rose">Loi 6 :</b> Accumulez des LikeCoins, ils sont précieux : ils vous permettent de reconstituer votre stock de Likes pour continuer à jouer 
								et de <b><u>Flamber sur Republike </u></b>';

$_['bloc_comment4']				= 'Titres & prestige';
$_['bloc_comment5']				= 'Chaque médaille rapporte des points, <br>
								<i class="repu-laurels bronze"></i><small>(bronze)</small> = 1 point ; <i class="repu-laurels argent"></i><small>(argent)</small> = 3 points ; <i class="repu-laurels or"></i><small>(or)</small> = 9 points, <br>
								qui s\'accumulent pour gravir les échelons de la RépubLike :';

$_['bloc_comment6']				= 'Flamber sur Republike';
$_['bloc_comment7']				= 'Comment gagner des LikeCoins';
$_['bloc_comment8']				= '• Vous vous inscrivez, vous gagnez <b>200 Likes</b> <br>
								• Vous validez votre profil, vous gagnez <b>300 LikeCoins</b><br>
								• Vous lancez une Bataille, chaque nouveau participant vous fait gagner <b>100 LikeCoins</b><br>
								• Vous likez le contenu d\'un autre, vous gagnez <b>10 LikeCoins (par Like)</b><br>
								• Un participant (autre que vous) like un de vos contenus, vous gagnez <b>10 LikeCoins (par Like obtenu)</b><br>
								• Vous partagez un contenu ou une Bataille hors de Republike, vous gagnez <b>50 LikeCoins</b><br />
								• Vous vous reconnectez après 12 heures, vous gagnez <b>50 LikeCoins</b><br>
								• Vous parrainez un ami sur Republike, vous gagnez <b>200 LikeCoins</b>';




// PASSWORD

$_['password']['empty_fields'] = 'Merci de remplir tous les champs !';
$_['password']['pass_not_equals'] = 'Mots de passe non identiques !';
$_['password']['user_not_found'] = 'Aucun utilisateur trouvé';

$_['my-account']['notif-email'] = 'MES NOTIFICATIONS EMAIL';
