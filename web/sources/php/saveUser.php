<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$speudo = null; //$_POST['speudo'];
$mail = $_POST['email'];
$password = $_POST['password'];

if (empty($password) || empty($mail)) {
	echo 'non';
	exit;
}

$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE email = :mail limit 0,1');
$sth->bindParam(':mail', $mail, PDO::PARAM_STR);
$rs = $sth->execute();

if ($sth->rowCount()) {
	echo 'identique';
	exit;
}

$password = password_hash($password, PASSWORD_DEFAULT);
$url =  trim(strtolower(str_replace(" ", "-", no_special_character($nom . '.' . $prenom))));

$insertReq = $dbh->prepare("INSERT INTO `bl_user`( `code`,`nom`,`prenom`, `speudo`, `email`, `password`, `photo`, `url`) 
		VALUES (:code, :nom, :prenom , NULL, :mail, :password, 'images/avatars/pirate.jpg', :url)");
$insertReq->bindParam(':code', $code, PDO::PARAM_STR);
$insertReq->bindParam(':nom', $nom, PDO::PARAM_STR);
$insertReq->bindParam(':prenom', $prenom, PDO::PARAM_STR);
// $insertReq->bindParam(':speudo', $speudo, PDO::PARAM_STR);
$insertReq->bindParam(':mail', $mail, PDO::PARAM_STR);
$insertReq->bindParam(':password', $password, PDO::PARAM_STR);
$insertReq->bindParam(':url', $url, PDO::PARAM_STR);
$insertReq->execute();
$id_user = $dbh->lastInsertId();

save_log($id_user, '4', $dbh);

//insertion traceur
save_connexion($id_user, $dbh);

//creation des alerts
save_alert($id_user, $dbh);

//UPDATE token connexion
$tokenUser = password_hash($prenom . date('YmdHHis'), PASSWORD_DEFAULT);
$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= :token WHERE id = :id_user");
$updateReq->bindParam(':token', $tokenUser, PDO::PARAM_STR);
$updateReq->bindParam(':id_user', $id_user, PDO::PARAM_STR);
$updateReq->execute();


/*** likes ***/
$likes = 200;
$updateReq = $dbh->prepare("UPDATE `bl_user` SET `likes`= likes+$likes WHERE id = :id");
$updateReq->bindParam(':id', $id_user, PDO::PARAM_STR);
$updateReq->execute();
/*** likes ***/


//mail confirmation
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n";
$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

//MAIL USER 1
/* Construction du message */
$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="' . $urlSite . '" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
										
											<p style="color:#777777;"' . $_['mailing']['hello']($prenom) . ',<br><br>
											' . $_['mailing']['user']['signup']['body']  . '</p>
											
											<table width="250" border="0" cellspacing="0" cellpadding="0" style=" background-color:#ff0000; color:#fff;">
												  <tr align="center" height="40">
													<td align="center" height="40">
														<a target="_blank" href="' . $urlSite . '/confirme.php?code=' . $_SESSION['code'] . '&id=' . $id_user . '&token=' . $password . '"
														style="text-decoration:none; color:#fff; font-size:14px; line-height:22px;"> 
															300 LIKECOINS
														</a>
													</td>
												</tr>
												</table>
												<br />
											
											<p style="font-size:10px; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																								
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';

$to = $mail;
mb_internal_encoding('UTF-8');
$sujet = mb_encode_mimeheader($_['mailing']['user']['signup']['subject']);

try {

	mail($to, $sujet, $msg, $headers);
} catch (Exception $e) { }

$_SESSION['securite'] = $password;
$_SESSION['prenom'] = $prenom;
$_SESSION['photo'] = 'images/avatars/pirate.jpg';
$_SESSION['id_user'] = $id_user;
$_SESSION['likes'] = 200;
$_SESSION['likeCoins'] = 0;

header('Content-Type: application/json');
echo json_encode(array($id_user, $password));
