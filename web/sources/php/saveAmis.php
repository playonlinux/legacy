<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (empty($_SESSION['securite'])) {
	echo 'non';
	exit;
}



$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);

$userResult->execute();

if (!$userResult->rowCount()) {
	echo '2-non';
	exit;
}

$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
$user = $row_userResult->id;

$user = intval($_POST['user']);
$friend = intval($_POST['friend']);
$type = $_POST['type'];

$userResult = $dbh->prepare("SELECT * FROM `bl_user_friend` WHERE `user` = :user and `friend` = :friend limit 0,1");
$userResult->bindParam(':user', $user, PDO::PARAM_STR);
$userResult->bindParam(':friend', $friend, PDO::PARAM_STR);
$rs = $userResult->execute();

if ($userResult->rowCount() == 0) {

	if ($type == 'yes') {

		$insertReq = $dbh->prepare("INSERT INTO `bl_user_friend` (`user`, `friend`) 
				VALUES ( :user, :friend)");
		$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
		$insertReq->bindParam(':friend', $friend, PDO::PARAM_STR);

		$insertReq->execute();
		$id_post = $dbh->lastInsertId();

		//Mise à jour de la demande				
		$updateReq = $dbh->prepare("UPDATE `bl_user_demande_friend` SET `statut`='2' WHERE `user` =:friend and `qui` =:user ");
		$updateReq->bindParam(':user', $user, PDO::PARAM_STR);
		$updateReq->bindParam(':friend', $friend, PDO::PARAM_STR);
		$updateReq->execute();

		//Mail demande d'amis
		if ($user != $friend && $type == 'yes') {

			//celui qui accepte
			$prenom = prenom_user($user, $dbh);
			$speudo = speudo_user($user, $dbh);
			$email = email_user($user, $dbh);
			$photo = photo_user($user, $dbh);

			$points = points_user($user, $dbh);
			$color = grade_user_color($points, $dbh);
			$grade = grade_user_between($points, $code, $dbh);

			//celui qui reçoit l'acceptation
			$prenom_ami = prenom_user($friend, $dbh);
			$email_ami = email_user($friend, $dbh);

			//email nouveau contenu si par detenteur de la battle
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\n";
			$headers .= 'Bcc: dev@mkael-group.com' . "\r\n";

			//MAIL USER 1
			/* Construction du message */
			$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
				<tbody>
					<tr>
						<td height="73" align="center" valign="top">
							<table width="4é0" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
								<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
										<tbody>
										<tr>
										<td height="58" valign="middle" style="padding:9px">
											<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr align="center">
													<td valign="middle" style="padding:0px 9px">
													<a href="#" target="_blank">
														<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" 
														width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  
														style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
													</a>
													</td>
												</tr>
											</tbody>
											</table>
										</td>
										</tr>
										</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
									<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											  <tr align="center">
												<td width="420" height="214" align="center">
												
													<p style="color:#777777;">Bonjour ' . $prenom_ami . ',<br>
													<br>
													' . $prenom . ' a accepté votre invitation, <br />découvrez son profil.
													<br></p>
													
													<table width="200" border="0" cellspacing="0" cellpadding="0" align="center" height="100">
													  <tr valign="middle">
														<td height="90" width="100" align="center">
														
															<a target="_blank" href="' . $urlSite . '/user.php?id=' . $user . '" style="text-decoration:none; border:none;"> 
																<img style="border-radius:50%;outline:none;color:#ffffff;text-decoration:none; border:2px solid ' . $color . ';" 
																src="' . $urlSite . '/' . $photo . '" width="70" border="0" />
															</a>
															
															<a target="_blank" href="' . $urlSite . '/user.php?id=' . $user . '" style="text-decoration:none; border:none;"> 
																<p style="color:#777777; font-size:13px;">' . $speudo . '<br />' . $grade . '</p>
															</a>
														</td>
														
													  </tr>
													  
												  </table><br />
	
													<p style="font-size:10px; color:#777777;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br />
													<b>Pour paramètrer vos alertes, rendez-vous dans "Mon compte / Paramètres".</b><br /><br />
													© 2019 REPUBLIKE</p>
																										
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
				</table>';

			$to = $email_ami;
			//$to='dev@mkael-group.com';
			mb_internal_encoding('UTF-8');
			$sujet = mb_encode_mimeheader($prenom . ' a accepté votre invitation');

			if ($to != '') {
				mail($to, $sujet, $msg, $headers);
				echo 'oui';
			} else {
				echo 'non';
			}
		}
	} else if ($type == 'no') {

		//Supp demande ami
		$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE `user` =:friend and `qui` =:user ");
		$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
		$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
		$deleteReq->execute();
	}
}

if ($type == 'annuler') {

	//Supp demande ami
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE `user` =:user and `qui` =:friend ");
	$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
	$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
	$deleteReq->execute();

	echo 'oui';
} else if ($type == 'supp') {


	//Supp ami	
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_friend` WHERE `user` =:user and `friend` =:friend ");
	$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
	$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
	$deleteReq->execute();

	//Supp ami	
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_friend` WHERE `user` =:friend and `friend` =:user ");
	$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
	$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
	$deleteReq->execute();

	//Supp demande ami
	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE `user` =:friend and `qui` =:user ");
	$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
	$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
	$deleteReq->execute();

	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_demande_friend` WHERE `user` =:user and `qui` =:friend ");
	$deleteReq->bindParam(':user', $user, PDO::PARAM_STR);
	$deleteReq->bindParam(':friend', $friend, PDO::PARAM_STR);
	$deleteReq->execute();

	echo 'oui';
}
