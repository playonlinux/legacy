<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();
if ($_GET['type'] == 'battle' && empty($_POST['id_battle'])) {
	echo 'non';
	exit;
}
if ($_GET['type'] == 'post' && empty($_POST['token_post'])) {
	echo 'non';
	exit;
}

if (empty($_SESSION['securite'])) {
	echo 'non';
	exit;
}

$emails = [];
for ($i = 1; $i < 5; $i++) {
	if (!empty($_POST['email' . $i])) {
		$emails[] = $_POST['email' . $i];
	}
}

if (!count($emails)) {
	echo 'non';
	exit;
}

$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
$userResult->execute();

if (!$userResult->rowCount()) {
	echo 'non';
	exit;
}
$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
$userId = $row_userResult->id;

function getMessage($email, $userId)
{

	global $dbh, $urlSite, $code, $_, $adr;

	$lang = code_user_by_email($email);

	if ($lang) {
		require './lang_' . $lang . '.php';
	} else {
		$lang = $code;
	}

	//celui qui fait la demande
	$prenom = prenom_user($userId, $dbh);
	$email = email_user($userId, $dbh);
	$photo = photo_user($userId, $dbh);
	$url = url_user($userId, $dbh);

	$object = '';

	if ($_GET['type'] == 'battle') {

		$id_battle = $_POST['id_battle'];
		$imagePartage = photo_battle_mail($id_battle, $dbh);
		$name_battle = name_battle($id_battle, $dbh);
		$url_battle = url_battle($id_battle, $dbh);

		save_log_ou($userId, '35', $name_battle, $dbh);

		$object = $_['mailing']['user']['battle']['subject']($prenom);

		/* Construction du message */
		$message = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
			<tbody>
				<tr>
					<td height="73" align="center" valign="top">
						<table width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
									<tr>
									<td height="58" valign="middle" style="padding:9px">
										<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr align="center">
												<td valign="middle" style="padding:0px 9px">
												<a href="#" target="_blank">
													<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" width="250" 
													src="' . $urlSite . '/images/logo-republilke-entier.png"  
													style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
												</a>
												</td>
											</tr>
										</tbody>
										</table>
									</td>
									</tr>
									</tbody>
								</table>
							</td>
							</tr>
							</tbody>
						</table>
						<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
								<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr align="center">
											<td width="420" height="214" align="center">
											<p style="color:#777777;">' . $_['mailing']['hello'](null) . ',<br>
											<br>
											' . $_['mailing']['user']['battle']['body']($prenom) . '
											<br>
											<br />
											<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_battle'] . '/' . $url_battle . '" style="text-decoration:none;"> 
												<img src="' . $imagePartage . '" width="250"> 
											</a><br/><br/>
											</p>
										  
											<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer'](null) . '</p>
																									
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>';
	} elseif ($_GET['type'] == 'post') {

		$token = $_POST['token_post'];
		$imagePartage = photo_post_mail($token, $dbh);
		$titre = titre_post_mail($token, $dbh);


		save_log_ou($userId, '34', $titre, $dbh);

		$object = $_['mailing']['user']['post']['subject']($prenom);

		/* Construction du message */
		$message = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
			<tbody>
				<tr>
					<td height="73" align="center" valign="top">
						<table width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
									<tr>
									<td height="58" valign="middle" style="padding:9px">
										<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr align="center">
												<td valign="middle" style="padding:0px 9px">
												<a href="#" target="_blank">
													<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" 
													width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  
													style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
												</a>
												</td>
											</tr>
										</tbody>
										</table>
									</td>
									</tr>
									</tbody>
								</table>
							</td>
							</tr>
							</tbody>
						</table>
						<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
								<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr align="center">
											<td width="420" height="214" align="center">
												<p style="color:#777777;">' . $_['mailing']['hello'](null) . ',<br>
												<br>
												' . $_['mailing']['user']['post']['body']($prenom) . '
												<br>
													<br />
													<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_contenu'] . '/' . $token . '" style="text-decoration:none;"> 
														<img src="' . $imagePartage . '"  width="250"> 
													</a><br /><br />
												</p>
												<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer'](null) . '</p>
																									
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>';
	} else {
		$object = $_['mailing']['user']['invite']['subject']($prenom);

		save_log_ou($userId, '33', $adr, $dbh);

		//MAIL USER 1
		/* Construction du message */
		$message = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
			<tbody>
				<tr>
					<td height="73" align="center" valign="top">
						<table width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
									<tr>
									<td height="58" valign="middle" style="padding:9px">
										<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr align="center">
												<td valign="middle" style="padding:0px 9px">
												<a href="#" target="_blank">
													<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
												</a>
												</td>
											</tr>
										</tbody>
										</table>
									</td>
									</tr>
									</tbody>
								</table>
							</td>
							</tr>
							</tbody>
						</table>
						<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
							<tbody>
							<tr>
								<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr align="center">
											<td width="420" height="214" align="center">
												<p style="color:#777777;">' . $_['mailing']['hello'](null) . ',<br>
												<br>
												' . $_['mailing']['user']['invite']['body']($prenom) . '
												<br></p>
												<table width="200" border="0" cellspacing="0" cellpadding="0" align="center" height="100">
													<tr valign="middle">
														<td height="80" width="100" align="center">
															<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_compte'] . '/' . $url . '" style="text-decoration:none; border:none;"> 
																<img style="border-radius:50%;outline:none;color:#ffffff;text-decoration:none" src="' . $urlSite . '/' . $photo . '" width="70" border="0" />
															</a>
														</td>
														
														<td height="25" align="center">
															<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_compte'] . '/' . $url . '" style="text-decoration:none; border:none;"> 
																<p color="#000000" face="Helvetica, sans-serif" style="color:#000000; font-size:14px;"><em>' . $prenom . ' </em></p>
															</a>
														</td>
													</tr>
												</table>
												<br />
												<table width="200" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000; color:#FFFFFF;">
														<tr align="center" height="50">
														<td align="center" height="50">
															<a target="_blank" href="' . $urlSite . '" 
															style="text-decoration:none; color:#FFFFFF; font-size:18px; line-height:26px;"> 
															' . $_['mailing']['user']['invite']['discover'] . '
															</a>
														</td>
													</tr>
												</table>
											<br />
											<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer'](null) . '</p>
																									
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>';
	}
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

	return [$headers,  mb_encode_mimeheader($object), $message];
}

//email nouveau contenu si par detenteur de la battle
mb_internal_encoding('UTF-8');
foreach ($emails as $email) {

	list($headers, $subject, $message) = getMessage($email, $userId);
	// var_export($message);
	mail($email, $sujet, $$message, $headers);
}
$mesLikeCoins = $_SESSION['likeCoins'];
$likesCoins = 50;
// BEWARE Only offer likeCoins for new emails (i.e. unknown in database)
$mesLikeCoins += count($emails) * $likesCoins;

if (isset($_GET['type']) && ($_GET['type'] == 'battle' || $_GET['type'] == 'post')) {
	/*** likesCoins ***/
	$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins`= `like_coins` + $mesLikeCoins WHERE `id` = :id");
	$updateReq->bindParam(':id', $userId, PDO::PARAM_STR);
	$updateReq->execute();
}
$_SESSION['likeCoins'] = $mesLikeCoins;

header('Content-Type: application/json');
echo json_encode(array('oui', $returnError, $mesLikeCoins));
