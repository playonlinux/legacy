<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

$inner = "";
$where = "";
$where2 = "";
$filterBy = "";
$orderBy = "ORDER BY nb_likes DESC";
$status = " and B.`statut` ='1'";
//LIMIT 0,20

$class = "col-lg-3 col-md-6 col-sm-6";

if (!empty($_POST['get'])) {
    $user = $_POST['get'];
    if ($_POST['trophee'] == 1) {
        $where = " AND P.user = $user AND P.trophee > 0 ";
        $status = " AND B.statut = 2";
    } else if ($_POST['trophee'] == 2) {
        $inner = " INNER JOIN bl_user_favoris F ON F.post = P.id ";
        $where = " AND F.user = $user ";
    } else {
        $where = " AND P.user = $user ";
    }
    $class = "col-lg-4 col-md-6 col-sm-6";

    $orderBy = "ORDER BY nb_likes DESC";
}

if (!empty($_POST['mots'])) {
    $name = $_POST['mots'];
    $mots = explode(' ', $name); //En separe l'expression en mots cles

    $k = 1;
    $where .= "and ";

    foreach ($mots as $mot) {
        if ($k == count($mots)) {
            $where .= "(P.`title` LIKE '%" . $mot . "%' OR U.`speudo` LIKE '%" . $mot . "%' OR U.`nom` LIKE '%" . $mot . "%' OR U.`prenom` LIKE '%" . $mot . "%')";
        } else {
            $where .= "(P.`title` LIKE '%" . $mot . "%' OR U.`speudo` LIKE '%" . $mot . "%' OR U.`nom` LIKE '%" . $mot . "%' OR U.`prenom` LIKE '%" . $mot . "%') AND ";
        }
        $k++;
    }

    $orderBy = "ORDER BY nb_likes DESC";
    $class = "col-lg-3 col-md-6 col-sm-6";
}

if (!empty($_POST['cat'])) {
    $cat = $_POST['cat'];
    $where = " AND B.category = '$cat'";
    $class = "col-lg-3 col-md-6 col-sm-6";
    $orderBy = " ORDER BY nb_likes DESC ";
    $limit = " LIMIT 0,100 ";
}

if (!empty($_POST['number'])) {
    $number = $_POST['number'];
    $orderBy = " ORDER BY nb_likes DESC ";
    $limit = " LIMIT 0, $number";
    $class = "col-lg-3 col-md-6 col-sm-6";
}

if (!empty($_POST['orderBy'])) {
    switch ($_POST['orderBy']) {
        case 'like': {
                $orderBy = ' ORDER BY nb_likes DESC ';
                break;
            }
        case 'date': {
                $orderBy = ' ORDER BY date_post DESC ';
                break;
            }
        case 'random': {
                $orderBy = ' ORDER BY rand() DESC ';
                break;
            }
    }
}

$select_posts = $dbh->prepare(
    "   SELECT  CONCAT(U.prenom, ' ', U.nom) AS username, P.post , P.date AS date_post, P.user AS user_id, P.title, 
            P.id, P.token, type, P.vues, P.statut,
            (SELECT count(id) FROM bl_likes_post WHERE posts = P.id) as nb_likes
        FROM bl_battle_posts P
            INNER JOIN bl_user U ON U.id = P.user
            INNER JOIN bl_battles B ON B.id = P.battle
            $inner
            $filterBy
        WHERE P.statut = 1 $status $where $orderBy"
);

// var_export($select_posts);
// die();

$select_posts->execute();

if (!$select_posts->rowCount()) {
    echo 'null';
    exit();
}

$rank = 1;
$laurels = [
    1 => ['title' => 'or', 'class' => ['rank' => 'rank gold-rank', 'border' => 'gold-border']],
    2 => ['title' => 'argent', 'class' => ['rank' => 'rank silver-rank', 'border' => 'silver-border']],
    3 => ['title' => 'bronze', 'class' => ['rank' => 'rank bronze-rank', 'border' => 'bronze-border']]
];
while ($row_posts = $select_posts->fetch(PDO::FETCH_OBJ)) {
    $select_share = $dbh->prepare("SELECT COUNT(*) AS nb_shares FROM bl_logs WHERE `type` = 31 AND page = :page");
    $select_share->bindParam(':page', $row_posts->id, PDO::PARAM_STR);
    $select_share->execute();

    $row_share = $select_share->fetch(PDO::FETCH_OBJ);

    ?>
        <!--section-->
        <div class="sh-section__item <?= $class ?>" id="post_<?= $row_posts->id ?>">
            <div class="sh-section box">
                <div class="sh-section__content">
                    <div class="sh-section__media">
                        <?php
                            switch ($row_posts->type) {
                                case 1: {
                                        $imagePartage = $row_posts->post;
                                        ?>
                                    <div class="element text-center">
                                        <img src="/<?= $row_posts->post ?>" alt="<?= $row_posts->title ?>">
                                    </div>
                                <?php
                                            break;
                                        }
                                    case 2: {
                                            list($video, $image) = video($row_posts->post);
                                            $imagePartage = ($image) ?  $image : '/images/partage_video_republike.jpg';
                                            ?>
                                    <img class="element" src="<?= $imagePartage; ?>">

                                <?php
                                            break;
                                        }
                                    case 3: {
                                            $imagePartage = '/images/partage_texte_republike.jpg';
                                            ?>
                                    <div class="element text-center">
                                        <p class="text-muted mb-2"><?= mb_strimwidth($row_posts->post, 0, 70, "[...]") ?> </p>
                                    </div>
                            <?php
                                        break;
                                    }
                            }

                            ?>
                            <div class="overlay">
                                <a class="btn-popup-post link" id="btn-popup-post_<?= $row_posts->id ?>" href="javascript:void(0)" data-id="<?= $row_posts->id ?>" data-token="<?= $row_posts->token ?>" data-ordre="<?= $rank ?>"></a>
                                <img src="/images/icons/search.svg" style="width: 100%; height: 100%">
                                </a>
                            </div>
                    </div>

                    <div>
                        <div style="padding:10px 0">
                            <h3 title="<?= $row_posts->title ?>" class="title" data-toggle="tooltip" data-placement="top">
                                <?= mb_strimwidth($row_posts->title, 0, 40, "[...]") ?>
                                <h3>
                        </div>
                    </div>
                    <div>
                        <i>
                            <?php
                                if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_posts->user_id) { ?>
                                <a href="/<?= $code ?>/<?= $_['url_compte'] ?>">
                                <?php   } else { ?>
                                    <a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_posts->user_id, $dbh) ?>">
                                    <?php   } ?>
                                    <?= $_['by'] . ' ' . strtoupper($row_posts->username) . ' ' . ago($row_posts->date_post) ?>
                                    </a>
                        </i>
                    </div>
                </div>
                <div class="sh-section__footer">
                    <div>
                        <span>
                            <img src="/images/icons/eye.svg" class="icons-30">
                            <span id="view_post_<?= $row_posts->id ?>"><?= $row_posts->vues; ?></span>
                        </span>
                    </div>
                    <div style="padding-bottom:10px">
                        <span style="cursor:pointer">
                            <?php
                                if (!empty($_SESSION['securite'])) {
                                    ?>
                                <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" id="like_<?= $row_posts->id ?>" data-id="<?= $row_posts->id ?>" class="<?= ($row_posts->statut == 1) ? "likeSave" : "inactif"; ?> sh-section__btn-like sh-btn-icon icons-30">
                            <?php
                                } else {
                                    ?>
                                <img src="/images/icons/liked-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" data-id="<?= $row_posts->id ?>" class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon icons-30">
                            <?php
                                }
                                ?>
                            <span class="like"><?= $row_posts->nb_likes; ?></span>
                        </span>
                        <?php
                            if (!empty($_SESSION['securite'])) {
                                ?>
                            <span style="cursor:pointer">
                                <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon icons-30" data-post="<?= $row_posts->id ?>" data-title="<?= $row_posts->title ?>" data-token="<?= $row_posts->token ?>" data-image="<?= $imagePartage ?>">
                                <span id="share_post_<?= $row_posts->id ?>"><?= $row_share->nb_shares; ?></span>
                            </span>

                        <?php
                            } else {
                                ?>
                            <span style="cursor:pointer">
                                <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon icons-30" data-post="<?= $row_posts->id ?>" data-title="<?= $row_posts->title ?>" data-token="<?= $row_posts->token ?>" data-image="<?= $imagePartage ?>">
                                <span><?= $row_share->nb_shares; ?></span>
                            </span>
                            </span>
                        <?php
                            }
                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_posts->user_id) {
                                ?>
                            <span style="cursor:pointer">
                                <img src="/images/icons/edit-grey.svg" data-toggle="tooltip" data-placement="left" title="<?= $_['modif_contenu'] ?>" href="javascript:void(0)" id="edit_<?= $row_posts->id ?>" data-id="<?= $row_posts->id ?>" data-name="<?= $row_posts->title ?>" class="editPost sh-section__btn-link icons-30">
                            </span>
                        <?php
                            }
                            if (!empty($_SESSION['securite']) && $_SESSION['id_user'] != $row_posts->user_id) {
                                ?>
                            <span style="cursor:pointer">
                                <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_posts->id ?>" data-title="<?= $row_posts->title ?>" title="<?= $_['signaler_contenu'] ?>" class="sh-post__signaler icons-30">
                            </span>
                        <?php
                            }
                            if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_posts->user_id) {
                                ?>
                            <span style="cursor:pointer">
                                <img src="/images/icons/trash-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_posts->id ?>" data-title="<?= $row_posts->title ?>" title="Supprimer ce contenu" class="sh-post__trash icons-30">
                            </span>
                        <?php
                            }
                            ?>
                    </div>
                </div>

            </div>
        </div>
    <? }
