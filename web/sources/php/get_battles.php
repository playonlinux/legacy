<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

define('NB_PER_PAGE', 10);
securePost();
$where = "";
$orderBy = "";
$page = !empty($_POST['page']) ? ($_POST['page'] - 1) * NB_PER_PAGE : 0;

$limit = "LIMIT $page, " . NB_PER_PAGE;
//limit 0,20
$class = "col-xl-3 col-lg-4 col-md-6 col-sm-6";

if (!empty($_POST['get'])) {
    $user = $_POST['get'];
    $where = " AND B.user = '$user' ";
    $class = "col-xl-3 col-lg-4 col-md-6 col-sm-6";
}

if (!empty($_POST['mots'])) {
    $name = $_POST['mots'];
    $mots = explode(' ', $name); //En separe l'expression en mots cles


    foreach ($mots as $mot) {
        $where .= " AND (B.`title` LIKE '%" . $mot . "%' OR B.`info` LIKE '%" . $mot . "%' OR U.`speudo` LIKE '%" . $mot . "%' OR U.`nom` LIKE '%" . $mot . "%' OR U.`prenom` LIKE '%" . $mot . "%') ";
    }

    $class = "col-xl-3 col-lg-4 col-md-6 col-sm-6";
}

if (!empty($_POST['cat'])) {
    $cat = $_POST['cat'];
    $where = " AND B.category = '$cat'";
    $class = "col-xl-3 col-lg-4 col-md-6 col-sm-6";
}

if (!empty($_POST['number'])) {
    $number = $_POST['number'];
    $orderBy = ' ORDER BY nb_users DESC ';
    $limit = " LIMIT 0,$number";
    $class = "col-xl-3 col-lg-4 col-md-6 col-sm-6";
}

if (!empty($_POST['orderBy'])) {
    switch ($_POST['orderBy']) {
        case 'users': {
                $orderBy = ' ORDER BY nb_users DESC ';
                break;
            }
        case 'date': {
                $orderBy = ' ORDER BY date_battle DESC ';
                break;
            }
        case 'random': {
                $orderBy = ' ORDER BY rand() DESC ';
                break;
            }
    }
}

$filterBy = '';
if (!empty($_POST['filterBy'])) {
    switch ($_POST['filterBy']) {
        case 'friends': {
                $filterBy = ' INNER JOIN bl_user_friend F ON (F.user = :user AND F.friend = B.user) OR (F.friend = :user AND F.user = B.user) ';
                break;
            }
        case 'category': {
                $filterBy = ' INNER JOIN bl_user_cat UC ON UC.user = :user AND UC.cat = C.id ';
                $where = ' AND B.user != :user AND NOT EXISTS (SELECT 1 FROM bl_battle_posts WHERE battle = B.id AND user = :user) ';
                break;
            }
    }
}

$select_battles = $dbh->prepare(
    "   SELECT CONCAT(U.prenom, ' ', U.nom) AS username, C.name_$code AS category, C.url_$code AS category_url,
        B.user AS user_id, B.category AS category_id, B.title, B.id AS id_battle, B.statut AS statut_battle, 
        B.info AS info_battle, B.url AS url_battle, B.date AS date_battle,
        B.vues,
        (SELECT COUNT(DISTINCT user) FROM bl_battle_posts WHERE battle = B.id) AS nb_users, 
        (SELECT COUNT(id) FROM bl_battle_posts WHERE battle = B.id AND statut=1) AS nb_posts,
        (SELECT SUM(likes) FROM bl_battle_posts WHERE battle = B.id) AS nb_likes,
        (SELECT id FROM bl_battle_posts WHERE battle = B.id AND statut = 1 ORDER BY likes DESC LIMIT 0,1 ) AS id_post
    FROM bl_battles B
        INNER JOIN bl_user U ON U.id = B.user
        INNER JOIN bl_categories C ON C.id = B.category
        $filterBy
    WHERE B.statut < 2 $where 
    AND (SELECT COUNT(id) FROM bl_battle_posts WHERE battle = B.id AND statut=1) > 0
    $orderBy
    $limit
"
);

if (!empty($_POST['filterBy'])) {
    $select_battles->bindParam(':user', $_SESSION['id_user'], PDO::PARAM_STR);
}

// var_export($select_battles);
// die();
$select_battles->execute();

if ($select_battles->rowCount() == 0) {
    echo 'null';
    exit;
}
while ($row_battles = $select_battles->fetch(PDO::FETCH_OBJ)) {
    $select_share = $dbh->prepare("SELECT COUNT(*) AS nb_shares FROM bl_logs WHERE `type` = 6 AND page = :page");
    $select_share->bindParam(':page', $row_battles->id_battle, PDO::PARAM_STR);
    $select_share->execute();

    $row_share = $select_share->fetch(PDO::FETCH_OBJ);
    if (empty($row_battles->id_post)) {
        continue;
    }
    list($post, $type) = photo_post($row_battles->id_post, $dbh);
    if (empty($post) || empty($type)) {
        continue;
    }
    $class_ami = '';
    $class_cat = '';
    if (!empty($_SESSION['securite'])) {
        $ami = recherche_ami($_SESSION['id_user'], $row_battles->user_id, $dbh);
        if ($ami == 1) {
            $class_ami = ' ami ';
        }
        $cat = recherche_cat($_SESSION['id_user'], $row_battles->category_id, $dbh);
        if ($cat == 1) {
            $class_cat = ' cat ';
        }
    }

    $class_statut = 'open';
    if ($row_battles->statut_battle == 2) {
        $class_statut = "closed";
    }

    ?>
            <!--section-->
            <div id="battle_<?= $row_battles->id_battle ?>" class="sh-section__item <?= $class ?> <?= $class_ami ?> <?= $class_cat ?> <?= $class_statut ?>">
                <div class="sh-section box">
                    <div class="sh-section__content">
                        <div class="sh-section__media">
                            <?php
                                switch ($type) {
                                    case 1: {
                                            $imagePartage = $post;
                                            ?>
                                        <div class="element text-center" style="background-image:url(/<?= $post ?>); background-size: cover; background-position: center;"></div>
                                    <?php
                                                break;
                                            }
                                        case 2: {
                                                list($video, $image) = video($post);
                                                $imagePartage = ($image) ? '/' . $image : '/images/partage_video_republike.jpg';
                                                ?>
                                        <div class="element text-center" style="background-image:url(<?= $image ?>); background-size: cover;">
                                            <?php
                                                        if ($video) {
                                                            ?>
                                                <video>
                                                    <source src="<?= $video ?>">
                                                </video>
                                            <?php
                                                        }
                                                        ?>
                                        </div>
                                    <?php
                                                break;
                                            }
                                        case 3: {
                                                $imagePartage = '/images/partage_texte_republike.jpg';
                                                ?>
                                        <div class="element text-center">
                                            <p class="text-muted mb-2"><?= mb_strimwidth($post, 0, 50, "[...]") ?> </p>
                                        </div>
                                <?php
                                            break;
                                        }
                                }

                                ?>
                                <div class="overlay">
                                    <a class="link" href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $row_battles->url_battle ?>">
                                        <img src="/images/icons/<?= $code ?>/participate-button-view.svg">
                                    </a>
                                </div>
                        </div>
                        <div>
                            <a href="/<?= $code ?>/<?= $_['url_theme'] ?>/<?= $row_battles->category_url ?>" class="w3-tag w3-purple">
                                <?= $row_battles->category; ?>
                            </a>
                            <div class="card-title">
                                <h3 title="<?= $row_battles->title ?>" data-toggle="tooltip" data-placement="top">
                                    <?= mb_strimwidth($row_battles->title, 0, 50, "[...]") ?>
                                </h3>
                            </div>
                            <div>
                                <i>
                                    <?php
                                        if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battles->user_id) { ?>
                                        <a href="/<?= $code ?>/<?= $_['url_compte'] ?>">
                                        <?php   } else { ?>
                                            <a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_battles->user_id, $dbh) ?>">
                                            <?php   } ?>
                                            <?= $_['by'] . ' ' . strtoupper($row_battles->username) . ' ' . ago($row_battles->date_battle) ?>
                                            </a>
                                </i>
                            </div>
                        </div>
                    </div> <!-- section__content -->
                    <div class="sh-section__footer">
                        <div>
                            <span>
                                <img src="/images/icons/friends.svg" class="icons-30">
                                <span class="users"><?= $row_battles->nb_users; ?></span>
                            </span>
                            <span>
                                <img src="/images/icons/bow.svg" class="icons-30">
                                <span class=""><?= $row_battles->nb_posts; ?></span>
                            </span>
                            <span>
                                <img src="/images/icons/eye.svg" class="icons-30">
                                <span class=""><?= $row_battles->vues; ?></span>
                            </span>
                            <span>
                                <img src="/images/icons/liked-pink.svg" class="icons-30">
                                <span class=""><?= $row_battles->nb_likes; ?></span>
                            </span>
                        </div>
                        <div>

                            <?php if ($row_battles->statut_battle != 2) { ?>
                                <span style="cursor:pointer">
                                    <?php if (!empty($_SESSION['securite'])) { ?>

                                        <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-partage-battle icons-30" data-battle="<?= $row_battles->id_battle ?>" data-url="<?= $row_battles->url_battle ?>" data-title="<?= $row_battles->title ?>" data-image="/<?= $imagePartage; ?>">
                                    <?php } else { ?>
                                        <!-- TODO: Add automatic redirection after login process -->
                                        <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-upload_btn-signup icons-30" data-battle="<?= $row_battles->id_battle  ?>">
                                    <?php } ?>
                                    <span id="share_battle_<?= $row_battles->id_battle ?>"><?= $row_share->nb_shares; ?></span>
                                </span>
                                <?php
                                        if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battles->user_id && $row_battles->nb_posts <= 1) {
                                            ?>
                                    <span style="cursor:pointer">
                                        <img src="/images/icons/trash-grey.svg" data-toggle="tooltip" data-placement="top" data-battle="<?= $row_battles->id_battle ?>" data-title="<?= $row_battles->title ?>" title="Supprimer ce contenu" class="sh-battle__trash icons-30">
                                    </span>
                                <?php
                                        }
                                        ?>
                            <?php } else { ?>

                                <div data-toggle="tooltip" data-placement="top" title="<?= $_['battle_close'] ?>" class="btn-closed-battle sh-section__btn-follow sh-btn-icon" data-battle="<?= $row_battles->id_battle ?>">
                                    <i class="fa fa-trophy"></i></span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div><!-- sh-section box-->
            </div>
            <!--battle-->
        <?php } //while

        ?>