<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (empty($_SESSION['securite']) || empty($_POST['type']) || !in_array($_POST['type'], ['battle', 'post'])) {
	echo 'non';
	exit;
}

$request = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$request->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
$request->execute();

if (!$request->rowCount()) {
	echo 'non';
	exit;
}

$row = $request->fetch(PDO::FETCH_OBJ);
$user = $row->id;

$params = getParams();
// list($id, $image, $logType, $url, $suject, $title)
//celui qui fait la demande
$prenom = prenom_user($user, $dbh);
$email = email_user($user, $dbh);
$photo = photo_user($user, $dbh);
//insert cat
$friends = $_POST['amis'];

foreach ($friends as $friend) {

	save_log_ou($user, $params['logType'], $params['id'], $dbh);

	//celui qui fait la demande
	$prenom_amis = prenom_user($friend, $dbh);
	$email_amis = email_user($friend, $dbh);

	if (empty($email_amis)) {
		continue;
	}
	//email nouveau contenu si par detenteur de la battle

	//MAIL USER 1
	/* Construction du message */
	$message = getMessageHeaders() . '
								<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
											  	<tr align="center">
														<td width="420" height="214" align="center">
															<p style="color:#777777;">' . $_['mailing']['hello']($prenom_amis) . ',<br><br>
																' . $params['body']($prenom) . '
																<br><br/>
																<a target="_blank" href="' . $params['url'] . '" style="text-decoration:none;"> 
																	<img src="' . $params['image'] . '" width="250"> 
																</a>
																<br/><br/>
															</p>												  
													
															<p style="font-size:10px; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>';


	$to = $email_amis;
	//$to='dev@mkael-group.com';
	mb_internal_encoding('UTF-8');
	$subject = mb_encode_mimeheader($prenom_amis . $params['subject']($prenom));

	mail($to, $subject, $message, getHeaders());

	$notification = '
		<div class="sh-notif__ligne">
			<a href="' . $urlSite . '/' . $code . '/' . $_['url_compte'] . '" class="sh-comments__notif sh-avatar">
				<img src="' . $urlSite . '/' . $photo . '" alt="' . $prenom . '">
			</a>
			<div>
				<a href="' . $params['url'] . '">' . $params['body']($prenom) . '</a>
				<span>[date]</span>
			</div>
		</div>';
	save_notifs($friend, $notification, $dbh);
}

echo "oui";



function getParams()
{
	global $urlSite, $dbh, $code, $_;

	if ($_POST['type'] == 'battle') {
		return [
			'id' => $_POST['id_battle'],
			'image' => photo_battle_mail($_POST['id_battle'], $dbh),
			'logType' => 6,
			'url' => $urlSite . '/' . $code . '/' . $_['url_battle'] . '/' . $_POST['id_battle'],
			'subject' => $_['mailing']['user']['battle']['subject'],
			'body' => $_['mailing']['user']['battle']['body']
		];
	} elseif ($_POST['type'] == 'post') {
		list($token) = info_post($_POST['id_post'], $dbh);
		return [
			'id' => $_POST['id_post'],
			'image' => photo_post_mail($token, $dbh),
			'logType' => 31,
			'url' =>  $urlSite . '/' . $code . '/' . $_['url_contenu'] . '/' . $token,
			'subject' => $_['mailing']['user']['post']['subject'],
			'body' => $_['mailing']['user']['post']['body']
		];
	}
}


function getHeaders()
{
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

	return $headers;
}
function getMessageHeaders()
{
	global $urlSite;

	return '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
	<tbody>
		<tr>
			<td height="73" align="center" valign="top">
				<table width="420" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
									<tbody>
										<tr>
											<td height="58" valign="middle" style="padding:9px">
												<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr align="center">
															<td valign="middle" style="padding:0px 9px">
																<a href="#" target="_blank">
																	<img alt="BATTLELIKE" title="BATTLELIKE" align="center" width="250" src="' . $urlSite . '/images/logo-republilke-entier.png" style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
																</a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>';
}
