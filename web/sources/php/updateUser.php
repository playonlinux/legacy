<?php
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

securePost();
secureGet();

$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$speudo = '';
$mail= $_POST['email_u'];

$sexe= $_POST['sexe'];
$ville= $_POST['ville'];

$naissance= $_POST['naissance'];
$naissance = explode('/',$naissance);
$naissance = $naissance[2].'-'.$naissance[1].'-'.$naissance[0];

$id= $_POST['user'];
$token= $_POST['token'];

if ($nom !='' && $mail != ''){
	
	$sth = $dbh->prepare('SELECT * FROM `bl_user` WHERE id = :id and password = :token  limit 0,1');
	$sth->bindParam(':id', $id, PDO::PARAM_STR);
	$sth->bindParam(':token', $token, PDO::PARAM_STR);
	$rs = $sth->execute();
	
	if ($sth->rowCount() > 0) {
		
		$statut =2;		

		$updateReq = $dbh->prepare("UPDATE `bl_user` SET `nom`=:nom,`prenom`=:prenom,`speudo`=:speudo,`email`=:mail,
		`sexe`=:sexe,`naissance`=:naissance,`ville`=:ville ,`statut`=:statut
		WHERE id = :id ");
		$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
		$updateReq->bindParam(':nom', $nom, PDO::PARAM_STR);
		$updateReq->bindParam(':prenom', $prenom, PDO::PARAM_STR);
		$updateReq->bindParam(':speudo', $speudo, PDO::PARAM_STR);
		$updateReq->bindParam(':mail', $mail, PDO::PARAM_STR);
		$updateReq->bindParam(':sexe', $sexe, PDO::PARAM_STR);
		$updateReq->bindParam(':naissance', $naissance, PDO::PARAM_STR);
		$updateReq->bindParam(':ville', $ville, PDO::PARAM_STR);
		$updateReq->bindParam(':statut', $statut, PDO::PARAM_STR);
		
		$updateReq->execute();
		
		save_log($id, '5', $dbh);
		
		
		//insert cat
		$catok = explode(',', $_POST['catok']);
		foreach ($catok as $cat){ 
			$insertReq = $dbh->prepare("INSERT INTO `bl_user_cat`(`user`, `cat`) VALUES (:id, :cat)");
			$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
			$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
			$insertReq->execute();
		}
		
		/*** likesCoins ***/
		$likesCoins = 300; 
		$updateReq =$dbh->prepare("UPDATE `bl_user` SET `like_coins`= like_coins + $likesCoins WHERE id = :id");
		$updateReq->bindParam(':id', $id, PDO::PARAM_STR);
		$updateReq->execute();
		/*** likesCoins ***/
		
		
		/*** on recupere les likes du USER ***/
		$userLikesResult= $dbh->prepare("SELECT like_coins FROM `bl_user` WHERE id = :id limit 0,1");
		$userLikesResult->bindParam(':id', $id, PDO::PARAM_STR);
		$userLikesResult->execute();
		
		if ($userLikesResult->rowCount() > 0) {
			$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
			$mesLikeCoins=$row_userLikesResult->like_coins;
			$_SESSION['likeCoins'] =$mesLikeCoins;

		}
				
		echo 'oui';
	}

}else{
	echo 'non';
}
	
?>