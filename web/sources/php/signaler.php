<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

$message = "";

if (!empty($_SESSION['securite'])) {

	$securite = $_SESSION['securite'];

	$userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
	$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
	// $userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
	// $userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();

	if ($userResult->rowCount() > 0) {

		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $row_userResult->id;

		$cat = $_POST['cat'];
		$mes = $_POST['mes'];

		if ($_GET['type'] == "commentaire") {
			$id_element = $_POST['comment'];

			$insertReq = $dbh->prepare("INSERT INTO `bl_signaler`(`type`, `texte`, `comment`, `user`) 
			VALUES (:cat, :message, :id_element, :user)");
			$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
			$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
			$insertReq->bindParam(':id_element', $id_element, PDO::PARAM_STR);
			$insertReq->bindParam(':message', $mes, PDO::PARAM_STR);
			$insertReq->execute();
		} else if ($_GET['type'] == "battle") {
			$id_element = $_POST['battle'];

			$insertReq = $dbh->prepare("INSERT INTO `bl_signaler`(`type`, `texte`, `battle`, `user`) 
			VALUES (:cat, :message, :id_element, :user)");
			$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
			$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
			$insertReq->bindParam(':id_element', $id_element, PDO::PARAM_STR);
			$insertReq->bindParam(':message', $mes, PDO::PARAM_STR);
			$insertReq->execute();
		} else {
			$id_element = $_POST['post'];

			$insertReq = $dbh->prepare("INSERT INTO `bl_signaler`(`type`, `texte`, `post`, `user`) 
			VALUES (:cat, :message, :id_element, :user)");
			$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
			$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
			$insertReq->bindParam(':id_element', $id_element, PDO::PARAM_STR);
			$insertReq->bindParam(':message', $mes, PDO::PARAM_STR);
			$insertReq->execute();
		}


		//celui qui a laissé le commentaire
		$prenom = prenom_user($user, $dbh);

		//email nouveau commentaire
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="400" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="' . $urlSite . '" target="_blank">
												<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" 
												width="250" src="' . $urlSite . '/images/logo-republilke-entier.png" 
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
											<p style="color:#777777;">Bonjour,<br>
											<br>
											' . $prenom . ' a signalé un contenu.
											<br><br /></p>
											
											<table width="200" border="0" cellspacing="0" cellpadding="0" style="background-color:#ff0000; color:#FFFFFF;">
												  <tr align="center" height="40">
													<td align="center" height="40">
														<a target="_blank" href="' . $urlSite . '/admin/" 
														style="text-decoration:none; color:#FFFFFF; font-size:14px; line-height:20px;"> 
														VOIR LE CONTENU
														</a>
													</td>
												</tr>
											  </table><br />
										  
											<p style="font-size:10px; color:#777777;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br />
											<b>Pour paramètrer vos alertes, rendez-vous dans "Mon compte / Paramètres".</b><br /><br />
											© 2019 REPUBLIKE</p>
																								
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';

		$to = 'contact@republike.io';
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader($prenom . ' a signalé un contenu !');

		if ($to != '') {
			mail($to, $sujet, $msg, $headers);
			echo 'oui';
		} else {
			echo 'non';
		}
	} else {
		echo 'non';
	}
}
