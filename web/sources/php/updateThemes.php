<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();


if (empty($_POST['catok']) || empty($_POST['user']) || empty($_POST['token'])) {
	echo 'non';
	exit;
}

$id = $_POST['user'];
$token = $_POST['token'];

$query = $dbh->prepare('SELECT 1 FROM `bl_user` WHERE id = :id AND password = :token LIMIT 0,1');
$query->bindParam(':id', $id, PDO::PARAM_STR);
$query->bindParam(':token', $token, PDO::PARAM_STR);
$query->execute();

if (!$query->rowCount()) {
	echo 'non';
	exit;
}

$gender = $_POST['gender'];
if (!empty($gender)) {
	$query = $dbh->prepare('UPDATE `bl_user` SET sexe = :sexe WHERE id = :id AND password = :token');
	$query->bindParam(':sexe', $gender, PDO::PARAM_STR);
	$query->bindParam(':id', $id, PDO::PARAM_STR);
	$query->bindParam(':token', $token, PDO::PARAM_STR);
	$query->execute();
}

for ($i = 1; $i < 10; $i++) {

	if (empty($_POST['alert_' . $i])) {
		continue;
	}

	$deleteReq = $dbh->prepare("DELETE FROM `bl_user_alerts` WHERE `user` = :id AND alert = :alert AND opt = :opt");
	$deleteReq->bindParam(':id', $id, PDO::PARAM_STR);
	$deleteReq->bindParam(':alert', $i, PDO::PARAM_STR);
	$deleteReq->bindParam(':opt', $_POST['alert_' . $i], PDO::PARAM_STR);
	$deleteReq->execute();


	$insertReq = $dbh->prepare("INSERT INTO `bl_user_alerts`(`user`, `alert`, `opt`) VALUES (:id, :alert, :opt)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':alert', $i, PDO::PARAM_STR);
	$insertReq->bindParam(':opt', $_POST['alert_' . $i], PDO::PARAM_STR);
	$insertReq->execute();
}

//insert cat
foreach ($_POST['catok'] as $cat) {
	$insertReq = $dbh->prepare("INSERT INTO `bl_user_cat`(`user`, `cat`) VALUES (:id, :cat)");
	$insertReq->bindParam(':id', $id, PDO::PARAM_STR);
	$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
	$insertReq->execute();
}

/*** on recupere les likes du USER ***/
$userLikesResult = $dbh->prepare("SELECT like_coins FROM `bl_user` WHERE id = :id LIMIT 0,1");
$userLikesResult->bindParam(':id', $id, PDO::PARAM_STR);
$userLikesResult->execute();

if ($userLikesResult->rowCount() > 0) {
	$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
	$mesLikeCoins = $row_userLikesResult->like_coins;
	$_SESSION['likeCoins'] = $mesLikeCoins;
}

save_log($id, '13', $dbh);

echo 'oui';
