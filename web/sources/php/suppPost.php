<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();

if (empty($_SESSION['securite'])) {
	echo 'non';
	exit;
}

$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id LIMIT 0,1");
$userResult->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
$userResult->execute();

if (!$userResult->rowCount()) {
	echo 'non';
	exit;
}

$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
$user = $row_userResult->id;

//POST
$id_post = $_POST['id_post'];

$deleteResultLike = $dbh->prepare("DELETE FROM `bl_likes_post` WHERE `posts`= :id_post");
$deleteResultLike->bindParam(':id_post', $id_post, PDO::PARAM_STR);
$deleteResultLike->execute();

$deleteResultPost = $dbh->prepare("DELETE FROM `bl_battle_posts` WHERE `id` = :id_post");
$deleteResultPost->bindParam(':id_post', $id_post, PDO::PARAM_STR);
$deleteResultPost->execute();

save_log($user, '3', $dbh);

echo 'oui';
