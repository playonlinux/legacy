<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();

$stock = 'non';
$post_prec = 0;

if (!empty($_SESSION['securite'])) {
	// var_export('1');
	$userResult = $dbh->prepare("SELECT id, likes, like_coins FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
	$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
	$userResult->execute();

	if ($userResult->rowCount() > 0) {
		// var_export('2');
		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $row_userResult->id;
		$likes_user = $row_userResult->likes;
		$like_coins = $row_userResult->like_coins;

		//POST
		$id_post = $_POST['id_post'];
		$type = $_POST['type'];
		$clos = 1;

		//battle		
		$battleResult = $dbh->prepare("SELECT battle FROM `bl_battle_posts` WHERE `id` =:id_post LIMIT 0,1");
		$battleResult->bindParam(':id_post', $id_post, PDO::PARAM_STR);
		$battleResult->execute();

		if ($battleResult->rowCount() > 0) {
			// var_export('3');
			$row_battleResult = $battleResult->fetch(PDO::FETCH_OBJ);
			$battle = $row_battleResult->battle;

			//placement avant like
			$userResult = $dbh->prepare("SELECT p.id,
			(SELECT id from bl_battle_posts p2 where `battle` = :battle and p2.likes > p.likes ORDER BY p2.likes ASC LIMIT 0,1 ) as precedant
			FROM `bl_battle_posts` p 
			WHERE p.battle = :battle and p.id =:post  ORDER BY p.likes DESC");
			$userResult->bindParam(':battle', $battle, PDO::PARAM_STR);
			$userResult->bindParam(':post', $id_post, PDO::PARAM_STR);
			$userResult->execute();

			if ($userResult->rowCount() > 0) {
				// var_export('4');
				$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
				$post_prec = $row_userResult->precedant;
			}
		}

		if ($type == 'like') {
			// var_export('5');
			//On verifie combien de like à le post
			$select_post1 = $dbh->prepare("SELECT count(id) as likes
			FROM `bl_likes_post`
			WHERE posts = :id_post");
			$select_post1->bindParam(':id_post', $id_post, PDO::PARAM_STR);
			$select_post1->execute();
			$row_post1 = $select_post1->fetch(PDO::FETCH_OBJ);
			$nbLikes = $row_post1->likes;

			if ($nbLikes < NB_MAX_LIKES) {
				// var_export('6');

				$clos = 2;
				// On verifie combien de fois j'ai liké ce POST
				$likePostResult = $dbh->prepare("SELECT * FROM `bl_likes_post` WHERE `user` =:user and `posts` =:id_post");
				$likePostResult->bindParam(':user', $user, PDO::PARAM_STR);
				$likePostResult->bindParam(':id_post', $id_post, PDO::PARAM_STR);
				$likePostResult->execute();

				if ($likePostResult->rowCount() < NB_MAX_LIKES_PER_CONTENT && $likes_user > 0) {
					// var_export('7');
					$stock = 'oui';

					//on recupere l'auteur du POST
					$userPostResult = $dbh->prepare("SELECT user FROM `bl_battle_posts` WHERE `id` =:id_post LIMIT 0,1");
					$userPostResult->bindParam(':id_post', $id_post, PDO::PARAM_STR);
					$userPostResult->execute();

					if ($userPostResult->rowCount() > 0) {
						// var_export('8');
						$row_userPostResult = $userPostResult->fetch(PDO::FETCH_OBJ);
						$user_post = $row_userPostResult->user;

						if ($user != $user_post) {
							// var_export('9');
							/*** likesCoins USER ***/
							$likesCoins = 10;
							$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins` = `like_coins` + $likesCoins WHERE `id` = :id");
							$updateReq->bindParam(':id', $user, PDO::PARAM_STR);
							$updateReq->execute();
							/*** likesCoins ***/

							/*** likesCoins USER POST ***/
							$likesCoins = 10;
							$updateReq2 = $dbh->prepare("UPDATE `bl_user` SET `like_coins` = `like_coins` + $likesCoins WHERE `id` = :id");
							$updateReq2->bindParam(':id', $user_post, PDO::PARAM_STR);
							$updateReq2->execute();
							/*** likesCoins ***/
						} else {
							// var_export('10');
							//JE PERD 100 LIKECOINS
							$likesCoins = $like_coins - 100;
							if ($likesCoins <= 0) {
								$likesCoins = 0;
							}
							$updateReq = $dbh->prepare("UPDATE `bl_user` SET `like_coins`= $likesCoins WHERE id = :id");
							$updateReq->bindParam(':id', $user, PDO::PARAM_STR);
							$updateReq->execute();
							/*** like_coins***/
						}
					}


					$insertReq = $dbh->prepare("INSERT INTO `bl_likes_post` (`user`, `posts`) 
					VALUES (:user, :id_post)");
					$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
					$insertReq->bindParam(':id_post', $id_post, PDO::PARAM_STR);
					$insertReq->execute();

					//JE PERD UN LIKE
					/*** likes ***/
					$likes = 1;
					$updateReq = $dbh->prepare("UPDATE `bl_user` SET `likes`= likes-$likes WHERE id = :id");
					$updateReq->bindParam(':id', $user, PDO::PARAM_STR);
					$updateReq->execute();

					/*** likes & like_coins***/
				}
			}
		}

		/*** on recupere les likes du USER ***/
		$userLikesResult = $dbh->prepare("SELECT likes, like_coins FROM `bl_user` WHERE id = :id LIMIT 0,1");
		$userLikesResult->bindParam(':id', $user, PDO::PARAM_STR);
		$userLikesResult->execute();

		if ($userLikesResult->rowCount() > 0) {
			// var_export('11');
			$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
			$mesLikes = $row_userLikesResult->likes;
			$_SESSION['likes'] = $mesLikes;
			$mesLikeCoins = $row_userLikesResult->like_coins;
			$_SESSION['likeCoins'] = $mesLikeCoins;
		}
		/*** on recupere les likes du USER ***/

		//RETOUR NOMBRE DE LIKE
		$select_post = $dbh->prepare("SELECT count(id) as likes
		FROM `bl_likes_post`
		WHERE posts = :id_post");
		$select_post->bindParam(':id_post', $id_post, PDO::PARAM_STR);
		$select_post->execute();
		$row_post = $select_post->fetch(PDO::FETCH_OBJ);
		$nbLikes = $row_post->likes;


		//UPDATE POST LIKES
		$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` 
		SET `likes`= :likes
		WHERE id = :id_post");
		$updateReq->bindParam(':id_post', $id_post, PDO::PARAM_STR);
		$updateReq->bindParam(':likes', $nbLikes, PDO::PARAM_STR);
		$updateReq->execute();

		//on recupere l'auteur du POST
		$userTotalLikes = $dbh->prepare("SELECT battle as battle_post,
		(SELECT sum(likes) FROM bl_battle_posts WHERE bl_battle_posts.battle = battle_post) as nb_like
		FROM `bl_battle_posts` WHERE `id` =:id_post LIMIT 0,1");
		$userTotalLikes->bindParam(':id_post', $id_post, PDO::PARAM_STR);
		$userTotalLikes->execute();

		if ($userTotalLikes->rowCount() > 0) {
			// var_export('12');
			$row_userTotalLikes = $userTotalLikes->fetch(PDO::FETCH_OBJ);
			$totalLikes = $row_userTotalLikes->nb_like;
			$battle = $row_userTotalLikes->battle_post;
		}


		//placement après like
		$userResult = $dbh->prepare("SELECT p.id,
		(select id from bl_battle_posts p2 where `battle` = :battle and (p2.likes > p.likes and p2.likes != p.likes) ORDER BY p2.`likes` ASC LIMIT 0,1 ) as precedant
		FROM `bl_battle_posts` p 
		WHERE `battle` = :battle and `id` =:post  ORDER BY p.`likes` DESC");
		$userResult->bindParam(':battle', $battle, PDO::PARAM_STR);
		$userResult->bindParam(':post', $id_post, PDO::PARAM_STR);
		$userResult->execute();

		if ($userResult->rowCount() > 0) {
			// var_export('13');
			$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
			$new_post_prec = $row_userResult->precedant;
		}

		if ($new_post_prec != $post_prec) {
			// var_export('14');
			mailPassDevant($id_post, $post_prec, $battle, $dbh);
		}

		//TOP 3
		$topReq = $dbh->prepare("SELECT * FROM bl_battle_posts WHERE battle=:id AND statut = 1 ORDER BY likes DESC LIMIT 0,3");
		$topReq->bindParam(':id', $battle, PDO::PARAM_STR);
		$topReq->execute();
		// var_export($topReq->rowCount());
		if ($topReq->rowCount() > 0) {
			// var_export('15');
			$i = 1;
			while ($row_top = $topReq->fetch(PDO::FETCH_OBJ)) {
				// var_export($row_top);
				if ($i == 1) {
					$color = "or";
				}
				if ($i == 2) {
					$color = "argent";
				}
				if ($i == 3) {
					$color = "bronze";
				}

				//Si 100 on cloture
				if ($nbLikes >= NB_MAX_LIKES && $clos == 2) {
					/* Envoie du mail */
					// var_export('mailToGagnant');
					mailToGagnant($battle, $row_top->user, $i, $dbh);

					$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `trophee`= $i WHERE `id` =:id");
					$updateReq->bindParam(':id', $row_top->id, PDO::PARAM_STR);
					$updateReq->execute();
				}

				//TODO : FIX TOP3 notif
				$top3 .= '<div class="sh-btn-icon">
					<a href=/' . $code . '/' . $_['url_contenu'] . '/' . $row_top->token . '"><i class="repu-laurels mt-2 ' . $color . '"></i></a> ' . $row_top->likes . ' <i class="fa fa-heart top"></i> - 
                    <a data-toggle="tooltip" data-placement="top" title="' . speudo_user($row_top->user, $dbh) . '" href="user.php?id=' . $row_top->user . '" >
                        <span class="sh-section__avatar-small" style="border-color:' . grade_user_color(points_user($row_top->user, $dbh), $dbh) . '; background-image:url(/' . photo_user($row_top->user, $dbh) . ')"></span>
                    </a>
					<a href="user.php?id=' . $row_top->user . '"><span>' . prenom_user($row_top->user, $dbh) . '</span></a>
			   </div>';
				$i++;
			}
		}

		//Si 100 on cloture
		if ($nbLikes >= NB_MAX_LIKES && $clos == 2) {
			// var_export('16');
			$updateReq = $dbh->prepare("UPDATE `bl_battles` SET `statut`= '2' WHERE `id` =:battle");
			$updateReq->bindParam(':battle', $battle, PDO::PARAM_STR);
			$rs = $updateReq->execute();
		}

		header('Content-Type: application/json');
		echo json_encode(array($nbLikes, $mesLikes, $stock, $totalLikes, $top3, $mesLikeCoins));
	} else {
		// TODO: Code ABSURDE
		/*** on recupere les likes du USER ***/
		$userLikesResult = $dbh->prepare("SELECT likes, like_coins FROM `bl_user` WHERE id = :id LIMIT 0,1");
		$userLikesResult->bindParam(':id', $user, PDO::PARAM_STR);
		$userLikesResult->execute();

		if ($userLikesResult->rowCount() > 0) {
			$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
			$mesLikes = $row_userLikesResult->likes;
			$mesLikeCoins = $row_userLikesResult->like_coins;
		}
		/*** on recupere les likes du USER ***/

		header('Content-Type: application/json');
		echo json_encode(array('non', $mesLikes, $stock, 0, 0, $mesLikeCoins));
	}
} else {

	// TODO: Code ABSURDE
	/*** on recupere les likes du USER ***/
	$userLikesResult = $dbh->prepare("SELECT likes, like_coins FROM `bl_user` WHERE id = :id LIMIT 0,1");
	$userLikesResult->bindParam(':id', $user, PDO::PARAM_STR);
	$userLikesResult->execute();

	if ($userLikesResult->rowCount() > 0) {
		$row_userLikesResult = $userLikesResult->fetch(PDO::FETCH_OBJ);
		$mesLikes = $row_userLikesResult->likes;
		$mesLikeCoins = $row_userLikesResult->like_coins;
	}
	/*** on recupere les likes du USER ***/

	header('Content-Type: application/json');
	echo json_encode(array('non', $mesLikes, $stock, 0, 0, $mesLikeCoins));
}

function mailToGagnant($battle, $user, $i, $dbh)
{

	global $code, $_, $urlSite;

	$email = email_user($user, $dbh);
	$prenom = prenom_user($user, $dbh);
	$photo = photo_user($user, $dbh);
	$url = url_battle($battle, $dbh);

	//On recupere son grade avant les points
	$points = points_user($user, $dbh);
	$grade = grade_user_between($points, $code, $dbh);

	$image = photo_battle_mail($battle, $dbh);


	$lang = code_user_by_id($user);

	if ($lang) {
		require './lang_' . $lang . '.php';
	} else {
		$lang = $code;
	}

	$battleName = '<a href="' . $urlSite . '/' . $lang . '/' . $_['url_battle'] . '/' . $url . '">' . name_battle($battle, $dbh) . '</a>';
	$messages = [
		1 => [
			'points' => 9,
			'rank' => 'or',
			'lauriet' => '<img src="' . $urlSite . '/images/icons/gold-number.svg" class="icons-70" alt="Classement OR">',
			'subject' => $_['mailing']['battle']['winners']['first']['subject']($battleName),
			'body' => $_['mailing']['battle']['winners']['first']['body']($battleName)
		],
		2 => [
			'points' => 3,
			'rank' => 'argent',
			'lauriet' => '<img src="' . $urlSite . '/images/icons/silver-number.svg" class="icons-70" alt="Classement ARGENT">',
			'subject' => $_['mailing']['battle']['winners']['second']['subject']($battleName),
			'body' => $_['mailing']['battle']['winners']['second']['body']($battleName)
		],
		3 => [
			'points' => 1,
			'rank' => 'bronze',
			'lauriet' => '<img src="' . $urlSite . '/images/icons/bronze-number.svg" class="icons-70" alt="Classement BRONZE">',
			'subject' => $_['mailing']['battle']['winners']['second']['subject']($battleName),
			'body' => $_['mailing']['battle']['winners']['third']['body']($battleName)
		]
	];

	if (empty($messages[$i])) {
		return;
	}

	list($points, $rank, $lauriet, $subject, $message) = array_values($messages[$i]);

	$query = $dbh->prepare("UPDATE `bl_user` SET `points` = `points` + $points, `$rank` = `$rank` + 1 WHERE `id` = :id");
	$query->bindParam(':id', $user, PDO::PARAM_STR);

	$query->execute();

	$notification =
		'<div class="sh-notif__ligne">
			<a href="' . $urlSite . '/' . $lang . '/' . $_['url_compte'] . '" class="sh-comments__notif sh-avatar">
				<img src="' . $urlSite . '/' . $photo . '" alt="' . $prenom . '">
			</a>
			<div>
				<a href="' . $urlSite . '/' . $lang . '/' . $_['url_battle'] . '/' . $url . '">' . $message . '</a>
				<span>[date]</span>
			</div>
		</div>';
	save_notifs($user, $notification, $dbh);

	if (!empty($email)) {
		// $message = str_replace('<span>[date]</span>', '', $message);
		//email nouveau contenu si par detenteur de la battle
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$content = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="#" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr align="center">
										<td width="420" height="214" align="center">
											
											<p style="color:#777777;">' . $lauriet . ' <br /></p>
											<p style="color:#777777;">' . $message . '<br /><br />
											
											<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_battle'] . '/' . $url . '" style="text-decoration:none;"> 
												<img src="' . $image . '" width="250"> 
											</a><br /><br />
											</p>
											
											<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';


		mb_internal_encoding('UTF-8');
		$subject = mb_encode_mimeheader($subject);
		mail($email, $subject, $content, $headers);
	}

	//On recupere son grade après les points
	$pointsB = points_user($user, $dbh);
	$colorB = grade_user_color($pointsB, $dbh);
	$gradeB = grade_user_between($pointsB, $code, $dbh);

	if ($gradeB == $grade) {
		return;
	}

	//notif
	$notification = '<div class="sh-notif__ligne">
			<a href="' . $urlSite . '/' . $lang . '/' . $_['url_compte'] . '" class="sh-comments__notif sh-avatar">
				<img src="' . get_user_avatar($user) . '" class="icons-30">
			</a>
			<div>
				<a href="' . $urlSite . '/' . $lang . '/' . $_['url_compte'] . '">
					Grade ' . ucfirst(strtolower($gradeB)) . ' atteint
				</a>
				<span>[date]</span>
			</div>
		</div>';
	save_notifs($user, $notification, $dbh);

	if (empty($email) || (alert_user(8, $user, $dbh) != 1)) {
		return;
	}

	//email nouveau contenu si par detenteur de la battle
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

	//MAIL USER 1
	/* Construction du message */
	$content = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="#" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
											
											<p style="color:#777777;">Bravo ' . $prenom . ', <br /><br />
											Félicitations ! Vos contenus sont excellents et vous êtes désormais :<br />
											"<b style="color:' . $colorB . '; font-size: 16px; ">' . $gradeB . '</b>"
											
											<br></p>
													
													<table width="200" border="0" cellspacing="0" cellpadding="0" align="center" height="100">
													  <tr valign="middle">
														<td height="90" width="100" align="center">
														
															<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_compte'] . '" style="text-decoration:none; border:none;"> 
																<img style="border-radius:50%;outline:none;color:#ffffff;text-decoration:none; border:2px solid ' . $colorB . ';" 
																src="' . $urlSite . '/' . $photo . '" class="icons-70" border="0" />
															</a>
														</td>
														
													  </tr>
													  
												  </table><br />
													<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';

	mb_internal_encoding('UTF-8');
	$subject = mb_encode_mimeheader('Votre influence s\'étend !');
	mail($email, $subject, $content, $headers);
}

function mailPassDevant($post, $post_prec, $battle, $dbh)
{

	global $urlSite, $code, $_;

	//selection $post
	$postResult = $dbh->prepare("SELECT `token`, `user` FROM `bl_battle_posts` WHERE `id` = :post LIMIT 0,1");
	$postResult->bindParam(':post', $post, PDO::PARAM_STR);
	$postResult->execute();

	if ($postResult->rowCount() > 0) {
		$row_postResult = $postResult->fetch(PDO::FETCH_OBJ);
		$token = $row_postResult->token;
		$user = $row_postResult->user;
		$amis = prenom_user($user, $dbh);
		$urlAmi = url_user($user, $dbh);
		$amis_photo = photo_user($user, $dbh);
		$imagePartage = photo_post_mail($token, $dbh);
	}


	//selection $post_prec
	$postPrecResult = $dbh->prepare("SELECT `token`, `user` FROM `bl_battle_posts` WHERE `id` = :post_prec LIMIT 0,1");
	$postPrecResult->bindParam(':post_prec', $post_prec, PDO::PARAM_STR);
	$postPrecResult->execute();

	if ($postPrecResult->rowCount() > 0) {
		$row_postPrecResult = $postPrecResult->fetch(PDO::FETCH_OBJ);
		$userB = $row_postPrecResult->user;

		$prenom = prenom_user($userB, $dbh);
		$email = email_user($userB, $dbh);
	}

	if ($user == $userB) {
		return;
	}

	//notif
	$notification = '<div class="sh-notif__ligne">
		<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '" class="sh-comments__notif sh-avatar">
		<img src="/' . $amis_photo . '" alt="' . $amis . '"></a>
		<div>
			<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '">' . $_['mailing']['content']['overtook']['sujbect'] . '</a>
			<span>[date]</span>
		</div>
	</div>';
	save_notifs($userB, $notification, $dbh);

	if (empty($email)) {
		return;
	}


	//selection amis
	$amisResult = $dbh->prepare("SELECT 1 FROM `bl_user_friend` WHERE (`user` = :user1 AND `friend` = :user2) OR (`user` = :user2 AND `friend` = :user1) LIMIT 0,1");
	$amisResult->bindParam(':user1', $user, PDO::PARAM_STR);
	$amisResult->bindParam(':user2', $userB, PDO::PARAM_STR);
	$amisResult->execute();

	$message = '<a href="' . $urlSite . '/' . $code . '/' . $_['url_compte'] . '/' . $urlAmi . '">' . $amis . '</a>';


	//bataille		
	$titleBattle = name_battle($battle, $dbh);
	$urlBattle = url_battle($battle, $dbh);

	//email nouveau contenu si par detenteur de la battle
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

	/* Construction du message */
	$body = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="#" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
											<p style="color:#777777;">' . $_['mailing']['hello']($prenom) . ',<br><br>
											' . $_['mailing']['content']['overtook']['body']($message, $titleBattle, $amisResult->rowCount() > 0) . '
												<br><br>
												<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_battle'] . '/' . $urlBattle . '/' . $token . '" style="text-decoration:none;"> 
													<img src="' . $imagePartage . '" width="250"> 
												</a><br><br>
											</p>
											
											<p style="font-size:1rem; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																								
										</td>
									</tr>
								</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>';


	mb_internal_encoding('UTF-8');
	$subject = mb_encode_mimeheader($_['mailing']['content']['overtook']['sujbect']);
	if (alert_user(7, $userB, $dbh) == 1) {
		mail($email, $subject, $body, $headers);
	}
}
