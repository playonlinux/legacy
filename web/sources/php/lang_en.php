<?php
// Locale
$_['code']                    = 'en-EN';
$_['code1']                   = 'en';
$_['codeBis'] 				  = 'en_EN';
$_['direction']               = 'ltr';
$_['date_format_short']       = 'd/m/Y';
$_['date_format_short2']		= '%Y %M %d';
$_['date_format_long']        = 'l dS F Y';
$_['time_format']             = 'h:i:s A';

$_['play'] = 'PLAY';

//lien
$_['url_bienvenue']				= 'welcome';
$_['url_battle']				= 'battle';
$_['url_contenu']				= 'post';
$_['url_theme']					= 'category';
$_['url_user']					= 'citizen';
$_['url_compte']				= 'my-account';
$_['url_search']				= 'search';
$_['url_search_amis']			= 'search-friends';
$_['views']['settings'] = 'settings';
$_['views']['trophies'] = 'trophies';
// $_['views']['friend-request'] = 'friend-request';
//Header
$_['menu_inviter']				= 'Invite friends';
$_['menu_notif']				= 'Notifications';
$_['menu_hall']					= 'Hall of fame';

$_['lang1']						= 'French';
$_['lang2']						= 'English';

$_['menu_condition_1']			= 'How it works';
$_['url_condition_1']			= 'laws-of-the-republike';
$_['menu_condition_2']			= 'FAQ';
$_['menu_condition_3']			= 'Legal Notice';
$_['menu_condition_4']			= 'TOS';
$_['menu_condition_5']			= 'GSCs';
$_['menu_condition_6']			= 'Sign Out';
$_['footer']['privacy']			= 'Privacy and Cookies';
$_['url_terms_of_service'] 	= 'terms-of-service';
$_['url_privacy'] 	= 'privacy-and-cookies';

$_['search']					= 'Search by title, keyword, theme, user...';

//Footer
$_['baseline']					= 'Form of political organization in which the holders of the power exercise it under a mandate conferred by the social body.';


//Accueil

$_['bigtitre_index']			= 'Prove that you hold<br><b>the best content in the world</b>';
$_['titre_index']				= 'Battles of content, all categories, all formats';
$_['ss_index']					= 'Battles in progress';


$_['bloc_1']					= 'SEARCH';
$_['txt_bloc_1']				= 'Find amazing content';
$_['bloc_2']					= 'CHALLENGE';
$_['txt_bloc_2']				= 'Confront them to those of others';
$_['bloc_3']					= 'VOTE';
$_['txt_bloc_3']				= 'Make the best win';
$_['bloc_4']					= 'PROGRESS';
$_['txt_bloc_4']				= 'Win battles and cross levels';

$_['bt_cat']					= 'Suggested Battles';
$_['bt_user']					= 'Most popular battles';
$_['bt_amis']					= 'Friends battles';
$_['bt_date']					= 'Most recent battles';

$_['bt_decouvrez']				= 'Laws of the Republike';

$_['discover_my_battles'] = 'DISCOVER MY BATTLES';
$_['by'] = 'BY';

//BATAILLES / CONTENUS
$_['nb_participants']			= 'Number of citizens';
$_['nb_vues']					= 'Number of views';
$_['partager']					= 'Share';
$_['partager_amis']				= 'Share with your friends';
$_['modif_battle']				= 'Edit this Battle';
$_['battle_close']				= 'Battle completed';

$_['modif_contenu']				= 'Edit this content';
$_['supp_contenu']				= 'Delete this content';
$_['like_contenu']				= 'Like this content';
$_['signaler_contenu']			= 'Report this content';
$_['signaler_comment']			= 'Report this comment';

$_['page_battle']				= 'Battle:';

$_['nb_contenus']				= 'Number of content';
$_['nb_likes']					= 'Number of likes';

$_['return_battle']				= 'Back to Battle';
$_['comment']					= 'Comments';
$_['msg_comment']				= 'Leave your comment here (140 characters max)*';
$_['bt_comment_envoyer']		= 'Send';
$_['bt_entry']					= 'Participate';



//MODIF CONTENUS
$_['edit_contenu']				= 'Edit or enter your title, #keywords (Optional)';
$_['edit_titre']				= 'Edit your title, #keywords (Optional)';

//MODIF BATTLE
$_['edit_catbattle']			= 'Change the theme of your battle*';
$_['edit_titrebattle']			= 'Change the title of your battle';
$_['edit_descbattle']			= 'Change the description of your battle';


//DELETE POST
$_['supp_contenu1']				= 'Do you want to delete this content';
$_['supp_contenu2']				= 'Warning, you will lose your earned likes!';

$_['bt_edit']					= 'Validate';
$_['bt_confirm']				= 'Confirm';


//popup bataille
$_['bt_battle']					= 'LAUNCH A BATTLE';
$_['champ_battle_1']			= 'Insert content*';
$_['champ_battle_2']			= 'Choose your theme*';
$_['champ_battle_3']			= 'Give a name to your Battle (max 50 characters)*';
$_['champ_battle_4']			= 'Give a title to your content (Optional)';
$_['champ_battle_5']			= 'Describe your battle (Optional)';

$_['champ_type1']				= 'Picture';
$_['champ_type2']				= 'Sound';
$_['champ_type3']				= 'Link';
$_['placehoder_type3']			= 'Video link';
$_['champ_type4']				= 'Text';
$_['placehoder_type4']			= 'Type your text here';
$_['placehoder2_type4']			= 'Link (Optional)';

$_['bt_battle_lancer']			= 'Launch your Battle';

$_['titre_battle_inviter']		= 'Invite friends (optional)';
$_['ami_battle_email']			= 'Email friend';
$_['bt_battle_envoyer']			= 'Send';

$_['bt_battle_non']				= 'Later';


//popup contenu
$_['bt_contenu']				= 'JOIN';
$_['post_contenu']				= 'POST YOU CONTENT';

$_['pop_post_1']				= 'Upload a picture';
$_['pop_post_2']				= 'Add a video link';
$_['pop_post_3']				= 'Upload an mp3';
$_['pop_post_4']				= 'Write a text';

$_['champs_post_1']				= 'Link of the video*';
$_['champs_post_2']				= 'Type your text here*';
$_['champs_post_3']				= 'Enter your title, #keywords (Optional)';

$_['bt_publier']				= 'Publish';


//Connexion

$_['creer_compte']				= 'SIGN UP';
$_['compte_nom']				= 'Last name*';
$_['compte_prenom']				= 'First name*';
$_['compte_email']				= 'Email*';
$_['compte_mdp']				= 'Password*: min. 8 characters';
$_['bt_inscription']			= 'Sign up';
$_['bt_connecter']				= 'Sign in';
$_['un_compte']					= 'Already have an account?';

$_['compte_felicitation']		= 'CONGRATS, YOU ARE A CITIZEN OF THE REPUBLIKE AND YOU HAVE EARNED 200 LIKES!';
$_['compte_inviter']			= 'Invite your friends to play with you and get 200 LikeCoins per guest.';


$_['me_connecter']				= 'LOG IN';
$_['connection_email']			= 'Email*';
$_['connection_mdp']			= 'Password*';
$_['bt_souvenir']				= 'Remember me';
$_['se_connecter']				= 'Log in';
$_['mdp_oublie']				= 'Forgot your password?';
$_['bt_creer_compte']			= 'Sign up';
$_['pas_compte']				= 'Don\'t have an account? ';
$_['login_facebook']			= 'Sign in with Facebook';


$_['mdp_oublier']				= 'FORGOT YOUR PASSWORD';
$_['mes_mdp']					= 'An email will be sent to you to reset your password';
$_['mdp_email']					= 'Email*';
$_['bt_envoyer']				= 'Send';
$_['bt_annuler']				= 'Cancel';

$_['categories']['select'] = 'Choose your favorite categories (5 minimum)';
$_['user']['are-you'] = 'Are you';

//PARTAGE BATAILLE
$_['partage_battle']			= 'Share this Battle';
$_['ou_envoyer']				= 'Or send it to';
$_['pas_amis']					= 'You have no friends yet!';
$_['bt_partager']				= 'SHARE';
$_['envoyer_a']					= 'Send to';

$_['bravo_partage']				= 'CONGRATS! content SENT!';


//PARTAGE CONTENU
$_['partage_contenu']			= 'Share this content';


//INVITATION AMI
$_['inviter_amis']				= 'INVITE FRIENDS TO JOIN THE REPUBLIKE';
$_['bt_inviter_amis']			= 'Invite friends';
$_['ou']						= 'or';
$_['bt_trouver_amis']			= 'Find friends';

$_['bravo_invitation']			= 'CONGRATS! YOUR INVITATIONS HAVE BEEN SENT!';


//CONVERTIR LikeCoins
$_['convertir_likecoins']		= 'Convert your LikeCoins to play';
$_['bt_convertir_maintenant']	= 'Convert now';


//SIGNALER CONTENU
$_['titre_signaler']			= 'Report this content';
$_['type_signaler1']			= 'Content off topic';
$_['type_signaler2']			= 'Protected content';
$_['type_signaler3']			= 'Other';
$_['msg_signaler']				= 'Leave your message here (140 characters max)*';
$_['bt_signaler']				= 'Report';

$_['bravo_signalement']			= 'THANK YOU. YOUR REPORT HAS BEEN SENT!';


//THEMES
$_['titre_theme']				= 'Theme:';
$_['result_theme']				= 'Result (s) for the theme:';

$_['bt_batailles']				= 'Battles';
$_['bt_contenus']				= 'content';

$_['bt_aleatoire']				= 'Random';
$_['bt_nblikes']				= 'By number of likes';

//RECHERCHE
$_['titre_search']				= 'Search';
$_['result_search']				= 'Result(s) for:';

$_['bt_participants']			= 'Citizens';

//RECHERCHE AMIS
$_['titre_search_friends']		= 'Find friends';
$_['input_search_friends']		= 'Search by name, pseudo, …';
$_['demande_friend_send']		= 'YOUR FRIEND REQUEST HAS BEEN SENT!';
$_['demande_friend_delete']		= 'YOUR FRIEND REQUEST HAS BEEN CANCELED!';
$_['demande_friend_yes']		= 'CONGRATULATIONS !';
$_['demande_friend_no']		= 'TOO BAD !';

//MON COMPTE // USER
$_['titre_compte']				= 'My account';

$_['nb_points']					= 'Number of points';
$_['nb_likes2']					= 'Number of Likes';
$_['nb_likecoins']				= 'Number of LikeCoinss';

$_['bt_convertir']				= 'Convert';
$_['compte_convertir']			= 'Convert LikeCoins to Likes';

$_['popup_convertir']			= 'CONVERT LIKECOINS TO LIKES';
$_['finaliser_convertir']		= 'Finalize your registration to convert your LikeCoins.';
$_['bt_finaliser']				= 'Finalize now';
$_['bravo_convertir']			= 'CONGRATS, YOU JUST GET NEW LIKES!';


$_['menu_compte1']				= 'my community';
$_['menu_compte2']				= 'my activity';
$_['menu_compte3']				= 'Settings';

$_['ssmenu_compte11']			= 'Friends';
$_['ssmenu_compte12']			= 'Pending invitations';
$_['ssmenu_compte13']			= 'Invite friends';

$_['ssmenu_compte21']			= 'Profile';
$_['ssmenu_compte22']			= 'Themes';
$_['ssmenu_compte23']			= 'Notifications';

$_['ssmenu_compte31']			= 'Battles';
$_['ssmenu_compte32']			= 'content';
$_['ssmenu_compte33']			= 'Trophies';
$_['ssmenu_compte34']			= 'Favorites';

$_['bt_deconnexion']			= 'Disconnect';

$_['menu_left_compte1']			= 'Joined since ';
$_['menu_left_compte2']			= 'Friends';
$_['menu_left_compte3']			= 'Invitations';

//USER
$_['menu_user1']				= 'Friends';
$_['menu_user2']				= 'Themes';
$_['menu_user3']				= 'Battles';
$_['menu_user4']				= 'content';
$_['menu_user5']				= 'Trophies';

$_['ami_depuis']				= 'Friend since ';

$_['bt_devenir']				= 'Friend request';
$_['bt_neplusetreami']			= 'Unfriend';
$_['bt_annulerami']				= 'Cancel friend request';
$_['voir_profil']				= 'See profile';

//FORM PROFIL
$_['titre_form']				= 'MY PROFILE';

$_['form_sexe1']				= 'Female';
$_['form_sexe2']				= 'Male';
$_['form_sexe3']				= 'Other';

$_['form_nom']					= 'Last Name';
$_['form_prenom']				= 'First Name';

$_['form_photo']				= 'Picture';
$_['form_photo1']				= 'Max weight: 200ko - JPG, GIF, PNG, BMP';

$_['form_email']				= 'Email';
$_['form_bio']					= 'Bio';
$_['form_naissance']			= 'Birth date';
$_['form_pays']					= 'Country';

$_['titre_form_mdp']			= 'CHANGE PASSWORD';
$_['form_mdp1']					= 'Password (Min. 8 characters)';
$_['form_mdp2']					= 'Confirm Password';

$_['titre_form_supp']			= 'DELETE MY ACCOUNT';
$_['sstitre_form_supp']			= 'If you delete your account, nobody will anymore be able to see your battles, content or other information on your Republike profile.';
$_['sstitre_form_supp2']		= 'Warning, you are about to delete your account. Are you sure you want to permanently delete your account on Republike?';
$_['bt_form_supp']				= 'Delete';
$_['bt_form_supp2']				= 'Yes, I am sure';
$_['mes_form_supp']				= 'Your account has been removed :-( In a few seconds, you will be redirected to the homepage.';

$_['upload']['avatar'] = 'Upload an avatar';
$_['upload']['image'] = 'Upload an image';
$_['upload']['sound'] = 'Upload a sound';
//FORM CAT
$_['titre_form_cat']			= 'MY THEMES';


//PAGE 404
$_['titre_404']					= 'OUPS! PAGE IN VACATION! ';
$_['sstitre_404']				= 'The page you are looking for can not be found. If the problem persists, report it to contact@republike.io 
or post a new battle about the most buggy websites and you can earn a bonus! ;-)';

//PAGE HALL OF FAME
$_['bt_trophees']				= 'Trophies';
$_['nb_or']						= 'Gold';
$_['nb_argent']					= 'Silver';
$_['nb_bronze']					= 'Bronze';


//COMMENT CA MARCHE
$_['h1titre_comment']			= 'How it works ?';
$_['sstitre_comment']			= 'On Republike you can';

$_['titre_comment']				= 'TITLE';
$_['points_comment']			= 'POINTS';
$_['vendre_comment']			= 'FOR SALE';
$_['prix_comment']				= 'PRICE';
$_['suite_comment']				= 'More to come…';

$_['bloc_comment1']				= 'You claim to hold wonders (pictures, photos, videos, musics, texts, etc.) on a particular subject?<br />
								You want to show the world that you are the best or just to share passions and finds?';


$_['how-it-works']['title'] = '<b>On <b class="majR">R</b>epublike you can</b>';
$_['how-it-works']['launch'] = '<b>Launch Battles of content </b>'; //<br><small>on your favorite topics</small>';
$_['how-it-works']['publish'] = '<b>Post your favorite content </b>'; //<br><small>and win Battles</small>';
$_['how-it-works']['like'] = '<b>Like your favorite content</b>'; //<br><small>to defend them</small>';
$_['how-it-works']['invite'] = '<b>Invite fans and friends </b>'; //<br><small>to participate or to support you</small>';
$_['how-it-works']['share'] = '<b>Battles are open to the whole world</b>'; //<br><small>anyone can see, like, and challenge your content</small>';
$_['how-it-works']['laws-of-republike'] = '<b>LAWS OF THE <b class="majR">R</b>EPUBLIKE</b>';

$_['how-it-works']['quest']['title'] = 'THE QUEST';
$_['how-it-works']['quest']['content'] = 'Become <b>IMPERATOR</b> of the <b class="majR">R</b>epublike and prove that you are the <b>Master of content</b>.
To achieve this <b>Supreme Goal</b>, you’ll have to cross <a href="#" class"how-it-works-link">the Levels</a> of the <b class="majR">R</b>epublike by <b>earning points</b>.';

$_['how-it-works']['points']['title'] = 'EARNING POINTS';
$_['how-it-works']['points']['content'] = 'Participate to Battles by posting your <b>best content</b>. As soon as a content obtains <b>100 £ikes</b>, the Battle is over. The 3 best content receive medals (gold, silver, bronze) and <b>earn points</b>.';

$_['how-it-works']['currency']['title'] = 'CURRENCY';
$_['how-it-works']['currency']['content'] = 'The <b>currency</b> of the <b class="majR">R</b>epublike is the <b>£ikeCoin</b>.To continue playing you will need to <a href="#" class"how-it-works-link">accumulate £ikeCoins</a> in order to buy <b>£ikes</b> and be able to vote.';

$_['how-it-works']['votes']['title'] = 'VOTES';
$_['how-it-works']['votes']['content'] = 'You can vote for one content <b>up to 10 times</b>. Liking one of your content <b>costs you 100 £ikeCoins per vote</b>. Liking ohter citizens content <b>earns you 10 £ikeCoins per vote</b>. <b>Buying 1 £ike costs 10 £ikeCoins</b>.';

$_['how-it-works']['accumulate']['title'] = '<b><span style="color:#fe585c">Accumulate</span><br>£ikeCoins</b>';

$_['how-it-works']['accumulate']['10']['title'] = '<b>10<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['10']['content'] = 'A citizen liked my content<sup>*</sup>. Liking other citizen content<sup>*</sup>. <small><i><sup>*</sup> 10 £ikeCoins received per vote</i></small>';

$_['how-it-works']['accumulate']['50']['title'] = '<b>50<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['50']['content'] = 'Sharing content or Battle outside of the Republike. Reconnection after 12 hours';

$_['how-it-works']['accumulate']['100']['title'] = '<b>100<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['100']['content'] = 'For each new citizen posting a first content on a Battle you have launched';

$_['how-it-works']['accumulate']['200']['title'] = '<b>200<br>£ikeCoins</b>';
$_['how-it-works']['accumulate']['200']['content'] = 'Invite a friend to join the <b class="majR">R</b>epublike (Invitation accepted)';

$_['how-it-works']['levels']['title'] = '<span class="title">THE LEVELS</span><small> of the</small> <b class="majR">R</b>epublike';

$_['mailing']['hello'] = function ($prenom) {
	return 'Hi' . (!empty($prenom)) ? ' ' . $prenom : '';
};
$_['mailing']['user']['signup']['body'] = 'You are nearly done! Click the link below and earn 300 LikeCoins! <br/><br/><em>Without this confirmation, your account will be deleted within 7 days.</em><br/><br/>';
$_['mailing']['user']['signup']['subject'] = 'Finalize your registration';


$_['mailing']['user']['invite']['body'] = function ($prenom) {
	return $prenom . ' wants you to be part of the Republike.<br><br>The game where you can prove that you hold the best contents in the world.';
};
$_['mailing']['user']['invite']['subject'] = function ($prenom) {
	return $prenom . ' souhaite vous inviter sur Republike';
};


$_['mailing']['user']['battle']['body'] = function ($prenom) {
	return $prenom . ' recommends you a battle !';
};
$_['mailing']['user']['battle']['subject'] = function ($prenom) {
	return $prenom . ' recommends you a battle !';
};
$_['mailing']['user']['post']['body'] = function ($prenom) {
	return $prenom . ' recommends you a content !';
};
$_['mailing']['user']['post']['subject'] = function ($prenom) {
	return $prenom . ' recommends you a content !';
};

$_['mailing']['user']['invite']['discover'] = 'DISCOVER';


$_['mailing']['user']['friend-request']['body'] = function ($prenom) {
	return $prenom . ' sends you a friend request.<br />Discover the profile.';
};
$_['mailing']['user']['friend-request']['subject'] = function ($prenom) {
	return $prenom . ' sends you a friend request';
};

$_['mailing']['battle']['lanch']['body'] = function ($prenom) {
	return 'Your friend ' . $prenom . ' is launching a new battle';
};
$_['mailing']['battle']['lanch']['subject'] = function ($prenom) {
	return $prenom . ' lanches a battle';
};


$_['mailing']['content']['post']['body'] = 'A new content was just posted in your battle';
$_['mailing']['content']['post']['subject'] = 'Great, your battles please !';

$_['mailing']['content']['overtook']['body'] = function ($message, $title, $isFriend) {
	return 'Careful ! ' . ($isFriend) ? 'Your friend ' : '' . $message . ' just overtook you in the battle : ' . $title . ' !';
};

$_['mailing']['content']['overtook']['subject'] = 'Your position is threatened';

$_['mailing']['battle']['winners']['first']['subject'] = function ($battleName) {
	return 'You won the battle ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['first']['body'] = function ($battleName) {
	return 'Congratulations, you win the battle <b>' . $battleName . '</b> and you earn 9 points !';
};
$_['mailing']['battle']['winners']['second']['subject'] = function ($battleName) {
	return 'You finished second in the battle ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['second']['body'] = function ($battleName) {
	return 'Congratulations, you finished second in the battle <b>' . $battleName . '</b> and you earn 3 points !';
};
$_['mailing']['battle']['winners']['thrid']['subject'] = function ($battleName) {
	return 'You finished thrid in the battle ' . $battleName . ' ! ';
};
$_['mailing']['battle']['winners']['thrid']['body'] = function ($battleName) {
	return 'Congratulations, you finished third in the battle <b>' . $battleName . '</b> and you earn 1 point !';
};


$_['mailing']['battle']['suggested']['subject'] = 'and more battles you may be interested in...';
$_['mailing']['battle']['suggested']['body'] = 'Battles you may be interessed in : ';

$_['mailing']['footer'] = function ($urlSite) {
	$message = 'This email is automated. For any question, contact us at <a href="mailto:contact@republike.io">contact@republike.io.</a><br>';
	$message .= (!empty($urlSite)) ? '<b> To customize alerts, click <a href="' . $urlSite . '/en/my-account/settings">here</a>.</b><br /><br />' : '';
	$message .= '© 2019 REPUBLIKE';

	return $message;
};


$_['bloc_comment2']				= 'On <b class="rose">REPUBLIKE</b>, you can:<br>
								<b>Launch contest (Battles) of content </b> on your favorite topics<br>
								<b>Publish your content</b> on any Battle<br>
								<b>Like</b> your favorite content to push or defend them<br>
								<b>Invite</b> fans and friends to participate or to support you';

$_['bloc_comment3']				= '<b class="rose">Law 1 :</b> Battles are open to the whole world; everyone can see your content, evaluate and challenge them<br>  
								<b class="rose">Law 2 :</b> The most liked content go up first<br>
								<b class="rose">Law 3 :</b> As soon as a content reaches 100 Likes, the Battle is over; the three most liked content receive medals (gold, silver, bronze)<br>
								<b class="rose">Law 4 :</b> Every Like (other than yours) received by one of your content earns you LikeCoins (the currency of the Republike)<br>
								<b class="rose">Law 5 :</b> To lauch Battles, to invite Friends and to like also earns you <u>LikeCoins</u> <br>
								<b class="rose">Law 6 :</b> Medals award you <u>titles and prestige</u><br>
								<b class="rose">Law 7 :</b> LikeCoins allow you to <u>blow money on Republike</u> <br> 
								<b class="rose">Law 8 :</b> You can like your favorite content, those of others, but yours too! However ATTENTION: <br>
								I. Your stock of Likes is limited,<br />
								II. You can not like more than 10 times the same content, and<br />
								III. To like one of your content costs you 100 LikeCoins while to like the content of another user brings you 10 LikeCoins (by Like)';

$_['bloc_comment4']				= 'Titles and prestige';
$_['bloc_comment5']				= 'Each medal earns you points, <br>
								<i class="repu-laurels bronze"></i><small>(bronze)</small> = 1 point ; <i class="repu-laurels argent"></i><small>(silver )</small> = 3 points ; <i class="repu-laurels or"></i><small>(gold)</small> = 9 points, <br>
								that you cumulate to climb the ranks of the Republike by winning titles:';

$_['bloc_comment6']				= 'Blow cash on Republike';
$_['bloc_comment7']				= 'Earning LikeCoins  ';
$_['bloc_comment8']				= '• You register, you earn <b>200 Likes</b> <br>
								• You complete your profile, you earn <b>300 LikeCoins</b><br>
								• You launch a Battle, each new participant makes you earn  <b>100 LikeCoins</b><br>
								• You share a content or a Battle out of Republike, you earn <b>50 LikeCoins</b><br>
								• You log back in after 12 hours, you earn  <b>50 LikeCoins</b><br>
								• A participant (other than you) like one of your content, you earn <b>10 LikeCoins (by Like obtained)</b><br>
								• You like the content of another user, you earn <b>10 LikeCoins (by Like)</b><br>
								• You refer a friend to Republike, you earn 200 LikeCoins  ';

// PASSWORD

$_['password']['empty_fields'] = 'Please fill every fields !';
$_['password']['pass_not_equals'] = 'Password and confirmation non identical !';
$_['password']['user_not_found'] = 'User not found';

$_['my-account']['notif-email'] = 'EMAIL NOTIFICATIONS';
