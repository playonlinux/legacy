<?php 
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

//UPDATE token connexion
$updateReq = $dbh->prepare("UPDATE `bl_user` SET `connexion`= '' WHERE id = :id_user");
$updateReq->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
$updateReq->execute();

//logs
save_log_ou($_SESSION['id_user'], 29, $adr, $dbh);
			
session_start();
session_unset(); 
unset($_SESSION);
session_destroy(); 
header('Location: ../index.php');
