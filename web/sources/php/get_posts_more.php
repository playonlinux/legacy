<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

$where = "";
$where2 = "";
$number_post = $_POST['number_post'];
$limit = "order by nb_likes desc limit 20 OFFSET " . $number_post;

$class = "col-lg-5ths col-md-4 col-6";
if ($_POST['get'] != '') {
	$user = $_POST['get'];
	$where = "and bl_battle_posts.user = '$user'";
	$class = "col-lg-5ths col-md-4 col-6";

	$limit = "order by nb_likes desc";
}

if ($_POST['mots'] != '') {
	$name = $_POST['mots'];
	$mots = explode(' ', $name); //En separe l'expression en mots cles

	$k = 1;
	$where .= "and ";

	foreach ($mots as $mot) {
		if ($k == count($mots)) {
			$where .= "(`bl_battle_posts`.`title` LIKE '%" . $mot . "%' or `bl_user`.`speudo` LIKE '%" . $mot . "%' or `bl_user`.`nom` LIKE '%" . $mot . "%' or `bl_user`.`prenom` LIKE '%" . $mot . "%')";
		} else {
			$where .= "(`bl_battle_posts`.`title` LIKE '%" . $mot . "%' or `bl_user`.`speudo` LIKE '%" . $mot . "%' or `bl_user`.`nom` LIKE '%" . $mot . "%' or `bl_user`.`prenom` LIKE '%" . $mot . "%') AND ";
		}
		$k++;
	}

	$limit = "order by nb_likes desc";
	$class = "col-lg-5ths col-md-4 col-6";
}

if ($_POST['cat'] != '') {
	$cat = $_POST['cat'];
	$where = " and `bl_battles`.`category` = '$cat'";
	$where2 = "inner join bl_battles on bl_battles.id = bl_battle_posts.battle";
	$class = "col-lg-5ths col-md-4 col-6";
	$limit = "order by nb_likes desc limit 0,100";
}

if ($_POST['number'] != '') {
	$number = $_POST['number'];
	$limit = " order by nb_likes desc limit 0,$number";
	$class = "col-lg-5ths col-md-4 col-6";
}

$select_posts = $dbh->prepare("SELECT *, bl_battle_posts.user as user_post, bl_battle_posts.title as title_post, bl_battle_posts.id as id_posts, bl_battle_posts.token as token_posts,
(SELECT count(id) FROM bl_likes_post WHERE posts = bl_battle_posts.id) as nb_likes
FROM `bl_battle_posts`
inner join bl_user on bl_user.id = bl_battle_posts.user
$where2
WHERE bl_battle_posts.`statut` ='1' $where $limit");
$select_posts->execute();

if ($select_posts->rowCount() > 0) {
	while ($row_posts = $select_posts->fetch(PDO::FETCH_OBJ)) {

		$result .= '<div class="sh-section__item ' . $class . '" id="post_' . $row_posts->id_posts . '">
			<div class="sh-section">
				
				<div class="sh-section__content">';

		if ($row_posts->type == 1) {
			$result .= '<div class="sh-section__image">
						<a href="single_post.php?id=' . $row_posts->token_posts . '">
							<img src="' . $row_posts->post . '" alt="">
						</a>
					</div>';
			$imagePartage = '' . $urlSite . '/' . $row_posts->post;
		}

		if ($row_posts->type == 4) {
			$result .= '<div class="sh-section__image sh-section__image-gif">
						<a href="single_post.php?id=' . $row_posts->token_posts . '">
							<img src="' . $row_posts->post . '" alt="">
						</a>
					</div>';
			$imagePartage = '' . $urlSite . '/' . $row_posts->post;
		}

		if ($row_posts->type == 2) {
			$result .= '<a href="single_post.php?id=' . $row_posts->token_posts . '">
							<iframe width="100%" height="auto" src="' . video($row_posts->post) . '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

						</a>';
			$imagePartage = '' . $urlSite . '/image/partage_video.jpg';
		}

		if ($row_posts->type == 3) {
			$result .= '<a href="single_post.php?id=' . $row_posts->token_posts . '">
							<p class="text-muted mb-2">' . mb_strimwidth($row_posts->post, 0, 100, "[...]") . ' </p>
						</a>';
			$imagePartage = '' . $urlSite . '/image/partage_texte.jpg';
		}

		$result .= '<p><a class="title" href="single_post.php?id=' . $row_posts->token_posts . '">' . $row_posts->title_post . '</a></p>
				</div>
				    
            	<div class="sh-section__footer">';

		if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') {

			$result .= '<a data-toggle="tooltip" data-placement="top" title="Liker le contenu" href="javascript:void(0)"  id="like_' . $row_posts->id_posts . '" data-id="' . $row_posts->id_posts . '" class="likeSave sh-section__btn-like sh-btn-icon">
                            <i class="icon-Favorite_Full"></i><span class="like">' . $row_posts->nb_likes . '</span>
                        </a>';
		} else {

			$result .= '<a data-toggle="tooltip" data-placement="top" title="Liker le contenu" href="javascript:void(0)" data-id="' . $row_posts->id_posts . '" 
                        class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon">
                            <i class="icon-Favorite_Full"></i><span class="like">' . $row_posts->nb_likes . '</span>
                        </a>';
		}

		$result .= '<div  style="margin-left:0;" data-toggle="tooltip" data-placement="top" title="Partager" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon" 
						data-post="' . $row_posts->token_posts . '" data-title="' . $row_posts->title_post . '" data-token="' . $row_posts->token_posts . '" data-image="' . $imagePartage . '">
                        	<i class="icon-Share"></i></span>
                        </div>';

		if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $user) {
			$result .= '<a data-toggle="tooltip" data-placement="left" title="Supprimer ce contenu" href="javascript:void(0)"  id="supp_' . $row_posts->id_posts . '" 
                        data-id="' . $row_posts->id_posts . '" data-name="' . $row_posts->title_post . '"  class="suppPost sh-section__btn-link sh-btn-icon">
                            <i class="fa fa-trash"></i>
                        </a>';
		}

		if (!isset($user)) {
			if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_posts->user_post) {
				$resultLien = 'mon_compte.php';
			} else {
				$resultLien = 'user.php?id=' . $row_posts->user_post . '';
			}
			$result .= '<a data-toggle="tooltip" data-html="true" data-placement="left" title="' . speudo_user($row_posts->user_post, $dbh) . '<br />' . grade_user_between(points_user($row_posts->user_post, $dbh), $dbh) . '" href="' . $resultLien . '" 
                        class="sh-section__btn-link sh-btn-icon">
                            <span class="sh-section__avatar" style=" border-color:' . grade_user_color(points_user($row_posts->user_post, $dbh), $dbh) . '; background-image:url(' . photo_user($row_posts->user_post, $dbh) . ')"></span>
                        </a>';
		}
		$result .= '</div>
                                
			</div>
		</div>';
	}

	$number_post += 20;
	header('Content-Type: application/json');
	echo json_encode(array($result, $number_post));
} else {
	echo 'null';
}
