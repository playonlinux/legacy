<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

if (empty($_SESSION['securite'])) {
	echo '1- non';
	exit;
}

$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);

$userResult->execute();

if (!$userResult->rowCount()) {
	echo '2-non';
	exit;
}

$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
$user = $row_userResult->id;

$user = intval($_POST['user']);
$friend = intval($_POST['friend']);

$userResult = $dbh->prepare("SELECT * FROM `bl_user_demande_friend` WHERE `user` = :user AND `qui` = :ami LIMIT 0,1");
$userResult->bindParam(':user', $user, PDO::PARAM_STR);
$userResult->bindParam(':ami', $friend, PDO::PARAM_STR);
$rs = $userResult->execute();

if ($userResult->rowCount() == 0) {

	$insertReq = $dbh->prepare("INSERT INTO `bl_user_demande_friend` (`user`, `qui`) 
			VALUES ( :user, :ami)");
	$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
	$insertReq->bindParam(':ami', $friend, PDO::PARAM_STR);

	$insertReq->execute();
}

//Mail demande d'amis
if ($user == $friend) {
	echo '3-non';
	exit;
}

//celui qui fait la demande			
$prenom = prenom_user($user, $dbh);
$speudo = speudo_user($user, $dbh);
$email = email_user($user, $dbh);
$photo = photo_user($user, $dbh);
$url = url_user($user, $dbh);

$notification = '<div class="sh-notif__ligne">
		<a href="/' . $code . '/' . $_['url_user'] . '/' . $url . '" class="sh-comments__notif sh-avatar">
			<img src="/' . $photo . '">
		</a>
		<div>
		<a href="/' . $code . '/' . $_['url_user'] . '/' . $url . '">' . $prenom . ' vous a envoyé une demande d\'ami</a>
			<span>[date]</span>
		</div>
	</div>';
save_notifs($friend, $notification, $dbh);

//celui qui reçoit la demande
$email_ami = email_user($friend, $dbh);

if (empty($email_ami)) {
	echo '4-non';
	exit;
}

$prenom_ami = prenom_user($friend, $dbh);


$points = points_user($user, $dbh);
$color = grade_user_color($points, $dbh);
$grade = grade_user_between($points, $code, $dbh);

//email nouveau contenu si par detenteur de la battle
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n";
$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

//MAIL USER 1
/* Construction du message */
$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
				<tbody>
					<tr>
						<td height="73" align="center" valign="top">
							<table width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
								<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
										<tbody>
										<tr>
										<td height="58" valign="middle" style="padding:9px">
											<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr align="center">
													<td valign="middle" style="padding:0px 9px">
													<a href="#" target="_blank">
														<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" 
														width="250" src="' . $urlSite . '/images/logo-republilke-entier.png"  
														style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
													</a>
													</td>
												</tr>
											</tbody>
											</table>
										</td>
										</tr>
										</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
									<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											  <tr align="center">
												<td width="420" height="214" align="center">
												
													<p style="color:#777777;">' . $_['mailing']['hello']($prenom_ami) . ',<br> <br>
													' . $_['mailing']['user']['friend-request']['body']($prenom) . '
													<br></p>
													
													<table width="200" border="0" cellspacing="0" cellpadding="0" align="center" height="100">
													  <tr valign="middle">
														<td height="90" width="100" align="center">
														
															<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_user']	. '/' . $url . '" style="text-decoration:none; border:none;"> 
																<img style="border-radius:50%;outline:none;color:#ffffff;text-decoration:none; border:2px solid ' . $color . ';" 
																src="' . $urlSite . '/' . $photo . '" width="70" border="0" />
															</a>
															
															<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_user']	. '/' . $url . '" style="text-decoration:none; border:none;"> 
																<p style="color:#777777; font-size:13px;">' . $speudo . '<br />' . $grade . '</p>
															</a>
														</td>
														
													  </tr>
													  
												  </table><br />
													  
													<p style="font-size:10px; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																										
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
				</table>';

$to = $email_ami;
//$to='dev@mkael-group.com';
mb_internal_encoding('UTF-8');
$sujet = mb_encode_mimeheader($_['mailing']['user']['friend-request']['subject']($prenom));

mail($to, $sujet, $msg, $headers);
echo 'oui';
