<?php
try {

	require_once('mysql.inc.php');
	require_once('funct_battelike.php');
	securePost();

	if (empty($_SESSION['securite'])) {
		echo 'non';
		exit;
	}

	$request = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
	$request->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
	$request->execute();

	if (!$request->rowCount()) {
		echo 'non';
		exit;
	}

	$row = $request->fetch(PDO::FETCH_OBJ);
	$user = $row->id;


	//BATTLE
	$title_b = $_POST['titleB'];
	$url_b =  hash("sha256", $title_b . time()); //trim(strtolower(str_replace(" ", "-", no_special_character($title_b))));
	$info = $_POST['info'];

	$cat = intval($_POST['cat']);

	$insertReq = $dbh->prepare("INSERT INTO `bl_battles` (`user`, `category`, `title`, `info`, `code`, `url`) 
		VALUES (:user, :cat, :title_b, :info, :code, :url)");
	$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
	$insertReq->bindParam(':title_b', $title_b, PDO::PARAM_STR);
	$insertReq->bindParam(':cat', $cat, PDO::PARAM_STR);
	$insertReq->bindParam(':info', $info, PDO::PARAM_STR);
	$insertReq->bindParam(':code', $code, PDO::PARAM_STR);
	$insertReq->bindParam(':url', $url_b, PDO::PARAM_STR);

	$insertReq->execute();
	$battle = $dbh->lastInsertId();


	//POST
	$type = intval($_POST['type']);
	$title_c = $_POST['titleC'];
	$post = null;

	if (!empty($_POST['text'])) {
		$post = $_POST['text'];
		if (!empty($_POST['lien_text'])) {
			$lien_post = $_POST['lien_text'];
		}
	} elseif (!empty($_POST['lien'])) {
		$post = $_POST['lien'];
	}

	$token = md5(time());
	// var_export($battle);
	// var_export($user);
	// var_export($title_c);
	// var_export($type);
	// var_export($post);
	// var_export($lien_post);
	// var_export($token);
	// die();

	$insertReq = $dbh->prepare("INSERT INTO `bl_battle_posts` (`battle`, `user`, `title`, `type`, `post`, `lien_post`, `token`) 
		VALUES (:battle, :user, :title, :type, :post, :lien_post, :token)");
	$insertReq->bindParam(':battle', $battle, PDO::PARAM_STR);
	$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
	$insertReq->bindParam(':title', $title_c, PDO::PARAM_STR);
	$insertReq->bindParam(':type', $type, PDO::PARAM_STR);
	$insertReq->bindParam(':post', $post, PDO::PARAM_STR);
	$insertReq->bindParam(':lien_post', $lien_post, PDO::PARAM_STR);
	$insertReq->bindParam(':token', $token, PDO::PARAM_STR);

	$insertReq->execute();
	$id_post = $dbh->lastInsertId();

	$dossierEnregistrementImg = "images/posts/";
	$name_file = 'avatar.jpg';

	if (!empty($_FILES['image']['name']) && !empty($_FILES['image']['type'])) {
		// IMAGE principale //
		// on test si le fichier est uploadĂŠ
		$tmp_file = $_FILES['image']['tmp_name'];
		$size_file = $_FILES['image']['size'];
		// renommons le fichier pour plus de sécurité

		$name_file = $_FILES['image']['name'];

		$info = new SplFileInfo($name_file);
		$extension = $info->getExtension();

		$type = mime_content_type($_FILES['image']['tmp_name']);
		$allowed_mime_types = array('image/gif', 'image/jpeg', 'image/png', 'image/bmp');

		$legalExtensions = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP");

		$legalSize = "10000000";

		if (in_array($type, $allowed_mime_types) && ($size_file != 0 && $size_file < $legalSize) && in_array($extension, $legalExtensions)) {

			if (0 < $_FILES['image']['error']) {
				echo 'Error: ' . $_FILES['image']['error'] . '<br>';
			} else {

				$data = file_get_contents($_FILES['image']['tmp_name']);
				$path =  'images/posts/' . hash("sha256", $_FILES['image']['tmp_name'] . time());

				if (file_put_contents('../' . $path, $data, LOCK_EX)) {
					$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `post`= :path WHERE `id` ='$id_post'");
					$updateReq->bindParam(':path', $path, PDO::PARAM_STR);
					$updateReq->execute();
				}
			}
		}
	}

	if (!empty($_FILES['son']['name']) && !empty($_FILES['son']['type'])) {
		// IMAGE principale //
		// on test si le fichier est uploadĂŠ
		$tmp_file = $_FILES['son']['tmp_name'];
		// renommons le fichier pour plus de sécurité

		$name_file = $_FILES['son']['name'];
		$info = new SplFileInfo($name_file);
		$extension = $info->getExtension();

		$type = mime_content_type($_FILES['son']['tmp_name']);
		$allowed_mime_types = array('audio/mpeg', 'audio/mp3');

		if (in_array($type, $allowed_mime_types) && $extension != 'php' && $extension != 'html' && $extension != 'zip' && $extension != 'js' && $extension != 'rar' && $extension != 'exe') {

			if (0 < $_FILES['son']['error']) {

				echo 'Error: ' . $_FILES['son']['error'] . '<br>';
			} else {

				$data = file_get_contents($_FILES['son']['tmp_name']);
				$path =  'images/posts/' . hash("sha256", $_FILES['son']['tmp_name'] . time());

				if (file_put_contents('../' . $path, $data, LOCK_EX)) {
					$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `post`= :path WHERE `id` ='$id_post'");
					$updateReq->bindParam(':path', $path, PDO::PARAM_STR);
					$updateReq->execute();
				}
			}
		}
	}

	//log poste une battle
	save_log_ou($user, '1', $adr, $dbh);

	//mails ami
	$select_amis = $dbh->prepare("SELECT * FROM `bl_user_friend`
		WHERE (user=:id or friend = :id) and statut = 1");
	$select_amis->bindParam(':id', $user, PDO::PARAM_STR);
	$select_amis->execute();

	if ($select_amis->rowCount() > 0) {
		while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) {

			if ($row_amis->user == $user) {
				$userAmis = $row_amis->friend;
			} else {
				$userAmis = $row_amis->user;
			}

			$email_amis = email_user($userAmis, $dbh);

			if (empty($email_amis)) {
				continue;
			}
			$lang = code_user_by_id($userAmis);

			require './lang_' . $lang . '.php';
			$prenom = prenom_user($user, $dbh);
			$photo = photo_user($user, $dbh);

			$prenom_amis = prenom_user($userAmis, $dbh);

			//email nouveau contenu si par detenteur de la battle
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

			$imagePartage = photo_battle_mail($battle, $dbh);

			//MAIL USER 1
			/* Construction du message */
			$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
				<tbody>
					<tr>
						<td height="73" align="center" valign="top">
							<table width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
								<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
									<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
										<tbody>
										<tr>
										<td height="58" valign="middle" style="padding:9px">
											<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr align="center">
													<td valign="middle" style="padding:0px 9px">
													<a href="#" target="_blank">
														<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
														src="' . $urlSite . '/images/logo-republilke-entier.png"  
														style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
													</a>
													</td>
												</tr>
											</tbody>
											</table>
										</td>
										</tr>
										</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
								<tbody>
								<tr>
									<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											  <tr align="center">
												<td width="420" height="214" align="center">
												
													<p style="color:#777777;">' . $_['mailing']['hello']($prenom_amis) . ',<br><br>
													
													' . $_['mailing']['battle']['lanch']['body']($prenom) . '<br /><b>' . $title_b . '</b>.
													<br><br>
													<a target="_blank" href="' . $urlSite . '/' . $lang . '/' . $_['url_battle'] . '/' . $url_b . '" style="text-decoration:none;"> 
															<img src="' . $imagePartage . '" width="250"> 
														</a>
													<br /><br />
													</p>
												
													<p style="font-size:10px; color:#777777;">' . $_['mailing']['footer']($urlSite) . '</p>
																										
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
				</table>';

			$to = $email_amis;
			mb_internal_encoding('UTF-8');
			$sujet = mb_encode_mimeheader($_['mailing']['battle']['lanch']['body']($prenom) . ' : ' . $title_b);

			if (alert_user(2, $userAmis, $dbh) == 1) {
				mail($to, $sujet, $msg, $headers);
			}

			//notif
			$message = '<div class="sh-notif__ligne">
						<a href="/' . $lang . '/' . $_['url_battle'] . '/' . $url_b . '" class="sh-comments__notif sh-avatar">
						<img src="/' . $photo . '" alt="' . $prenom . '"></a>
						<div>
							<a href="/' . $lang . '/' . $_['url_battle'] . '/' . $url_b . '">' . $_['mailing']['battle']['lanch']['body']($prenom) . ' !</a>
							<span>[date]</span>
						</div>
					</div>';
			save_notifs($userAmis, $message, $dbh);
		}
	}

	header('Content-Type: application/json');
	echo json_encode(array($battle, $url_b));
} catch (Exception $e) {
	var_export($e);
}
