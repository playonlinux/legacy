<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();
secureGet();

$message = "";

if (!empty($_SESSION['securite'])) {

	$securite = $_SESSION['securite'];

	$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
	$userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
	$rs = $userResult->execute();

	if ($userResult->rowCount() > 0) {

		$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
		$user = $row_userResult->id;

		$message = $_POST['commentaire'];
		$post = $_POST['post'];

		$insertReq = $dbh->prepare("INSERT INTO `bl_comments_post`(`post`, `user`, `message`) 
		VALUES (:post, :user, :message)");
		$insertReq->bindParam(':user', $user, PDO::PARAM_STR);
		$insertReq->bindParam(':post', $post, PDO::PARAM_STR);
		$insertReq->bindParam(':message', $message, PDO::PARAM_STR);
		$insertReq->execute();

		//celui qui a laissé le commentaire
		$prenom = prenom_user($user, $dbh);
		$photo = photo_user($user, $dbh);

		//celui a qui appartient le contenu
		list($token, $user_post) = info_post($post, $dbh);
		$prenom_post = prenom_user($user_post, $dbh);
		$email_post = email_user($user_post, $dbh);

		$imagePartage = photo_post_mail($token, $dbh);

		//email nouveau commentaire
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

		//MAIL USER 1
		/* Construction du message */
		$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: font-family:Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="#" target="_blank">
												<img  alt="BATTLELIKE" title="BATTLELIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="400" height="214" align="center">
										<p style="color:#777777;">Bonjour ' . $prenom_post . ',<br>
											<br>
											' . $prenom . ' a laissé un commentaire.
											<br> <br />
												<a target="_blank" href="' . $urlSite . '/' . $code . '/' . $_['url_contenu'] . '/' . $token . '" style="text-decoration:none;"> 
													<img src="' . $imagePartage . '" width="250"> 
												</a><br /><br />
											</p>
											
											<p style="font-size:10px; color:#777777;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br />
											<b>Pour paramètrer vos alertes, rendez-vous dans "Mon compte / Paramètres".</b><br /><br />
											© 2019 REPUBLIKE</p>
																								
										</td>
									</tr>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
		</table>';

		$to = $email_post;
		mb_internal_encoding('UTF-8');
		$sujet = mb_encode_mimeheader('On réagit à vos contenus');

		if ($to != '') {
			mail($to, $sujet, $msg, $headers);

			//notif
			$mes = '<div class="sh-notif__ligne">
				<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '" class="sh-comments__notif sh-avatar">
				<img src="/' . $photo . '" alt="' . $prenom . '"></a>
				<div>
					<a href="/' . $code . '/' . $_['url_contenu'] . '/' . $token . '">On réagit à vos contenus</a>
					<span>[date]</span>
				</div>
			</div>';
			save_notifs($user_post, $mes, $dbh);

			echo 'oui';
		} else {
			echo 'non';
		}
	} else {
		echo 'non';
	}
}
