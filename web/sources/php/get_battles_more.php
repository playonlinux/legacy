<?php
require_once ('mysql.inc.php');
require_once ('funct_battelike.php');

$where ="";
$number_post =$_POST['number_post'];
$limit ="order by nb_user desc limit 30 OFFSET ".$number_post;
$class = "col-lg-5ths col-md-4 col-6";

if($_POST['get'] != '') {
	$user=$_POST['get'];
	$where = "and bl_battles.user = '$user'";
	$class = "col-lg-5ths col-md-4 col-6";
}

if($_POST['mots'] != '') {
	$name=$_POST['mots'];
	$mots = explode(' ',$name);//En separe l'expression en mots cles
	
	$k=1;
	$where .= "and ";

	foreach($mots as $mot)
	{
		if($k==count($mots)){
			$where .= "(`bl_battles`.`title` LIKE '%".$mot."%' or `bl_battles`.`info` LIKE '%".$mot."%' or `bl_user`.`speudo` LIKE '%".$mot."%' or `bl_user`.`nom` LIKE '%".$mot."%' or `bl_user`.`prenom` LIKE '%".$mot."%')";
		}else{
			$where .= "(`bl_battles`.`title` LIKE '%".$mot."%' or `bl_battles`.`info` LIKE '%".$mot."%' or `bl_user`.`speudo` LIKE '%".$mot."%' or `bl_user`.`nom` LIKE '%".$mot."%' or `bl_user`.`prenom` LIKE '%".$mot."%') AND ";
		}
		$k++;
	}
	
	$class = "col-lg-5ths col-md-4 col-6";
}

if($_POST['cat'] != '') {
	$cat=$_POST['cat'];
	$where = " and `bl_battles`.`category` = '$cat'";
	$class = "col-lg-5ths col-md-4 col-6";
}

if($_POST['number'] != '') {
	$number=$_POST['number'];
	$limit = " order by nb_user desc limit 0,$number";
	$class = "col-lg-5ths col-md-4 col-6";
}

$select_battles = $dbh->prepare("SELECT *, bl_battles.id as id_battle,  
(SELECT count(distinct(user)) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_user, 
(SELECT sum(likes) FROM bl_battle_posts WHERE battle = bl_battles.id) as nb_like,
(SELECT id FROM bl_battle_posts WHERE battle = bl_battles.id order by likes DESC limit 0,1 ) as id_post
 FROM `bl_battles`
 inner join bl_user on bl_user.id = bl_battles.user
 WHERE bl_battles.`statut` <'3' $where $limit");
$select_battles->execute();

if ($select_battles->rowCount() > 0) {
	while ( $row_battles = $select_battles->fetch(PDO::FETCH_OBJ) ){ 
		list($post, $type) = photo_post($row_battles->id_post, $dbh); 
		
		$class_ami = ''; $class_close ='';
		if(isset($_SESSION['securite']) && $_SESSION['securite'] !=''){ 
			$ami = recherche_ami($_SESSION['id_user'], $row_battles->user, $dbh);
			if($ami==1){
				$class_ami = ' ami ';
			}
		}
		
		$class_cat = '';
		if(isset($_SESSION['securite']) && $_SESSION['securite'] !=''){ 
			$cat = recherche_cat($_SESSION['id_user'], $row_battles->category, $dbh);
			if($cat==1){
				$class_cat = ' cat ';
			}
		}
		
		if($row_battles->statut_battle ==2){ $class_close= "closed";}
		
		$result .='<div class="sh-section__item '.$class.' '.$class_ami.' '.$class_cat.' '.$class_close.'">
			
				<div class="sh-section">
					<a href="battle.php?id='.$row_battles->id_battle.'">
                    	<div class="sh-section__content">';
                        
							if($type ==1){ 
                                $result .='<div class="sh-section__image">
                                    <img src="'.$post.'" alt="">
                                </div>';
								$imagePartage = ''.$urlSite.'/'.$post;
                             } 
                            
                             if($type ==4){ 
                                $result .='<div class="sh-section__image sh-section__image-gif">
                                     <img src="'.$post.'" alt="">
                                </div>';
								$imagePartage = ''.$urlSite.'/'.$post;
                             } 
                            
                            if($type ==2){ 
                                  $result .='<iframe width="100%" height="auto" src="'.video($post).'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
								  $imagePartage = ''.$urlSite.'/image/partage_video.jpg';
                            }
                            
                            if($type ==3){
                                    $result .='<p class="text-muted mb-2">'.mb_strimwidth($post, 0, 100, "[...]").' </p>';
									$imagePartage = ''.$urlSite.'/image/partage_texte.jpg';
                            }
                            
                            $result .='<p>'.$row_battles->title.'</p>
                        </div>
					</a> 
                    <div class="sh-section__footer">
                        <a href="battle.php?id='.$row_battles->id_battle.'">
                            <div data-toggle="tooltip" data-placement="top" title="Nombre de participants" class="sh-section__btn-comment sh-btn-icon">
                                <p><i class="fa fa-user-o"></i> <span class="users  hidden-xs">'.$row_battles->nb_user.' Participants</span> </p>
                            </div>
                            <div data-toggle="tooltip" data-placement="top" title="Nombre de vues" class="sh-section__btn-comment sh-btn-icon">
                                <i class="fa fa-eye"></i><span  class="users">'.$row_battles->vues.'</span>
                            </div>
                        </a>
                        <div data-toggle="tooltip" data-placement="top" title="Partager" class="btn-partage-battle sh-section__btn-follow sh-btn-icon" 
							data-battle="'.$row_battles->id_battle.'" data-title="'.$row_battles->title .'" data-image="'. $imagePartage .'">

                        	<i class="icon-Share"></i></span>
                        </div>
					</div>
				</div>
			  
		</div>';
	
	 } 
	 
	$number_post+=20;
    header('Content-Type: application/json');
	echo json_encode(array($result, $number_post));
	
}else{
	echo 'null';
}
