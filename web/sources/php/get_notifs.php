<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

$now = date("Y-m-d H:i:s");
if (empty($_SESSION['securite'])) {
	exit;
}

$securite = $_SESSION['securite'];

$userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `id` = :id_user LIMIT 0,1");
$userResult->bindParam(':id_user', $_SESSION['id_user'], PDO::PARAM_STR);
// $userResult = $dbh->prepare("SELECT id FROM `bl_user` WHERE `password` = :securite LIMIT 0,1");
// $userResult->bindParam(':securite', $securite, PDO::PARAM_STR);
$userResult->execute();

if (!$userResult->rowCount()) {
	exit;
}

$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);
$user = $row_userResult->id;

if (!empty($_POST['view'])) {
	$updateReq = $dbh->prepare("UPDATE `bl_notifications` SET `statut`= 2 WHERE `user` = :id ");
	$updateReq->bindParam(':id', $user, PDO::PARAM_STR);
	$updateReq->execute();
}

$notifResult = $dbh->prepare("SELECT notif, `date` FROM `bl_notifications` WHERE `user` = :id ORDER BY id desc LIMIT 10");
$notifResult->bindParam(':id', $user, PDO::PARAM_STR);
$notifResult->execute();

$output = '';

if ($notifResult->rowCount() > 0) {
	while ($row_notifResult = $notifResult->fetch(PDO::FETCH_OBJ)) {

		$datetime = new DateTime($row_notifResult->date);
		$notif = str_replace('[date]', '<p>' . ago($datetime) . '</p>', $row_notifResult->notif);
		$output .= $notif;
	}
} else {
	$output .= '<p>Aucune notification</p>';
}

$notifResults = $dbh->prepare("SELECT COUNT(*) AS nb_notifs FROM `bl_notifications` WHERE `user` =:id AND `statut`= 1");
$notifResults->bindParam(':id', $user, PDO::PARAM_STR);
$notifResults->execute();

$row = $notifResults->fetch(PDO::FETCH_OBJ);
echo json_encode([
	'notification' => $output,
	'count_notif'  => $row->nb_notifs
]);
