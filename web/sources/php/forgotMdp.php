<?php
require_once('mysql.inc.php');
require_once('funct_battelike.php');

securePost();

if (empty($_POST['email'])) {
	echo 'non';
	exit;
}

$email = $_POST['email'];

$userResult = $dbh->prepare("SELECT * FROM `bl_user` WHERE email = :email and statut !=3 limit 0,1");
$userResult->bindParam(':email', $email, PDO::PARAM_STR);
$userResult->execute();

if (!$userResult->rowCount()) {
	echo 'non';
	exit;
}

$row_userResult = $userResult->fetch(PDO::FETCH_OBJ);

$userId = $row_userResult->id;
$prenom = $row_userResult->prenom;

$token = password_hash($email, PASSWORD_DEFAULT);

//UPDATE POST LIKES
$updateReq = $dbh->prepare("UPDATE `bl_user` SET `token`= :token WHERE id = :id_user");
$updateReq->bindParam(':token', $token, PDO::PARAM_STR);
$updateReq->bindParam(':id_user', $userId, PDO::PARAM_STR);
$updateReq->execute();


$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n";
$headers .= 'From: "REPUBLIKE"<contact@republike.io>' . "\r\n";

//MAIL USER 1
/* Construction du message */
$msg = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:Tahoma, Geneva, Arial,sans-serif;">
		<tbody>
			<tr>
				<td height="73" align="center" valign="top">
					<table width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="" valign="top" style="padding-top:5px;padding-bottom:5px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
								<tbody>
								<tr>
								<td height="58" valign="middle" style="padding:9px">
									<table align="left" width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr align="center">
											<td valign="middle" style="padding:0px 9px">
											<a href="https://www.republike.io" target="_blank">
												<img  alt="REPUBLIKE" title="REPUBLIKE" align="center" width="250" 
												src="' . $urlSite . '/images/logo-republilke-entier.png"  
												style="padding-bottom:0px; vertical-align:bottom; display:inline!important; max-width:300px">
											</a>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
								</tbody>
							</table>
						</td>
						</tr>
						</tbody>
					</table>
					<table align="center" width="420" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
							<td height="172" valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#777777;">
								
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr align="center">
										<td width="420" height="214" align="center">
										
											<p style="color:#777777;">Bonjour ' . $prenom . ',<br><br>
											vous avez fait une demande de réinitialisation de mot de passe.</p>
											
											<p style="color:#777777;"> Si ce n\'est pas vous, merci de ne pas cliquer sur 
												<em><a target="_blank" href="' . $urlSite . '/password.php?id=' . $userId . '&token=' . base64_encode($token) . '" style="color:#777777;"> ce lien</a></em>.
												<br><br>
											</p>
												
											  <p style="font-size:10px; color:#777777;">Cet email est automatisé. Pour toutes questions, écrivez-nous à contact@republike.io.<br />
												<b>Pour paramètrer vos alertes, rendez-vous dans "Mon compte / Paramètres".</b><br /><br />
												© 2019 REPUBLIKE
											</p>
																								
										</td>
									</tr>
								
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>';

$to = $email;
mb_internal_encoding('UTF-8');
$sujet = mb_encode_mimeheader('Demande de réinitialisation de votre mot de passe - REPUBLIKE');

mail($email, $sujet, $msg, $headers);
echo 'ok';
