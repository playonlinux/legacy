<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

if (!empty($_SESSION['securite'])) {

    $select_user = $dbh->prepare(
        "SELECT id, points, photo, connexion, likes, like_coins, statut, sexe, info, ville, nom, prenom, email,
            DATE_FORMAT(date, '%m/%Y') AS date_format,
            (SELECT COUNT(id) FROM bl_battles WHERE user = U.id) AS nb_battles, 
            (SELECT COUNT(id) FROM bl_battle_posts WHERE user = U.id) AS nb_post,
            (SELECT COUNT(id) FROM bl_battle_posts WHERE user = U.id AND trophee != 0) AS nb_trophees,
            (SELECT COUNT(id) FROM bl_user_favoris WHERE user = U.id) AS nb_favoris,
            (SELECT SUM(likes) FROM bl_battle_posts WHERE user = U.id) AS nb_like,
            DATE_FORMAT(naissance, '%d/%m/%Y') AS naissance_format
        FROM bl_user U
        WHERE id = :id
        LIMIT 0,1
    "
    );

    $select_user->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
    $select_user->execute();

    $row_user = $select_user->fetch(PDO::FETCH_OBJ);

    //theme
    $select_theme = $dbh->prepare("SELECT cat FROM `bl_user_cat` WHERE user = :id");
    $select_theme->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
    $select_theme->execute();
    $mes_themes = array();
    if ($select_theme->rowCount() > 0) {
        while ($row_theme = $select_theme->fetch(PDO::FETCH_OBJ)) {
            $mes_themes[] = $row_theme->cat;
        }
    }

    /* Classement */
    $select_grade = $dbh->prepare("SELECT `points`, `titre_" . $code . "` AS name FROM bl_grade WHERE `points` > :point LIMIT 1");
    $select_grade->bindParam(':point', $row_user->points, PDO::PARAM_STR);
    $select_grade->execute();
    $row_gradeSupp = $select_grade->fetch(PDO::FETCH_OBJ);
    $nbPointGrade = $row_gradeSupp->points;
    $nomPointGrade = $row_gradeSupp->name;

    //logs
    save_log_ou($_SESSION['id_user'], 27, $adr, $dbh);
} else {
    session_start();
    session_unset();
    session_destroy();
    header('Location: /');
}
?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  <?= $_['titre_compte'] ?>">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | <?= $_['titre_compte'] ?></title>

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_compte'] ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/my-account'; ?>
        <link rel="alternate" hreflang="en" href="/en/my-account" />
    <?php } elseif ($code == 'en') {
        $footerFR = '/fr/mon-compte'; ?>
        <link rel="alternate" hreflang="fr" href="/fr/mon-compte" />
    <?php } ?>

    <?php include('required.php'); ?>

    <script>
        var page = "compte";
        var onglet = "";
        var parametre = <?= !empty($_GET['pm']) ? 1 : 0 ?>;
        var trophees = <?= !empty($_GET['trophees']) ? 1 : 0 ?>;
    </script>
</head>

<body>
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main style="padding-top: 110px;">
        <div class="container" style="background-color:#e6e9f1;">
            <!--content head-->
            <div class="row">
                <div class="sh-head-user col-lg-3 col-sm-12 col-3">
                    <div class="sh-head-user__content offset-1">
                        <?php
                        $progess = (!empty($_SESSION['securite'])) ? get_user_progress($_SESSION['id_user']) : '';
                        $color = (!empty($_SESSION['securite'])) ? grade_user_color((points_user($_SESSION['id_user'], $dbh)), $dbh) : '';
                        $colorClass = (!empty($_SESSION['securite'])) ? get_grade_color_class(points_user($_SESSION['id_user'], $dbh)) : '';
                        ?>
                        <div class="text-center sh-head-user__image sh-head-user__profil col-lg-2 col-md-2 col-4">
                            <span class="sh-section__user__image" style="border-color:<?= $color ?>; 
                        background-image:url(/<?= $row_user->photo; ?>);">
                            </span>
                        </div>
                        <div class="row mt-3 mb-3">
                            <div class="col-12">
                                <b style="font-size:2rem"><?= $row_user->nom . ' ' . $row_user->prenom ?></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12"><?= grade_user_between(points_user($_SESSION['id_user'], $dbh), $code, $dbh); ?></div>
                            <div class="col-sm-8 col-11 w3-container">
                                <div class="col-12 w3-light-grey w3-round-xlarge" style="padding: 0">
                                    <div class="w3-container w3-round-xlarge <?= $colorClass ?>" style="width:<?= $progess + 15 ?>%;padding-left:<?= ($progess) ? $progess - 10 : 1 ?>%"><b><?= $progess ?>%</b></div>
                                </div>
                            </div>
                            <div class="col-1" style="font-size: smaller; padding-left:15px;"><?= get_user_next_grade($_SESSION['id_user']); ?></div>

                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 offset-1 mt-5 mb-5">

                    <div class="mb-3">
                        <span class="profil mt-5 mb-0">
                            <a href="/<?= $code ?>/<?= $_['url_condition_1'] ?>" target="_blank" class="sh-btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['nb_likes2'] ?>">
                                <i class="fa fa-heart"></i><span><?= $row_user->likes  ?></span>
                            </a>

                            <a href="/<?= $code ?>/<?= $_['url_condition_1'] ?>" target="_blank" class="sh-btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['nb_likecoins'] ?>">
                                <i class="repu-coins"></i><span><?= $row_user->like_coins  ?></span>
                            </a>
                        </span>

                        <button class="mt-1 sh-btn btn-convertir_action" data-toggle="tooltip" data-placement="top" data-original-title="<?= $_['compte_convertir'] ?>">
                            <i class="fa fa-refresh"></i> <span><?= $_['bt_convertir'] ?></span>
                        </button>

                        <?php if ($row_user->statut == '1') { ?>
                            <div class="sh-convertir">
                                <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                                <div class="sh-upload__content">
                                    <p class="text-center mb-3">
                                        <b><?= $_['finaliser_convertir'] ?></b><br>
                                        <a target="_blank" class="sh-btn center-block m-3" href="/confirme.php?id=<?= $_SESSION['id_user'] ?>&token=<?= $_SESSION['securite'] ?>">
                                            <?= $_['bt_finaliser'] ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="sh-convertir">
                                <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                                <div class="sh-upload__content">
                                    <p class="text-center"><b><?= $_['popup_convertir'] ?></b></p>
                                </div>
                                <div class="sh-upload__content convertisseur">
                                    <div class="sh-upload__form">
                                        <form method="post" action="" name="form_convertir" id="form_convertir" enctype="multipart/form-data">
                                            <div class="row mt-3 mb-4">
                                                <div class="col-lg-5 text-center">
                                                    <p>LIKECOINS</p>
                                                    <input type="number" class="form-control  text-center" value="<?= intdiv($row_user->like_coins, 10) * 10;  ?>" max="<?= intdiv($row_user->like_coins, 10) * 10  ?>" min="0" name="like_coins" id="like_coins" step="10" required>
                                                </div>
                                                <div class="col-lg-2 text-center">
                                                    <h2 class="mt-5"><b>=</b></h2>
                                                </div>
                                                <div class="col-lg-5 text-center">
                                                    <p>LIKES</p>
                                                    <input type="number" class="form-control text-center" value="<?= intdiv($row_user->like_coins, 10)  ?>" max="<?= intdiv($row_user->like_coins, 10)  ?>" min="0" name="likes" id="likes" required>
                                                </div>
                                            </div>
                                            <div class="sh-login__send mt-2">
                                                <button type="submit" class="sh-btn" id="bt_convertir"><?= $_['bt_convertir'] ?></button>
                                            </div>
                                            <div id="errorConvertir" class="text-center"></div>

                                        </form>
                                    </div>
                                </div>

                                <div class="sh-login__content confirm" style="display:none;">
                                    <p class="text-center"><?= $_['bravo_convertir'] ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clear"></div>
                    <p><?= $row_user->info ?></p>
                    <div class="row mt-4 mb-2">
                        <div class="col-lg-12 col-md-12">
                            <p class="mb-3"><i class="fa fa-map-marker"></i> <?= $row_user->ville ?></p>
                            <p class="mb-3"><i class="fa fa-calendar-check-o"></i> <?= $_['menu_left_compte1'] ?> <?= $row_user->date_format ?></p>
                        </div>
                    </div>

                    <div class="sh-head-user__info-communautaire row mt-3 mb-0 ttg-grid-padding--none">
                        <?php $select_amis = $dbh->prepare(
                            "   SELECT friend AS user_id FROM bl_user_friend WHERE user = :id AND statut = 1
                            UNION 
                                SELECT user FROM bl_user_friend WHERE friend = :id AND statut = 1
                            "
                        );
                        $select_amis->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
                        $select_amis->execute();

                        if ($select_amis->rowCount() > 0) { ?>

                            <div class="col-lg-12 col-md-12 mb-2">
                                <p><i class="fa fa-user-o"></i> <?= $_['menu_left_compte2'] ?> (<?= $select_amis->rowCount() ?>)</p>
                            </div>

                            <?php while ($row_amis = $select_amis->fetch(PDO::FETCH_OBJ)) { ?>
                                <!--section-->
                                <div class="col-lg-1 col-md-1 col-sm-1 col-1">
                                    <div class="sh-section-amis">
                                        <div class="sh-section__content">
                                            <a data-toggle="tooltip" data-placement="right" data-original-title="<?= speudo_user($row_amis->user_id, $dbh) ?>" href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_amis->user_id, $dbh) ?> ">
                                                <div class="sh-section__image">
                                                    <span class="sh-section__user__image" style="border-color:<?= grade_user_color(points_user($row_amis->user_id, $dbh), $dbh) ?>;
                                                 background-image:url(/<?= photo_user($row_amis->user_id, $dbh) ?>)"></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        <? }
                        } ?>
                    </div>

                    <div class="sh-head-user__info-communautaire row mt-2 mb-0">
                        <?php
                        $select_demandeAmis = $dbh->prepare("SELECT qui FROM `bl_user_demande_friend` WHERE `user` = :id AND statut = 1 ORDER BY `id` DESC ");
                        $select_demandeAmis->bindParam(':id', $_SESSION['id_user'], PDO::PARAM_STR);
                        $select_demandeAmis->execute();

                        if ($select_demandeAmis->rowCount() > 0) { ?>

                            <div class="col-lg-12 col-md-12 mb-2">
                                <p><i class="fa fa-user-o"></i> <?= $_['menu_left_compte3'] ?> (<?= $select_demandeAmis->rowCount() ?>)</p>
                            </div>

                            <?php while ($row_demandeAmis = $select_demandeAmis->fetch(PDO::FETCH_OBJ)) {
                                    ?>
                                <!--section-->
                                <div id="user_<?= $row_demandeAmis->qui ?>" class="col-lg-1 col-md-1 col-sm-1 col-1">
                                    <div class="sh-section-amis">
                                        <div class="sh-section__content">
                                            <a data-toggle="tooltip" data-placement="right" data-original-title="<?= speudo_user($row_demandeAmis->qui, $dbh) ?>" href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_demandeAmis->qui, $dbh) ?>">
                                                <div class="sh-section__image">
                                                    <span class="sh-section__user__image" style="border-color:<?= grade_user_color(points_user($row_demandeAmis->qui, $dbh), $dbh) ?>; 
                                                    background-image:url(/<?= photo_user($row_demandeAmis->qui, $dbh) ?>)"></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        <? }
                        } ?>
                    </div>

                    <button type="submit" class="sh-btn sh-content-head__btn-tab mb-2 btn-inviter"> <?= $_['ssmenu_compte13'] ?></button>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#fff;">
                    <div class="col-sm-3 col-2 pull-right" style="background-color:#fff;">
                        <div class="row">
                            <div class="sh-head-user__info col-lg-9 col-md-9 col-9 offset-3  mt-4 mb-4">
                                <div class="sh-head-user__info-head mb-0">
                                    <div class="sh-head-user__name">
                                        <a href="javascript:void(0)" class="pull-right menu parametre_bt sh-header__parametres <?= !empty($_GET['pm']) ? 'active' : ''; ?>" data-id="profil">
                                            <i class="fa fa-cog"></i> <?= $_['menu_compte3'] ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sh-parametres">
                            <div class="sh-user__head">
                                <p></p>
                                <a href="javascript:void(0)" class="sh-parametres__close sh-btn-icon">
                                    <i class="fa fa-close"></i>
                                </a>
                            </div>
                            <div class="sh-parametres__content">
                                <ul class="sh-parametres__dropdown" style="display: block;">
                                    <li><a href="javascript:void(0);" class="sous-menu" data-menu="profil_menu" data-id="profil"><?= $_['ssmenu_compte21'] ?></a></li>
                                    <li><a href="javascript:void(0);" class="sous-menu" data-menu="profil_menu" data-id="themes"><?= $_['ssmenu_compte22'] ?></a></li>
                                    <li><a href="javascript:void(0);" class="sous-menu" data-menu="profil_menu" data-id="parametres"><?= $_['ssmenu_compte23'] ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="sh-content-head header-battle">
                        <div class="sh-content-head__btns text-center hidden-xs">
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2 active" data-id="defis" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte31'] ?> (<?= $row_user->nb_battles ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="posts" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte32'] ?> (<?= $row_user->nb_post ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="trophees" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte33'] ?> (<?= $row_user->nb_trophees ?>)</a>
                            <a href="javascript:void(0)" class="menu sh-btn sh-content-head__btn-sstab mb-2" data-id="favoris" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte34'] ?> (<?= $row_user->nb_favoris ?>)</a>
                        </div>
                        <select class="sh-content-head__btns visible-xs menu col-sm-12 form-control2">
                            <option class="active" data-id="defis" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte31'] ?> (<?= $row_user->nb_battles ?>)</option>
                            <option data-id="posts" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte32'] ?> (<?= $row_user->nb_post ?>)</option>
                            <option data-id="trophees" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte33'] ?> (<?= $row_user->nb_trophees ?>)</option>
                            <option data-id="favoris" data-user="<?= $row_user->id ?>"><?= $_['ssmenu_compte34'] ?> (<?= $row_user->nb_favoris ?>)</option>
                        </select>
                    </div>
                    <div>
                        <!-- PARTIE NOTIFS -->
                        <div class="sh-section__profil row tab-content" style="display:none;" id="profil">
                            <div class="col-lg-10 offset-lg-1 mt-5 ">
                                <div class="row">
                                    <div class="col-lg-12 mt-3 mb-3">
                                        <h4 class="text-center center-block">
                                            <b><?= $_['titre_form'] ?></b>
                                        </h4>
                                    </div>
                                </div>

                                <!--section-->
                                <div class="sh-section__edit-post">
                                    <form method="post" action="" name="form_save_profil" id="form_save_profil" enctype="multipart/form-data" style="width:100%">
                                        <!--section-->
                                        <div class="sh-confirm__content row">
                                            <div class="col-lg-12 mb-3">
                                                <div class="sh-confirm__form">
                                                    <label class="sh-radio"> <input type="radio" name="sexe" value="1" <?php if ($row_user->sexe == 1) {
                                                                                                                            echo "checked";
                                                                                                                        } ?>> <span></span>
                                                        <p><?= $_['form_sexe1'] ?></p>
                                                    </label>
                                                    <label class="sh-radio"> <input type="radio" name="sexe" value="2" <?php if ($row_user->sexe == 2) {
                                                                                                                            echo "checked";
                                                                                                                        } ?>> <span></span>
                                                        <p><?= $_['form_sexe2'] ?></p>
                                                    </label>
                                                    <label class="sh-radio"> <input type="radio" name="sexe" value="3" <?php if ($row_user->sexe == 3) {
                                                                                                                            echo "checked";
                                                                                                                        } ?>> <span></span>
                                                        <p><?= $_['form_sexe3'] ?></p>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_nom'] ?></p>
                                                    <input type="text" class="form-control" placeholder='Nom*' name="nom" id="nom" value="<?= $row_user->nom ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_prenom'] ?></p>
                                                    <input type="text" class="form-control" placeholder='Prénom*' name="prenom" id="prenom" value="<?= $row_user->prenom ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <p><?= $_['form_photo'] ?></p>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="sh-confirm__form">
                                                    <div class="sh-confirm__image">
                                                        <span class="sh-section__confirm__image" style="background-image:url(/<?= $row_user->photo; ?>)"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-10">
                                                <div class="sh-confirm__form">
                                                    <p><em><?= $_['form_photo1'] ?></em></p>
                                                    <input type="file" name="photo" id="file" class="form-control inputfile" />
                                                    <label for="file">
                                                        <i class="fa fa-upload">&nbsp;<?= $_['upload']['avatar'] ?></i>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="sh-confirm__content row">
                                            <div class="col-lg-12">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_email'] ?></p>
                                                    <input type="email" class="form-control" placeholder='<?= $_['form_email'] ?>*' name="email" id="email" value="<?= $row_user->email ?>">
                                                </div>
                                            </div>

                                            <div class="col-lg-12 mb-4">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_bio'] ?></p>
                                                    <textarea name="info" id="info" class="form-control" placeholder="<?= $_['form_bio'] ?>"><?= $row_user->info ?></textarea>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_naissance'] ?></p>
                                                    <input type="text" class="form-control" placeholder='<?= $_['form_naissance'] ?> - 01/01/1970' name="naissance" id="naissance" value="<?= $row_user->naissance_format ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_pays'] ?></p>
                                                    <input type="text" class="form-control" placeholder='<?= $_['form_pays'] ?>' name="ville" id="ville" value="<?= $row_user->ville ?>">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">

                                                <div class="sh-confirm__send">
                                                    <button type="submit" class="sh-btn sh-btn-rose" id="bt_save_profil"><?= $_['bt_edit'] ?></button>
                                                </div>
                                                <div id="errorSave" class="text-center"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr />

                                <div class="row">
                                    <div class="col-lg-12 mt-3 mb-3">
                                        <h4 class="text-center center-block">
                                            <b><?= $_['titre_form_mdp'] ?></b>
                                        </h4>
                                    </div>
                                </div>
                                <div class="sh-section__edit-post">
                                    <form method="post" action="" name="form_save_mdp" id="form_save_mdp" enctype="multipart/form-data" style="width:100%">
                                        <!--section-->
                                        <div class="sh-confirm__content row">

                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_mdp1'] ?></p>
                                                    <div class="sh-input-copy">
                                                        <input type="password" name="pass" class="form-control" autocomplete="off" min="8" required>
                                                        <a href="#" class="show-password"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="sh-confirm__form">
                                                    <p><?= $_['form_mdp2'] ?></p>
                                                    <div class="sh-input-copy">
                                                        <input type="password" name="pass_confirm" class="form-control" min="8" autocomplete="off" required>
                                                        <a href="#" class="show-password"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">

                                                <div class="sh-confirm__send">
                                                    <button type="submit" class="sh-btn sh-btn-rose pull-right" id="bt_save_mdp"><?= $_['bt_edit'] ?></button>
                                                </div>
                                                <div id="errorSaveMdp" class="text-center"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <hr />

                                <div class="row">
                                    <div class="col-lg-12 mt-3 mb-3">
                                        <h4 class="text-center  center-block">
                                            <b><?= $_['titre_form_supp'] ?></b>
                                        </h4>
                                    </div>
                                </div>
                                <div class="sh-section__edit-post">
                                    <p><?= $_['sstitre_form_supp'] ?></p>

                                    <div class="sh-confirm__content">
                                        <div class="sh-confirm__form">
                                            <div class="sh-confirm__send">
                                                <a class="sh-btn sh-btn-rose pull-right bt_supp_compte" style="text-align: center;"><?= $_['bt_form_supp'] ?></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sh-supp-compte">
                                        <div class="sh-upload__logo"><a href="./" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                                        <div class="sh-upload__content">
                                            <p class="text-center"><b><?= $_['titre_form_supp'] ?></b></p>
                                        </div>
                                        <div class="sh-upload__content form">
                                            <div class="sh-upload__form">
                                                <form method="post" action="" name="form_supp_compte" id="form_supp_compte" enctype="multipart/form-data" style="width:100%">
                                                    <!--section-->
                                                    <div class="sh-confirm__content row">

                                                        <div class="col-lg-12">
                                                            <div class="sh-confirm__form">
                                                                <p><?= $_['sstitre_form_supp2'] ?></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="sh-confirm__content">
                                                        <div class="sh-confirm__form">
                                                            <div class="sh-confirm__send">
                                                                <button type="submit" class="sh-btn sh-btn-rose pull-right" id="bt_supp_compte"><?= $_['bt_form_supp2'] ?></button>
                                                            </div>
                                                            <div id="errorSuppCompte"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="sh-login__content confirm" style="display:none;">
                                            <p class="text-center"><?= $_['mes_form_supp'] ?></p>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!-- PARTIE THEME -->
                        <div class="sh-section__profil row tab-content" style="display:none;" id="themes">
                            <div class="col-lg-12  mt-5">
                                <div class="row">
                                    <div class="col-lg-12 mt-3 mb-3">
                                        <h4 class="text-center  center-block">
                                            <b><?= $_['titre_form_cat'] ?></b>
                                        </h4>
                                    </div>
                                </div>

                                <!--section-->
                                <div class="sh-section__edit-post">
                                    <form method="post" action="" name="form_save_theme" id="form_save_theme" enctype="multipart/form-data" style="width:100%">
                                        <div class="sh-confirm__content row">
                                            <?php $select_cats = $dbh->prepare("SELECT id, name_" . $code . " as name, visuel FROM `bl_categories` WHERE `statut` = '1' order by name_" . $code . " asc ");
                                            $select_cats->execute();

                                            if ($select_cats->rowCount() > 0) {
                                                while ($row_cats = $select_cats->fetch(PDO::FETCH_OBJ)) { ?>
                                                    <!--section-->
                                                    <div class="sh-section__item col-lg-2 col-md-4 col-6">
                                                        <div class="sh-section">

                                                            <div class="sh-section__content">
                                                                <div class="sh-section__image">
                                                                    <label class="sh-checkbox-cat">
                                                                        <input type="checkbox" <?php if (in_array($row_cats->id, $mes_themes)) {
                                                                                                            echo "checked";
                                                                                                        } ?> name="cat[]" id='<?= $row_cats->id ?>' value="<?= $row_posts->id ?>">
                                                                        <span>R</span>

                                                                        <?php if ($row_cats->visuel != '') { ?>
                                                                            <img src="/<?= $row_cats->visuel ?>" alt="<?= $row_cats->name ?>">
                                                                        <?php } else { ?>
                                                                            <img src="/images/logo-republilke.png" alt="<?= $row_cats->name ?>">
                                                                        <?php } ?>

                                                                    </label>
                                                                </div>


                                                            </div>
                                                            <div class="sh-section__footer text-center">
                                                                <p class="text-center" style="width:100%;"><?= $row_cats->name ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <? }
                                            } ?>
                                        </div>

                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">
                                                <div class="sh-confirm__send">
                                                    <button type="submit" class="sh-btn sh-btn-rose pull-right" id="bt_save_theme"><?= $_['bt_edit'] ?></button>
                                                </div>
                                                <div id="errorSaveCat"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- PARTIE NOTIFS -->
                        <div class="sh-section__profil row tab-content" style="display:none" id="parametres">
                            <div class="col-lg-10 offset-lg-1  mt-5">
                                <div class="row">
                                    <div class="col-lg-12 mt-3 mb-3">
                                        <h4 class="text-center  center-block">
                                            <b><?= $_['my-account']['notif-email'] ?></b>
                                        </h4>
                                    </div>
                                </div>

                                <!--section-->
                                <div class="sh-section__edit-post">
                                    <form method="post" action="" name="form_save_parametre" id="form_save_parametre" enctype="multipart/form-data" style="width:100%">
                                        <!--section-->
                                        <div class="sh-confirm__content row">
                                            <?php
                                            // $select_alerts = $dbh->prepare("SELECT titre_fr, opt1, opt2, opt3, opt4, opt5 FROM `bl_alerts` WHERE `statut` = 1 ORDER BY id ASC");
                                            $select_alerts = $dbh->prepare("SELECT titre_" . $code . " alert_name, opt1, opt5 FROM `bl_alerts` WHERE `statut` = 1 ORDER BY id ASC");
                                            $select_alerts->execute();

                                            if ($select_alerts->rowCount() > 0) {
                                                $num_alert = 1;
                                                while ($row_alerts = $select_alerts->fetch(PDO::FETCH_OBJ)) { ?>
                                                    <div class="col-lg-12 mb-3">
                                                        <div class="sh-confirm__form">
                                                            <div class="row">
                                                                <div class="col-6" style="font-size:1.8rem;"><?= $row_alerts->alert_name ?></div>
                                                                <div class="col-6 text-right">
                                                                    <label class="switch">
                                                                        <input type="checkbox" class="chk_alert" name="alert_<?= $num_alert ?>" id="alert_<?= $num_alert ?>" <?= ($option == $row_alerts->opt1) ? "checked" : "" ?>>
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <!-- <?php $option = alert_option_user($_SESSION['id_user'], $num_alert, $dbh); ?> -->
                                                            <!-- <label class="sh-radio">
                                                                <input type="radio" name="alert_<?= $num_alert ?>" <?php if ($option == $row_alerts->opt1) {
                                                                                                                                echo "checked";
                                                                                                                            } ?> value="<?= $row_alerts->opt1 ?>"> <span></span>
                                                                <p><?= alert_option($row_alerts->opt1, $dbh) ?></p>
                                                            </label> -->
                                                            <!-- <label class="sh-radio"> <input type="radio" name="alert_<?= $num_alert ?>" <?php if ($option == $row_alerts->opt2) {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?> value="<?= $row_alerts->opt2 ?>"> <span></span>
                                                                <p><?= alert_option($row_alerts->opt2, $dbh) ?></p>
                                                            </label>
                                                            <label class="sh-radio"> <input type="radio" name="alert_<?= $num_alert ?>" <?php if ($option == $row_alerts->opt3) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?> value="<?= $row_alerts->opt3 ?>"> <span></span>
                                                                <p><?= alert_option($row_alerts->opt3, $dbh) ?></p>
                                                            </label>
                                                            <label class="sh-radio"> <input type="radio" name="alert_<?= $num_alert ?>" <?php if ($option == $row_alerts->opt4) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?> value="<?= $row_alerts->opt4 ?>"> <span></span>
                                                                <p><?= alert_option($row_alerts->opt4, $dbh) ?></p>
                                                            </label> -->

                                                            <!-- <label class="sh-radio"> <input type="radio" name="alert_<?= $num_alert ?>" <?php if ($option == $row_alerts->opt5) {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?> value="<?= $row_alerts->opt5 ?>"> <span></span>
                                                                <p><?= alert_option($row_alerts->opt5, $dbh) ?></p>
                                                            </label> -->
                                                        </div>
                                                        <hr />
                                                    </div>
                                            <? $num_alert++;
                                                }
                                            } ?>
                                        </div>

                                        <div class="sh-confirm__content">
                                            <div class="sh-confirm__form">

                                                <div class="sh-confirm__send">
                                                    <button type="submit" class="sh-btn sh-btn-rose" id="bt_save_parametre"><?= $_['bt_edit'] ?></button>
                                                </div>
                                                <div id="errorSaveParametre"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div>

                        <div class="row tab-content" id="defis" data-user="<?= $row_user->id ?>"></div>
                        <div class="row tab-content" id="posts" data-user="<?= $row_user->id ?>" style="display:none;"></div>
                        <div class="row tab-content" id="trophees" data-user="<?= $row_user->id ?>" style="display:none;"></div>
                        <div class="row tab-content" id="favoris" data-user="<?= $row_user->id ?>" style="display:none;"></div>

                        <!-- SUPP POST -->
                        <!-- <div class="sh-supp">
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-login__content">
                                <div class="sh-login__form text-center mt-1 mb-5">
                                    <?= $_['supp_contenu1'] ?> <br>"<b><span id="postName"></span></b>" ?
                                    <br><br><?= $_['supp_contenu2'] ?>
                                </div>
                                <div class="sh-login__send">
                                    <input type="hidden" name="id_post" id="id_post" required>
                                    <button type="submit" class="sh-btn" id="suppPost"><?= $_['bt_confirm'] ?></button>
                                </div>
                            </div>
                        </div> -->

                        <div class="sh-popup-post">
                            <div class="sh-popup__content" id="contenu_post"></div>
                        </div>

                        <!-- edit POST -->
                        <!-- <div class="sh-editPost" >
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-upload__content" >
                                <div class="sh-upload__form"> 
                                    <form method="post" name="form_edit_post" id="form_edit_post" enctype="multipart/form-data">
                                        <p><?= $_['edit_contenu'] ?></p>
                                        <input class="form-control mt-3" placeholder="<?= $_['edit_titre'] ?>" name="name_post" id="name_post"type="text">
                                        <input type="hidden" name="id_post" id="id_post" required>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn mx-auto" id="editPost"><?= $_['bt_edit'] ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->

                        <!-- edit Battle -->
                        <!-- <div class="sh-editBattle" >
                            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                            <div class="sh-upload__content" >
                                <div class="sh-upload__form"> 
                                    <form method="post" name="form_edit_battle" id="form_edit_battle" enctype="multipart/form-data">
                                        <p><?= $_['edit_catbattle'] ?></p>
                                        <?php $select_cat = $dbh->prepare("SELECT id, name_" . $code . " as name  FROM `bl_categories` WHERE `statut` = '1' order by name_" . $code . " ");
                                        $select_cat->execute(); ?>
                                        <select class="form-control mt-3 mb-4" name="cat_battle">
                                            <?php if ($select_cat->rowCount() > 0) {
                                                while ($row_cat = $select_cat->fetch(PDO::FETCH_OBJ)) { ?>
                                                <option value="<?= $row_cat->id ?>"><?= $row_cat->name ?></option>
                                                <?php }
                                                } ?>
                                        </select>
                                        <p><?= $_['edit_titrebattle'] ?></p>
                                        <input class="form-control mt-3" placeholder="<?= $_['champ_battle_3'] ?>" name="name_battle" type="text">
                                        <p><?= $_['edit_descbattle'] ?></p>
                                        <textarea name="info_battle" id="info_battle" class="form-control mb-4" placeholder="<?= $_['champ_battle_5'] ?>"></textarea>
                                        <input type="hidden"  name="id_battle" id="id_battle" required>
                                        <div class="sh-login__send">
                                            <button type="submit" class="sh-btn mx-auto" id="editBattle"><?= $_['bt_edit'] ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->

                    </div>

                    <!--Inviter amis-->
                    <div class="sh-inviter">
                        <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>

                        <div class="sh-upload__content invitation">
                            <div class="sh-upload__form">
                                <form method="post" action="" name="form_save_inviter" id="form_save_inviter" enctype="multipart/form-data">

                                    <p class="text-center m-3"><b><?= $_['inviter_amis'] ?></b></p>

                                    <div class="row">
                                        <div class="col-lg-6 mt-3 mb-3">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 1*" name="email1" required type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 3" name="email3" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mt-3 mb-3">
                                            <div class="sh-confirm__content">
                                                <div class="sh-confirm__form">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 2" name="email2" type="text">
                                                    <input class="form-control" placeholder="<?= $_['ami_battle_email'] ?> 4" name="email4" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input class="form-control" name="user" value="<?= $_SESSION['id_user'] ?>" required type="hidden">


                                    <div class="sh-login__send mb-1">
                                        <button type="submit" class="sh-btn sh-btn-rose" id="bt-inviter"><?= $_['bt_inviter_amis'] ?></button>
                                    </div>
                                    <p class="text-center m-3"><b><?= $_['ou'] ?></b></p>
                                    <div class="sh-login__send">
                                        <a href="/<?= $code ?>/<?= $_['url_search_amis'] ?>" class="sh-btn sh-btn-rose"><?= $_['bt_trouver_amis'] ?></a>
                                    </div>
                                    <div class="errorLogInviter"></div>
                                </form>
                            </div>
                        </div>

                        <div class="sh-login__content confirm" style="display:none;">
                            <p class="text-center"><?= $_['bravo_invitation'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/profil']);
        });
    </script>
</body>

</html>