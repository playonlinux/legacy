<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
  <meta name="description" content="<?= $nameSite ?> |  Confidentialité et cookies">
  <meta name="author" content="battlelike.com">
  <title><?= $nameSite ?> | Confidentialité et cookies</title>

  <link rel="canonical" href="/<?= $code ?>/privacy-and-cookies" />
  <?php if ($code == 'fr') {
    $footerEN = '/en/privacy-and-cookies'; ?>
    <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
  <?php } else if ($code == 'en') {
    $footerFR = '/fr/confidentialite-et-cookies'; ?>
    <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
  <?php } ?>

  <?php include('required.php'); ?>

</head>

<body>
  <?php include('header.php'); ?>
  <main style="padding-top: 13rem;">
    <div class="container">
      <h1 style="text-align:center">
        <strong>Politique de confidentialité</strong>
      </h1>
      <br>
      <p style="text-align:justify">
        Republike souhaite vous informer de manière claire et transparente des
        données personnelles qui peuvent être collectées lors de votre navigation
        sur le site, ainsi que de la finalité de leur traitement.
      </p>
      <br>
      <h2 style="text-align:justify">
        <strong>1. Vos données de compte personnel</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        Lors de votre inscription (formulaire d’inscription) et, à tout moment sur
        votre compte personnel, vous pourrez compléter/modifier :
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            votre nom, prénom, âge, sexe et adresse électronique, et
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            d’autres informations permettant d’affiner votre profil personnel
            (ex. photo de profil, pays d’habitation).
          </p>
        </li>
      </ul>
      <br>
      <h2 style="text-align:justify">
        <strong>2. Vos données de navigation</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        Lors de votre navigation, le site pourra également collecter les
        informations suivantes vous concernant :
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            Fréquence et durée de navigation ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Contenus publiés, contenus partagés, contenus Likés, contenus
            signalés etc. ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Pages visités etc.
          </p>
        </li>
      </ul>
      <br>
      <h2 style="text-align:justify">
        <strong>3. Finalités du traitement</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        Le traitement de ces données répond aux finalités suivantes :
      </p>
      <ul>
        <li>
          <p style="text-align:justify">
            Authentifier les utilisateurs du site, administrer le site et
            optimiser l’expérience utilisateur à travers la personnalisation du
            site ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Permettre au site de contacter les utilisateurs pour, notamment,
            promouvoir et optimiser l’utilisation du site ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Etablir des analyses statistiques anonymes mesurant notamment
            l’audience et la qualification de l’audience du site afin
            d’améliorer ses services ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Etablir des analyses comportementales anonymes destinées à la
            vente, lesquelles analyses pourront notamment étudier les
            corrélations entre des profils et des comportements ;
          </p>
        </li>
        <li>
          <p style="text-align:justify">
            Personnaliser la publicité sur le site en fonction du profil de
            chaque utilisateur (publicité ciblée en fonction de ses données
            personnelles et/ou de ses données de navigation) afin de n’afficher
            que les contenus publicitaires susceptibles de l’intéresser et/ou
            permettre aux partenaires du site d’adresser des offres directement
            aux utilisateurs enregistrés dans la base de donnée du site.
          </p>
        </li>
      </ul>
      <br>
      <h2 style="text-align:justify">
        <strong>4. Les cookies</strong>
      </h2>
      <br>
      <p style="text-align:justify">
        Les données ci-dessus pourront notamment être collectées à l’aide de<em>cookies</em>, qu’ils soient enregistrés par nous ( <em>cookies propriétaires</em>) et/ou par des tiers (<em>cookies tiers</em>
        ). Les <em>cookies tiers</em> sont par exemple ceux qu’installent une régie
        ou une agence publicitaire pour notamment (i) afficher des publicités
        pertinentes pour vous en fonction de vos centres d'intérêts, (ii) limiter
        le nombre de fois où une même publicité vous sera adressée, et (iii)
        mesurer l'efficacité d'une campagne publicitaire. Les liens de partage vers
        Facebook, Twitter et les autres réseaux sociaux vous permettent quant à eux
        de partager des contenus trouvés sur Republike sur lesdits réseaux sociaux.
        Lorsque vous utilisez ces boutons de partage, un <em>cookie tiers</em> est
        (ou sera) installé par le réseau social concerné.
      </p>
      <p style="text-align:justify">
        Un cookie (ou témoin de connexion) est un ensemble de données stocké sur le
        disque dur de votre terminal (ordinateur, tablette, mobile) par le biais de
        votre logiciel de navigation à l'occasion de la consultation d'un service
        en ligne. Les cookies enregistrés (ou qui seront enregistrés) par nous ( <em>cookies propriétaires</em>) ou par des tiers (<em>cookies tiers</em>)
        lorsque vous visitez notre site reconnaissent l'appareil que vous êtes en
        train d'utiliser. Ces cookies, loin de causer un quelconque dommage à votre
        appareil, permettent par exemple de plus facilement retrouver vos
        préférences de configuration, de pré-remplir certains champs et d'adapter
        le contenu de nos services.
      </p>
      <p style="text-align:justify">
        Vous seul(e) choisissez si vous souhaitez avoir des cookies enregistrés sur
        votre appareil, sachant que vous pouvez facilement refuser et/ou supprimer
        l'enregistrement de tout ou partie de ces cookies. Toutefois, il est
        important de noter que tout paramétrage que vous pouvez entreprendre sera
        susceptible d’altérer la fluidité de votre navigation sur Republike ainsi
        que l'accès à certaines des fonctionnalités proposées. Pour paramétrer vos
        cookies, vous pouvez configurer votre logiciel de navigation de manière à
        ce que des cookies soient enregistrés dans votre terminal ou, au contraire,
        qu'ils soient rejetés. Certains navigateurs vous permettent de définir des
        règles pour gérer les cookies site par site. Pour la gestion des cookies,
        la configuration de chaque navigateur est différente. Elle est généralement
        décrite dans le menu d'aide de votre navigateur, qui vous permettra de
        savoir de quelle manière modifier vos souhaits.
      </p>
      <p style="text-align:justify">
        Notez que cette procédure n'empêchera pas l'affichage des<em>publicités contextuelles</em> (dites aussi <em>publicités sémantiques</em>) sur les sites Internet que vous visitez.
        Elle ne bloquera que les publicités ciblées et personnalisées en fonction
        de vos données personnelles.
      </p>
      <br>
      <h2 style="text-align:justify">

        <strong>5. Modification de notre politique de confidentialité</strong>
        </u>
      </h2>
      <br>
      <p style="text-align:justify">
        Nous pouvons être amené à modifier occasionnellement la présente politique
        de confidentialité. Lorsque cela est nécessaire, nous vous en informerons
        et/ou solliciterons votre accord. Nous vous conseillons de consulter
        régulièrement cette page pour prendre connaissance des éventuelles
        modifications ou mises à jour apportées à notre politique de
        confidentialité.
      </p>
      <br>
      <br>
    </div>
  </main>
  <?php include('footer.php'); ?>
</body>

</html>