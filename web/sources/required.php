<link type="text/css" rel="icon" type="image/png" href="/images/logo-republilke.png">
<link type="text/css" rel="shortcut icon" href="/images/logo-republilke.ico" type="image/x-icon" />

<!-- icon -->
<!-- Font Awesome -->
<link type="text/css" rel="stylesheet" href="/fonts/icons/fontawesome/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="/fonts/icons/fontrepu/css/repu_font.css" />
<!--  Font Awesome  -->
<link type="text/css" rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<!-- Vendor -->
<!-- Custom -->
<link type="text/css" rel="stylesheet" href="/vendor/magnificPopup/dist/magnific-popup.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<link type="text/css" rel="stylesheet" href="/vendor/jquery-ui/jquery-ui.css" />
<link type="text/css" href="/css/common.css" rel="stylesheet" />
<link type="text/css" href="/css/form.css" rel="stylesheet" />
<link type="text/css" href="/css/margin.css" rel="stylesheet" />
<link type="text/css" href="/css/table.css" rel="stylesheet" />
<link type="text/css" href="/css/sharehub.css" rel="stylesheet" />
<link type="text/css" href="/css/switch.css" rel="stylesheet" />
<link type="text/css" href="/css/style.css" rel="stylesheet" />

<!-- Google Tag Manager -->
<script>
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime(),
      event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
      'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-T2MHKRC');
</script>
<!-- End Google Tag Manager -->
<!-- JAVA SCRIPT -->
<!-- require -->
<script data-main="/js/app" src="/vendor/require/require.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="/vendor/datePicker/css/datepicker.css" />

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId: '2865617423500592',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v4.0',
      cookie: true,
    });
  };
  (function(d, s, id) { // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/<?= $_['codeBis'] ?>/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/<?= $_['codeBis'] ?>/sdk.js#xfbml=1&version=v4.0&appId=2865617423500592&autoLogAppEvents=1"></script>

<?php
if (in_array($_SERVER['HTTP_HOST'], ['republike.io', 'www.republike.io'])) {
  ?>
  <!-- Hotjar Tracking Code for www.republike.io -->
  <script>
    (function(h, o, t, j, a, r) {

      h.hj = h.hj || function() {
        (h.hj.q = h.hj.q || []).push(arguments)
      };

      h._hjSettings = {
        hjid: 1555730,
        hjsv: 6
      };

      a = o.getElementsByTagName('head')[0];

      r = o.createElement('script');
      r.async = 1;

      r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;

      a.appendChild(r);

    })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
  </script>
<?php
}
?>