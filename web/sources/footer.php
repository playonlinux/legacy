<!-- FOOTER -->
<footer>
    <a id="backToTop"></a>
    <div class="sh-footer">
        <div class="container">
            <div class="sh-logo__home"><img src="/images/logo-republilke-entier.png" alt="REPUBLIKE"></div>
            <p class="text-center"><em>« <?= $_['baseline'] ?> »</em></p>
            <p class="text-center small mt-3">
                © 2019 - Republike -
                <a href="/"><?= $_['menu_condition_3'] ?></a> -
                <a href="/<?= $code ?>/<?= $_['url_terms_of_service'] ?>"><?= $_['menu_condition_4'] ?>
                </a> -
                <a href="/<?= $code ?>/<?= $_['url_privacy'] ?>"><?= $_['footer']['privacy'] ?>
                </a> -
                <a href="/"><?= $_['menu_condition_5'] ?></a> - <a href="mailto:assemblee@republike.io" target="__blank">Contact</a></p>
        </div>
    </div>
    <script>
        require(['app'], function() {
            require(['modules/footer']);
        });
    </script>
</footer>