<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

//update vues
$updateReq = $dbh->prepare("UPDATE `bl_battle_posts` SET `vues`= vues+1 WHERE token=:id");
$updateReq->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
$updateReq->execute();

$select_post = $dbh->prepare("SELECT *
FROM `bl_battle_posts`
WHERE `statut` ='1' and token = :id limit 0,1 ");
$select_post->bindParam(':id', $_GET['id'], PDO::PARAM_STR);
$select_post->execute();

$row_post = $select_post->fetch(PDO::FETCH_OBJ);
$title_battle = name_battle($row_post->battle, $dbh);
$statut_battle = statut_battle($row_post->battle, $dbh);
$url_battle = url_battle($row_post->battle, $dbh);

list($post, $type) = photo_post($row_post->id, $dbh);

if (in_array($type, [1, 4])) {
    $imagePartage = $post;
}
if ($type == 2) {
    $imagePartage = 'images/partage_video_republike.jpg';
}
if ($type == 3) {
    $imagePartage = 'images/partage_texte_republike.jpg';
}

list($width, $height, $type, $attr) = getimagesize($imagePartage);
?>

<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> | <?= $row_post->title ?>">
    <meta name="author" content="<?= $nameSite ?>">
    <title><?= $nameSite ?> | <?= $row_post->title ?> - <?= $title_battle ?></title>

    <!--share-->
    <meta property="og:title" content="<?= $nameSite ?> | <?= $row_post->title ?>" />
    <meta property="og:site_name" content="<?php echo $nameSite; ?>" />
    <meta property="og:description" content="<?= $row_post->title ?> - <?= $_['page_battle'] ?> <?= $title_battle ?>" />
    <meta property="og:image" content="<?php echo $imagePartage; ?>" />
    <meta property="og:image:width" content="<?php echo $width; ?>" />
    <meta property="og:image:height" content="<?php echo $height; ?>" />
    <meta property="og:image:alt" content="<?= $row_post->title ?>" />
    <meta property="og:url" content="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $_GET['id'] ?>" />


    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $_GET['id'] ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/post/' . $_GET['id']; ?>
        <link rel="alternate" hreflang="en" href="/en/post/<?= $_GET['id'] ?>" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/contenu/' . $_GET['id']; ?>
        <link rel="alternate" hreflang="fr" href="/fr/contenu/<?= $_GET['id'] ?>" />
    <?php } ?>

    <?php include('required.php'); ?>

    <script>
        var page = 'post';
    </script>
</head>

<body>
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container-fluid">
            <!--content head-->
            <div class="sh-content-head sh-content-head__flex-off">
                <div class="row">
                    <div class="col-lg-6 offset-3 col-12">
                        <div class="sh-content-head__navigation">
                            <a href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $url_battle ?>" class="sh-content-head__btn-prev sh-btn-icon">
                                <i class="icon-Arrow_Left"></i>
                                <span><?= $_['return_battle'] ?></span>
                            </a>

                        </div>
                    </div>

                </div>
            </div>
            <!--sections-->
            <div class="row">
                <div class="col-lg-4 col-md-8 offset-lg-3 offset-md-1">
                    <!--section-->
                    <div class="sh-section">

                        <div class="sh-section__content">
                            <?php if ($row_post->type == 1) { ?>
                                <div class="sh-section__image">
                                    <img src="/<?= $row_post->post ?>" alt="<?= $row_post->title ?>">
                                </div>
                            <?php } ?>

                            <?php if ($row_post->type == 4) { ?>
                                <div class="sh-section__image sh-section__image-gif">
                                    <img src="/<?= $row_post->post ?>" alt="<?= $row_post->title ?>">
                                </div>
                            <?php } ?>

                            <?php if ($row_post->type == 2) {
                                list($video, $image) = video($row_post->post) ?>

                                <?php if ($image == '') { ?>
                                    <iframe width="100%" height="auto" src="<?= $video ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                                <?php } else { ?>

                                    <div class="sh-section__image">
                                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('https://i.imgur.com/TxzC70f.png') no-repeat;"></div>
                                        <img src="<?= $image ?>" alt="<?= $row_posts->title_post ?>">
                                    </div>

                                <?php } ?>
                            <?php  } ?>

                            <?php if ($row_post->type == 3) { ?>
                                <p class="text-muted mb-2"><?= nl2br($row_post->post) ?> </p>
                                <p class="text-muted mb-2"><a href="<?= $row_post->lien_post ?>"><?= $row_post->lien_post ?></a> </p>
                            <?php } ?>
                        </div>
                        <h3><?= $row_post->title ?></h3>
                        <div class="sh-section__footer">

                            <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>

                                <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)" id="like_<?= $row_post->id ?>" data-id="<?= $row_post->id ?>" class="<?php if ($statut_battle == 1) {
                                                                                                                                                                                                                    echo "likeSave";
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    echo "inactif";
                                                                                                                                                                                                                } ?> sh-section__btn-like sh-btn-icon">
                                    <i class="fa fa-heart"></i><span class="like"><?= $row_post->likes ?></span>
                                </a>
                            <?php } else { ?>

                                <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)" data-id="<?= $row_post->id ?>" class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon">
                                    <i class="fa fa-heart"></i><span class="like"><?= $row_post->likes ?></span>
                                </a>

                            <?php } ?>

                            <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>

                                <div style="margin-left:0;" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon" data-post="<?= $row_post->token ?>" data-title="<?= $row_post->title ?>" data-token="<?= $row_post->token ?>" data-image="/<?= $imagePartage ?>">
                                    <i class="fa fa-share-alt"></i></span>
                                </div>

                            <?php } else { ?>

                                <div style="margin-left:0;" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon" data-post="<?= $row_posts->token ?>">
                                    <i class="fa fa-share-alt"></i></span>
                                </div>

                            <?php } ?>

                            <div class="sh-section__btn-comment sh-btn-icon"><i class="fa fa-eye"></i><span class="users"><?= $row_post->vues ?></span></div>

                            <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>
                                <span style="cursor:pointer">
                                    <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_post->id ?>" data-title="<?= $row_post->title ?>" title="<?= $_['signaler_contenu'] ?>" class="sh-post__signaler sh-btn-icon icons-20">
                                </span>

                            <?php } ?>

                            <?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user) { ?>
                                <span style="cursor:pointer">
                                    <img src="/images/icons/edit-grey.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['modif_contenu'] ?>" id="edit_<?= $row_posts->id ?>" data-id="<?= $row_posts->id ?>" data-name="<?= $row_posts->title ?>" class="editPost_one sh-section__btn-link icons-30">
                                </span>

                            <?php } ?>

                            <a data-toggle="tooltip" data-placement="left" title="<?= prenom_user($row_post->user, $dbh) ?>" href="<?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_post->user) { ?>
                                                /<?= $code ?>/<?= $_['url_compte'] ?>
							<?php } else { ?>
                            	                /<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_post->user, $dbh) ?>
							<?php } ?>" class="sh-section__btn-link sh-btn-icon">
                                <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_post->user, $dbh), $dbh) ?>; background-image:url(<?= '/' . photo_user($row_post->user, $dbh) ?>)"></span>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="sh-editPost">
                    <div class="sh-upload__logo"><a href="index.php" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                    <div class="sh-upload__content">
                        <div class="sh-upload__form">
                            <form method="post" name="form_edit_post" id="form_edit_post" enctype="multipart/form-data">
                                <p><?= $_['edit_contenu'] ?></p>
                                <input class="form-control mt-3" placeholder="Entrez votre titre, #motscles (Optionnel)" name="name_post" id="name_post" value="<?= $row_post->title ?>" type="text">
                                <input type="hidden" name="id_post" id="id_post" value="<?= $row_post->id ?>" required>
                                <div class="sh-login__send">
                                    <button type="submit" class="sh-btn mx-auto" id="editPost_one"><?= $_['bt_edit'] ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3" id="commentaire">
                    <!--section-->
                    <div class="sh-section">
                        <h3><?= $_['comment'] ?></h3>
                        <div class="sh-section__content">
                            <?php $select_comments = $dbh->prepare("SELECT * FROM `bl_comments_post` WHERE `post` = :post and `statut` = 1 order by date desc limit 0, 10 ");
                            $select_comments->bindParam(':post', $row_post->id, PDO::PARAM_STR);
                            $select_comments->execute();

                            if ($select_comments->rowCount() > 0) { ?>
                                <div class="sh-comments">
                                    <?php while ($row_comments = $select_comments->fetch(PDO::FETCH_OBJ)) {
                                        $datetime = new DateTime($row_comments->date); ?>
                                        <!--comment-->
                                        <div class="sh-comments__section">
                                            <a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_comments->user, $dbh) ?>" class="sh-comments__avatar sh-avatar">
                                                <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_comments->user, $dbh), $dbh) ?>; background-image:url(<?= '/' . photo_user($row_comments->user, $dbh) ?>)"></span>
                                            </a>
                                            <div class="sh-comments__content">
                                                <a href="/user.php?id=<?= $row_comments->user ?>&code=<?= $code ?>" class="sh-comments__name"><?= prenom_user($row_comments->user, $dbh) ?></a>
                                                <span class="sh-comments__passed"><?= ago($datetime) ?></span>
                                                <span style="cursor:pointer">
                                                    <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="left" data-comment="<?= $row_comments->id ?>" title="<?= $_['signaler_comment'] ?>" class="sh-comments__signaler icons-20">
                                                </span>
                                                <p><?= $row_comments->message ?></p>
                                            </div>
                                        </div>
                                        <!--comment-->
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>
                            <div class="sh-add-comment">
                                <form method="post" action="" name="form_save_commentaire" id="form_save_commentaire" enctype="multipart/form-data">
                                    <textarea name="commentaire" id="commentaire" cols="3" maxlength="140" class="form-control ttg-border-none" placeholder="<?= $_['msg_comment'] ?>"></textarea>
                                    <input name="post" value="<?= $row_post->id ?>" required type="hidden">
                                    <div id="errorLogCommentaire" class="text-center mb-3"></div>
                                    <div class="sh-login__send mb-1">
                                        <button type="submit" class="sh-btn" id="btn-commentaire"><?= $_['bt_comment_envoyer'] ?></button>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>

            <div class="row">
                <h1 class="center-block mb-5"><a href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $url_battle ?>"><b><?= $title_battle ?></b></a></h1>
            </div>

            <div class="sh-section__wrap mur row mt-3">
                <!--section-->
                <?php $select_posts = $dbh->prepare("SELECT *,
                 (SELECT count(id) FROM bl_likes_post WHERE posts = bl_battle_posts.id) as nb_likes
				 FROM `bl_battle_posts`
                 WHERE `statut` ='1' and battle = :battle and id != :id  order by nb_likes desc ");
                $select_posts->bindParam(':battle', $row_post->battle, PDO::PARAM_STR);
                $select_posts->bindParam(':id', $row_post->id, PDO::PARAM_STR);
                $select_posts->execute();

                if ($select_posts->rowCount() > 0) {
                    while ($row_posts = $select_posts->fetch(PDO::FETCH_OBJ)) { ?>
                        <!--section-->
                        <div class="sh-section__item col-lg-5ths col-md-4 col-6">
                            <div class="sh-section">

                                <div class="sh-section__content">

                                    <?php if ($row_posts->type == 1) { ?>
                                        <div class="sh-section__image">
                                            <a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>">
                                                <img src="/<?= $row_posts->post ?>" alt="<?= $row_posts->title ?>">
                                            </a>
                                        </div>
                                        <?php $imagePartage = $urlSite . '/' . $row_posts->post;
                                    } ?>

                                    <?php if ($row_posts->type == 4) { ?>
                                        <div class="sh-section__image sh-section__image-gif">
                                            <a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>">
                                                <img src="/<?= $row_posts->post ?>" alt="<?= $row_posts->title ?>">
                                            </a>
                                        </div>
                                        <?php $imagePartage = $urlSite . '/' . $row_posts->post;
                                    } ?>

                                    <?php if ($row_posts->type == 2) {
                                        list($video, $image) = video($row_posts->post) ?>

                                        <?php if ($image == '') { ?>
                                            <a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>">
                                                <iframe width="100%" height="auto" src="<?= $video ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </a>

                                            <?php $imagePartage = $urlSite . '/images/partage_video.jpg';
                                        } else { ?>

                                            <div class="sh-section__image">
                                                <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('https://i.imgur.com/TxzC70f.png') no-repeat;"></div>
                                                <a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>">
                                                    <img src="<?= $image ?>" alt="<?= $row_posts->post ?>">
                                                </a>
                                            </div>

                                            <?php $imagePartage = $image;
                                        } ?>
                                    <?php  } ?>




                                    <?php if ($row_posts->type == 3) { ?>
                                        <a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>">
                                            <p class="text-muted mb-2"><?= mb_strimwidth($row_posts->post, 0, 100, "[...]") ?> </p>
                                        </a>
                                        <?php $imagePartage = $urlSite . '/images/partage_texte.jpg';
                                    } ?>

                                    <p><a href="/<?= $code ?>/<?= $_['url_contenu'] ?>/<?= $row_posts->token ?>"><?= $row_posts->title ?></a></p>
                                </div>
                                <div class="sh-section__footer">
                                    <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>

                                        <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)" id="like_<?= $row_posts->id ?>" data-id="<?= $row_posts->id ?>" class="<?php if ($statut_battle == 1) {
                                                                                                                                                                                                                                echo "likeSave";
                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                echo "inactif";
                                                                                                                                                                                                                            } ?> sh-section__btn-like sh-btn-icon">
                                            <i class="fa fa-heart"></i><span class="like"><?= $row_posts->likes ?></span>
                                        </a>

                                    <?php } else { ?>

                                        <a data-toggle="tooltip" data-placement="top" title="<?= $_['like_contenu'] ?>" href="javascript:void(0)" id="coeur_<?= $row_posts->id ?>" data-id="<?= $row_posts->id ?>" class="btn-upload_btn-signup sh-section__btn-like sh-btn-icon">
                                            <i class="fa fa-heart"></i><span class="like"><?= $row_posts->likes ?></span>
                                        </a>

                                    <?php } ?>

                                    <?php if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') { ?>

                                        <div style="margin-left:0;" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-partage-contenu sh-section__btn-follow sh-btn-icon" data-post="<?= $row_posts->token ?>" data-title="<?= $row_posts->title ?>" data-token="<?= $row_posts->token ?>" data-image="<?= $imagePartage ?>">
                                            <i class="fa fa-share-alt"></i></span>
                                        </div>
                                        <span style="cursor:pointer">
                                            <img src="/images/icons/warning-grey.svg" data-toggle="tooltip" data-placement="top" data-post="<?= $row_posts->id ?>" data-title="<?= $row_posts->title ?>" title="<?= $_['signaler_contenu'] ?>" class="sh-post__signaler sh-btn-icon icons-20">
                                        </span>

                                    <?php } else { ?>

                                        <div style="margin-left:0;" data-toggle="tooltip" data-placement="top" title="<?= $_['partager'] ?>" class="btn-upload_btn-signup sh-section__btn-follow sh-btn-icon" data-post="<?= $row_posts->token ?>">
                                            <i class="fa fa-share-alt"></i></span>
                                        </div>

                                    <?php } ?>

                                    <a data-toggle="tooltip" data-placement="left" title="<?= prenom_user($row_posts->user, $dbh) ?>" href="<?php if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_posts->user) { ?>
                                        	                                                /<?= $code ?>/<?= $_['url_compte'] ?>
										                                <?php } else { ?>
                                        	                                                /<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_posts->user, $dbh) ?>
										                                <?php } ?>" class="sh-section__btn-link sh-btn-icon">
                                        <span class="sh-section__avatar" style="border-color:<?= grade_user_color(points_user($row_posts->user, $dbh), $dbh) ?>; background-image:url(/<?= photo_user($row_posts->user, $dbh) ?>)"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <? }
                } ?>
            </div>
        </div>
    </main>


    <!-- Signaler -->
    <div class="sh-signalerComment">
        <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
        <div class="sh-upload__content signaler">
            <p class="text-center mb-3">
                <b><?= $_['titre_signaler'] ?></b><br>
                <form method="post" action="" name="form_signaler_commentaire" id="form_signaler_commentaire" enctype="multipart/form-data">
                    <select class="form-control mb-4" name="cat">
                        <option value="1"><?= $_['type_signaler1'] ?></option>
                        <option value="2"><?= $_['type_signaler2'] ?></option>
                        <option value="3"><?= $_['type_signaler3'] ?></option>
                    </select>
                    <textarea name="mes" id="mes" cols="3" maxlength="140" class="form-control" placeholder="<?= $_['msg_signaler'] ?>"></textarea>
                    <input name="comment" value="" required type="hidden">
                    <div id="errorLogCommentaireSignaler" class="text-center mb-3"></div>
                    <div class="sh-login__send mb-4">
                        <button type="submit" class="sh-btn" id="btn-signaler-comment"><?= $_['bt_signaler'] ?></button>
                    </div>
                </form>
            </p>
        </div>
        <div class="sh-login__content confirm" style="display:none;">
            <p class="text-center"><?= $_['bravo_signalement'] ?></p>
        </div>
    </div>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/battle']);
        });
    </script>
</body>

</html>