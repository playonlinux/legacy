<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

//compte Battle
$cat = "";
if (!empty($_GET['cat'])) {
    $cat = $_GET['cat'];
    // var_export($_SERVER);

    $selectCat = $dbh->prepare(
        "   SELECT id, `name_" . $code . "` AS name 
        FROM `bl_categories` 
        WHERE `url_" . $code . "`  = '$cat'
    "
    );
    // var_dump($selectCat);
    // die();
    $selectCat->execute();
    if ($selectCat->rowCount() > 0) {
        $rowCat = $selectCat->fetch(PDO::FETCH_OBJ);
        $name_cat = $rowCat->name;
        $id_cat = $rowCat->id;
    }
    //url langue
    $selectCatLang = $dbh->prepare(
        "   SELECT `url_fr`, `url_en` 
        FROM `bl_categories`
        WHERE `id`  = '$id_cat'
    "
    );
    $selectCatLang->execute();
    if ($selectCatLang->rowCount() > 0) {
        $rowCatLang = $selectCatLang->fetch(PDO::FETCH_OBJ);
        $url_fr = $rowCatLang->url_fr;
        $url_en = $rowCatLang->url_en;
    }
}

?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  <?= $_['url_theme'] ?> : <?= $name_cat ?>">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | <?= $titre_theme ?> <?= $name_cat ?></title>

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_theme'] ?>/<?= $cat ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/category/' . $url_en; ?>
        <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/theme/' . $url_fr; ?>
        <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
    <?php } ?>

    <?php include('required.php'); ?>

    <script>
        var cat = '<?= $id_cat ?>';
    </script>
    <script>
        var page = 'theme';
    </script>
</head>

<body>

    <div class="bg-img-home" style="color: #ffffff;">
        <div class="container">
            <div class="topnav">
                <? include('header.php'); ?>
                <div class="container-fluid">
                    <!--content head-->
                    <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 160px;">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <span class="text-left mb-1 theme-title"><?= $_['bigtitre_index'] ?></span>
                            </div>
                            <div class="col-sm-12">
                                <h5 class="text-left mb-4" style="color:#ffffff;"><?= $_['titre_index'] ?></h5>
                            </div>
                        </div>
                        <?php include('lancerBattle.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MAIN -->
    <main style="padding-top:0px;">
        <div class="container">
            <!--content head-->
            <div class="sh-content-head">
                <div class="sh-content-head__btns center-block text-center">
                    <a href="javascript:void(0)" class="theme-switcher switcher active" data-id="theme-defis"><?= $_['bt_batailles'] ?></a>
                    <a href="javascript:void(0)" class="theme-switcher switcher" data-id="theme-posts"><?= $_['bt_contenus'] ?></a>

                    <div id="theme-sorts" class="defis sh-content-head__control center-block mt-4" style="padding-top: 35px">
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_user'] ?>" href="javascript:void(0)" data-sort-by="users" class="sh-btn-icon active">
                            <img src="/images/icons/friends.svg" class="icons-30 search-popular">
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_date'] ?>" href="javascript:void(0)" data-sort-by="date" class="sh-btn-icon">
                            <img src="/images/icons/calendar-grey.svg" class="icons-30 search-calendar">
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_aleatoire'] ?>" href="javascript:void(0)" data-sort-by="random" class="sh-btn-icon">
                            <img src="/images/icons/random-grey.svg" class="icons-30 search-random">
                        </a>
                    </div>

                    <div id="theme-sorts" class="posts sh-content-head__control center-block mt-4" style="padding-top:35px;display:none;">
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_nblikes'] ?>" href="javascript:void(0)" data-sort-by="like" class="sort-button sh-btn-icon active">
                            <img src="/images/icons/liked-pink.svg" class="icons-30 search-liked">
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_date'] ?>" href="javascript:void(0)" data-sort-by="date" class="sort-button sh-btn-icon">
                            <img src="/images/icons/calendar-grey.svg" class="icons-30 search-calendar">
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_aleatoire'] ?>" href="javascript:void(0)" data-sort-by="random" class="sort-button sh-btn-icon">
                            <img src="/images/icons/random-grey.svg" class="icons-30 search-random">
                        </a>
                    </div>
                </div>
            </div>

            <!--sections-->
            <div class="row tab-submit" id="theme-defis" style="min-height:100px;"></div>
            <div class="row tab-submit" style="display:none; min-height:100px;" id="theme-posts"> </div>

            <div class="sh-popup-post">
                <div class="sh-popup__content" id="contenu_post">
                </div>
            </div>
            <!-- <div class="sh-supp">
                <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
                <div class="sh-login__content">
                    <div class="sh-login__form text-center mt-1 mb-5">
                        <?= $_['supp_contenu1'] ?> <br>"<b><span id="postName"></span></b>" ?
                        <br><br><?= $_['supp_contenu2'] ?>
                    </div>
                    <div class="sh-login__send">
                        <input type="hidden" name="id_post" id="id_post" required>
                        <button type="submit" class="sh-btn" id="suppPost"><?= $_['bt_confirm'] ?></button>
                    </div>
                </div>
            </div> -->
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/cat']);
        });
    </script>

</body>

</html>