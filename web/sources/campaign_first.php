<?php
try {
    require_once('php/mysql.inc.php');
    require_once('php/funct_battelike.php');

    securePost();

    $class = "col-lg-4 col-md-4 col-sm-12";
    $id = $_POST['user'];
    $select_battles = $dbh->prepare(
        "   SELECT CONCAT(U.prenom, ' ', U.nom) AS username, C.name_$code AS category, C.url_$code AS category_url,
        B.user AS user_id, B.category AS category_id, B.title, B.id AS id_battle, B.statut AS statut_battle, 
        B.info AS info_battle, B.url AS url_battle, TIMESTAMPDIFF(MINUTE, B.date, NOW()) date_battle,
        B.vues,
        (SELECT COUNT(DISTINCT user) FROM bl_battle_posts WHERE battle = B.id) AS nb_users, 
        (SELECT COUNT(id) FROM bl_battle_posts WHERE battle = B.id AND statut=1) AS nb_posts,
        (SELECT SUM(likes) FROM bl_battle_posts WHERE battle = B.id) AS nb_likes,
        (SELECT id FROM bl_battle_posts WHERE battle = B.id AND statut = 1 order by likes DESC LIMIT 0,1 ) AS id_post
    FROM bl_battles B
        INNER JOIN bl_user U ON U.id = B.user
        INNER JOIN bl_user_cat UC ON UC.cat = B.category AND UC.user = U.id
        INNER JOIN bl_categories C ON C.id = UC.cat
    WHERE B.statut < 2 AND U.id = :id
    ORDER BY rand() 
    LIMIT 0,6
"
    );

    $select_battles->bindParam(':id', $id, PDO::PARAM_STR);

    $select_battles->execute();

    if (!$select_battles->rowCount()) {
        $id = $_POST['user'];
        $sth = $dbh->prepare('SELECT email, `password` FROM `bl_user` WHERE id = :id');
        $sth->bindParam(':id', $id, PDO::PARAM_STR);
        $sth->execute();

        if ($sth->rowCount()) {
            $row = $sth->fetch(PDO::FETCH_OBJ);
            logUser($row->email, $_SESSION['password']);
        }
        header("Location:/" . $code . "/" . $_['url_bienvenue']);
        exit(0);
    }

    ?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> | Félicitations !">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | Félicitations !</title>

    <?php include('required.php'); ?>
    <script>
        var page = 'fin';
    </script>

</head>

<body>
    <!-- HEADER -->
    <?php include('header.php'); ?>

    <!-- MAIN -->
    <main>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-3 mb-3">
                    <h4 class="center-block">
                        <b>2/ Choisissez une bataille ci-dessous, postez votre premier contenu</b>
                        <br>
                        <a href="/<?= $code ?>/<?= $_['url_bienvenue']; ?>"><u>Passer</u></a>
                    </h4>
                </div>
            </div>

            <!--sections BEST-->
            <div class="sh-section__wrap row tab-submit" id="defis" style="min-height:100px;">

                <?php
                    while ($row_battles = $select_battles->fetch(PDO::FETCH_OBJ)) {

                        list($post, $type) = photo_post($row_battles->id_post, $dbh);

                        $class_cat = '';
                        if (isset($_SESSION['securite']) && $_SESSION['securite'] != '') {
                            $cat = recherche_cat($_SESSION['id_user'], $row_battles->category, $dbh);
                            if ($cat == 1) {
                                $class_cat = ' cat ';
                            }
                        }
                        $class_statut = 'open';
                        if ($row_battles->statut_battle == 2) {
                            $class_statut = "closed";
                        }

                        ?>
                <!--section-->
                <div id="battle_<?= $row_battles->id_battle ?>" class="sh-section__item <?= $class ?> <?= $class_statut ?> wow fadeInUp">
                    <div class="sh-section box">
                        <div class="sh-section__content">
                            <!---->
                            <div class="sh-section__media">
                                <?php
                                        switch ($type) {
                                            case 1: {
                                                    $imagePartage = $post;
                                                    ?>
                                <div class="element text-center" style="background-image:url(/<?= $post ?>); background-size: cover; background-position: center;"></div>
                                <?php
                                                break;
                                            }
                                        case 2: {
                                                list($video, $image) = video($post);
                                                $imagePartage = ($image) ? '/' . $image : '/images/partage_video_republike.jpg';
                                                ?>
                                <div class="element text-center" style="background-image:url(<?= $image ?>); background-size: cover;">
                                    <?php
                                                    if ($video) {
                                                        ?>
                                    <video>
                                        <source src="<?= $video ?>">
                                    </video>
                                    <?php
                                                    }
                                                    ?>
                                </div>
                                <?php
                                                break;
                                            }
                                        case 3: {
                                                $imagePartage = '/images/partage_texte_republike.jpg';
                                                ?>
                                <div class="element text-center">
                                    <p class="text-muted mb-2"><?= mb_strimwidth($post, 0, 50, "[...]") ?> </p>
                                </div>
                                <?php
                                                break;
                                            }
                                    }

                                    ?>
                                <div class="overlay">
                                    <a class="link" href="/<?= $code ?>/<?= $_['url_battle'] ?>/<?= $row_battles->url_battle ?>">
                                        <img src="/images/icons/<?= $code ?>/participate-button.svg">
                                    </a>
                                </div>
                            </div>
                            <div>
                                <a href="/<?= $code ?>/<?= $_['url_theme'] ?>/<?= $row_battles->category_url ?>" class="w3-tag w3-purple">
                                    <?= $row_battles->category; ?>
                                </a>
                                <div style="padding:10px 0">
                                    <h3 title="<?= $row_battles->title ?>" data-toggle="tooltip" data-placement="top">
                                        <?= mb_strimwidth($row_battles->title, 0, 40, "[...]") ?>
                                        <h3>
                                </div>
                                <div>
                                    <i>
                                        <?php
                                                if (isset($_SESSION['id_user']) && $_SESSION['id_user'] == $row_battles->user_id) { ?>
                                        <a href="/<?= $code ?>/<?= $_['url_compte'] ?>">
                                            <?php   } else { ?>
                                            <a href="/<?= $code ?>/<?= $_['url_user'] ?>/<?= url_user($row_battles->user_id, $dbh) ?>">
                                                <?php   } ?>
                                                <?= $_['by'] . ' ' . strtoupper($row_battles->username) . ' ' . $ago ?>
                                            </a>
                                    </i>
                                </div>
                                <span class="date" style="visibility: hidden;"><?= $row_battles->id_battle ?></span>
                            </div>
                        </div> <!-- section__content -->
                        <div class="sh-section__footer">
                            <div>
                                <span>
                                    <img src="/images/icons/friends.svg" class="icons-30">
                                    <span class="users"><?= $row_battles->nb_users; ?></span>
                                </span>
                                <span>
                                    <img src="/images/icons/bow.svg" class="icons-30">
                                    <span class=""><?= $row_battles->nb_posts; ?></span>
                                </span>
                                <span>
                                    <img src="/images/icons/eye.svg" class="icons-30">
                                    <span class=""><?= $row_battles->vues; ?></span>
                                </span>
                                <span>
                                    <img src="/images/icons/liked-pink.svg" class="icons-30">
                                    <span class=""><?= $row_battles->nb_likes; ?></span>
                                </span>
                            </div>
                            <div>

                                <?php if ($row_battles->statut_battle != 2) { ?>
                                <span style="cursor:pointer">
                                    <?php if (!empty($_SESSION['securite'])) { ?>

                                    <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-partage-battle icons-30" data-battle="<?= $row_battles->id_battle ?>" data-url="<?= $row_battles->url_battle ?>" data-title="<?= $row_battles->title ?>" data-image="/<?= $imagePartage; ?>">
                                    <?php } else { ?>
                                    <!-- TODO: Add automatic redirection after login process -->
                                    <img src="/images/icons/share.svg" data-toggle="tooltip" data-placement="top" title="<?= $_['partager_amis'] ?>" class="btn-upload_btn-signup icons-30" data-battle="<?= $row_battles->id_battle  ?>">
                                    <?php } ?>
                                    <span id="share_battle_<?= $row_battles->id_battle ?>"><?= $row_share->nb_shares; ?></span>
                                </span>
                                <?php

                                            ?>
                                <!-- <span data-toggle="tooltip" data-placement="left" title="<?= $_['modif_battle'] ?>" id="edit-b_<?= $row_battles->id_battle ?>" 
                                                                                                                                                                                                                                                                    class="editBattle sh-btn-icon">
                                                                                                                                                                                                                                                                    <i class="fa fa-edit"></i>
                                                                                                                                                                                                                                                                </span> -->
                                <?php
                                            ?>
                                <?php } else { ?>

                                <div data-toggle="tooltip" data-placement="top" title="<?= $_['battle_close'] ?>" class="btn-closed-battle sh-section__btn-follow sh-btn-icon" data-battle="<?= $row_battles->id_battle ?>">
                                    <i class="fa fa-trophy"></i></span>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div><!-- sh-section box-->
                </div>
                <?php } ?>

                <script>
                    require(['app'], function() {
                        require(['modules/main']);
                        require(['modules/signup']);
                        require(['modules/felicitations']);
                    });
                </script>
            </div>
    </main>

    <!-- FOOTER -->
    <?php include('footer.php'); ?>
</body>

</html>
<?php
} catch (Exception $e) {
    var_export($e);
    die();
}
