<?php
require_once('php/mysql.inc.php');
require_once('php/funct_battelike.php');

securePost();

$raw = basename($_SERVER['REQUEST_URI']);
$keywords = ($raw != $_['url_search']) ?  urldecode(base64_decode($raw)) : "";

?>
<!DOCTYPE html>
<html lang="<?= $_['codeBis'] ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="<?= $nameSite ?> |  <?= $_['titre_search'] ?>">
    <meta name="author" content="battlelike.com">
    <title><?= $nameSite ?> | <?= $_['titre_search'] ?></title>

    <link rel="canonical" href="/<?= $code ?>/<?= $_['url_search'] ?>/<?= $raw ?>" />
    <?php if ($code == 'fr') {
        $footerEN = '/en/search/' . $raw; ?>
        <link rel="alternate" hreflang="en" href="<?= $footerEN ?>" />
    <?php } else if ($code == 'en') {
        $footerFR = '/fr/recherche/' . $raw; ?>
        <link rel="alternate" hreflang="fr" href="<?= $footerFR ?>" />
    <?php } ?>

    <?php include('required.php'); ?>

    <script>
        var mots = "<?= $keywords ?>";
    </script>
    <script>
        var page = 'search';
    </script>
</head>

<body>

    <div class="bg-img-home" style="color: #ffffff;">
        <div class="container">
            <div class="topnav">
                <? include('header.php'); ?>
                <div class="container-fluid">
                    <!--content head-->
                    <div class="sh-content-head sh-content-head__flex-off" style="padding-bottom: 5px; padding-top: 160px;">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="col-sm-12">
                                <span class="text-left mb-1 search-title"><?= $_['bigtitre_index'] ?></span>
                            </div>
                            <div class="col-sm-12">
                                <h5 class="text-left mb-4" style="color:#ffffff;"><?= $_['titre_index'] ?></h5>
                            </div>
                        </div>
                        <?php include('lancerBattle.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MAIN -->
    <main style="padding-top:0px;">
        <!--content head-->
        <div class="sh-content-head">
            <div class="sh-content-head__btns center-block text-center">
                <a href="javascript:void(0)" class="search-switcher switcher active" data-id="search-defis"><?= $_['bt_batailles'] ?></a>
                <a href="javascript:void(0)" class="search-switcher switcher" data-id="search-posts"><?= $_['bt_contenus'] ?></a>
                <a href="javascript:void(0)" class="search-switcher switcher" data-id="search-users"><?= $_['bt_participants'] ?></a>

                <div id="search-sorts" class="search-defis sh-content-head__control center-block mt-4" style="padding-top: 35px">
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_user'] ?>" href="javascript:void(0)" data-sort-by="search-users" class="sh-btn-icon active">
                        <img src="/images/icons/friends.svg" class="icons-30 search-popular">
                    </a>
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_date'] ?>" href="javascript:void(0)" data-sort-by="date" class="sh-btn-icon">
                        <img src="/images/icons/calendar-grey.svg" class="icons-30 search-calendar">
                    </a>
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_aleatoire'] ?>" href="javascript:void(0)" data-sort-by="random" class="sh-btn-icon">
                        <img src="/images/icons/random-grey.svg" class="icons-30 search-random">
                    </a>
                </div>

                <div id="search-sorts" class="search-posts sh-content-head__control center-block mt-4" style="padding-top:35px;display:none;">
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_nblikes'] ?>" href="javascript:void(0)" data-sort-by="like" class="sh-btn-icon active">
                        <img src="/images/icons/liked-pink.svg" class="icons-30 search-liked">
                    </a>
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_date'] ?>" href="javascript:void(0)" data-sort-by="date" class="sh-btn-icon">
                        <img src="/images/icons/calendar-grey.svg" class="icons-30 search-calendar">
                    </a>
                    <a data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $_['bt_aleatoire'] ?>" href="javascript:void(0)" data-sort-by="random" class="sh-btn-icon">
                        <img src="/images/icons/random-grey.svg" class="icons-30 search-random">
                    </a>
                </div>
            </div>
        </div>

        <!--sections-->
        <div class="row tab-submit" id="search-defis" style="min-height:100px;"></div>
        <div class="row tab-submit" style="display:none; min-height:100px;" id="search-posts"> </div>
        <div class="row tab-submit" style="display:none; min-height:100px;padding-top:3rem;" id="search-users"> </div>

        <div class="sh-reponse">
            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
            <div class="sh-login__content">
                <p class="text-center"><b><?= $_['demande_friend_send'] ?></b></p>
            </div>
        </div>
        <div class="sh-popup-post">
            <div class="sh-popup__content" id="contenu_post">
            </div>
        </div>
        <div class="sh-annuler">
            <div class="sh-upload__logo"><a href="/" class="sh-logo"><img src="/images/logo-republilke-entier.png" alt=""></a></div>
            <div class="sh-login__content">
                <p class="text-center"><b><?= $_['demande_friend_delete'] ?></b></p>
            </div>
        </div>
    </main>

    <?php include('footer.php'); ?>

    <script>
        require(['app'], function() {
            require(['modules/search']);
        });
    </script>

</body>

</html>